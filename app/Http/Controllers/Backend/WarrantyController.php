<?php

namespace App\Http\Controllers\Backend;
use App\Http\Traits\CommonTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
class WarrantyController extends Controller
{
    use CommonTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        View::share('js', ['master']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        View::share('js', ['dashboard']);
        $this->data['data'] = $this->getFAQDataList(''); 
        $this->data['country_id'] = $this->FieldByID("branch","country_id",Auth::user()->branch_id);
        return view('warranty.dashboard',$this->data);
    }
}

@extends('layouts.app')

@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                  <h4 class="card-title-text">{{((isset($permission)) ? "Edit":'Create')}} Permission </h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('permission') }}"> <i
                                class="feather icon-list"></i> Permission List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($permission, ['route' => ['permission.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-permission']) }}
            @else
                {{ Form::open(['route' => 'permission.store','class' => 'form-horizontal','role' => 'form','method' => 'post','id' => 'create-permission']) }}
            @endif
            @csrf
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('module', __('Module')) }}
                    {{ Form::select('module_id', $module, old('module_id',(isset($permission)) ? $permission->module_id:''), ['class' => 'form-control', 'placeholder' => 'Select Module', 'required' => 'required']) }}
                </div>
                <div class="col form-group">
                    {{ Form::label('name', __('Name')) }}
                    {{ Form::text('name', old('name',(isset($permission)) ? $permission->name:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
            </div>
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection

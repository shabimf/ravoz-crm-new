(function () {
    FTX.TRANSFER = {

        list: {

            selectors: {
                Transfer_table: $('#transfer-table'),
            },

            init: function (from_branch_id,to_branch_id,from,to,serial_num) {
                this.selectors.Transfer_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.Transfer_table.data('ajax_url'),
                        type: 'post',
                        data: function (d) {
                            d.from = from,
                            d.to = to,
                            d.from_branch_id = from_branch_id,
                            d.to_branch_id = to_branch_id,
                            d.serial_num = serial_num
                        },
                    },
                    columns: [
                        {
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'transfer_date', name: 'transfer_date'},
                        { data: 'from_branch_name', name: 'from_branch_name' },
                        { data: 'to_branch_name', name: 'to_branch_name'},
                        { data: 'serial_no', name: 'serial_no'},
                        { data: 'customer_name', name: 'customer_name'},
                        { data: 'customer_phone', name: 'customer_phone'},
                        { data: 'is_warranty', name: 'is_warranty' , searchable: false},
                        { data: 'brand_name', name: 'brand_name'},
                        { data: 'model_name', name: 'model_name'},
                        // { data: 'remarks', name: 'remarks'},
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                       
                    ],
                    order: [[2, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
    
})();

@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Price Comparison Report</h4>
            </div>
        </div>
        <div class="row">
			<div class="col-lg-12">
				<div class="card">
				  <div class="card-body">
                    <div class="dt-buttons float-right">  
						<a class="btn btn-primary buttons-excel buttons-html5"  href="{{ route('report.price-file-export') }}"><span>Excel</span></a>
					</div>
					  <div class="table-responsive">
                        <table id="price-report-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("report.price.get") }}">
                        <thead>
                            <tr>
                               
                                <th></th>
                                <th></th>
                                @foreach ($branch as $key=>$branch_name)
                                    <th colspan="2">{{$branch_name}}</th>
                                @endforeach
                            </tr>    
                            <tr>
                                
                                <th>FOCUS CODE</th>
                                <th>PRODUCT DESCRIPTION</th>
                                @foreach ($branch as $key=>$branch_name)
                                    <th>QUANTITY</th>
                                    <th>PRICE</th>
                                @endforeach
                            </tr>
                        </thead>
						</table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
FTX.Utils.documentReady(function() {
    FTX.PRICECOM.list.init();
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use App\Http\Traits\CommonTrait;

class ServiceComplaintExport implements FromArray, WithHeadings, WithMapping
{
    protected $from,$to,$country_id,$branch_id,$model_id,$branch_ids;

    use CommonTrait;

    function __construct($from, $to, $country_id, $branch_id, $model_id, $branch_ids) {
        $this->data['from'] = $from;
        $this->data['to'] = $to;
        $this->data['country_id'] = $country_id;
        $this->data['branch_id'] = $branch_id;
        $this->data['model_id'] = $model_id;
        $this->data['branch_ids'] = $branch_ids;
    }

    public function array(): array
    {
        
        return $this->getServiceComplaintTableData($this->data);
    }

    public function headings(): array
    {
        return [
            'Sl No',
            'Repair Order No',
            'Activation date',
            'Purchase Date',
            'Receive Date',
            'Receive time',
            'Completed Date',
            'Region/Country',
            'SC Code',
            'SC Name',
            'Order Status',
            'device status',
            'Repair type',
            'Brand',
            'Product Type',
            'Marketing Model',
            'Internal Model',
            'Color',
            'RAM',
            'ROM',
            'International Warranty Service',
            'IMEI/SN',
            'Factory IMEI/SN',
            'Board serial No',
            'Submission Method',
            'Customer name',
            'Customer phone no',
            'Customer Address',
            'Dealer Name',
            'Dealer Type',
            'Salesman name',
            'Date Of Manufacture',
            'Repaired Date',
            'Repaired Time',
            'Handover Date',
            'Handover Time',
            'Repair duration (hours)',
            'Old Software Version',
            'New Software Version',
            'Receiver',
            'Engineer',
            'Customer Fault Description',
            'Fault Cause',
            'Remark',
            'Special Description',
            'Good Material Code',
            'Defective Material Code',
            'Good Material Consumed Qty',
            'Good material name',
            'Remark for Replace Materials',
            'Have defective materials been taken away or not',
            'Local currency',
            'Unit Price',
            'Receivable price',
            'Net Charge',
            'Customer Charged amount'
        ];
    }
    
    public function map($claim): array
    {
        $dateDiff = intval((strtotime($claim['created_at'])-strtotime($claim['bill_date']))/60);
        $receive_time = explode(" ",$claim['created_at']);
        $repair_hours = intval($dateDiff/60);
        return [
            $claim['rownum'],
            $claim['serial_no'],
            $claim['active_date'],
            $claim['sale_date'],
            $claim['created_at'],
            $receive_time[1],
            $claim['complete_date'],
            $claim['country_name'],
            $claim['branch_code'],
            $claim['branch_name'],
            $claim['status'],
            NULL,
            NULL,
            $claim['brand_name'],
            $claim['device_name'],
            $claim['model_name'],
            $claim['model_name'],
            $claim['color'],
            $claim['ram'],
            $claim['rom'],
            NULL,
            $claim['IMEI_no1'],
            $claim['IMEI_no1'],
            $claim['pcb_serial_no'],
            NULL,
            $claim['customer_name'],
            $claim['customer_phone'],
            $claim['address'],
            $claim['customer_name'],
            NULL,
            $claim['customer_name'],
            $claim['date_of_manufacture'],
            $claim['bill_date'],
            $claim['bill_time'],
            $claim['bill_date'],
            $claim['bill_time'],
            $repair_hours,
            $claim['old_software'],
            $claim['old_software'],
            $claim['technician_name'],
            $claim['technician_name'],
            $claim['complaint_name'],
            $claim['service_complaints'],
            $claim['remarks'],
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            $claim['customer_amount']
        ];
    }
}

<?php
namespace App\Http\Controllers\Backend\Sales;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use DB;
use Auth;
use Session;

class SalesRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['model'] = $this->NameAndID('models');
            $this->data['branch'] = $this->getBranches(Session::get('sessi_branch_id'));
            return $next($request);
        });
        View::share('js', ['master','sales-request']);
    }

    public function index (Request $request) {
        $this->data['branch_id'] = $request->branch_id ? $request->branch_id: 0;
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['nums'] = $request->nums ? $request->nums : "";
        $this->data['serial_num'] = $request->serial_num? $request->serial_num: "";
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        return view('admin.sales-request.index', $this->data);
    }

    public function getSalesRequest(Request $request)
    {
        $data =  DB::table('sales_request')
            ->leftJoin('service', 'sales_request.id', '=', 'service.sales_request_id')
            ->leftJoin('branch', 'branch.id', '=', 'sales_request.branch_id')
            ->leftJoin(
                'service_customers',
                'service_customers.customer_id',
                '=',
                'service.customer_id'
            )
            ->leftJoin('brands', 'brands.id', '=', 'service.brand_id')
            ->leftJoin(
                'variant_details',
                'variant_details.id',
                '=',
                'service.variant_id'
            )
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->select([
                'sales_request.id',
                'sales_request.created_at',
                'branch.name as branch_name',
                'service.serial_no',
                'service.status',
                'service_customers.customer_name',
                'service_customers.customer_phone',
                'service.is_warranty',
                'brands.name as brand_name',
                'models.name as model_name',
                'color.name as color',
                'ram.name as ram',
                'rom.name as rom',
                'sales_request.shop_name',
                'sales_request.sale_date',
                'sales_request.status as request_status',
            ]);

        if (!empty($request->branch_id)) {
            $data->where("sales_request.branch_id", $request->branch_id);
        }else {
            if (Session::get('sessi_branch_id')) {
                $data->whereIn('sales_request.branch_id', explode(',',Session::get('sessi_branch_id')));
            }
        }
        if (!empty($request->nums)) {
            $data->where(
                "service_customers.customer_phone",
                "=",
                $request->nums
            );
        }
        if (!empty($request->serial_num)) {
            $data->where(
                "service.serial_no",
                'like',
                '%' . $request->serial_num . '%'
            );
        }
        if (!empty($request->model_id)) {
            $data->where("models.id", $request->model_id);
        }

        if ($request->status == 0 && isset($request->status)) {
            $data->where("sales_request.status", $request->status);
        }

        $from = $request->from
            ? date('Y-m-d', strtotime($request->from))
            : "";
        $to = $request->to
            ? date('Y-m-d', strtotime($request->to))
            : "";
        if (!empty($from) && !empty($to)) {
           $data->whereBetween('sales_request.created_at', [$from, $to]);
        }
        $data->where("service.sales_request_id", ">" , 0);
        $data->get();  

        return DataTables::of($data)
            ->escapeColumns(['created_at', 'branch_name'])
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw(DB::raw("(DATE_FORMAT(sales_request.created_at,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('shop_name', function ($query, $keyword) {
                $query->whereRaw('sales_request.shop_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('branch_name', function ($query, $keyword) {
                $query->whereRaw('branch.name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('brand_name', function ($query, $keyword) {
                $query->whereRaw('brands.name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('model_name', function ($query, $keyword) {
                $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('serial_no', function ($query, $keyword) {
                $query->whereRaw('service.serial_no like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('customer_name', function ($query, $keyword) {
                $query->whereRaw('service_customers.customer_name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('customer_phone', function ($query, $keyword) {
                $query->whereRaw('service_customers.customer_phone like ?', ["%{$keyword}%"]);
            })
            ->editColumn('created_at', function ($data) {
                return date('d-m-Y', strtotime($data->created_at));
            })
            ->editColumn('is_warranty', function ($data) {
                return $data->is_warranty == 1 ? "Warranty" : "Non Warranty";
            })
            ->filterColumn('sale_date', function ($query, $keyword) {
                $query->whereRaw(DB::raw("(DATE_FORMAT(sales_request.sale_date,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
            })
            ->editColumn('model_name', function ($data) {
                $model = "";
                if ($data->model_name) {
                    $model = $data->model_name . "(" . $data->color;
                    if ($data->ram) {
                        $model .= "," . $data->ram;
                    }
                    if ($data->rom) {
                        $model .= "," . $data->rom;
                    }
                    $model .= ")";
                }
                return $model;
            })
            ->editColumn('sale_date', function ($data) {
                return date('d-m-Y', strtotime($data->sale_date));
            })
            ->editColumn('serial_no', function ($data) {
                return '<a href="' .
                    url('service/sales/request/' . $data->id) .
                    '">' .
                    $data->serial_no .
                    '</a>';
            })
            ->editColumn('status', function ($data) {
                if ($data->request_status == 0) {
                    $status =
                        '<i class="fa fa-circle text-warning mr-2"></i> Pending';
                } if($data->request_status == 1) {
                    $status =
                        '<i class="fa fa-circle text-success mr-2"></i> Accepted';
                } elseif($data->request_status == 2) {
                    $status =
                        '<i class="fa fa-circle text-danger mr-2"></i> Rejected';
                }
                return $status;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function show($id) {

        $this->data['request'] =  DB::table('sales_request')
            ->leftJoin('service', 'sales_request.id', '=', 'service.sales_request_id')
            ->leftJoin(
                'service_customers',
                'service_customers.customer_id',
                '=',
                'service.customer_id'
            )
            ->leftJoin(
                'customers',
                'service_customers.parent_id',
                '=',
                'customers.customer_id'
            )
            ->leftJoin('users', 'users.id', '=', 'service.technician_id')
            ->leftJoin('brands', 'brands.id', '=', 'service.brand_id')
            ->leftJoin(
                'variant_details',
                'variant_details.id',
                '=',
                'service.variant_id'
            )
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('software_version', 'software_version.id', '=', 'service.version_id')
            ->leftJoin('imei_details', 'imei_details.id', '=', 'sales_request.imei_id')
            ->select(
                'service_customers.*',
                'customers.type',
                'customers.customer_id as parent_cus_id',
                'service.serial_no',
                'sales_request.id',
                'service.id as service_id',
                'users.first_name as technician',
                'service.is_warranty',
                'brands.name as brand',
                'models.name as model',
                'color.name as color',
                'ram.name as ram',
                'rom.name as rom',
                'service.customer_id',
                'service.id as service_id',
                'service.sn_no',
                'service.security_code',
                'service.sale_date',
                'service.active_date',
                'service.manufacture_date',
                'service.delivery_date',
                'service.accessories_id',
                'service.status as service_status',
                'software_version.name as version',
                'sales_request.created_at',
                'sales_request.created_by',
                'sales_request.status as status',
                'imei_details.IMEI_no1',
                'imei_details.IMEI_no2',
                'imei_details.id as imei_id'
            )
            ->where('sales_request.id', $id)
            ->first();

        $this->data['request']->images = DB::table('service_image')
                                  ->leftJoin('service', 'service.id', '=', 'service_image.service_id')
                                  ->where('service_image.service_id',  $this->data['request']->service_id)
                                  ->pluck('filename');
        $this->data['request']->invoice = DB::table('invoice_file')
                                            ->leftJoin('service', 'service.id', '=', 'invoice_file.service_id')
                                            ->where('invoice_file.service_id',  $this->data['request']->service_id)
                                            ->pluck('filename');
        //echo "<pre>"; print_r($this->data['request']->images); echo "</pre>"; exit;
        return view('admin.sales-request.view', $this->data);
    }

    public function getStatusUpdate (Request $request) {
        // echo "<pre>"; print_r($request->input()); echo "</pre>"; exit;
        if($request->ajax()){
          DB::beginTransaction();
          $msg_txt = "rejected";
          if ($request->type == 1) {
           $msg_txt = "accepted";
           $service = $this->GetAllByID('service',$request->service_id);
           if($service[0]->active_date) $date = $service[0]->active_date;
           else $date = $service[0]->sale_date;
           if ($date) {
            $imei_id = $service[0]->imei_id;
            $country_id = $this->FieldByID('imei_details','country_id',$imei_id);
            if($country_id) {
                $invoice_no = $this->FieldByID('sales_request','invoice_no',$request->request_id);
                $country = $this->GetByID('country', $country_id);
                $warranty = ($country?$country->warranty:0);
                $expiry_date = date('Y-m-d', strtotime("+".$warranty." months", strtotime($date)));
                if ($expiry_date) {
                    $data['expiry_date'] = $expiry_date;
                    $data['sales_date'] = $service[0]->sale_date;
                    $data['activation_date'] = $service[0]->active_date;
                    $insertdata['imei_id'] = $imei_id;
                    $insertdata['invoice_no'] = $invoice_no;
                    $insertdata['sales_date'] = $service[0]->sale_date;
                    $insertdata['created_by'] = Auth::id();
                    $id = $this->insertGetId('customer_sales',$insertdata);
                    $data['sale_id'] = $id;
                    $this->updatedata('imei_details',$data,$imei_id);
                }
            } else {
                session()->flash('error', 'Kindly transfer IMEI to any country');
                return false;   
            }
            
           }
          }
          $salereq['status']=$request->type;
          $salereq['updated_by'] = Auth::id();
          $salereq['updated_at'] = date('Y-m-d H:i:s');
          $this->updatedata('sales_request',$salereq,$request->request_id);
          DB::commit();
          session()->flash('success', 'Sales request '.$msg_txt.' successfully');
          return true;   
        }
    }
    public function saleRequestResend(Request $request)
    {
        $service=$this->GetFirst('service',array('id'=>$request->input('service')));
        $salereq['sale_date']=$service->sale_date;
        $salereq['status']=0;
        // echo "<pre>"; print_r($salereq); echo "</pre>"; exit;
        $salereq_id=$service->sales_request_id;
        return $this->updatedata('sales_request', $salereq, $salereq_id);
    }
}

<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use DB;

class SoftwareController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['model'] = $this->NameAndID('models');
            return $next($request);
        });
        View::share('js', ['master']);
    }

    public function index (Request $request) {
        $this->data['model_id'] = ($request->model_id?$request->model_id:0);
        return view('admin.softwareversion.download', $this->data);
    }

    public function get(Request $request) {
       
        $data = DB::table("software_version")
                ->leftJoin('models', 'software_version.model_id', '=', 'models.id')
                ->select('software_version.name', 'software_version.url','models.name as model');
        if (!empty($request->model_id))
        {
            $data->where("software_version.model_id", $request->model_id);
        }
        $data->OrderBy('models.name');
        return DataTables::of($data)
            ->escapeColumns(['name','model'])
            ->addColumn('actions', function($data) {
                $downlaod = '';
                if($data->url) {
                    $downlaod = '<a href="'.$data->url.'"><button class="btn btn-secondary"><i class="la la-download"></i> Download</button></a>';
                }
                return $downlaod;
            })->filterColumn('model', function ($query, $keyword) {
                $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
            })
            ->addIndexColumn()
            ->make(true);

    }

}
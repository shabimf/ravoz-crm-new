<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" href="{{asset('images/ravoz_logo.svg')}}" type="image/x-icon">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend.css') }}" rel="stylesheet">
    <style>
        .hidden {
            display: none !important;
        }

    </style>

</head>

<body>
    <div id="app">
        <!-- **********  HEADER  ********** -->
        @include('inc.header')
        <!-- **********  HEADER  ********** -->
        <main class="py-4">
            <!-- **********  WRAPPER  ********** -->
            <div class="wrapper">
            <div class="sidebar-panel nicescrollbar sidebar-panel-light">
                <ul class="sidebar-menu">
                    <li class="menu-expand">
                    <a class="side-toggle" href="javascript:void(0);">
                    <span></span> <span class="sidebar-badge"> </span><i class="la la-compress"></i>
                    </a>
                    </li>
                    <li class="first-menu {{ active_class(request()->is('service')) }}">
                        <a href="{{ route('service.index') }}">
                            <span>DASHBOARD</span> <span class="sidebar-badge"> </span> <i class="la la-home"></i> 
                        </a>
                    </li>
                    @if (userHasPermission('Service','stock_list') || userHasPermission('Service','purchase_list') || userHasPermission('Service','transfer_list') || userHasPermission('Service','purchase_order_list')|| userHasPermission('Service','spare_sale_list'))
                        <li class="{{ active_class(request()->is('service/transfer/*/edit')) }}
                        {{ active_class(request()->is('service/request/*/edit')) }}
                        {{ active_class(request()->is('service/sparesale/*')) }}
                        {{ active_class(request()->is('service/purchase/*')) }}
                        {{ set_active(['service/stock','service/purchase','service/purchase/create','service/sales/transfer','service/sales/transfer/create','service/request','service/request/create','service/sparesale/create','service/sparesale']) }}">
                            <a href="#">
                            <i class="fa fa-caret-down down-arrow"></i>  <span class="menu-left">STOCK</span> <i class="la la-exchange"></i> 
                            </a>
                            <ul class="sidebar-submenu">
                            
                                <li class="{{(userHasPermission('Service','stock_list')?'':'hide')}}"><a href="{{ route('stock') }}" class="nav-link {{ request()->is('service/stock*') ? 'active font-weight-bolder' : '' }}"> <span> Opening Stock </span> </a></li>
                                <li class="{{(userHasPermission('Service','purchase_list')?'':'hide')}}"><a href="{{ route('purchase') }}" class="nav-link {{ request()->is('service/purchase*') ? 'active font-weight-bolder' : '' }}"> <span>  Purchase Spare </span> </a></li>
                                <li class="{{(userHasPermission('Service','transfer_list')?'':'hide')}}"><a href="{{ route('transfer') }}" class="nav-link {{ request()->is('service/sales/transfer*') ? 'active font-weight-bolder' : '' }}"> <span> Stock Transfer  </span> </a></li>
                                <li class="{{(userHasPermission('Service','purchase_order_list')?'':'hide')}}"><a href="{{ route('request') }}" class="nav-link {{ request()->is('service/request*') ? 'active font-weight-bolder' : '' }}"> <span> Purchase Order  </span> </a></li>
                                <li class="{{(userHasPermission('Service','spare_sale_list')?'':'hide')}}"><a href="{{ route('sparesale') }}" class="nav-link {{ request()->is('service/sparesale*') ? 'active font-weight-bolder' : '' }}"> <span> Spare Sale  </span> </a></li>
                            </ul>
                        </li>
                    @endif
                    @if (userHasPermission('Service','service_list') || userHasPermission('Service','sales_request_list') || userHasPermission('Service','service_transfer_list'))
                    <li class=" {{ set_active(['service/list','service/create','service/sales/request/create','service/servicetransfer','service/sales/request','service/approve/request']) }}">
                        <a href="#"><i class="fa fa-caret-down down-arrow"></i> <span class="menu-left">SERVICE </span> <i class="la la-cogs"></i></a>
                        <ul class="sidebar-submenu">
                            <li class="{{(userHasPermission('Service','service_list')?'':'hide')}}"><a href="{{ route('service.list') }}"   class="nav-link {{ request()->is('service/list*') ? 'active font-weight-bolder' : '' }}"> <span> SERVICE LIST </span> </a></li>
                            <li class="{{(userHasPermission('Service','sales_request_list')?'':'hide')}}"><a href="{{ url('service/sales/request') }}"   class="nav-link {{ request()->is('service/sales/request*') ? 'active font-weight-bolder' : '' }}"> <span> SALES REQUEST </span> </a></li>
                            <li class="{{(userHasPermission('Service','service_transfer_list')?'':'hide')}}"><a href="{{ url('service/servicetransfer') }}"   class="nav-link {{ request()->is('service/servicetransfer*') ? 'active font-weight-bolder' : '' }}"> <span> SERVICE TRANSFER </span> </a></li>       
                            <li class="{{(userHasPermission('Service','appeal_request_approval')?'':'hide')}}"><a href="{{ url('service/approve/request') }}"   class="nav-link {{ request()->is('service/approve/request*') ? 'active font-weight-bolder' : '' }}"> <span> APPROVE REQUEST </span> </a></li>                               
                        </ul>
                    </li>
                    @endif
                    <li class="{{ set_active(['service/doa','service/doa/create']) }} {{(userHasPermission('Service','doa_list')?'':'hide')}}">
                        <a href="{{ route('doa.index') }}">
                            <span>DOA</span>  <i class="la la-exchange"></i> 
                        </a>
                    </li>  
                    <li class="{{ set_active(['service/stock_service','service/stock_service/create']) }} {{(userHasPermission('Service','stock_service_list')?'':'hide')}}">
                        <a href="{{ route('stock_service.index') }}">
                            <span>STOCK SERVICE</span> <i class="la la-tasks"> </i> 
                        </a>
                    </li> 
                    <li class="{{ set_active(['service/bom']) }} {{(userHasPermission('Service','bom_list')?'':'hide')}}">
                        <a href="{{ route('bom.index') }}">
                            <span>BOM</span>  <i class="la la-tasks"></i>
                        </a>
                    </li>  
                    <li class="{{ set_active(['service/software']) }} {{(userHasPermission('Service','software_list')?'':'hide')}}">
                        <a href="{{ route('software.index') }}">
                            <span>SOFTWARE</span>  <i class="la la-download"></i>
                        </a>
                    </li>  
                    
                    @if (userHasPermission('Service','doa_report') || userHasPermission('Service','activation_report') || userHasPermission('Service','stock_report') || userHasPermission('Service','consumption_report') || userHasPermission('Service','spare_claim_report') || userHasPermission('Service','service_claim_report') || userHasPermission('Service','complaint_report') || userHasPermission('Service','forecast_report')) 
                    <li class=" {{ set_active(['report/doa','report/activation','report/stock_service/claim','report/stock','report/consumption','report/spare','report/forecast','report/service/claim','report/service/compliant','report/complaint','report/price','report/level']) }}">
                        <a href="#"> <i class="fa fa-caret-down down-arrow"></i><span class="menu-left">REPORTS </span> <i class="la la-bar-chart"></i></a>
                        <ul class="sidebar-submenu">
                            <li class="{{(userHasPermission('Service','doa_report')?'':'hide')}}"><a href="{{ route('report.doa') }}"   class="nav-link {{ request()->is('report/doa*') ? 'active font-weight-bolder' : '' }}"> <span>  DOA Report </span> </a></li>
                            <li class="{{(userHasPermission('Service','activation_report')?'':'hide')}}"><a href="{{ route('report.activation') }}"   class="nav-link {{ request()->is('report/activation*') ? 'active font-weight-bolder' : '' }}"> <span>  Activation Report </span> </a></li>
                            <li class="{{(userHasPermission('Service','stock_report')?'':'hide')}}"><a href="{{ route('report.stock') }}"   class="nav-link {{ request()->is('report/stock') ? 'active font-weight-bolder' : '' }}"> <span> Stock Report </span> </a></li>
                            <li class="{{(userHasPermission('Service','consumption_report')?'':'hide')}}"><a href="{{ route('report.consumption') }}"   class="nav-link {{ request()->is('report/consumption*') ? 'active font-weight-bolder' : '' }}"> <span> Consumption Report </span> </a></li>
                            <li class="{{(userHasPermission('Service','spare_claim_report')?'':'hide')}}"><a href="{{ route('report.spare') }}"   class="nav-link {{ request()->is('report/spare*') ? 'active font-weight-bolder' : '' }}"> <span> Spare Claim Report </span> </a></li> 
                            <li class="{{(userHasPermission('Service','service_claim_report')?'':'hide')}}"><a href="{{ route('report.service.claim') }}"   class="nav-link {{ request()->is('report/service/claim*') ? 'active font-weight-bolder' : '' }}"> <span> Service Claim Report </span> </a></li>
                            <li class="{{(userHasPermission('Service','stock_service_claim_report')?'':'hide')}}"><a href="{{ route('report.stock_service.claim') }}"   class="nav-link {{ request()->is('report/stock_service/claim') ? 'active font-weight-bolder' : '' }}"> <span> Stock Service Claim Report </span> </a></li>
                            <li class="{{(userHasPermission('Service','complaint_report')?'':'hide')}}"><a href="{{ route('report.service.compliant') }}"   class="nav-link {{ request()->is('report/service/compliant*') ? 'active font-weight-bolder' : '' }}"> <span> Complaint Report </span> </a></li>  
                            <li class="{{(userHasPermission('Service','forecast_report')?'':'hide')}}"><a href="{{ route('report.forecast') }}"   class="nav-link {{ request()->is('report/forecast*') ? 'active font-weight-bolder' : '' }}"> <span> Forecast Report </span> </a></li>
                            <li class="{{(userHasPermission('Service','model_wise_complaint_report')?'':'hide')}}"><a href="{{ route('report.complaint') }}"   class="nav-link {{ request()->is('report/complaint*') ? 'active font-weight-bolder' : '' }}"> <span> Model Wise Complaint Report </span> </a></li>
                            <li class="{{(userHasPermission('Service','price_comparison_report')?'':'hide')}}"><a href="{{ route('report.price') }}"   class="nav-link {{ request()->is('report/price*') ? 'active font-weight-bolder' : '' }}"> <span> Price Comparison Report </span> </a></li>
                            <li class="{{(userHasPermission('Service','level_claim_report')?'':'hide')}}"><a href="{{ route('report.level.claim') }}"   class="nav-link {{ request()->is('report/level*') ? 'active font-weight-bolder' : '' }}"> <span> Level Claim Report </span> </a></li>
                        </ul>
                    </li>
                    @endif
                    </ul>
                </div>

                @yield('content')

            </div>
            <!-- **********  WRAPPER  ********** -->
        </main>
    </div>
    <!-- **********  JAVASCRIPT  ********** -->

    <!-- jquery -->
    <script src="{{ asset('js/jquery-3.5.1.min.js')}}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- jquery mask -->
    <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('js/bootstrap/bootstrap.bundle.min.js') }}"></script>

    <!-- jquery-nicescroll -->
    <script src="{{ asset('js/jquery-nicescroll/jquery.nicescroll.min.js') }}"></script>


    <!-- custom -->
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/backend/common.js') }}"></script>

    @isset($js)
        @foreach ($js as $j)
            <script src="{{ asset('js/backend/' . $j . '.js') }}"></script>
        @endforeach
        @endif

        @yield('pagescript')
    </body>

    </html>

var _serviceForm = $('#create-service');
var imei_no = getUrlParameter('imei_no');
var request_id = getUrlParameter('request_id');
if (imei_no) {
    $("#imei_no").val(imei_no);
    $("#sales_request_id").val(request_id);
    searchIMEI();
}
var _validator = _serviceForm.validate({
    rules: {
        brand_id: "required",
        model_id: "required",
        // delivery_date: "required",
        sale_date: "required",
        technician_id: "required",
        customer_name: "required",
        customer_phone: "required",
        shop_name: "required",
        'complaint[]': {
            required: true
        },
        // customer_email: {
        //     required: true,
        //     email: true
        // },
        'invoice[]': {
            required: function(element) {
                var shop_name = $("#shop_name").val();
                if (shop_name == "") {
                    return false;
                } else {
                    return true;
                }
            }
        }

    },
    messages: {
        brand_id: "Please select brand",
        model_id: "Please select model",
        delivery_date: "Please enter delivery date",
        sale_date: "Please enter sales date",
        technician_id: "Please select technician",
        customer_name: "Please enter name",
        customer_phone: "Please enter phone no",
        'complaint[]': "Please select complaint",
        customer_email: "Please enter a valid email address",
        shop_name: "Please enter shop name",
        'invoice[]': {
            required: "Please upload invoice"
        }
    },
    submitHandler: function(form) {
        var formData = new FormData(form);
        var brand_id = $('#brand_id').find(":selected").val();
        var model_id = $('#model_id').find(":selected").val();
        var manufacture_date = $('#manufacture_date').val();
        var sale_date = $('#sale_date').val();
        var active_date = $('#active_date').val();
        formData.append('brand_id', brand_id);
        formData.append('model_id', model_id);
        formData.append('manufacture_date', manufacture_date);
        formData.append('sale_date', sale_date);
        formData.append('active_date', active_date);
        $('.txt_brand_id').attr("disabled", false);
        $('.txt_model_id').attr("disabled", false);
        $('#is_warranty').attr("disabled", false);
        $("#sale_date").attr("disabled", false);
        $("#active_date").attr("disabled", false);
        $("#manufacture_date").attr("disabled", false);
        $('#btnAddRequest').prop('disabled', true);
        $.ajax({
            url: form.action,
            type: form.method,
            // data: $(form).serialize(),
            data: formData,
            contentType: false,
            processData: false,
            cache: false,
            success: function(response) {
                $('#btnAddRequest').prop('disabled', false);
                if (response != 0) {
                    var page = '/service/receipt/' + response;
        
                    window.open(page, "_blank");
                    window.location.reload();
                }
            }
        });
    }
});

$(function() {
    $('input[name="delivery_date"],input[name="sale_date"],input[name="active_date"],input[name="manufacture_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput: false,
        locale: {
            format: 'DD-MM-YYYY'
        }
    }).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });

});
$('#submission_cat_id').on('change', function() {
    if (this.value == 1) {
        $(".dealer_block").slideUp();
        $(".salesman_block").slideUp();
        $(".cus_block").slideDown();
    } else if (this.value == 2) {
        $(".cus_block").slideUp();
        $(".salesman_block").slideUp();
        $(".dealer_block").slideDown();
    } else if (this.value == 3) {
        $(".cus_block").slideUp();
        $(".dealer_block").slideUp();
        $(".salesman_block").slideDown();
    }
});
$('#product_type').on('change', function() {
    var sel = $("#product_type").val();
    $("#create-service")[0].reset();
    $("#product_type").val(sel);
    if (sel == 1) {
        $('#is_warranty').attr("disabled", true);
        $("#brand_id").attr("disabled", true);
        $('#model_id').attr("disabled", true);
        $(".block_div").slideUp();
        //$(".imeicls").removeClass('hide');
        $("#sale_date").attr("disabled", true);
        $('#active_date').attr("disabled", true);
        $("#manufacture_date").attr("disabled", true);

    } else if (sel > 1) {
        //$(".imeicls").addClass('hide');
        $('#is_warranty').attr("disabled", false);
        $("#brand_id").attr("disabled", false);
        $('#model_id').attr("disabled", false);
        $("input[type=text]").attr("readonly", false);
        $("#sale_date").attr("disabled", false);
        $('#active_date').attr("disabled", false);
        $("#manufacture_date").attr("disabled", false);
        $("#brand_id").val('');
        $(".scan_result").slideUp();
        $(".block_div").slideDown();
        $("#brand_id").select2({
            placeholder: "Select a Brand"
        });
        $("#model_id").select2({
            placeholder: "Select a Model"
        });
        $("#model_id").empty();
        $("#version").select2({
            placeholder: "Select a Version"
        });
        $("#version").empty();
        $("#technician_id").select2({
            placeholder: "Select a Technician"
        });
    }
});

$(".cus_search").click(function() {
    var type = $(this).attr("data-id");
    if (type == 1) {
        var customer_phone = $("#customer_phone").val();
    } else if (type == 2) {
        var customer_phone = $("#dealer_phone").val();
    }
    if (customer_phone) {
        $.ajax({
            url: "/service/getCustomerData",
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                query: customer_phone,
                type: type
            },
            cache: false,
            dataType: 'json',
            success: function(dataResult) {
                console.log(dataResult);
                if (dataResult.length > 0) {

                    if (type == 1) {
                        $('#customer_name').val(dataResult[0].customer_name);
                        $('#customer_email').val(dataResult[0].customer_email);
                        $('#address').val(dataResult[0].address);
                        $('#gender').val(dataResult[0].gender);
                        $('#postal_code').val(dataResult[0].postal_code);
                        $('#backup_no').val(dataResult[0].backup_no);
                    } else if (type == 2) {
                        $('#dealer_name').val(dataResult[0].customer_name);
                        $('#dealer_address').val(dataResult[0].address);
                        $('#dealer_contact').val(dataResult[0].dealer_contact);
                    } else if (type == 3) {
                        $('#salesman_name').val(dataResult[0].customer_name);
                        $('#salesman_district').val(dataResult[0].salesman_district);
                    }
                } else {
                    $('#customer_name').val('');
                    $('#customer_email').val('');
                    $('#address').val('');
                    $('#gender').val('');
                    $('#postal_code').val('');
                    $('#backup_no').val('');
                    $('#dealer_name').val('');
                    $('#dealer_address').val('');
                    $('#dealer_contact').val('');
                    $('#salesman_name').val('');
                    $('#salesman_district').val('');
                }
            }

        });
    }
});

function searchIMEI() {
    var query = $("#imei_no").val();
    var product_type = $("#product_type").val();
    $(".block_div").slideUp();
    if (query) {
        $.ajax({
            url: "/service/getIMEIData",
            type: "POST",
            cache: false,
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                query: query,
                product_type: product_type

            },
            cache: false,
            dataType: 'json',
            success: function(dataResult) {
                console.log(dataResult);
                var resultData = dataResult.data;

                var historyData = dataResult.history;
                if (resultData.length > 0) {
                    $.each(resultData, function(index, row) {
                        //alert(row.model_country_id);
                        var country_id = $("#country_id").val();
                        if (row.country_id == null) {
                            $(".jumbotron").html('<h2 class="text-danger">This IMEI no is freezone</h2>');
                        } else {
                            if (row.doa_id > 0 && row.doa_status != 2) {
                                $(".jumbotron").html('<h2 class="text-danger">This IMEI no already exists in DOA list</h2>');
                            } else {

                                if (row.service_status != 5 && row.service_status != null && row.service_status != 6) {
                                    if (row.sales_request_id > 0 && row.service_status == 0) {
                                        $(".jumbotron").html('<h2 class="text-danger">IMEI/SL No Already exists in sales request <i class="fa fa-times-circle-o"></i></h2>');
                                    } else {
                                        $(".jumbotron").html('<h2 class="text-danger">This IMEI/SL No has already given for repair <i class="fa fa-times-circle-o"></i></h2>');
                                    }
                                } else {
                                    var button = '<button class="btn btn-success verify btn-outline"> Warranty Service <i class="fa fa-check-circle-o"></i></button>';
                                    var flag = 0;
                                    var flagstatus = 0;
                                    if (row.model_country_id != null) {
                                    var dataArr = row.model_country_id;
                                    var arr = dataArr.split(',');
                                                
                                     $.each(arr, function(index, value) {
                                         if (country_id == value) {
                                               flagstatus = 1;
                                          }
                                    });
                                    }
                                    if (row.expiry_date) {
                                        if (country_id == row.country_id) {
                                            var expiry_date = row.expiry_date;
                                        } else {
                                              if (row.model_country_id != null) {
                                                if (flagstatus == 1) {
                                                     var international_warranty =  row.international_warranty;
                                                     var dateAr = international_warranty.split('-');
                                                     var expiry_date = dateAr[2]+'-'+dateAr[1]+'-'+dateAr[0];
                                                } else {
                                                    var expiry_date = "--";
                                                    flag = 1;
                                                }
                                            } else {
                                                var expiry_date = "--";
                                                flag = 1;
                                            }
                                        }
                                    } else {

                                        var expiry_date = null;
                                        flag = 0;
                                    }

                                    if (expiry_date) {
                                        var dateAr = expiry_date.split('-');
                                        var GivenDate = dateAr[2]+'-'+dateAr[1]+'-'+dateAr[0];
                                        var CurrentDate = new Date();
                                        GivenDate = new Date(GivenDate);
                                    }
                                       
                                    $('#is_warranty').attr("disabled", true);
                                    if (Date.parse(GivenDate) < Date.parse(CurrentDate)) {
                                        var txt_warranty = '<h2 class="text-danger">WARRANTY EXPIRED <i class="fa fa-times-circle-o"></i></h2>';
                                        var txt_war_cls = "text-danger";
                                        var button = '<button class="btn btn-info verify btn-outline"> Non-Warranty Service <i class="fa fa-check-circle-o"></i></button>';
                                        $('#is_warranty [value=0]').attr('selected', 'true');

                                    } else if (expiry_date == "--" && flag == 1) {
                                      
                                        var txt_warranty = '<h2 class="text-danger">NON-WARRANTY SERVICE <i class="fa fa-times-circle-o"></i></h2>';
                                        var txt_war_cls = "text-danger";
                                        var button = '<input type="button" class="btn btn-info nonwarranty_cls" value="Non-Warranty Service"> ';
                                    } else if (expiry_date == null) {
                                        var saler_btn = '';
                                        var saler_title = '';

                                        if (country_id == row.country_id || flagstatus == 1) {
                                            
                                            saler_btn = '<input type="button" class="btn btn-primary request" value="Sales Request"> ';
                                            saler_title = 'SALES REQUEST/';


                                        }  
                                           var txt_warranty = '<h2 class="text-danger">' + saler_title + 'NON-WARRANTY SERVICE <i class="fa fa-times-circle-o"></i></h2>';
                                           var txt_war_cls = "text-danger";
                                           var button = saler_btn + '<input type="button" class="btn btn-info nonwarranty_cls" value="Non-Warranty Service"> ';
                                        
                                    } else {
                                        var txt_warranty = '<h2 class="text-success">WARRANTY SERVICE</h2>';
                                        var txt_war_cls = "text-success";
                                        $('#is_warranty').attr("disabled", false);
                                        $('#is_warranty [value=1]').attr('selected', 'true');
                                    }
                                    var imeis = row.IMEI_no1 + "<br/>" + row.IMEI_no2;
                                    var device = row.ram_name + "," + row.rom_name + "," + row.color;
                                    var date_txt = htmlhistory = '';
                                    if (access_all == 1) {
                                        var date_txt = '<tr><th> Date Of Manufacture </th><td align="center">' + row.date_of_manufacture + '</td></tr>';
                                    }
                                    if (historyData.length > 0) {
                                        var htmlhistory = '<tr><th scope="col" align="center" valign="center" colspan="2"><h4 class="card-title-text text-info">Service History </h4></th></tr>';
                                        $.each(historyData, function(index, row) {
                                            htmlhistory += '<tr><td>Serial.Num</td><td><a href="/service/' + row.id + '">' + row.serial_no + '</a></td></tr><tr><td>Service Date</td><td>' + row.created_at + '</td></tr><tr><td>Branch</td><td>' + row.name + '</td></tr>';
                                        })
                                    }
                                    var html = txt_warranty + '<table class="table table-condensed"><tbody><tr class="' + txt_war_cls + '"><th> Expiry </th><td align="center">' + expiry_date + '</td></tr><tr><th> IMEI </th><td align="center">' + imeis + '</td></tr><tr><th> Serial.Num </th><td align="center">' + row.serial_no + '</td></tr><tr><th> Model </th><td align="center">' + row.model_name + '</td></tr><tr><th> Brand </th><td align="center">' + row.brand_name + '</td></tr>' + date_txt + ' ' + htmlhistory + '<th></th><td align="center"></td></tbody></table>' + button + '<button class="btn btn-danger close_scan_window btn-outline" type="button"> CLOSE <i class="fa fa-times-circle-o"></i></button>';
                                    $(".txt_imei_id").val(row.serial_no);
                                    $("#brand_id").val(row.brand_id);
                                    $('.txt_brand_id').attr("disabled", true);
                                    $('.txt_model_id').attr("disabled", true);
                                    $(".txt_manufature").val(row.date_of_manufacture);
                                    $("#imeiId").val(row.id);
                                    $(".txt-factory_name").val(row.factory_name);
                                    $(".txt_device").val(device);
                                    $(".txt_rom").val(row.rom_name);
                                    $(".txt_color").val(row.color);
                                    $(".txt_sales_date").val(row.sales_date);
                                    $(".txt_active_date").val(row.activation_date);
                                    if (row.model_id) {
                                        getVersion(row.variant_id);
                                    }
                                    if (row.brand_id) {
                                        getBrand(row.brand_id, row.variant_id);
                                    }
                                    $('.js-example-basic-single').select2();
                                }
                                $(".jumbotron").html(html);
                            }
                        }
                    })
                } else {
                    $(".jumbotron").html('<h2 class="text-danger"> <i class="fa fa-times-circle"></i><br>IMEI number not registered under RAVOZ warranty</h2><button class="btn  btn-danger close_scan_window  btn-outline" onclick="return close();" type="button"> CLOSE<i class="fa fa-times-circle-o"></i></button>');
                }
                $(".scan_result").slideDown();
                $(".dealer_block").slideUp();
                $(".salesman_block").slideUp();
            },
            complete: function() {
                $(".close_scan_window").click(function() {
                    $(".scan_result").slideUp();
                });
                $(".verify").click(function() {
                    $(".block_request_div").addClass('hide');
                    $("#sale_date").attr("disabled", true);
                    $("#active_date").attr("disabled", true);
                    $("#manufacture_date").attr("disabled", true);
                    $(".scan_result").slideUp();
                    $(".block_div").slideDown();
                    $("#btnAddRequest").html('Submit');
                });
                $(".request").click(function() {
                    $(".scan_result").slideUp();
                    $(".block_div").slideDown();
                    $(".block_request_div").removeClass('hide');
                    $('#is_warranty').attr("disabled", false);
                    $("#sale_date").attr("disabled", false);
                    $("#active_date").attr("disabled", true);
                    $("#manufacture_date").attr("disabled", true);
                    $("#btnAddRequest").html('Sales Request');
                });
                $(".nonwarranty_cls").click(function() {
                    $(".block_request_div").addClass('hide');
                    $("#sale_date").attr("disabled", true);
                    $("#active_date").attr("disabled", true);
                    $("#manufacture_date").attr("disabled", true);
                    $(".scan_result").slideUp();
                    $(".block_div").slideDown();
                    $('#is_warranty').attr("disabled", true);
                    $('#is_warranty [value=0]').attr('selected', 'true');
                    $("#btnAddRequest").html('Submit');
                });

            }
        });
    } else {
        $('.scan_result').css("display", "none");
    }
}



function getBrand(brand_id, variant_id) {
    if (brand_id) {
        $.ajax({
            type: "get",
            url: "/service/getModel/" + brand_id,
            success: function(res) {
                $("#model_id").empty();
                $("#model_id").append('<option>--Select Model--</option>');
                $.each(res, function(key, value) {
                    var varient_txt = "(" + value.color_name;
                    if (value.ram_name) varient_txt += "," + value.ram_name;
                    if (value.rom_name) varient_txt += "," + value.rom_name;
                    varient_txt += ")";
                    $('#model_id').append($("<option/>", {
                        value: value.id,
                        text: value.name + varient_txt
                    }));
                    $("#model_id").val(variant_id);
                });

            }
        });
    } else {
        $("#model_id").append('<option>--Select Model--</option>');
    }

}



$("#warrantylink").click(function() {
    $(".warrantycls").toggle();
    $(".warranty_hide").toggle();
});


$(".warrantycls").change(function() {
    var formData = 'is_warranty=' + $(this).val() + '&service_id=' + $("input[name='service_id']").val();
    $.ajax({
        type: "POST",
        url: "/service/update/warranty",
        data: formData,
        success: function(data) {
            window.location.reload();
        }
    });
});
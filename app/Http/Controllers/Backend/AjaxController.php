<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class AjaxController extends Controller
{
    public function __construct()
    {
        //dd(Auth::user());
        // check if session expired for ajax request
        $this->middleware('ajax-session-expired');

        // check if user is autenticated for non-ajax request
        $this->middleware('auth');
    }
    public function Activation(Request $request)
    {
        $this->updateStatus($request['name'], $request['ref'], $request['val'], 'status');
    }
    public function GoogleAuthActivation(Request $request)
    {
        $data['google_auth_status'] = $request['val'];
        $data['google2fa_secret'] = NULL;
        $this->updatedata($request['name'],$data,$request['ref']);
    }
    public function GoogleRestAuthActivation(Request $request)
    {
        $data['google2fa_secret'] = NULL;
        $this->updatedata($request['name'],$data,$request['ref']);
    }

    
}

<?php

namespace App\Http\Traits;

use DB;
use Auth;
use Session;
use Carbon\Carbon;

trait CommonTrait
{
    public function NameAndID($table)
    {
        return DB::table($table)
            ->where('status', '=', 1)
            ->orderBy('name', 'ASC')
            ->select("id", "name")
            ->pluck("name", "id")
            ->all();
    }
    public function GetNameAndID($table)
    {
        return DB::table($table)
            ->select("id", "name")
            ->whereNull("deleted_at")
            ->get();
    }
    public function GetId($table, $name)
    {

        $row = DB::table($table)
            ->select("id")
            ->where("name", $name)
            ->first();
        return $row->id ? $row->id : null;
    }

    public function GetAll($table)
    {
        return DB::table($table)
            ->select("*")
            ->whereNull("deleted_at")
            ->get();
    }
    public function GetAllByStatus($table)
    {
        return DB::table($table)
            ->select("*")
            ->where("status", 1)
            ->get();
    }
    public function getLastRecordId($table)
    {
        return DB::table($table)
            ->orderBy("id", "desc")
            ->first()->id;
    }
    public function getRolePermission($role_id)
    {
        $rolePermission = DB::table("roles")
            ->leftJoin(
                "permission_role",
                "permission_role.role_id",
                "=",
                "roles.id"
            )
            ->leftJoin(
                "permissions",
                "permission_role.permission_id",
                "=",
                "permissions.id"
            )
            ->where("roles.id", $role_id)
            ->pluck("permissions.id")
            ->toArray();
        return $rolePermission;
    }

    public function getModulePermission($module_id)
    {
        $modulePermission = DB::table("permissions")
            ->select('id', 'name')
            ->where(["module_id" => $module_id, "status" => 1])
            ->get();
        return $modulePermission;
    }
    public function GetByID($table, $id)
    {
        $this->result = DB::table($table)
            ->where("id", "=", $id)
            ->first();
        return $this->result;
    }
    public function GetAllByID($table, $id)
    {
        return DB::table($table)
            ->select("*")
            ->where("id", "=", $id)
            ->get();
    }
    public function FieldByID($table, $field, $id)
    {
        $this->result = DB::table($table)
            ->select($field)
            ->where("id", "=", $id)
            ->first();
        return ($this->result ? $this->result->$field : "");
    }
    public function GetFirst($table, $where)
    {
        $this->result = DB::table($table)
            ->where($where)
            ->first();
        return $this->result;
    }
    public function updateStatus($table, $id, $status, $column)
    {
        return DB::table($table)->where('id', $id)->update(array($column => $status));
    }

    public function insertdata($table, $data)
    {
        return DB::table($table)->insert($data);
    }

    public function deletedata($table, $field, $id)
    {
        return DB::table($table)->where($field, $id)->delete();
    }

    public function updatedata($table, $data, $id)
    {

        return DB::table($table)->where('id', $id)->update($data);
    }

    public function insertGetId($table, $data)
    {
        return DB::table($table)->insertGetId($data);
    }

    public function FieldByName($table, $field, $name)
    {
        $this->result = DB::table($table)
            ->select($field)
            ->where("name", "=", $name)
            ->first();
        return ($this->result ? $this->result->$field : 0);
    }

    public function WhereByData($table, $where, $id)
    {
        $this->result = DB::table($table)
            ->select('*')
            ->where($where, "=", $id)
            ->get();
        return ($this->result ? $this->result : 0);
    }

    public function WhereModelByData($id)
    {
        $this->result = DB::table('variant_details')
            ->leftJoin(
                "color",
                "variant_details.color_id",
                "=",
                "color.id"
            )
            ->leftJoin(
                "rom",
                "variant_details.rom_id",
                "=",
                "rom.id"
            )
            ->leftJoin(
                "ram",
                "variant_details.ram_id",
                "=",
                "ram.id"
            )
            ->select('variant_details.*', 'color.name as color_name', 'ram.name as ram_name', 'rom.name as rom_name')
            ->where('variant_details.model_id', "=", $id)
            ->get();
        return ($this->result ? $this->result : 0);
    }

    public function isModelRecordExist($table, $where, $field)
    {
        if ($data = DB::table($table)->where($where, '=', $field)->first()) {
            return $data->id;
        } else {
            return 0;
        }
    }


    public function transformDate($value, $format = 'Y-m-d')
    {
        if (gettype($value) === "integer") {
            $date = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value))->format('Y-m-d');
            if ($date > "2018-01-01" && $date <= date("Y-m-d")) {
                return $date;
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }

    public function getWarranty($branch_id)
    {
        if ($data = DB::table('branch')->leftJoin(
            "country",
            "branch.country_id",
            "=",
            "country.id"
        )->where('branch.id', '=', $branch_id)->select('country.warranty')->first()) {
            return $data->warranty;
        } else {
            return 0;
        }
    }

    public function getCountryWarranty($name)
    {
        if ($data = DB::table('country')->where('name', '=', $name)->first()) {
            return $data;
        } else {
            return 0;
        }
    }


    public function GetSpareData()
    {
        $spares = DB::table('spare')->select('id', 'spare_name', 'part_code', 'focus_code')->get();
        return ($spares ? $spares : []);
    }

    public function getStock($branch_id, $spare_id)
    {
        if ($data = DB::table('stock')->where('branch_id', '=', $branch_id)->where('spare_id', '=', $spare_id)->first()) {
            return $data->id;
        } else {
            return 0;
        }
    }

    public function viewItems($table, $id)
    {
        $query = DB::table($table . "_items as item")
            ->leftJoin(
                $table . " as parent",
                "item.parent_id",
                "=",
                "parent.id"
            )
            ->leftjoin("spare", "item.spare_id", "=", "spare.id")
            ->leftjoin("models", "spare.model_id", "=", "models.id")
            ->leftjoin("users", "users.id", "=", "parent.created_by")
            ->leftjoin("branch as from", "parent.from_branch", "=", "from.id")
            ->leftjoin("branch as to", "parent.to_branch", "=", "to.id")
            ->select([
                \DB::raw('
                            spare.spare_name,
                            spare.part_code,
                            spare.focus_code,
                            models.name as model_name,
                            parent.date,
                            from.name as from_name,
                            to.name as to_name,
                            item.id,
                            parent.from_branch,
                            parent.to_branch,
                            parent.status,
                            item.*'),
            ])
            ->where("item.parent_id", $id)
            ->get();
        return $query;
    }

    public function getAvailableStock($branch_id, $spare_id)
    {
        if ($data = DB::table('stock')->where('branch_id', '=', $branch_id)->where('spare_id', '=', $spare_id)->first()) {
            return $data->qty;
        } else {
            return 0;
        }
    }

    public function serialNumber($table, $branch_id)
    {
        $branch = $this->GetByID('branch', $branch_id);
        $unique_no = DB::table($table)->orderBy('id', 'DESC')->pluck('id')->first();
        if ($unique_no == null or $unique_no == "") {
            $unique_no = 1;
        } else {
            $unique_no = $unique_no + 1;
        }
        return $branch->code . "-" . sprintf('%04d', $unique_no);
    }


    public function GetComplaints()
    {
        return DB::table("complaints")
            ->leftjoin("levels", "levels.id", "=", "complaints.level_id")
            ->select("complaints.id", "complaints.description", "levels.name")
            ->Where('complaints.status', '=', 1)
            ->get();
    }

    public function GetSpares($model_id)
    {
        return DB::table("spare")
            ->leftjoin("models", DB::raw("FIND_IN_SET(models.id,spare.model_id)"), ">", DB::raw("'0'"))
            ->select("spare.id", "spare.spare_name", "models.name as name")
            ->whereRaw("find_in_set('" . $model_id . "',spare.model_id)")
            ->Where('spare.status', '=', 1)->groupBy('spare.id')
            ->orwhereNull('spare.model_id')
            ->get();
    }

    public function GetUsers($branch_id)
    {
        return DB::table("users")
            ->where('branch_id', $branch_id)->pluck('first_name', 'id');
    }


    public function GetModel($id)
    {
        $data = DB::table("variant_details")
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('brands', 'brands.id', '=', 'models.brand_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->where("models.brand_id", $id)
            ->select("variant_details.id", "models.name", "models.id as model_id", "color.name as color_name", "ram.name as ram_name", "rom.name as rom_name")
            ->get();
        return response()->json($data);
    }


    public function GetCurrencyRate($branch_id)
    {
        return DB::table("currency")
            ->leftJoin('country', 'country.currency_id', '=', 'currency.id')
            ->leftJoin('branch', 'branch.country_id', '=', 'country.id')
            ->where("branch.id", $branch_id)->select('currency.name', 'currency.rate')
            ->get();
    }

    public function GetAverageCost($branch_id, $spare_id)
    {
        return DB::table('stock')->select('local_price', 'price')
            ->where('branch_id', $branch_id)
            ->where('spare_id', $spare_id)
            ->first();
    }
    public function getServiceHistory($where)
    {
        return json_decode(json_encode(DB::table('service')->where($where)->get()), 1);
    }
    public function getService($id)
    {
        return DB::table('service')
            ->leftJoin(
                'service_customers',
                'service_customers.customer_id',
                '=',
                'service.customer_id'
            )
            ->leftJoin(
                'customers',
                'service_customers.parent_id',
                '=',
                'customers.customer_id'
            )
            ->leftJoin('users', 'users.id', '=', 'service.technician_id')
            ->leftJoin('brands', 'brands.id', '=', 'service.brand_id')
            ->leftJoin(
                'variant_details',
                'variant_details.id',
                '=',
                'service.variant_id'
            )
            ->leftJoin('imei_details', 'imei_details.id', '=', 'service.imei_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('software_version as old_software', 'old_software.id', '=', 'service.version_id')
            ->leftJoin('software_version as new_software', 'new_software.id', '=', 'service.new_version_id')
            ->leftJoin('product_type', 'product_type.id', '=', 'models.device_id')
            ->leftJoin('service_transfer', 'service.id', '=', 'service_transfer.service_id')
            ->leftJoin('users as transfer_accept_user', 'service_transfer.accepted_user', '=', 'transfer_accept_user.id')
            ->leftJoin('users as transfer_return_user', 'service_transfer.return_user', '=', 'transfer_return_user.id')
            ->leftJoin('branch as transfer_branch', 'transfer_branch.id', '=', 'service_transfer.to_branch')
            ->leftJoin('branch as transfer_from_branch', 'transfer_from_branch.id', '=', 'service_transfer.from_branch')
            ->leftJoin('service_charge', 'service.id', '=', 'service_charge.service_id')
            ->leftJoin('branch', 'branch.id', '=', 'service.branch_id')
            ->leftJoin('doa', 'service.id', '=', 'doa.service_id')
            ->leftJoin('appeal_master', 'appeal_master.service_id', '=', 'service.id')
            ->leftJoin('service_bill', 'service_bill.service_id', '=', 'service.id')
            ->leftJoin('pcb_history', 'pcb_history.service_id', '=', 'service.id')
            ->select(
                'service_customers.*',
                'customers.type',
                'customers.customer_id as parent_cus_id',
                'service.serial_no',
                'service.id',
                'users.first_name as technician',
                'service.is_warranty',
                'brands.name as brand',
                'brands.id as brand_id',
                'models.name as model',
                'models.id as model_id',
                'color.name as color',
                'ram.name as ram',
                'rom.name as rom',
                'service.customer_id',
                'service.sn_no',
                'service.branch_id',
                'service.security_code',
                'service.sale_date',
                'service.active_date',
                'service.manufacture_date',
                'service.delivery_date',
                'old_software.name as old_software_name',
                'new_software.name as new_software_name',
                'service.status',
                'service.technician_id',
                'service.accessories_id',
                'service.version_id',
                'service.new_version_id',
                'service.remarks',
                'service.variant_id',
                'service.imei_id',
                'service.sales_request_id',
                'service.created_at',
                'service.is_approved',
                'product_type.name as product_type',
                'service_transfer.id as transfer_id',
                'service_transfer.status as transfer_status',
                'service_transfer.return_user',
                'service_transfer.received_back_user',
                'service_transfer.received_back_date',
                'service_transfer.from_branch',
                'service_transfer.to_branch',
                'transfer_branch.name as to_branch_name',
                'transfer_from_branch.name as from_branch_name',
                'service_transfer.transfer_date',
                'service_transfer.accepted_date',
                'service_transfer.return_date',
                'transfer_accept_user.first_name as accept_user',
                'transfer_return_user.first_name as return_user',
                'service_transfer.remarks as transfer_remarks',
                'service_charge.local_price as service_charge',
                'imei_details.id as imei_id',
                'imei_details.IMEI_no1',
                'imei_details.IMEI_no2',
                'imei_details.pcb_serial_no',
                'doa.id as doa_id',
                'doa.status as doa_status',
                'appeal_master.id as appeal_id',
                'appeal_master.status as appeal_status',
                'service.level_id',
                'service.discount_local_price',
                'branch.name as branch_name',
                'service_bill.description as work_description',
                'pcb_history.pcb_serial_no as new_pcb_serial_no',
            )
            ->where('service.id', $id)
            ->first();
    }
    public function getServiceById($id)
    {
        $this->data['data'] = DB::table('service')
            ->leftJoin(
                'service_customers',
                'service_customers.customer_id',
                '=',
                'service.customer_id'
            )
            ->leftJoin(
                'customers',
                'service_customers.parent_id',
                '=',
                'customers.customer_id'
            )
            ->leftJoin('users', 'users.id', '=', 'service.technician_id')
            ->leftJoin('brands', 'brands.id', '=', 'service.brand_id')
            ->leftJoin(
                'variant_details',
                'variant_details.id',
                '=',
                'service.variant_id'
            )
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('product_type', 'product_type.id', '=', 'models.device_id')
            ->leftJoin('software_version', 'software_version.id', '=', 'service.version_id')
            ->leftJoin('imei_details', 'imei_details.id', '=', 'service.imei_id')
            ->leftjoin("complaints", DB::raw("FIND_IN_SET(complaints.id,service.complaint)"), ">", DB::raw("'0'"))
            ->select(
                DB::raw("GROUP_CONCAT(complaints.description) as complaint_name"),
                'service_customers.*',
                'customers.type',
                'customers.customer_id as parent_cus_id',
                'service.serial_no',
                'service.id',
                'users.first_name as technician',
                'service.is_warranty',
                'brands.name as brand',
                'models.name as model',
                'color.name as color',
                'ram.name as ram',
                'rom.name as rom',
                'service.customer_id',
                'service.sn_no',
                'service.security_code',
                'service.sale_date',
                'service.active_date',
                'service.manufacture_date',
                'service.delivery_date',
                'software_version.name as version',
                'service.created_at',
                'product_type.name as product_type',
                'service.remarks',
                'service.accessories_id',
                'imei_details.IMEI_no1',
                'imei_details.IMEI_no2',
                'imei_details.serial_no as imei_sn_no'
            )
            ->where('service.id', $id)
            ->first();

        $this->data['data']->complaints = DB::table('service_complaint')
            ->leftJoin('service', 'service.id', '=', 'service_complaint.service_id')
            ->leftJoin('complaints', 'complaints.id', '=', 'service_complaint.complaint_id')
            ->where('service_complaint.service_id', $id)
            ->pluck('complaints.description')->implode(',');

        return $this->data;
    }
    public function getServiceBill($id)
    {
        $this->data['data'] = DB::table('service')
            ->leftJoin(
                'service_customers',
                'service_customers.customer_id',
                '=',
                'service.customer_id'
            )
            ->leftJoin(
                'customers',
                'service_customers.parent_id',
                '=',
                'customers.customer_id'
            )
            ->leftJoin('users', 'users.id', '=', 'service.technician_id')
            ->leftJoin('brands', 'brands.id', '=', 'service.brand_id')
            ->leftJoin(
                'variant_details',
                'variant_details.id',
                '=',
                'service.variant_id'
            )
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('product_type', 'product_type.id', '=', 'models.device_id')
            ->leftJoin('software_version', 'software_version.id', '=', 'service.version_id')
            ->leftJoin('service_bill', 'service_bill.service_id', '=', 'service.id')
            ->leftJoin('imei_details', 'imei_details.id', '=', 'service.imei_id')
            ->leftJoin('service_charge', 'service_charge.service_id', '=', 'service.id')

            ->select(
                'service_customers.*',
                'customers.type',
                'customers.customer_id as parent_cus_id',
                'service.serial_no',
                'service.id',
                'users.first_name as technician',
                'service.is_warranty',
                'brands.name as brand',
                'models.name as model',
                'color.name as color',
                'ram.name as ram',
                'rom.name as rom',
                'service.customer_id',
                'service.sn_no',
                'service.security_code',
                'service.sale_date',
                'service.active_date',
                'service.manufacture_date',
                'service.delivery_date',
                'software_version.name as version',
                'service.created_at',
                'product_type.name as product_type',
                'service.remarks',
                'service.discount_local_price',
                'service.accessories_id',
                'service_bill.bill_number',
                'service_bill.created_at as billdate',
                'service_bill.branch_id as branch_id',
                'service_bill.description as work_description',
                'imei_details.IMEI_no1',
                'imei_details.IMEI_no2',
                'imei_details.serial_no as imei_sn_no',
                'service_charge.local_price as service_charge'
            )
            ->where('service.id', $id)
            ->orderBy('service_bill.created_at', 'DESC')
            ->first();
        $this->data['data']->complaints = DB::table('service_complaint')
            ->leftJoin('service', 'service.id', '=', 'service_complaint.service_id')
            ->leftJoin('complaints', 'complaints.id', '=', 'service_complaint.complaint_id')
            ->where('service_complaint.service_id', $id)
            ->pluck('complaints.description')->implode(',');
        $this->data['data']->spares = DB::table('service_spare')
            ->leftJoin('service', 'service.id', '=', 'service_spare.service_id')
            ->leftJoin('spare', 'spare.id', '=', 'service_spare.spare_id')
            ->where('service_spare.service_id', $id)
            //->where('service_spare.is_warranty', 0)
            ->select(
                'service_spare.*',
                'spare.spare_name as sparename',
                'spare.focus_code'
            )
            ->get();
        return $this->data;
    }
    public function getStockService($id)
    {
        $stock=DB::table("stock_service")->where('id', $id)->first();
        if($stock->imei_id!='')
        {
            return DB::table("stock_service")
                ->leftJoin('users', 'users.id', '=', 'stock_service.technician_id')
                ->leftJoin('imei_details', 'imei_details.id', '=', 'stock_service.imei_id')
                ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
                ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
                ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
                ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
                ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
                ->leftJoin('brands', 'brands.id', '=', 'models.brand_id')
                ->leftJoin('software_version as old_software', 'old_software.id', '=', 'stock_service.version_id')
                ->leftJoin('software_version as new_software', 'new_software.id', '=', 'stock_service.new_version_id')
                ->leftJoin('branch', 'branch.id', '=', 'stock_service.branch_id')
                ->leftJoin('appeal_master', 'appeal_master.stock_service_id', '=', 'stock_service.id')
                ->leftJoin('stock_service_imei_history', 'stock_service_imei_history.service_id', '=', 'stock_service.id')
                ->leftJoin('pcb_history', 'pcb_history.stock_service_id', '=', 'stock_service.id')
                ->select(
                    DB::raw(
                        "DATE_FORMAT(imei_details.expiry_date, '%d-%m-%Y') as expiry_date"
                    ),
                    'stock_service.*',
                    'users.first_name as technician',
                    'variant_details.model_id',
                    'brands.name as brand',
                    'models.name as model',
                    'color.name as color',
                    'ram.name as ram',
                    'rom.name as rom',
                    'imei_details.serial_no as sn_no',
                    'imei_details.IMEI_no1',
                    'imei_details.IMEI_no2',
                    'imei_details.pcb_serial_no',
                    'old_software.name as old_software_name',
                    'new_software.name as new_software_name',
                    'branch.name as branch_name',
                    'appeal_master.id as appeal_id',
                    'appeal_master.status as appeal_status',
                    'stock_service_imei_history.imei_no1 as new_imei_no1',
                    'stock_service_imei_history.imei_no2 as new_imei_no2',
                    'pcb_history.pcb_serial_no as new_pcb_serial_no'
                )
                ->where("stock_service.id", $id)
                ->first();
        }
        else
        {
            return DB::table("stock_service")
            ->leftJoin('users', 'users.id', '=', 'stock_service.technician_id')
            ->leftJoin('doa', 'stock_service.doa_id', '=', 'doa.id')
            ->leftJoin('variant_details', 'variant_details.id', '=', 'doa.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('brands', 'brands.id', '=', 'models.brand_id')
            ->leftJoin('software_version as old_software', 'old_software.id', '=', 'stock_service.version_id')
            ->leftJoin('software_version as new_software', 'new_software.id', '=', 'stock_service.new_version_id')
            ->leftJoin('branch', 'branch.id', '=', 'stock_service.branch_id')
            ->leftJoin('appeal_master', 'appeal_master.stock_service_id', '=', 'stock_service.id')
            ->leftJoin('stock_service_imei_history', 'stock_service_imei_history.service_id', '=', 'stock_service.id')
            ->leftJoin('pcb_history', 'pcb_history.stock_service_id', '=', 'stock_service.id')
            ->leftJoin('imei_details', 'imei_details.id', '=', 'stock_service.imei_id')
            ->select(
                'stock_service.*',
                'users.first_name as technician',
                'variant_details.model_id',
                'brands.name as brand',
                'models.name as model',
                'color.name as color',
                'ram.name as ram',
                'rom.name as rom',
                'old_software.name as old_software_name',
                'new_software.name as new_software_name',
                'branch.name as branch_name',
                'appeal_master.id as appeal_id',
                'appeal_master.status as appeal_status',
                'stock_service_imei_history.imei_no1 as new_imei_no1',
                'stock_service_imei_history.imei_no2 as new_imei_no2',
                'pcb_history.pcb_serial_no as new_pcb_serial_no',
                'imei_details.serial_no as sn_no',
                'imei_details.IMEI_no1',
                'imei_details.IMEI_no2',
                'imei_details.pcb_serial_no'
            )
            ->where("stock_service.id", $id)
            ->first();
        }
    }
    public function GetRowCount($table, $where, $count_column)
    {
        return DB::table($table)->where($where)->count($count_column);
    }

    public function spareRestore($service_spareid)
    {
        $service_spare = $this->GetFirst('service_spare', array("id" => $service_spareid));
        $quantity = $service_spare->quantity;
        $branch_id = $service_spare->branch_id;
        $spare_id = $service_spare->spare_id;
        // $currency = $this->GetCurrencyRate($branch_id);
        // $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        // $price = $service_spare->price;
        // $local_price = $service_spare->local_price;
        $cost = $this->GetAverageCost($branch_id, $spare_id);
        $cost = ($cost ? $cost->local_price : 0);
        $stock = $this->getAvailableStock($branch_id, $spare_id);
        // $local_cost = ($quantity*$local_price+$stock*$cost)/($stock+$quantity);
        // $av_cost_usd = ($local_cost/$currency_rate);
        $total_qty = $stock + $quantity;
        $stock_det = $this->GetFirst('stock', array("branch_id" => $branch_id, "spare_id" => $spare_id));
        $data['qty'] = $total_qty;
        // $data['price']=$av_cost_usd;
        // $data['local_price']=$local_cost;
        return $update = $this->updatedata("stock", $data, $stock_det->id);
    }

    public function getNameByID($table, $field, $id)
    {
        return DB::table($table)
            ->select("id", "name")
            ->where($field, $id)
            ->pluck("name", "id")
            ->all();
    }

    public function getBranch($country_id, $branch_id)
    {
        return DB::table("branch")
            ->where('country_id', $country_id)
            ->where('id', '!=', $branch_id)
            ->where('status', '=', 1)
            ->select("id", "name")
            ->pluck("name", "id")
            ->all();
    }


    public function getModelSpecification()
    {
        $models = array();
        $query = DB::table("variant_details")
            ->leftjoin("models", "models.id", "=", "variant_details.model_id")
            ->leftjoin("brands", "brands.id", "=", "models.brand_id")
            ->leftjoin("color", "color.id", "=", "variant_details.color_id")
            ->leftjoin("ram", "ram.id", "=", "variant_details.ram_id")
            ->leftjoin("rom", "rom.id", "=", "variant_details.rom_id")
            ->select("brands.name as brand_name", "variant_details.id", "models.name as model_name", "color.name as color_name", "ram.name as ram_name", "rom.name as rom_name")
            ->orderBY('models.name')
            ->get();
        foreach ($query as $v) {
            if (!isset($models[$v->brand_name])) {
                $models[$v->brand_name] = array();
            }
            $name = $v->model_name;
            if ($v->color_name) $name .= "(" . $v->color_name;
            if ($v->ram_name) $name .= "," . $v->ram_name;
            if ($v->rom_name) $name .= "," . $v->rom_name;
            $name .= ")";
            $models[$v->brand_name][$v->id] = $name;
        }
        return $models;
    }
    public function checkDeviceType($request)
    {
        $name = $request['query'];
        $imei_data=DB::table("imei_details")
                    ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
                    ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
                    ->select('models.device_id')
                    ->where(function ($query) use ($name) {
                        $query
                            ->where('imei_details.IMEI_no1', '=', $name)
                            ->orWhere('imei_details.IMEI_no2', '=', $name)
                            ->orWhere('imei_details.serial_no', '=', $name)
                            ->orWhere('imei_details.pcb_serial_no', '=', $name);
                    })->get();
        return $imei_data[0]->device_id;
    }

    public function checkIMEIData($input)
    {
        $name = $input['query'];
        $product_type = $input['product_type'];

        $data = DB::table("imei_details")
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('brands', 'brands.id', '=', 'models.brand_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('service', 'imei_details.id', '=', 'service.imei_id')
            ->leftJoin('doa', 'doa.imei_id', '=', 'imei_details.id')
            ->select([
                DB::raw(
                    "DATE_FORMAT(imei_details.expiry_date, '%d-%m-%Y') as expiry_date"
                ),
                DB::raw(
                    "DATE_FORMAT(imei_details.activation_date, '%d-%m-%Y') as activation_date"
                ),
                DB::raw(
                    "DATE_FORMAT(imei_details.sales_date, '%d-%m-%Y') as sales_date"
                ),
                DB::raw(
                    "DATE_FORMAT(imei_details.date_of_manufacture, '%d-%m-%Y') as date_of_manufacture"
                ),
                "imei_details.id",
                "imei_details.IMEI_no1",
                "imei_details.IMEI_no2",
                "imei_details.serial_no",
                "imei_details.country_id",
                "imei_details.warranty_end_date",
                "imei_details.international_warranty",
                "models.name as model_name",
                "brands.name as brand_name",
                "brands.id as brand_id",
                "models.factory_name",
                "models.id as model_id",
                "models.country_id as model_country_id",
                "ram.name as ram_name",
                "rom.name as rom_name",
                "color.name as color",
                "variant_details.id as variant_id",
                "service.sales_request_id",
                "service.status as service_status",
                "imei_details.is_doa",
                "imei_details.pcb_serial_no",
                "doa.id as doa_id",
                "doa.status as doa_status"
            ])
            ->where(function ($query) use ($name) {
                $query
                    ->where('imei_details.IMEI_no1', '=', $name)
                    ->orWhere('imei_details.IMEI_no2', '=', $name)
                    ->orWhere('imei_details.serial_no', '=', $name)
                    ->orWhere('imei_details.pcb_serial_no', '=', $name);
            });
        if ($input['product_type']) {
            $data->where("models.device_id", "=", $product_type);
        }
        $query = $data->groupBy('imei_details.id')->get();
        $service_history = [];
        $doa_history = [];
        if ($query->count()) {
            $imei_id = $query[0]->id;
            $service_history = DB::table('service')
                ->leftJoin('branch', 'branch.id', '=', 'service.branch_id')
                ->select([DB::raw("DATE_FORMAT(service.created_at, '%d-%b-%Y') as created_at"), 'service.serial_no', 'branch.name', 'service.id'])
                ->where('service.imei_id', $imei_id)
                ->get();
            $doa_history = DB::table('doa')
                ->leftJoin('branch', 'branch.id', '=', 'doa.branch_id')
                ->select([DB::raw("DATE_FORMAT(doa.created_at, '%d-%b-%Y') as created_at"), 'doa.serial_no', 'branch.name', 'doa.id'])
                ->where(['doa.imei_id'=> $imei_id, 'doa.service_id' => 0])
                ->get();
        }

        $json['dataArr']['data'] =  ($query ? $query : []);
        $json['dataArr']['history'] = $service_history;
        $json['dataArr']['doa_history'] = $doa_history;
        return $query ? json_encode($json['dataArr']) : [];
    }

    public function getDOADetails($id)
    {
        $doa = DB::table('doa')->where('doa.id', $id)->first();
        // echo "<pre>";
        // print_r($doa);
        // echo "</pre>";
        // exit;
        if ($doa->imei_id != '') {
            $this->data['doa'] =  DB::table('doa')
                ->leftJoin('branch', 'branch.id', '=', 'doa.branch_id')
                ->leftJoin('imei_details', 'imei_details.id', '=', 'doa.imei_id')
                ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
                ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
                ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
                ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
                ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
                ->leftJoin('imei_details as replace_imei', 'replace_imei.id', '=', 'doa.replace_imei_id')
                ->leftJoin('service', 'service.id', '=', 'doa.service_id')
                ->select([
                    'doa.*',
                    'branch.name as branch_name',
                    'models.name as model_name',
                    'color.name as color',
                    'ram.name as ram',
                    'rom.name as rom',
                    'imei_details.IMEI_no1',
                    'imei_details.IMEI_no2',
                    'imei_details.serial_no as sn_no',
                    'imei_details.sales_date',
                    'imei_details.activation_date',
                    'replace_imei.IMEI_no1 as replace_imei1',
                    'replace_imei.IMEI_no2 as replace_imei2',
                    'service.created_at as service_date'
                ])
                ->where('doa.id', $id)
                ->first();
        } else {
            $this->data['doa'] =  DB::table('doa')
                ->leftJoin('branch', 'branch.id', '=', 'doa.branch_id')
                ->leftJoin('variant_details', 'variant_details.id', '=', 'doa.variant_id')
                ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
                ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
                ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
                ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
                ->leftJoin('service', 'service.id', '=', 'doa.service_id')
                ->select([
                    'doa.*',
                    'branch.name as branch_name',
                    'models.name as model_name',
                    'color.name as color',
                    'ram.name as ram',
                    'rom.name as rom',
                    'service.created_at as service_date'
                ])
                ->where('doa.id', $id)
                ->first();
        }
        if ($this->data['doa']->device_check) {
            $this->data['doa']->access = DB::table('accessories')
                ->whereIn('id', explode(',', $this->data['doa']->device_check))
                ->pluck('accessories.name')
                ->implode(',');
        } else {
            $this->data['doa']->access = "";
        }

        return $this->data['doa'];
    }

    public function checkDOAData($input)
    {

        $name = $input['query'];
        $data = DB::table("imei_details")
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('brands', 'brands.id', '=', 'models.brand_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.ram_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('doa', 'imei_details.id', '=', 'doa.imei_id')
            ->leftJoin('stock_service', 'doa.id', '=', 'stock_service.doa_id')
            ->select([
                DB::raw(
                    "DATE_FORMAT(imei_details.expiry_date, '%d-%m-%Y') as expiry_date"
                ),
                DB::raw(
                    "DATE_FORMAT(imei_details.activation_date, '%d-%m-%Y') as activation_date"
                ),
                DB::raw(
                    "DATE_FORMAT(imei_details.sales_date, '%d-%m-%Y') as sales_date"
                ),
                DB::raw(
                    "DATE_FORMAT(imei_details.date_of_manufacture, '%d-%m-%Y') as date_of_manufacture"
                ),
                "imei_details.id",
                "imei_details.IMEI_no1",
                "imei_details.IMEI_no2",
                "imei_details.serial_no",
                "imei_details.country_id",
                "models.name as model_name",
                "brands.name as brand_name",
                "brands.id as brand_id",
                "models.factory_name",
                "models.id as model_id",
                "ram.name as ram_name",
                "rom.name as rom_name",
                "color.name as color",
                "variant_details.id as variant_id",
                "doa.status",
                "doa.replace_imei_id",
                "doa.issue_doa",
                "doa.imei_id",
                "imei_details.is_doa",
                "imei_details.pcb_serial_no",
                "doa.branch_id",
                "doa.id as doa_id",
                "stock_service.status as stock_status",
                "stock_service.id as stock_service_id"
            ])
            ->where(function ($query) use ($name) {
                $query
                    ->where('imei_details.IMEI_no1', '=', $name)
                    ->orWhere('imei_details.IMEI_no2', '=', $name)
                    ->orWhere('imei_details.serial_no', '=', $name);
            });
        $query = $data->orderBy('doa.id', 'DESC')->get();

        return $query
            ? json_encode(array(
                'data' => $query
            )) : [];
    }


    public function getDoaTableData($from, $to, $model_id)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table("doa")->select([
            DB::raw('YEAR(doa.created_at) year, MONTH(doa.created_at) month'),
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'doa.serial_no',
            'doa.remarks',
            'doa.created_at',
            'doa.status',
            'doa.issue_doa',
            'doa.is_special_request',
            'doa.replace_imei_id',
            'imei_details.IMEI_no1',
            'imei_details.IMEI_no2',
            'replace_imei.IMEI_no1 as replace_imei1',
            'replace_imei.IMEI_no2 as replace_imei2'
        ])
            ->leftJoin('imei_details', 'imei_details.id', '=', 'doa.imei_id')
            ->leftJoin('imei_details as replace_imei', 'replace_imei.id', '=', 'doa.replace_imei_id')
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->where('doa.status', 1);

        if (!empty($from) && !empty($to)) {
            $data->whereRaw(
                "(doa.created_at >= ? AND doa.created_at <= ?)",
                [
                    dateformat($from) . " 00:00:00",
                    dateformat($to) . " 23:59:59"
                ]
            );
        }
        if (!empty($model_id)) {
            $data->where('variant_details.model_id', "=", $model_id);
        }
        return $data;
    }

    public function getActivationTableData($input)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table("imei_details")->select([
            DB::raw("count(imei_details.id) as total_dispatch_qty"),
            DB::raw("count(imei_details.activation_date) as total_activated_qty"),
            DB::raw('(count(imei_details.id) - count(imei_details.activation_date)) balance'),
            DB::raw('YEAR(imei_details.activation_date) year, MONTH(imei_details.activation_date) month'),
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'models.name as model',
            'color.name as color',
            'ram.name as ram',
            'rom.name as rom',
            'country.name as country_name',
            'variant_details.id'
        ])
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('country', 'country.id', '=', 'imei_details.country_id');

        if (!empty($input['from']) && !empty($input['to'])) {
            $data->whereRaw(
                "(imei_details.activation_date >= ? AND imei_details.activation_date <= ?)",
                [
                    dateformat($input['from']),
                    dateformat($input['to'])
                ]
            );
        }
        if (!empty($input['country_id'])) {
            $data->where('imei_details.country_id', '=', $input['country_id']);
        } else {
            $data->where('imei_details.country_id', '>', 0);
        }

        if (!empty($input['model_id'])) {
            $data->where('variant_details.model_id', '=', $input['model_id']);
        }

        return $data->groupBy('imei_details.variant_id', 'imei_details.country_id');
    }

    public function getActivationModelTableData($input)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table("imei_details")->select([
            DB::raw("count(imei_details.activation_date) as total_activated_qty"),
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'models.name as model',
            'color.name as color',
            'ram.name as ram',
            'rom.name as rom',
            'variant_details.id'
        ])
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id');

        if (!empty($input['from']) && !empty($input['to'])) {
            $data->whereRaw(
                "(imei_details.activation_date >= ? AND imei_details.activation_date <= ?)",
                [
                    dateformat($input['from']),
                    dateformat($input['to'])
                ]
            );
        }
        if (!empty($input['country_id'])) {
            $data->where('imei_details.country_id', '=', $input['country_id']);
        } else {
            $data->where('imei_details.country_id', '>', 0);
        }

        if (!empty($input['model_id'])) {
            $data->where('variant_details.model_id', '=', $input['model_id']);
        }

        return $data->orderBy('imei_details.id', 'ASC')->groupBy('imei_details.variant_id');
    }

    public function getMonthWiseActivationTableData($input)
    {
        //\DB::enableQueryLog();
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table("imei_details")->select([
            DB::raw("count(imei_details.activation_date) as total_activated_qty"),
            DB::raw("DATE_FORMAT(imei_details.activation_date,'%M-%Y') as month"),
            'models.name as model',
            'color.name as color',
            'ram.name as ram',
            'rom.name as rom',
            'variant_details.id'
        ])
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id');

        if (!empty($input['from']) && !empty($input['to'])) {
            $data->whereRaw(
                "(imei_details.activation_date >= ? AND imei_details.activation_date <= ?)",
                [
                    dateformat($input['from']),
                    dateformat($input['to'])
                ]
            );
        }
        if (!empty($input['country_id'])) {
            $data->where('imei_details.country_id', '=', $input['country_id']);
        } else {
            $data->where('imei_details.country_id', '>', 0);
        }

        if (!empty($input['model_id'])) {
            $data->where('variant_details.model_id', '=', $input['model_id']);
        }
        $data->whereNotNull('imei_details.activation_date');
        // $data->groupBy(['imei_details.variant_id','DATE_FORMAT(imei_details.activation_date,"%M-%Y")'])->get();
        $head_list = array("model", "color", "ram", "rom");
        $range = array();
        $list =  $data->groupBy(DB::raw("imei_details.variant_id,DATE_FORMAT(imei_details.activation_date,'%M-%Y')"))->orderBy('imei_details.activation_date', 'ASC')->get()->toArray();
        foreach ($list as $item) {
            if (!in_array($item->month, $head_list)) {
                $head_list[] = $item->month;
                $range[] = $item->month;
            }
        }
        $new_list = array();
        $new_list[0] =  $head_list;
        foreach ($list as $item) {
            if (!isset($new_list[$item->id])) {
                $new_list[$item->id] = array();

                $new_list[$item->id]['model'] = $item->model;
                $new_list[$item->id]['color'] = $item->color;
                $new_list[$item->id]['ram'] = $item->ram;
                $new_list[$item->id]['rom'] = $item->rom;
                foreach ($range as $m) {
                    $new_list[$item->id][$m] = '0';
                }
                $new_list[$item->id][$item->month] = $item->total_activated_qty;
            } else {
                $new_list[$item->id][$item->month] = $item->total_activated_qty;
            }
        }
        return $new_list;
        //dd(\DB::getQueryLog());
    }

    public function getStockTableData($input)
    {
        $spare = $this->GetAllByStatus('spare');
        $row_array = $stockList_array = [];
        $num = 1;
        foreach ($spare as $k => $v) {
            $row_array = array();
            $row_array[] = $num;
            $row_array[] =  $v->focus_code;
            $row_array[] =  $v->part_code;
            $row_array[] =  $v->description;
            if (isset($input['branch_id'])) {
                $row_branches  = [$input['branch_id'] => 0];
            } elseif (isset($input['country_id'])) {
                $branch_ids = $this->getBranchIds($input['country_id']);
                $row_branches  = array_flip($branch_ids);
            } else {
                $countries = $this->getCountries($input['branch_ids']);
            }

            $total = 0;
            if (isset($input['branch_id']) || isset($input['country_id'])) {
                foreach ($row_branches as $key => $value) {
                    $fetch_stock = DB::table("stock")->select('qty', 'price')->where('spare_id', $v->id)->where('branch_id', $key)->first();
                    $qty = $fetch_stock ? $fetch_stock->qty : 0;
                    $row_array[] =  ($fetch_stock ? $fetch_stock->qty : 0);
                    $row_array[] =  ($fetch_stock ? number_format($fetch_stock->price, 2) : "0.00");
                    $total += $qty;
                }
            } else {
                foreach ($countries as $country_id => $val) {
                    $branch_id = DB::table("branch")->where('country_id', $country_id)->where('status', 1)->pluck('id')->toArray();
                    $fetch_stock = DB::table("stock")->select(DB::raw("SUM(qty) as qty"), DB::raw("SUM(price) as price"))->where('spare_id', $v->id)->whereIn('branch_id', $branch_id)->first();
                    $qty = $fetch_stock ? $fetch_stock->qty : 0;
                    $row_array[] =  ($fetch_stock ? $fetch_stock->qty : 0);
                    $row_array[] =  ($fetch_stock ? number_format($fetch_stock->price, 2) : "0.00");
                    $total += $qty;
                }
            }
            $row_array[] =  $total;
            array_push($stockList_array, $row_array);
            $num++;
        }


        return $stockList_array;
    }

    public function getConsumptionTableData($input)
    {
        $join = $spare_join = $purchase_join = $sale_join = '';

        if (!empty($input['model_id'])) {
            $join .= " and FIND_IN_SET('" . $input['model_id'] . "',s.model_id) > 0";
        }

        if (!empty($input['country_id']) && empty($input['branch_id'])) {
            $branch_ids = $this->getBranchIds($input['country_id']);
            //$service_spare_ids = "'" . implode(', ', $branch_ids). "'";
            $service_spare_ids = "'" . implode("','", $branch_ids) . "'";
            $spare_join = 'WHERE service_spare.branch_id IN (' . $service_spare_ids . ')';
            $purchase_join = 'WHERE pm.branch_id IN (' . $service_spare_ids . ')';
            $sale_join = 'WHERE sm.branch_id IN (' . $service_spare_ids . ')';
        }
        if (!empty($input['branch_id'])) {
            $spare_join = 'WHERE service_spare.branch_id =' . $input['branch_id'];
            $purchase_join = 'WHERE pm.branch_id =' . $input['branch_id'];
            $sale_join = 'WHERE sm.branch_id =' . $input['branch_id'];
        } else {
            if (empty($input['country_id'])) {
                $branch_ids = "'" . implode("','", explode(",", $input['branch_ids'])) . "'";
                $spare_join .= " WHERE service_spare.branch_id IN ($branch_ids)";
                $purchase_join .= " WHERE pm.branch_id IN ($branch_ids)";
                $sale_join .= " WHERE sm.branch_id IN ($branch_ids)";
            }
        }

        $data = DB::select(DB::raw("SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS number, GROUP_CONCAT(m.name) as model_name, s.spare_name, IFNULL(p.purchase, 0) AS purchase_count, IFNULL(ss.warranty, 0) AS warranty_count, IFNULL(ss.nonwarranty, 0) AS nonwarranty_count, IFNULL(sa.sale, 0) AS sale_count FROM spare as s LEFT JOIN models m on FIND_IN_SET(m.id,s.model_id) > 0 LEFT JOIN ( SELECT spare_id, sum(qty) AS purchase FROM purchase_items LEFT JOIN purchase_master pm
        ON pm.id = purchase_items.parent_id $purchase_join GROUP BY spare_id ) p ON (s.id = p.spare_id) LEFT JOIN ( SELECT spare_id, SUM( case when is_warranty = 1 then quantity else 0 end ) as warranty, SUM( case when is_warranty = 0 then quantity else 0 end ) as nonwarranty,branch_id  FROM service_spare $spare_join GROUP BY spare_id ) ss ON (s.id = ss.spare_id) LEFT JOIN ( SELECT spare_id, sum(qty) AS sale FROM spare_sale_items  LEFT JOIN spare_sale_master sm
        ON sm.id = spare_sale_items.parent_id $sale_join GROUP BY spare_id ) sa ON (s.id = sa.spare_id) where p.purchase > 0 $join  GROUP by s.id order by number"));
        return $data;
    }

    public function getSpareCliamTableData($input)
    {
        if ($input['type_id'] == 1) {
            DB::statement(DB::raw('set @rownum=0'));
            $data = DB::table("service_spare")->select([
                DB::raw('YEAR(stock_service.is_approved_at) year, MONTH(stock_service.is_approved_at) month'),
                DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                'service_spare.quantity',
                'service_spare.local_price as price',
                'stock_service.serial_no',
                'branch.name as branch_name',
                'spare.spare_name',
                'country.name as country_name',
                'currency.name as currency',
                'country.currency_id',
                'companies.name as company_name'
            ])
                ->leftJoin('stock_service', 'stock_service.id', '=', 'service_spare.stock_service_id')
                ->leftJoin('spare', 'spare.id', '=', 'service_spare.spare_id')
                ->leftJoin('branch', 'branch.id', '=', 'service_spare.branch_id')
                ->leftJoin('country', 'country.id', '=', 'branch.country_id')
                ->leftJoin('companies', 'companies.id', '=', 'branch.company_id')
                ->leftJoin('currency', 'currency.id', '=', 'country.currency_id')
                ->where('service_spare.is_warranty', "=", 1)
                ->WhereNull('service_spare.service_id')
                ->where('stock_service.is_approved', "=", 1);

            if (!empty($input['from']) && !empty($input['to'])) {
                $data->whereRaw(
                    "(stock_service.is_approved_at >= ? AND stock_service.is_approved_at <= ?)",
                    [
                        dateformat($input['from']) . " 00:00:00",
                        dateformat($input['to']) . " 23:59:59"
                    ]
                );
            }
            if (!empty($input['country_id']) && empty($input['branch_id'])) {
                $branch_ids = $this->getBranchIds($input['country_id']);
                $data->whereIn('service_spare.branch_id', $branch_ids);
            }
            if (!empty($input['branch_id'])) {
                $data->where('service_spare.branch_id', '=', $input['branch_id']);
            } else {
                $data->whereIn('service_spare.branch_id', explode(',', $input['branch_ids']));
            }

            if (!empty($input['company_id'])) {
                $data->where('branch.company_id', '=', $input['company_id']);
            }

            if (!empty($input['model_id'])) {
                $data->whereRaw("find_in_set('" . $input['model_id'] . "',spare.model_id)");
            }
            return $data->orderBy('service_spare.id', 'DESC');
        } else {
            DB::statement(DB::raw('set @rownum=0'));
            $data = DB::table("service_spare")->select([
                DB::raw('YEAR(service.is_approved_at) year, MONTH(service.is_approved_at) month'),
                DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                'service_spare.quantity',
                'service_spare.local_price as price',
                'service.serial_no',
                'branch.name as branch_name',
                'spare.spare_name',
                'country.name as country_name',
                'currency.name as currency',
                'country.currency_id',
                'companies.name as company_name'
            ])
                ->leftJoin('service', 'service.id', '=', 'service_spare.service_id')
                ->leftJoin('spare', 'spare.id', '=', 'service_spare.spare_id')
                ->leftJoin('branch', 'branch.id', '=', 'service_spare.branch_id')
                ->leftJoin('country', 'country.id', '=', 'branch.country_id')
                ->leftJoin('companies', 'companies.id', '=', 'branch.company_id')
                ->leftJoin('currency', 'currency.id', '=', 'country.currency_id')
                ->where('service_spare.is_warranty', "=", 1)
                ->WhereNull('service_spare.stock_service_id')
                ->where('service.is_approved', "=", 1);

            if (!empty($input['from']) && !empty($input['to'])) {
                $data->whereRaw(
                    "(service.is_approved_at >= ? AND service.is_approved_at <= ?)",
                    [
                        dateformat($input['from']) . " 00:00:00",
                        dateformat($input['to']) . " 23:59:59"
                    ]
                );
            }
            if (!empty($input['country_id']) && empty($input['branch_id'])) {
                $branch_ids = $this->getBranchIds($input['country_id']);
                $data->whereIn('service_spare.branch_id', $branch_ids);
            }
            if (!empty($input['branch_id'])) {
                $data->where('service_spare.branch_id', '=', $input['branch_id']);
            } else {
                $data->whereIn('service_spare.branch_id', explode(',', $input['branch_ids']));
            }

            if (!empty($input['company_id'])) {
                $data->where('branch.company_id', '=', $input['company_id']);
            }

            if (!empty($input['model_id'])) {
                $data->whereRaw("find_in_set('" . $input['model_id'] . "',spare.model_id)");
            }
            return $data->orderBy('service_spare.id', 'DESC');
        }
    }


    public function getBranchIds($country_id)
    {
        $branch = $this->WhereByData("branch", "country_id", $country_id);
        foreach ($branch as $val) {
            $branch_arr[] = $val->id;
        }
        $branch_ids = [];
        if ($branch) {
            $branch_ids = ($branch ? explode(",", implode(",", $branch_arr)) : []);
        }
        return $branch_ids;
    }

    public function getActivationCount($model_id, $type, $country_id = NULL)
    {
        if ($type == 2) {
            $field = "imei_details.activation_date as activation_total_count";
        } else {
            $field = DB::raw("COUNT(imei_details.id) as activation_total_count");
        }

        $row = DB::table("imei_details")
            ->select([$field])
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->where('variant_details.model_id', '=', $model_id)
            ->where('imei_details.country_id', '>', 0);
        if ($country_id) {
            $row->where('imei_details.country_id', '=', $country_id);
        }
        if ($type == 0) {
            $row->whereNotNull('imei_details.expiry_date');
        }
        if ($type == 1) {
            $row->whereNull('imei_details.expiry_date');
        }
        if ($type == 2) {
            $row->whereNotNull('imei_details.activation_date');
        }
        $data = $row->first();
        return ($data ? $data->activation_total_count : 0);
    }

    public function getUsedSpare($spare_id, $is_warranty, $country_id = NULL)
    {
        $data = DB::table("service_spare")
            ->select([DB::raw("SUM(service_spare.quantity) as qty")])
            ->where('spare_id', '=', $spare_id)
            ->where('is_warranty', '=', $is_warranty);
        if ($country_id) {
            $branch_ids = $this->getBranchIds($country_id);
            $data->whereIn('branch_id', $branch_ids);
        }
        $qry = $data->first();
        return ($qry ? $qry->qty : 0);
    }

    public function getTotalStock($spare_id, $country_id = NULL)
    {
        $data = DB::table("stock")
            ->select([DB::raw("SUM(stock.qty) as total_qty")])
            ->where('spare_id', '=', $spare_id);
        if ($country_id) {
            $branch_ids = $this->getBranchIds($country_id);
            $data->whereIn('branch_id', $branch_ids);
        }
        $qry = $data->first();
        return ($qry ? $qry->total_qty : 0);
    }

    public function getForecastTableData($input)
    {
        $join = $joinc = '';
        if (!empty($input['model_id'])) {
            $join .= " WHERE FIND_IN_SET('" . $input['model_id'] . "',m.id) > 0";
        }
        if (!empty($input['country_id'])) {
            $joinc .= " WHERE imd.country_id =" . $input['country_id'];
        }

        $spare = DB::select(DB::raw("SELECT m.NAME AS model_name, m.id AS model_id, s.focus_code, s.part_code, s.spare_name AS description, IFNULL(i.activation_total, 0) AS activation_total_count, IFNULL(i.activation_balance, 0) AS activation_balance_count, IFNULL(i.activation_date, 0) AS activation_date, IFNULL(ss.warranty, 0) AS warranty_count, IFNULL(ss.nonwarranty, 0) AS nonwarranty_count, IFNULL(st.total_stock, 0) AS current_stock FROM spare AS s LEFT JOIN models m ON Find_in_set(m.id, s.model_id) > 0 LEFT JOIN ( SELECT CASE WHEN imd.activation_date IS NOT NULL THEN imd.activation_date ELSE '' END AS activation_date, SUM( case when imd.activation_date IS NOT NULL then 1 else 0 end ) as activation_total ,SUM( case when imd.expiry_date IS NULL then 1 else 0 end ) as activation_balance , model_id FROM imei LEFT JOIN imei_details imd ON imd.group_id = imei.id $joinc GROUP BY model_id ) i ON (m.id = i.model_id) LEFT JOIN ( SELECT spare_id, SUM( case when is_warranty = 1 then quantity else 0 end ) as warranty, SUM( case when is_warranty = 0 then quantity else 0 end ) as nonwarranty, branch_id FROM service_spare GROUP BY spare_id ) ss ON (s.id = ss.spare_id)  LEFT JOIN ( SELECT spare_id, SUM(qty) as total_stock  FROM stock GROUP BY spare_id ) st ON (s.id = st.spare_id) $join ORDER BY m.NAME;"));
        $row_array = $stockList_array = array();
        foreach ($spare as $k => $v) {
            $currentDate = date('Y-m-d');
            $months = $current_stock_diff = 0;
            if ($v->activation_date) {
                $diff = abs(strtotime($currentDate) - strtotime($v->activation_date));
                $years = floor($diff / (365 * 60 * 60 * 24));
                $months = intval(floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24)));
            }
            $average_usage_inwarranty = $average_usage_outwarranty = 0;
            if (!empty($v->warranty_count) && $months) {
                $average_usage_inwarranty = ($v->warranty_count / $months);
            }
            if (!empty($v->nonwarranty_count) && $months) {
                $average_usage_outwarranty = ($v->nonwarranty_count / $months);
            }
            if (!empty($months) && $v->activation_total_count > 0) {
                $difference = ($months / $v->activation_total_count);
                $diff_stock = ($v->activation_balance_count * $difference);
                $current_stock_diff = ($v->current_stock - $diff_stock);
            }
            $row_array['focus_code']  =  $v->focus_code;
            $row_array['part_code']   =  $v->part_code;
            $row_array['description'] =  $v->description;
            $row_array['model'] = ($v->model_name ? $v->model_name : "Other");
            $row_array['activation_total']   = $v->activation_total_count;
            $row_array['activation_balance']   = $v->activation_balance_count;
            $row_array['activation_date']   = $v->activation_date;
            $row_array['activation_month']   =  ($months ?? "");
            $row_array['in_warranty_spare']  = $v->warranty_count;
            $row_array['out_warranty_spare']  =  $v->nonwarranty_count;
            $row_array['total_spare_used']  = ($v->warranty_count + $v->nonwarranty_count);
            $row_array['average_usage_inwarranty']   = $average_usage_inwarranty;
            $row_array['average_usage_outwarranty']   = $average_usage_outwarranty;
            $row_array['total_average_usage'] = ($average_usage_inwarranty + $average_usage_outwarranty);
            $row_array['current_stock'] = ($v->current_stock ?? 0);
            $row_array['difference'] = ($current_stock_diff ?? 0);
            array_push($stockList_array, $row_array);
        }
        //dd($stockList_array);
        return $stockList_array;
    }

    public function getServiceComplaints($id)
    {
        return DB::table('service_complaint')
            ->leftJoin('service', 'service.id', '=', 'service_complaint.service_id')
            ->leftJoin('complaints', 'complaints.id', '=', 'service_complaint.complaint_id')
            ->where('service_complaint.service_id', $id)
            ->pluck('complaints.description')->implode(',');
    }

    public function getServiceSpare($id)
    {
        return DB::table('service_spare')
            ->leftJoin('service', 'service.id', '=', 'service_spare.service_id')
            ->leftJoin('spare', 'spare.id', '=', 'service_spare.spare_id')
            ->where('service_spare.service_id', $id)
            ->pluck('spare.spare_name')->implode(',');
    }

    public function getServiceComplaintRate($id)
    {
        return DB::table('service_complaint')
            ->leftJoin('service', 'service.id', '=', 'service_complaint.service_id')
            ->leftJoin('complaints', 'complaints.id', '=', 'service_complaint.complaint_id')
            ->leftJoin('levels', 'levels.id', '=', 'complaints.level_id')
            ->where('service_complaint.service_id', $id)
            ->where('service_complaint.status', '=', 1)
            ->max('service_complaint.price',);
    }

    public function getHighestLevel($id)
    {
        $data = DB::table('service_complaint')
            ->leftJoin('service', 'service.id', '=', 'service_complaint.service_id')
            ->leftJoin('complaints', 'complaints.id', '=', 'service_complaint.complaint_id')
            ->leftJoin('levels', 'levels.id', '=', 'complaints.level_id')
            ->select(['levels.name as level_name', 'levels.id as level_id'])
            ->where('service_complaint.service_id', $id)
            ->where('service_complaint.status', '=', 1)
            ->orderBy('service_complaint.price', 'desc')
            ->first();
        return ($data ? $data : NULL);
    }

    public function getServiceSpareRate($id, $type)
    {
        $data = DB::table('service_spare')->select(DB::raw('SUM(cus_local_price*quantity) as price'))
            ->where('service_id', $id)
            ->where('is_warranty', $type)->first();
        return ($data ? $data->price : 0);
    }

    public function getServiceClaimTableData($input)
    {
        DB::statement(DB::raw('set @rownum=0'));
        //\DB::enableQueryLog();
        $query = DB::table("service")
            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), DB::raw("GROUP_CONCAT(complaints.description) as complaint_name"), "service_bill.created_at as bill_date", "service.id as service_id", "service.serial_no", "service.active_date", "service.sale_date", "service.created_at", "imei_country.name as country_name", "order_status.name as status", "models.name as model_name", "color.name as color", "ram.name as ram", "rom.name as rom", "imei_details.IMEI_no1", "imei_details.IMEI_no2", "imei_details.serial_no as sno", "imei_details.date_of_manufacture", "imei_details.pcb_serial_no", "product_type.name as device_name", "service.submission_cat_id", "service_customers.customer_name", "service_customers.customer_phone", "service_customers.address", "users.first_name as technician_name", "service.remarks", "branch.code as branch_code", "branch.name as branch_name", "brands.name as brand_name", "software_version.name as old_software", "service.level_id", "service.level_price", "companies.name as company_name", "company_currency.name as currency", "company_currency.id as currency_id", "branch_currency.name as branch_currency_name", "branch_currency.id as branch_currency_id","service.is_approved_at as approved_date"])
            ->leftJoin('imei_details', 'imei_details.id', '=', 'service.imei_id')
            ->leftJoin('country as imei_country', 'imei_country.id', '=', 'imei_details.country_id')
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('product_type', 'product_type.id', '=', 'service.product_type')
            ->leftJoin('service_customers', 'service_customers.customer_id', '=', 'service.customer_id')
            ->leftJoin('service_bill', 'service.id', '=', 'service_bill.service_id')
            ->leftJoin('users', 'users.id', '=', 'service.technician_id')
            ->leftJoin('branch', 'branch.id', '=', 'service_bill.branch_id')
            ->leftJoin('companies', 'companies.id', '=', 'branch.company_id')
            ->leftJoin('currency as company_currency', 'company_currency.id', '=', 'companies.currency_id')
            ->leftJoin('country as branch_country', 'branch_country.id', '=', 'branch.country_id')
            ->leftJoin('currency as branch_currency', 'branch_currency.id', '=', 'branch_country.currency_id')
            ->leftJoin('order_status', 'order_status.id', '=', 'service.status')
            ->leftJoin('brands', 'brands.id', '=', 'models.brand_id')
            ->leftJoin('software_version', 'software_version.id', '=', 'service.version_id')
            ->leftjoin("complaints", DB::raw("FIND_IN_SET(complaints.id,service.complaint)"), ">", DB::raw("'0'"))
            ->where('service.is_approved', '=', 1)
            ->where('service.is_warranty', '=', 1);

        if (!empty($input['from']) && !empty($input['to'])) {
            $query->whereRaw(
                "(service.is_approved_at >= ? AND service.is_approved_at <= ?)",
                [
                    dateformat($input['from']) . " 00:00:00",
                    dateformat($input['to']) . " 23:59:59"
                ]
            );
        }
        if (!empty($input['branch_id'])) {
            $query->where('service_bill.branch_id', '=', $input['branch_id']);
        } else {
            $query->whereIn('service_bill.branch_id', explode(',', $input['branch_ids']));
        }

        if (!empty($input['country_id']) && empty($input['branch_id'])) {
            $branch_ids = $this->getBranchIds($input['country_id']);
            $query->whereIn('service_bill.branch_id', $branch_ids);
        }
        if (!empty($input['model_id'])) {
            $query->where('variant_details.model_id', '=', $input['model_id']);
        }
        if (!empty($input['model_id'])) {
            $query->where('variant_details.model_id', '=', $input['model_id']);
        }
        if (!empty($input['company_id'])) {
            $query->where('companies.id', '=', $input['company_id']);
        }

        $data = $query->orderBy('service.id', 'ASC')
            ->groupBy('service.id')
            ->get();
        //dd(\DB::getQueryLog());
        $row_array = $claimList_array = array();
        $i = 1;
        foreach ($data as $k => $v) {
            $charge = $this->WhereByData("service_charge", "service_id", $v->service_id);
            $service_charge = 0;
            if (is_array($charge)) {
                $service_charge = $charge[0]->local_price;
            }
            $customer_spare_charge = $this->getServiceSpareRate($v->service_id, 0);
            if (!empty($input['currency_id']) && $v->currency_id != $input['currency_id']) {
                $currency_convert =  $this->GetByID('currency', $v->currency_id);
                $currency_convert_d =  $this->GetByID('currency', $input['currency_id']);
                $curr  = $this->FieldByID('currency', 'name', $input['currency_id']);
                $amt = number_format($v->level_price / $currency_convert->rate, 2);
                if ($curr == "USD") $claim_amount  = $curr . " " . $amt;
                else  $claim_amount  = $curr . " " . number_format($amt * $currency_convert_d->rate, 2);
            } else {
                $claim_amount = $v->currency . " " . number_format(($v->level_price), 2);
            }
            if (!empty($input['currency_id']) && $v->branch_currency_id != $input['currency_id']) {
                $currency_convert =  $this->GetByID('currency', $v->branch_currency_id);
                $currency_convert_d =  $this->GetByID('currency', $input['currency_id']);
                $curr  = $this->FieldByID('currency', 'name', $input['currency_id']);
                $amt = number_format(($service_charge + $customer_spare_charge) / $currency_convert->rate, 2);
                if ($curr == "USD") $customer_amount  = $curr . " " . $amt;
                else  $customer_amount  = $curr . " " . number_format($amt * $currency_convert_d->rate, 2);
            } else {
                $customer_amount = $v->branch_currency_name . " " . number_format(($service_charge + $customer_spare_charge), 2);
            }


            //$center_spare_charge = $this->getServiceSpareRate($v->service_id,1);

            $level = $this->WhereByData("levels", "id", $v->level_id);
            $row_array['service_id']      =  $v->service_id;
            $row_array['rownum']         =  $i++;
            $row_array['serial_no']      =  $v->serial_no;
            $row_array['active_date']    =  ($v->active_date ? date("d-m-Y", strtotime($v->active_date)) : "");
            $row_array['sale_date']      =  ($v->sale_date ? date("d-m-Y", strtotime($v->sale_date)) : "");
            $row_array['created_at']     =  ($v->created_at ? date('d-m-Y', strtotime($v->created_at)) . " " . date("h:i:sa", strtotime($v->created_at)) : "");
            $row_array['country_name']   =  $v->country_name;
            $row_array['branch_code']    =  $v->branch_code;
            $row_array['branch_name']    =  $v->branch_name;
            $row_array['status']         =  $v->status;
            $row_array['brand_name']     =  $v->brand_name;
            $row_array['device_name']    =  $v->device_name;
            $row_array['model_name']     =  $v->model_name;
            $row_array['color']          =  $v->color;
            $row_array['ram']            =  $v->ram;
            $row_array['rom']            =  $v->rom;
            $row_array['IMEI_no1']       =  $v->IMEI_no1;
            $row_array['IMEI_no2']       =  $v->IMEI_no2;
            $row_array['sno']     =  $v->sno;
            $row_array['pcb_serial_no']     =  $v->pcb_serial_no;
            $row_array['customer_name']     =  $v->customer_name;
            $row_array['customer_phone']     =  $v->customer_phone;
            $row_array['address']     =  $v->address;
            $row_array['date_of_manufacture']     = ($v->date_of_manufacture ? date("d-m-Y", strtotime($v->date_of_manufacture)) : "");
            $row_array['bill_date']     =  ($v->bill_date ? date("d-m-Y", strtotime($v->bill_date)) : "");
            $row_array['bill_time']     =  ($v->bill_date ? date("h:i:sa", strtotime($v->bill_date)) : "");
            $row_array['old_software']        =  $v->old_software;
            $row_array['technician_name']     =  $v->technician_name;
            $row_array['complaint_name']      =  $v->complaint_name;
            $row_array['service_complaints']  = $this->getServiceComplaints($v->service_id);
            $row_array['remarks']     =  $v->remarks;
            $row_array['level']     = ($level ? $level[0]->name : '');
            $row_array['company_name']     =  $v->company_name;
            $row_array['claim_amount']     =  $claim_amount;
            $row_array['customer_amount']  = $customer_amount;
            $row_array['spare_name']     =  $this->getServiceSpare($v->service_id);
            $row_array['currency']  =  $v->currency;
            $row_array['approved_date'] = ($v->approved_date ? date('d-m-Y', strtotime($v->approved_date)) . " " . date("h:i:sa", strtotime($v->approved_date)) : "");
            array_push($claimList_array, $row_array);
        }
        //dd($claimList_array);
        return $claimList_array;
    }

    public function getServiceComplaintTableData($input)
    {

        DB::statement(DB::raw('set @rownum=0'));
        $query = DB::table("service")
            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), DB::raw("GROUP_CONCAT(complaints.description) as complaint_name"), "service_bill.created_at as bill_date", "service.id as service_id", "service.serial_no", "service.active_date", "service.sale_date", "service.created_at", "country.name as country_name", "order_status.name as status", "models.name as model_name", "color.name as color", "ram.name as ram", "rom.name as rom", "imei_details.IMEI_no1", "imei_details.IMEI_no2", "imei_details.serial_no as sno", "imei_details.date_of_manufacture", "imei_details.pcb_serial_no", "product_type.name as device_name", "service.submission_cat_id", "service_customers.customer_name", "service_customers.customer_phone", "service_customers.address", "users.first_name as technician_name", "service.remarks", "brands.name as brand_name", "software_version.name as old_software", "service.level_id", "branch.code as branch_code", "branch.name as branch_name"])
            ->leftJoin('imei_details', 'imei_details.id', '=', 'service.imei_id')
            ->leftJoin('country', 'country.id', '=', 'imei_details.country_id')
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('product_type', 'product_type.id', '=', 'service.product_type')
            ->leftJoin('service_customers', 'service_customers.customer_id', '=', 'service.customer_id')
            ->leftJoin('service_bill', 'service.id', '=', 'service_bill.service_id')
            ->leftJoin('users', 'users.id', '=', 'service.technician_id')
            ->leftJoin('branch', 'branch.id', '=', 'service.branch_id')
            //->leftJoin('service_status', 'service.id', '=', 'service_status.service_id')
            ->leftJoin('order_status', 'order_status.id', '=', 'service.status')
            ->leftJoin('brands', 'brands.id', '=', 'models.brand_id')
            ->leftJoin('software_version', 'software_version.id', '=', 'service.version_id')
            ->leftjoin("complaints", DB::raw("FIND_IN_SET(complaints.id,service.complaint)"), ">", DB::raw("'0'"))
            ->where("service.status", "!=", 6);
        if (!empty($input['from']) && !empty($input['to'])) {
            $query->whereRaw(
                "(service.is_approved_at >= ? AND service.is_approved_at <= ?)",
                [
                    dateformat($input['from']) . " 00:00:00",
                    dateformat($input['to']) . " 23:59:59"
                ]
            );
        }

        if (!empty($input['branch_id'])) {
            $query->where('service.branch_id', '=', $input['branch_id']);
        } else {
            $query->whereIn('service.branch_id', explode(',', $input['branch_ids']));
        }

        if (!empty($input['country_id']) && empty($input['branch_id'])) {
            $branch_ids = $this->getBranchIds($input['country_id']);
            $query->whereIn('service.branch_id', $branch_ids);
        }

        if (!empty($input['model_id'])) {
            $query->where('variant_details.model_id', '=', $input['model_id']);
        }

        $data = $query->orderBy('service.id', 'ASC')->groupBy('service.id')->get();
        $row_array = $claimList_array = array();
        $i = 1;
        foreach ($data as $k => $v) {

            $charge = $this->WhereByData("service_charge", "service_id", $v->service_id);
            $service_charge = 0;
            if (is_array($charge)) {
                $service_charge = $charge[0]->price;
            }
            $center_spare_charge = $this->getServiceSpareRate($v->service_id, 1);
            $customer_spare_charge = $this->getServiceSpareRate($v->service_id, 0);
            $service_status = $this->GetFirst('service_status', array("service_id" => $v->service_id, "status" => 4));
            $level = $this->WhereByData("levels", "id", $v->level_id);
            $row_array['service_id']      =  $v->service_id;
            $row_array['rownum']         =  $i++;
            $row_array['serial_no']      =  $v->serial_no;
            $row_array['active_date']    =  ($v->active_date ? date("d-m-Y", strtotime($v->active_date)) : "");
            $row_array['sale_date']      =  ($v->sale_date ? date("d-m-Y", strtotime($v->sale_date)) : "");
            $row_array['created_at']     =  ($v->created_at ? date('d-m-Y', strtotime($v->created_at)) . " " . date("h:i:sa", strtotime($v->created_at)) : "");
            $row_array['country_name']   =  $v->country_name;
            $row_array['branch_code']    =  $v->branch_code;
            $row_array['branch_name']    =  $v->branch_name;
            $row_array['status']         =  $v->status;
            $row_array['brand_name']     =  $v->brand_name;
            $row_array['device_name']    =  $v->device_name;
            $row_array['model_name']     =  $v->model_name;
            $row_array['color']          =  $v->color;
            $row_array['ram']            =  $v->ram;
            $row_array['rom']            =  $v->rom;
            $row_array['IMEI_no1']       =  $v->IMEI_no1;
            $row_array['IMEI_no2']       =  $v->IMEI_no2;
            $row_array['sno']     =  $v->sno;
            $row_array['pcb_serial_no']     =  $v->pcb_serial_no;
            $row_array['customer_name']     =  $v->customer_name;
            $row_array['customer_phone']     =  $v->customer_phone;
            $row_array['address']     =  $v->address;
            $row_array['spare_name']     =  $this->getServiceSpare($v->service_id);
            $row_array['date_of_manufacture']     = ($v->date_of_manufacture ? date("d-m-Y", strtotime($v->date_of_manufacture)) : "");
            $row_array['bill_date']     =  ($v->bill_date ? date("d-m-Y", strtotime($v->bill_date)) : "");
            $row_array['bill_time']     =  ($v->bill_date ? date("h:i:sa", strtotime($v->bill_date)) : "");
            $row_array['complete_date']     =  ($service_status ? date("d-m-Y", strtotime($v->created_at)) : "");;
            $row_array['old_software']        =  $v->old_software;
            $row_array['technician_name']     =  $v->technician_name;
            $row_array['complaint_name']      =  $v->complaint_name;
            $row_array['service_complaints']  = $this->getServiceComplaints($v->service_id);
            $row_array['remarks']     =  $v->remarks;
            $row_array['level']     = ($v->level_id ? $level[0]->name : '');
            $row_array['claim_amount']     =  number_format(($this->getServiceComplaintRate($v->service_id) + $center_spare_charge), 2);
            $row_array['customer_amount']  = number_format(($service_charge + $customer_spare_charge), 2);
            array_push($claimList_array, $row_array);
        }
        return $claimList_array;
    }



    public function getBranches($branch_ids, $country_id = NULL)
    {
        $data = DB::table('branch')->where('status', '=', 1);
        if ($branch_ids) {
            $data->whereIn('id', explode(',', $branch_ids));
        }
        if (!empty($country_id)) {
            $data->where('country_id', '=', $country_id);
        }
        $qry = $data->select("id", "name")->pluck("name", "id")->all();
        return $qry;
    }

    public function getCountries($branch_ids)
    {
        $data = DB::table('country')->where('country.status', '=', 1)->leftJoin('branch', 'branch.country_id', '=', 'country.id');
        if ($branch_ids) {
            $data->whereIn('branch.id', explode(',', $branch_ids));
        }
        $qry = $data->select("country.id", "country.name")->pluck("country.name", "country.id")->all();
        return $qry;
    }

    public function getDashboardCount($table, $where, $count_column, $branch_ids, $flag = NULL)
    {
        if(empty($where[1][2]) && empty($where[2][2]) ) {
            unset($where[1]);
            unset($where[2]);
        }
        // DB::enableQueryLog();
        $data = DB::table($table);
        if ($branch_ids && $table == 'order') {
            $data->whereIn('to_branch', explode(',', $branch_ids));
        } elseif ($branch_ids && $table == 'appeal_master') {
            $data->whereIn('to_branch', explode(',', $branch_ids));
        } elseif ($flag == 1) {
            $data->leftJoin('appeal_master', 'service.id', '=', 'appeal_master.service_id');
            $data->whereNull("appeal_master.service_id");

        } else {
            $data->whereIn('branch_id', explode(',', $branch_ids));
        }

        $qry = $data->where($where)->count($count_column);
        // dd(DB::getQueryLog());
        return $qry;
    }

    public function updateStock($data)
    {
        DB::table('stock')->where('branch_id', $data['branch_id'])->where('spare_id', $data['spare_id'])->update(['qty' => DB::raw('qty - ' . $data['qty']), 'updated_by' => Auth::id()]);
        return true;
    }


    public function getLevels($service_id, $type = 0)
    {
        $service_transfer = $this->GetFirst('service_transfer', array("service_id" => $service_id, "status" => 1));
        if (!empty($service_transfer)) {
            $branch_id = $service_transfer->to_branch;
        } else {
            $branch_id = $this->FieldByID('service', 'branch_id', $service_id);
        }
        $company_id = $this->FieldByID('branch', 'company_id', $branch_id);
        $row = DB::table('level_rate')->select(['levels.id', 'levels.name'])->leftJoin('levels', 'levels.id', '=', 'level_rate.level_id')->where('level_rate.company_id', $company_id)->where('levels.service_type_id', $type)->orderBy('levels.name')->distinct()->groupBy('level_rate.level_id')->get();
        $level_arr = [];
        foreach ($row as $val) {
            $level_arr[$val->id] = $val->name;
        }
        return $level_arr;
    }

    public function getComapnyLevelRate($id, $service_id)
    {
        $service_transfer = $this->GetFirst('service_transfer', array("service_id" => $service_id, "status" => 1));
        if (!empty($service_transfer)) {
            $branch_id = $service_transfer->to_branch;
        } else {
            $branch_id = $this->FieldByID('service', 'branch_id', $service_id);
        }
        $company_id = $this->FieldByID('branch', 'company_id', $branch_id);
        $this->result = DB::table('level_rate')
            ->select('rate')
            ->where("level_id", "=", $id)
            ->where("company_id", "=", $company_id)
            ->orderBy('id', 'DESC')
            ->first();
        return ($this->result ? $this->result->rate : "");
    }


    public function getFAQDataList($search)
    {
        $query = DB::table("faq");
        if ($search) {
            $query->where('question', 'LIKE', '%' . $search . '%');
        }
        return $query->OrderBy('id', 'DESC')->paginate(5);
    }

    public function getIMEIDataList($input)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table('imei_details')->leftJoin('imei', 'imei.id', '=', 'imei_details.group_id')
            ->leftJoin('country', 'imei_details.country_id', '=', 'country.id')
            ->leftJoin('models', 'imei.model_id', '=', 'models.id')
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('color', 'variant_details.color_id', '=', 'color.id')
            ->leftJoin('ram', 'variant_details.ram_id', '=', 'ram.id')
            ->leftJoin('rom', 'variant_details.rom_id', '=', 'rom.id')
            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'imei_details.IMEI_no1', 'imei_details.IMEI_no2', 'imei_details.pcb_serial_no', 'imei_details.serial_no', 'imei_details.date_of_manufacture', 'models.name as model_name', 'country.name as country_name', 'color.name as color_name', 'ram.name as ram_name', 'rom.name as rom_name']);

        if (!empty($input['id'])) {
            $data->where("imei.id", $input['id']);
        }
        if (!empty($input['country_id'])) {
            if ($input['country_id'] == "-1") {
                $data->whereNull("imei_details.country_id");
            } else {
                $data->where("imei_details.country_id", $input['country_id']);
            }
        }
        if (!empty($input['model_id'])) {
            $data->where("imei_details.variant_id", $input['model_id']);
        }
        return $data;
    }

    public function checkAllPermission($role_id)
    {

        $service_module_id = $this->GetId("module", "Service");
        $service_permission_result = $this->GetPermission($role_id, $service_module_id);

        $warranty_module_id = $this->GetId("module", "Warranty");
        $warranty_permission_result = $this->GetPermission($role_id, $warranty_module_id);

        $admin_module_id = $this->GetId("module", "Admin");
        $admin_permission_result = $this->GetPermission($role_id, $admin_module_id);

        $access_all = 0;
        if ($service_permission_result->all == 1 && $warranty_permission_result->all == 1 && $admin_permission_result->all == 1) {
            $access_all = 1;
        }
        return $access_all;
    }

    public function insertPurchaseSpare($post)
    {
        $post_items['local_price'] = $post['local_price'];
        $post_items['price'] = $post['price'];
        $post_items['parent_id'] = $post['parent_id'];
        $post_items['spare_id'] = $post['spare_id'];
        $post_items['qty'] = $post['qty'];
        $item_id = $this->insertGetId('purchase_items', $post_items);
        $is_stock = $this->getStock($post['branch_id'], $post['spare_id']);
        if ($is_stock > 0) {
            DB::table('stock')->where('branch_id', $post['branch_id'])->where('spare_id', $post['spare_id'])->update(['qty' => DB::raw('qty + ' . $post['qty']), 'updated_by' => Auth::id(), 'price' =>  $post_items['price'], 'local_price' => $post_items['local_price']]);
        } else {
            DB::table('stock')->insertGetId(['branch_id' => $post['branch_id'], 'spare_id' => $post['spare_id'], 'qty' => $post['qty'], 'created_by' => Auth::id(), 'updated_by' => Auth::id(), 'price' =>  $post_items['price'], 'local_price' => $post_items['local_price']]);
        }
        return $item_id;
    }

    public function getActivationReportTableData($from, $to)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table('imei_details')->leftJoin('imei', 'imei.id', '=', 'imei_details.group_id')->leftJoin('country', 'imei_details.country_id', '=', 'country.id')
            ->leftJoin('models', 'imei.model_id', '=', 'models.id')
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('color', 'variant_details.color_id', '=', 'color.id')
            ->leftJoin('ram', 'variant_details.ram_id', '=', 'ram.id')
            ->leftJoin('rom', 'variant_details.rom_id', '=', 'rom.id')
            ->select(['imei_details.IMEI_no1', 'imei_details.IMEI_no2', 'imei_details.pcb_serial_no', 'imei_details.serial_no', 'imei_details.date_of_manufacture', 'models.name as model_name', 'country.name as country_name', 'color.name as color_name', 'ram.name as ram_name', 'rom.name as rom_name', 'imei_details.activation_date', 'imei_details.expiry_date', DB::raw('@rownum  := @rownum  + 1 AS rownum')])
            ->whereNotNull("imei_details.activation_date");
        if (!empty($from) && !empty($to)) {
            $data->whereRaw(
                "(imei_details.activation_date >= ? AND imei_details.activation_date <= ?)",
                [
                    dateformat($from) . " 00:00:00",
                    dateformat($to) . " 23:59:59"
                ]
            );
        }
        return $data;
    }

    public function getSpareTableData($model_id)
    {
        $data =  DB::table("spare")->select(DB::raw("GROUP_CONCAT(models.name) as model_name"), 'spare.*')
            ->leftjoin("models", DB::raw("FIND_IN_SET(models.id,spare.model_id)"), ">", DB::raw("'0'"))
            ->groupBy('spare.id');
        if (!empty($model_id)) {
            $data->whereRaw("find_in_set('" . $model_id . "',spare.model_id)");
        }
        return $data;
    }

    public function getCompanyCurrency($id)
    {
        if ($id) $company_id = $id;
        else $company_id = $this->FieldByID("branch", "company_id", Auth::user()->branch_id);
        $company = DB::table("companies")
            ->select("currency.name")
            ->leftjoin("currency", 'currency.id', "=", 'companies.currency_id')
            ->where("companies.id", $company_id)
            ->first();
        return ($company->name ? $company->name : 'USD');
    }

    public function getModelComplaintTableData($input)
    {
        $join = '';
        if ($input['model_id']) {
            $join = "join variant_details as vd ON vd.id=s.variant_id where vd.model_id='" . $input['model_id'] . "'";
        }

        $data = DB::select(DB::raw("SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS number,c.description, SUM(case when s.is_warranty = 1 then 1 else 0 end) as warranty, SUM(case when s.is_warranty = 0 then 1 else 0 end) as nonwarranty, COUNT(s.id) as total FROM `service_complaint` as sc join service as s on s.id=sc.service_id join complaints as c ON c.id=sc.complaint_id $join group by sc.complaint_id order by number"));
        return $data;
    }

    public function getPriceComparisonTableData($input)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $stock = DB::table("stock")
            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), "spare.spare_name", "spare.focus_code", "spare.id"])
            ->leftjoin("spare", 'spare.id', "=", 'stock.spare_id')
            ->GroupBy("stock.spare_id")
            ->orderBy("stock.spare_id")
            ->get();
        $row_array = $stockList_array = [];
        $num = 1;
        if ($input['branch_ids']) $branch_ids = explode(',', $input['branch_ids']);
        sort($branch_ids);
        $length = count($branch_ids);

        foreach ($stock as $k => $v) {
            $row_array = array();
            //$row_array[] =  $v->rownum;
            $row_array[] =  $v->focus_code;
            $row_array[] =  $v->spare_name;
            if ($input['branch_ids']) {
                for ($x = 0; $x < $length; $x++) {
                    $fetch_stock = DB::table("stock")->select('stock.qty', 'stock.local_price', 'currency.name')
                        ->leftjoin("branch", 'branch.id', "=", 'stock.branch_id')
                        ->leftjoin("country", 'country.id', "=", 'branch.country_id')
                        ->leftjoin("currency", 'currency.id', "=", 'country.currency_id')
                        ->where('stock.spare_id', $v->id)
                        ->where('stock.branch_id',  $branch_ids[$x])->first();
                    $row_array[] =  ($fetch_stock ? $fetch_stock->qty : 0);
                    $currency =  ($fetch_stock ? $fetch_stock->name : '');
                    $row_array[] =  $currency . " " . ($fetch_stock ? number_format($fetch_stock->local_price, 2) : "0.00");
                }
            }
            $num++;
            array_push($stockList_array, $row_array);
        }
        return $stockList_array;
    }

    public function getLastPurchaseItem($spare_id, $branch_id)
    {
        $last_record =  DB::table('purchase_master')->select('purchase_items.id')
            ->leftJoin('purchase_items', 'purchase_master.id', '=', 'purchase_items.parent_id')
            ->where([
                'purchase_items.spare_id' => $spare_id,
                'purchase_master.branch_id' => $branch_id,
            ])
            ->orderBy('purchase_items.id', 'DESC')->take(1)
            ->get();
        return ($last_record ? $last_record[0]->id : 0);
    }


    public function getStockServiceClaimTableData($input)
    {
        DB::statement(DB::raw('set @rownum=0'));
        //\DB::enableQueryLog();
        $query = DB::table("stock_service")
            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), "stock_service.id as service_id", "stock_service.completion_date", "stock_service.serial_no", "stock_service.created_at", "imei_country.name as country_name", "order_status.name as status", "models.name as model_name", "color.name as color", "ram.name as ram", "rom.name as rom", "imei_details.IMEI_no1", "imei_details.IMEI_no2", "imei_details.serial_no as sno", "imei_details.date_of_manufacture", "imei_details.pcb_serial_no", "users.first_name as technician_name", "stock_service.remarks", "branch.code as branch_code", "branch.name as branch_name", "brands.name as brand_name", "software_version.name as old_software", "stock_service.level_id", "stock_service.level_price", "companies.name as company_name", "company_currency.name as currency", "company_currency.id as currency_id", "branch_currency.name as branch_currency_name", "branch_currency.id as branch_currency_id"])
            ->leftJoin('imei_details', 'imei_details.id', '=', 'stock_service.imei_id')
            ->leftJoin('country as imei_country', 'imei_country.id', '=', 'imei_details.country_id')
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('users', 'users.id', '=', 'stock_service.technician_id')
            ->leftJoin('branch', 'branch.id', '=', 'stock_service.branch_id')
            ->leftJoin('companies', 'companies.id', '=', 'branch.company_id')
            ->leftJoin('currency as company_currency', 'company_currency.id', '=', 'companies.currency_id')
            ->leftJoin('country as branch_country', 'branch_country.id', '=', 'branch.country_id')
            ->leftJoin('currency as branch_currency', 'branch_currency.id', '=', 'branch_country.currency_id')
            ->leftJoin('order_status', 'order_status.id', '=', 'stock_service.status')
            ->leftJoin('brands', 'brands.id', '=', 'models.brand_id')
            ->leftJoin('software_version', 'software_version.id', '=', 'stock_service.version_id')
            ->where('stock_service.is_approved', '=', 1);

        if (!empty($input['from']) && !empty($input['to'])) {
            $query->whereRaw(
                "(stock_service.is_approved_at >= ? AND stock_service.is_approved_at <= ?)",
                [
                    dateformat($input['from']) . " 00:00:00",
                    dateformat($input['to']) . " 23:59:59"
                ]
            );
        }
        if (!empty($input['branch_id'])) {
            $query->where('stock_service.branch_id', '=', $input['branch_id']);
        } else {
            $query->whereIn('stock_service.branch_id', explode(',', $input['branch_ids']));
        }

        if (!empty($input['country_id']) && empty($input['branch_id'])) {
            $branch_ids = $this->getBranchIds($input['country_id']);
            $query->whereIn('stock_service.branch_id', $branch_ids);
        }
        if (!empty($input['model_id'])) {
            $query->where('variant_details.model_id', '=', $input['model_id']);
        }
        if (!empty($input['model_id'])) {
            $query->where('variant_details.model_id', '=', $input['model_id']);
        }
        if (!empty($input['company_id'])) {
            $query->where('companies.id', '=', $input['company_id']);
        }

        $data = $query->orderBy('stock_service.id', 'ASC')
            ->groupBy('stock_service.id')
            ->get();
        $row_array = $claimList_array = array();
        $i = 1;
        foreach ($data as $v) {
            if (!empty($input['currency_id']) && $v->currency_id != $input['currency_id']) {
                $currency_convert =  $this->GetByID('currency', $v->currency_id);
                $currency_convert_d =  $this->GetByID('currency', $input['currency_id']);
                $curr  = $this->FieldByID('currency', 'name', $input['currency_id']);
                $amt = number_format($v->level_price / $currency_convert->rate, 2);
                if ($curr == "USD") $claim_amount  = $curr . " " . $amt;
                else  $claim_amount  = $curr . " " . number_format($amt * $currency_convert_d->rate, 2);
            } else {
                $claim_amount = $v->currency . " " . number_format(($v->level_price), 2);
            }
            $level = $this->WhereByData("levels", "id", $v->level_id);
            $row_array['service_id']      =  $v->service_id;
            $row_array['rownum']         =  $i++;
            $row_array['serial_no']      =  $v->serial_no;
            $row_array['created_at']     =  ($v->created_at ? date('d-m-Y', strtotime($v->created_at)) . " " . date("h:i:sa", strtotime($v->created_at)) : "");
            $row_array['country_name']   =  $v->country_name;
            $row_array['branch_code']    =  $v->branch_code;
            $row_array['branch_name']    =  $v->branch_name;
            $row_array['status']         =  $v->status;
            $row_array['brand_name']     =  $v->brand_name;
            $row_array['model_name']     =  $v->model_name;
            $row_array['color']          =  $v->color;
            $row_array['ram']            =  $v->ram;
            $row_array['rom']            =  $v->rom;
            $row_array['IMEI_no1']       =  $v->IMEI_no1;
            $row_array['IMEI_no2']       =  $v->IMEI_no2;
            $row_array['sno']     =  $v->sno;
            $row_array['pcb_serial_no']     =  $v->pcb_serial_no;
            $row_array['date_of_manufacture']     = ($v->date_of_manufacture ? date("d-m-Y", strtotime($v->date_of_manufacture)) : "");
            $row_array['old_software']        =  $v->old_software;
            $row_array['technician_name']     =  $v->technician_name;
            $row_array['service_complaints']  = $this->getServiceComplaints($v->service_id);
            $row_array['remarks']     =  $v->remarks;
            $row_array['level']     = ($level ? $level[0]->name : '');
            $row_array['company_name']     =  $v->company_name;
            $row_array['claim_amount']     =  $claim_amount;
            $row_array['spare_name']     =  $this->getServiceSpare($v->service_id);
            $row_array['bill_date']     =  ($v->completion_date ? date("d-m-Y", strtotime($v->completion_date)) : "");
            $row_array['bill_time']     =  ($v->completion_date ? date("h:i:sa", strtotime($v->completion_date)) : "");
            $row_array['currency']  =  $v->currency;
            array_push($claimList_array, $row_array);
        }
        return $claimList_array;
    }


    public function getLevelClaimTableData($input)
    {
        if ($input['type_id'] == 2) {
            $tabel = "stock_service";
        } else {
            $tabel = "service";
        }
        $branch_ids = DB::table('branch')->where('company_id', $input['company_id'])->pluck('id')->implode(',');
        $level_claim = DB::select(DB::raw("select DATE_FORMAT(" . $tabel . ".created_at,'%Y-%m') as period, year(" . $tabel . ".created_at) as year ,MONTHNAME(" . $tabel . ".created_at) as month,levels.name as level_name,levels.id,SUM(case when " . $tabel . ".level_id>0 then 1 else 0 end) as qty," . $tabel . ".level_price from " . $tabel . " LEFT JOIN levels ON " . $tabel . ".level_id = levels.id where " . $tabel . ".level_id>0 and year(" . $tabel . ".created_at) = " . $input['year'] . " and " . $tabel . ".branch_id IN(" . $branch_ids . ")  group by year(" . $tabel . ".created_at),month(" . $tabel . ".created_at)," . $tabel . ".level_id order by year(" . $tabel . ".created_at),month(" . $tabel . ".created_at)"));
        return $level_claim;
    }


    public function UploadFile($path, $file)
    {

        $new_filename = preg_replace('/[ ,]+/', '_', trim($file->getClientOriginalName()));
        $path = $path . $new_filename;

        $new_path = $path;
        $filename = pathinfo($path, PATHINFO_FILENAME);

        $directory = dirname($path);


        if (file_exists($path)) {

            $i = 1;
            while (file_exists($new_path)) {

                // get file extension
                $extension = pathinfo($path, PATHINFO_EXTENSION);

                // get file's name
                $filename = pathinfo($path, PATHINFO_FILENAME);

                // get file's directory
                $directory = dirname($path);

                // add and combine the filename, iterator, extension
                $new_filename = $filename . '_' . $i . '.' . $extension;

                // add file name to the end of the path to place it in the new directory; the while loop will check it again
                $new_path = $directory . "/" . $new_filename;

                $i++;
            }
        }

        $file->move($directory, $new_filename);
        // dd($file);

        return $new_filename;
    }
}

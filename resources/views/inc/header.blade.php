<header class="header header-fixed header-light">
  <div class="header-middle">
    <div class="logo-color logo-color-light">
      <div class="logo">
        <div class="logo-middle">
          <a href="/">
            <img src="{{asset('images/ravoz_logo.svg')}}" alt="Logo">
          </a>
        </div>
      </div>
    </div>
    <div class="header-topbar">
      <div class="topbar-left mobile-menu">
        <!-- <a class="side-toggle" href="javascript:void(0);"><i class="la la-bars"></i></a> -->
        {{-- <div class="search-box">
						<div class="search">
							<input class="form-control border-0" type="search" placeholder="Type something..."
                            aria-label="Search">
								<a href="#">
									<i class="la la-search"></i>
								</a>
							</div>
						</div> --}}
      </div>
      <div class="topbar-right" style="display: flex;" >
        <p class="user_txt">{{ auth()->user()->first_name }}</p>
        <ul class="profile_cls">
          <li class="dropdown show mobile-menu">
            <a class="side-toggle" href="javascript:void(0);">
              <i class="la la-bars"></i>
            </a>
          </li>
          <li class="dropdown show user-profile ">
            <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  onclick="userclose()">
              <div class="avatar avatar-sm mr-1">
                <img class="img-fluid" src="{{asset('images/10.svg')}}" alt="">
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="profile-pic">
                <div class="row">
                  <div class="col-8">
                    <div class="profile-name">
                      <h4>{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}</h4>
                      {{ auth()->user()->email }}
                    </div>
                  </div>
                  <!-- <div class="col-4">
                    <div class="avatar mr-1">
                      <img class="img-fluid" src="{{asset('images/10.png')}}" alt="">
                    </div>
                  </div> -->
                </div>
              </div>
              <div class="profile-info"> 
                @if (userHasPermission('Admin','user_access') || userHasPermission('Admin','settings')) 
                <a class="dropdown-item" href="/">
                  <i class="la la-user"></i> ADMIN </a> 
                @endif 
                @if (userHasPermission('Service','stock_list') || userHasPermission('Service','purchase_list') || userHasPermission('Service','transfer_list') || userHasPermission('Service','purchase_order_list') || userHasPermission('Service','service_list') || userHasPermission('Service','sales_request_list') || userHasPermission('Service','service_transfer_list') || userHasPermission('Service','doa_report') || userHasPermission('Service','activation_report') || userHasPermission('Service','stock_report') || userHasPermission('Service','consumption_report') || userHasPermission('Service','spare_claim_report') || userHasPermission('Service','service_claim_report') || userHasPermission('Service','complaint_report') || userHasPermission('Service','forecast_report') || userHasPermission('Service','doa_list') || userHasPermission('Service','stock_service_list')) 
                <a class="dropdown-item" href="{{route('service.index')}}">
                  <i class="la la-cogs"></i> SERVICE </a> 
                @endif 
                @if (userHasPermission('Warranty','imei_list') || userHasPermission('Warranty','imei_transfer_list') || userHasPermission('Warranty','sales_list') || userHasPermission('Warranty','import_activation') || userHasPermission('Warranty','activation_return_list')) 
                <a class="dropdown-item" href="{{route('warranty.index')}}">
                  <i class="la la-get-pocket"></i> WARRANTY </a> 
                @endif 
                <div class="separator my-2"></div>
                
                <a class="btn btn-outline-primary outline-gray btn-sm mt-3" href="{{route('logout')}}">
                  <i class="la la-power-off"></i> LOGOUT </a>
                <a class="btn btn-outline-primary outline-gray btn-sm mt-3" href="{{url('/change-password')}}">
                  <i class="la la-cog"></i> Settings </a>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</header>
<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LevelRate;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use DB;
use Auth;

class LevelRateController extends Controller
{
    public function __construct()
    {
        $this->data['level'] = $this->NameAndID('levels');
        $this->data['companies'] = $this->NameAndID('companies');
        View::share('js', ['master']);
    }

    public function index()
    {
        return view('admin.level-rate.index');
    }

    public function getLevelRateData(Request $request) {
        $data = LevelRate::leftJoin('levels', 'levels.id', '=', 'level_rate.level_id')
                    ->leftJoin('companies', 'companies.id', '=', 'level_rate.company_id')
                    ->leftJoin('currency','currency.id', '=', 'companies.currency_id')
                    ->select([
                        'level_rate.*',
                        'levels.name as level_name',
                        'companies.name as company_name',
                        'currency.name as currency'
                    ])->get();

        return DataTables::of($data)
                ->escapeColumns(['name'])
                ->editColumn('date', function($data) {
                    return date('M-Y',strtotime($data->date));
                })->editColumn('rate', function($data) {
                    return $data->currency." ".$data->rate;
                })
                ->addIndexColumn()
                ->make(true);
    }

    public function create()
    {
        return view('admin.level-rate.create', $this->data);
    }

    public function store(Request $request) {

        $post = new LevelRate();
        $validator = $this->validateLevelRateForm($request);
        $fromDate = dateformat("01-".$request->date);
        $toDate  = date("Y-m-t", strtotime($fromDate));
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        if (DB::table('level_rate')->where('level_id', $request->level_id)->where('company_id', '=', $request->company_id)
            ->whereRaw(
                "(date >= ? AND date <= ?)", 
                [
                $fromDate, 
                $toDate
                ]
            )->first()) {
            return redirect()->back()->with("error", "Rate of ".$this->FieldByID("companies","name",$request->company_id)." level ".$this->FieldByID("levels","name",$request->level_id) ." on ".date("F",strtotime($fromDate)). " is updated..");
        }

        $post = $this->GetLevelRateForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The level rate was successfully created.");
        }
    }

    public function getRate($id,$service_id)
    {
        //echo $service_id;
        return $this->getComapnyLevelRate($id,$service_id);
    }
    
}
@extends('layouts.warranty')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                  <h4 class="card-title-text">Upload IMEI</h4>
                </div>
                <div class="col-md-6 {{(userHasPermission('Warranty','imei_list')?'':'hide')}}">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ url('warranty/devices') }}"> <i
                            class="feather icon-list"></i> Uploaded IMEI List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['route' => 'devices.import','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-model', 'enctype'=>"multipart/form-data"]) }}
            @csrf
            <div class="form-row">
                <!-- <div class="form-group col-md-6">
                    {{ Form::label('name', __('Country')) }}
                    {{ Form::select('country_id', $country, old('country_id'), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Country', 'required' => 'required']) }}
                </div> -->
                <div class="form-group col-md-4">
                    {{ Form::label('name', __('Model')) }}
                    {{ Form::select('model_id', $model, old('model_id'), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model', 'required' => 'required', 'id' => 'model_id']) }}
                    {{ Form::hidden('model_hidden', null, ['id' => 'model_hidden']) }}
                </div>
                <div class="form-group col-md-4">
                    {{ Form::label('name', __('Excel Type')) }}
                    <select name="type" class="form-control" id="type">
                        <option value="">Select Type</option>
                        <option value="1">IMEI & Serial no</option>
                        <option value="2">Serial no Only</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    {{ Form::label('name', __('File')) }}
                    <input type="file" name="file" class="form-control" id="customFile">
                    <a class="pull-right" href="{{url('/uploads/format/imei_template_format.xlsx')}}"><small>Download Sample xlsx File <i class="fa fa-download"></i></small></a>
                </div>

            </div>
            <button class="btn btn-success float-right">Import</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script>
    $("#model_id").change(function(){
      $("#model_hidden").val($("#model_id").find(":selected").text());
    });
</script>
@endsection

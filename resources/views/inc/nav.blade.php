<div class="sidebar-panel nicescrollbar sidebar-panel-light">
  

    <ul class="sidebar-menu">
        <li class="menu-expand">
                    <a class="side-toggle" href="javascript:void(0);">
                    <span></span> <span class="sidebar-badge"> </span><i class="la la-compress"></i>
                    </a>
        </li>
        <li class="first-menu {{ active_class(request()->is('/')) }}">
            <a href="{{ route('dashboard') }}">
                <i class="la la-home"></i> <span>Dashboard</span> <span class="sidebar-badge"> </span>
            </a>
        </li>
        @if (userHasPermission('Admin','user_access'))
        <li class = "{{ set_active(['admin/module','admin/module/create','admin/permission','admin/permission/create','admin/role','admin/role/create','admin/user','admin/user/create']) }}
        {{ active_class(request()->is('admin/module/*/edit')) }}
        {{ active_class(request()->is('admin/permission/*/edit')) }}
        {{ active_class(request()->is('admin/role/*/edit')) }}
        {{ active_class(request()->is('admin/user/*/edit')) }}">
            <a href="#">
            <i class="fa fa-caret-down down-arrow"></i><span class="menu-left">Access </span> <i class="la la-user"></i> 
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{ route('role') }}" class="nav-link {{ request()->is('admin/role*') ? 'active font-weight-bolder' : '' }}"> <span> Role Management </span> </a></li>
                <li><a href="{{ route('user') }}" class="nav-link {{ request()->is('admin/user*') ? 'active font-weight-bolder' : '' }}"> <span> User Management </span> </a></li>
            </ul>
        </li>
        @endif
        @if (userHasPermission('Admin','settings'))
        <li class="{{ active_class(request()->is('admin/settings*')) }}{{ active_class(request()->is('admin/master*')) }}{{ active_class(request()->is('admin/complaint*')) }}{{ active_class(request()->is('admin/level*')) }}{{ active_class(request()->is('admin/model*')) }}{{ active_class(request()->is('admin/accessories*')) }}">
            <a href="#">
            <i class="fa fa-caret-down down-arrow"></i><span class="menu-left">Settings </span> <i class="la la-gear"></i> 
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{ url('admin/master/companies/index') }}" class="nav-link {{ request()->is('admin/master/companies*') ? 'active font-weight-bolder' : '' }}"> <span> Company</span> </a></li>
                <li><a href="{{ route('branch.index') }}" class="nav-link {{ request()->is('admin/settings/branch*') ? 'active font-weight-bolder' : '' }}"> <span> Branch</span> </a></li>
                <li><a href="{{ route('country.index') }}" class="nav-link {{ request()->is('admin/settings/country*') ? 'active font-weight-bolder' : '' }}"> <span> Country  </span> </a></li>
                <li><a href="{{ route('currency.index') }}" class="nav-link {{ request()->is('admin/settings/currency*') ? 'active font-weight-bolder' : '' }}"> <span> Currency  </span> </a></li>
                <li><a href="{{ route('level.index') }}" class="nav-link {{ request()->is('admin/level') ? 'active font-weight-bolder' : '' }}"> <span> Level  </span> </a></li>
                <li><a href="{{ route('level.rate.index') }}" class="nav-link {{ request()->is('admin/level/rate*') ? 'active font-weight-bolder' : '' }}"> <span> Level Rate </span> </a></li>
                <li><a href="{{ route('complaint.index') }}" class="nav-link {{ request()->is('admin/complaint*') ? 'active font-weight-bolder' : '' }}"> <span> Complaints  </span> </a></li>
                <li><a href="{{ url('admin/master/brands/index') }}" class="nav-link {{ request()->is('admin/master/brands*') ? 'active font-weight-bolder' : '' }}"> <span> Brand  </span> </a></li>
                <li><a href="{{ route('model.index') }}" class="nav-link {{ request()->is('admin/model*') ? 'active font-weight-bolder' : '' }}"> <span> Model </span> </a></li>
                <li><a href="{{ url('admin/master/ram/index') }}" class="nav-link {{ request()->is('admin/master/ram*') ? 'active font-weight-bolder' : '' }}"> <span> RAM  </span> </a></li>
                <li><a href="{{ url('admin/master/rom/index') }}" class="nav-link {{ request()->is('admin/master/rom*') ? 'active font-weight-bolder' : '' }}"> <span> ROM  </span> </a></li>
                <li><a href="{{ url('admin/master/color/index') }}" class="nav-link {{ request()->is('admin/master/color*') ? 'active font-weight-bolder' : '' }}"> <span> Color </span> </a></li>
                <li><a href="{{ url('admin/master/product_type/index') }}" class="nav-link {{ request()->is('admin/master/product_type*') ? 'active font-weight-bolder' : '' }}"> <span> Product Type  </span> </a></li>
                <li><a href="{{ url('admin/master/order_status/index') }}" class="nav-link {{ request()->is('admin/master/order_status*') ? 'active font-weight-bolder' : '' }}"> <span> Order Status  </span> </a></li>
                <!-- <li><a href="{{ url('admin/master/repair_type/index') }}" class="nav-link {{ request()->is('admin/master/repair_type*') ? 'active font-weight-bolder' : '' }}"> <span> Repair Type  </span> </a></li>
                <li><a href="{{ url('admin/master/repair_method/index') }}" class="nav-link {{ request()->is('admin/master/repair_method*') ? 'active font-weight-bolder' : '' }}"> <span> Repair Method  </span> </a></li> 
                <li><a href="{{ url('admin/master/submission_method/index') }}" class="nav-link {{ request()->is('admin/master/submission_method*') ? 'active font-weight-bolder' : '' }}"> <span> Submission Method </span> </a></li>-->
                <li><a href="{{ url('admin/master/submission_category/index') }}" class="nav-link {{ request()->is('admin/master/submission_category*') ? 'active font-weight-bolder' : '' }}"> <span> Submission Category </span> </a></li>
                <li><a href="{{ route('accessories.index') }}" class="nav-link {{ request()->is('admin/accessories*') ? 'active font-weight-bolder' : '' }}"> <span> Accessories </span> </a></li>
                <li><a href="{{ route('softwareversion.index') }}" class="nav-link {{ request()->is('admin/settings/softwareversion*') ? 'active font-weight-bolder' : '' }}"> <span> Software Version  </span> </a></li>
                <li><a href="{{ route('spare.index') }}" class="nav-link {{ request()->is('admin/settings/spare*') ? 'active font-weight-bolder' : '' }}"> <span> Spare  </span> </a></li>
                <li><a href="{{ route('faqs.index') }}" class="nav-link {{ request()->is('admin/settings/faqs*') ? 'active font-weight-bolder' : '' }}"> <span> FAQ  </span> </a></li>
                
            </ul>
        </li>
        @endif

    </ul>
</div>

<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Http\Traits\PermissionTrait;
use DB;
use Session;

trait AuthenticatesUsers
{
    
    use RedirectsUsers, ThrottlesLogins, PermissionTrait;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.

     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse

     * @throws \Illuminate\Validation\ValidationException
     */

    public function login(Request $request)
    {
        $this->validateLogin($request);
        //dd($request->all());
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if ($request->hasSession()) {
                $request->session()->put('auth.password_confirmed_at', time());
            }
            $user=$this->guard()->user();
           

            $json = json_decode($user['multiple_access'], true);
            // echo $user['branch_id'];exit;
           // echo "<pre>"; print_r($user); echo "</pre>"; exit;
            $array_key = '';
            if( isset($json)) {
                if(array_key_exists("country_id",$json))
                {
                    $array_key="country_id";
                }
                if(array_key_exists("company_id",$json))
                {
                    $array_key="company_id";
                }
                if(array_key_exists("branch_id",$json))
                {
                    $array_key="branch_id";
                }
            }
            
            if(!empty($array_key)) {
                $result = array($user['branch_id']);
                foreach($json[$array_key] as $res)
                {
                    if($array_key == 'country_id' || $array_key == 'company_id'){
                        $result = json_decode(json_encode(DB::table('branch')->where($array_key, $res)->where("status",1)->pluck('id')),1);
                        
                    }
                    else{
                        $result = $res;
                    }
                   
                    $multiaccess[] = $result;
                    
                }
                //echo "<pre>"; print_r($multiaccess); echo "</pre>"; exit;
                $result=[];
                if($array_key == 'country_id' || $array_key == 'company_id'){
                    
                    foreach($multiaccess as $array){
                       $result = array_merge($result, $array);
                    }
                } else {
                    $result =$multiaccess;
                }
                if(count($result)>0) { 
                   $branch_id = (implode(",", $result));
                }
                $request->session()->put('sessi_branch_id', $branch_id);
                $request->session()->save();
                //Session::put('sessi_branch_id', $branch_id);
             
            }


            if($user->google_auth_status==1)
            {
                return $this->sendLoginResponse($request);
                // if($user->google2fa_secret=="")

                // {
                //     $google2fa = app('pragmarx.google2fa');
                //     $registration_data['email'] = $user['email'];
                //     $registration_data["google2fa_secret"] = $google2fa->generateSecretKey();
                //     $QR_Image = $google2fa->getQRCodeInline(
                //         config('app.name'),
                //         $registration_data['email'],
                //         $registration_data['google2fa_secret']
                //     );
                //     $registration_data["QR_Image"] = $QR_Image;
                //     $request->session()->flash('registration_data', $registration_data);
                //     return redirect()->route('google.auth');
                // }
            }
            else
            {
                $module_service_id = $this->GetId("module", "Service");
                $permission_service_result= $this->GetPermission(Auth::user()->role_id, $module_service_id);
                
                $module_warranty_id = $this->GetId("module", "Warranty");
                $permission_warranty_result= $this->GetPermission(Auth::user()->role_id, $module_warranty_id);


                if ($permission_service_result->all == 1 || $permission_service_result->permission_ids!="")  {
                    return redirect()->to('/service');
                } else if ($permission_warranty_result->all == 1 || $permission_warranty_result->permission_ids!="")  {
                    return redirect()->to('/warranty');
                } else {
                    return redirect()->to('/');
                }
                exit;
        
                // return $this->sendLoginResponse($request);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->boolean('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        return $request->wantsJson()
                    ? new JsonResponse([], 204)
                    : redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
  
        if (empty($user->google2fa_secret)) {
            // Initialise the 2FA class
          $google2fa = app('pragmarx.google2fa');
  
          // Save the registration data in an array
          $registration_data = $request->all();
  
          // Add the secret key to the registration data
          $registration_data["google2fa_secret"] = $google2fa->generateSecretKey();
  
          // Save the registration data to the user session for just the next request
          $request->session()->flash('registration_data', $registration_data);
  
          // Generate the QR image. This is the image the user will scan with their app
          // to set up two factor authentication
          $QR_Image = $google2fa->getQRCodeInline(
              config('app.name'),
              $user->email,
              $registration_data['google2fa_secret']
          );
          $user->google2fa_secret = $registration_data["google2fa_secret"];
         
          $user->save();
          //dd($user);
          // Pass the QR barcode image to our view
          return view('google2fa.register', ['QR_Image' => $QR_Image, 'secret' => $registration_data['google2fa_secret']]);
        }
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        //
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    public function completeRegistration(Request $request)
    {
        $request->merge(session('registration_data'));
        return $this->registration($request);
    }
}

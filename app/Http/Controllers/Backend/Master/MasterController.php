<?php

namespace App\Http\Controllers\Backend\Master;

use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use DB;

class MasterController extends Controller
{
    public function __construct()
    {
        $this->table = request()->route('table');
        $this->data[$this->table] =  $this->NameAndID($this->table);
        
        //dd($this->data['currency']);
        View::share('js', ['master']);
    }

    public function index()
    {
        // if(str_contains($this->table, '_'))
        // {
            $data['module_name']=ucwords(str_replace('_',' ', $this->table));
        // }
        // else
        // {
        //     $data['module_name']=ucfirst($this->table);
        // }
        $data['module']=$this->table;
        if ( $data['module'] == "companies" ) {
            return view('admin.companies.index', $data);
        } else {
            return view('admin.master.index', $data);
        }
       
    }

    public function create(Request $request)
    {
        if(str_contains($this->table, '_'))
        {
            $data['module_name']=ucwords(str_replace('_',' ', $this->table));
        }
        else
        {
            $data['module_name']=ucfirst($this->table);
        }
        $data['module']=$this->table;
        $data['currency'] =  $this->NameAndID('currency');
        if($request->route('id')!=NULL)
        {
            $id=$request->route('id');
            $getdata = $this->GetByID($this->table, $id);
            $data['details'] = $getdata;
            $data['edit_id'] = $id;
        }
        if ( $data['module'] == "companies" ) {
            return view('admin.companies.create', $data);
        } else {
            return view('admin.master.create', $data);
        }
       
    }


    public function store(Request $request)
    {
        //dd($request->all());
        $id=$request->route('id');

        $post=array();

        $validator = $this->validateMasterForm($request, $this->table, $id, 0);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetMasterForm($post, $request, $id);

        if ($request->table == "companies" ) {
           $post['currency_id'] = $request->currency_id;
        }

        if($id!=NULL)
        {
            $post['updated_by'] = Auth::id();
            $this->updatedata($this->table, $post, $id);
            $msg = "The ".str_replace('_', '', $this->table)." was successfully updated.";
        }
        else
        {
            $post['created_by'] = Auth::id();
            $this->insertdata($this->table, $post);
            $msg = "The ".str_replace('_', ' ', $this->table)." was successfully created.";
        }
        return redirect()->back()->with("success", $msg);

    }

    public function getMasterData()
    {
        $qry = DB::table($this->table);
        if ($this->table == "companies") {
            $qry->select('companies.*','currency.name as currency_name')->leftJoin('currency', 'currency.id', '=', 'companies.currency_id');
        } else {
            $qry->select('*');
        }
        $data = $qry->get();
        return DataTables::of($data)
            ->escapeColumns(['name'])
            ->addColumn('actions', function($data) {
                $edit_url=url('admin/master/'.$this->table.'/create/'.$data->id);
                return '<a href="'.$edit_url.'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>
                ';
            })
            ->addIndexColumn()
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="'.$this->table.'" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->make(true);
    }

    public function Activation(Request $request)
    {
        echo $this->updateStatus($request['name'], $request['ref'], $request['val']);
    }

}

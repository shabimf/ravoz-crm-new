<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IMEI extends Model
{
    use HasFactory;
    protected $table = "imei";  
    protected $guarded = array();
}

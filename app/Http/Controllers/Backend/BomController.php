<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SpareExport;

class BomController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['model'] = $this->NameAndID('models');
            return $next($request);
        });
        View::share('js', ['master']);
    }

    public function index (Request $request) {
        $this->data['model_id'] = ($request->model_id?$request->model_id:0);
        return view('admin.service.spare', $this->data);
    }

    public function get (Request $request) {
        $data =  $this->getSpareTableData($request->model_id);
        return DataTables::of($data)
                ->escapeColumns(['model', 'name', 'partcode', 'focuscode', 'uniquecode'])
                ->addIndexColumn()
                ->filterColumn('model_name', function ($query, $keyword) {
                    $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
                })
                ->filterColumn('spare_name', function ($query, $keyword) {
                    $query->whereRaw('spare.spare_name like ?', ["%{$keyword}%"]);
                })
                ->filterColumn('part_code', function ($query, $keyword) {
                    $query->whereRaw('spare.part_code like ?', ["%{$keyword}%"]);
                })
                ->filterColumn('focus_code', function ($query, $keyword) {
                    $query->whereRaw('spare.focus_code like ?', ["%{$keyword}%"]);
                })
                ->filterColumn('unique_code', function ($query, $keyword) {
                    $query->whereRaw('spare.unique_code like ?', ["%{$keyword}%"]);
                })
                ->make(true);

    }

    public function export(Request $request) {
        $model_id = $request->model_id ? $request->model_id : "";
        return Excel::download(new SpareExport($model_id), 'bom.xlsx');
    }
}
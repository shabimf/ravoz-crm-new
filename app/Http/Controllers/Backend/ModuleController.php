<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use Auth;
use DB;

class ModuleController extends Controller
{
    public function __construct()
    {
        View::share('js', ['master']);
    }

    public function index()
    {
        return view('admin.module.index');
    }

    public function create()
    {
        return view('admin.module.create');
    }

    public function edit($id)
    {

        $this->data['details'] = $this->GetByID('module', $id);
        $this->data['edit_id'] = $id;
        return view('admin.module.create', $this->data);

    }

    public function store(Request $request)
    {

        $post = array();

        $validator = $this->validateMasterForm($request, 'module', $id=NULL, 0);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetMasterForm($post, $request, $id);
        $post['created_by'] = Auth::id();
        $this->insertdata('module', $post);

        return redirect()->back()->with("success","The module was successfully created.");

    }

    public function update(Request $request, $id)
    {

        $post = array();

        $validator = $this->validateMasterForm($request, 'module', $id);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetMasterForm($post, $request, $id);
        $post['updated_by'] = Auth::id();
        $this->updatedata('module', $post, $id);

        return redirect()->back()->with("success","The module was successfully updated.");
    }


    public function getModule()
    {
        $data = DB::table('module')
                ->select('*')
                ->get();
        return DataTables::of($data)
            ->escapeColumns(['name'])
            ->addIndexColumn()
            ->addColumn('actions', function($data) {
                return '<a href="'.route('module.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            })
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="module" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->make(true);
    }

}

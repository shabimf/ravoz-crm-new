(function () {
    var oldExportAction = function (self, e, dt, button, config) {
        if (button[0].className.indexOf('buttons-excel') >= 0) {
            if ($.fn.dataTable.ext.buttons.excelHtml5.available(dt, config)) {
                $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config);
            }
            else {
                $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
            }
        } else if (button[0].className.indexOf('buttons-print') >= 0) {
            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
        }
    };
    
    var newExportAction = function (e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;
    
        dt.one('preXhr', function (e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = 2147483647;
    
            dt.one('preDraw', function (e, settings) {
                // Call the original action function 
                oldExportAction(self, e, dt, button, config);
    
                dt.one('preXhr', function (e, s, data) {
                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                    // Set the property to what it was before exporting.
                    settings._iDisplayStart = oldStart;
                    data.start = oldStart;
                });
    
                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                setTimeout(dt.ajax.reload, 0);
    
                // Prevent rendering of the full data to the DOM
                return false;
            });
        });
    
        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    };
    FTX.Devices = {
        list: {

            selectors: {
                device_table: $('#device-table'),
            },

            init: function (para,country_id,model_id) {
                this.selectors.device_table.dataTable({
                    processing: false,
                    serverSide: true,
                    cache: false,
                    pageLength: 10,
                    // dom: 'lBfrtip',
                    // buttons: [
                    //     {
                    //         extend: 'excel',
                    //         action: newExportAction
                    //     }
                    // ],
                    ajax: {
                        url: this.selectors.device_table.data('ajax_url'),
                        type: 'post',
                        data: function (d) {
                            d.id = para,
                            d.country_id = country_id,
                            d.model_id = model_id
                        },  
                    },
                    columns: [
                        {
                            "data": 'DT_RowIndex',
                            orderable: false, 
                            searchable: false
                        },
                        { data: 'country_name', name: 'country_name'},
                        { data: 'model_name', name: 'model_name'},
                        { data: 'IMEI_no1', name: 'IMEI_no1'},
                        { data: 'IMEI_no2', name: 'IMEI_no2'},
                        { data: 'serial_no', name: 'serial_no'},
                        { data: 'color_name', name: 'color_name'},
                        { data: 'ram_name', name: 'ram_name'},
                        { data: 'rom_name', name: 'rom_name'},
                        { data: 'date_of_manufacture', name: 'date_of_manufacture'},
                        { data: 'pcb_serial_no', name: 'pcb_serial_no'}
                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();

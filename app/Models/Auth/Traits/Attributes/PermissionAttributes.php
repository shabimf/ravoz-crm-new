<?php

namespace App\Models\Auth\Traits\Attributes;

trait PermissionAttributes
{
    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                    '.$this->getEditButtonAttribute('edit-permission', 'permission.edit').'
                    '.$this->getDeleteButtonAttribute('delete-permission', 'permission.destroy').'
                </div>';
    }
}

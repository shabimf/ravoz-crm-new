<?php

namespace App\Http\Controllers\Backend;

use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use DB;
use Auth;


class FAQController extends Controller
{
    public function __construct()
    {
        View::share('js', ['master']);
    }

    public function index() {
        return view('admin.faq.index');
    }

    public function getFAQData(Request $request) {
        $data = DB::table("faq")
                    ->select('faq.*')
                    ->get();
        return DataTables::of($data)
            ->escapeColumns(['question','answer'])
            ->addColumn('actions', function($data) {
                return '<a href="'.route('faqs.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            })
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="faq" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        return view('admin.faq.create');
    }

    public function edit($id)
    {

        $this->data['edit_faq'] = $this->GetByID('faq', $id);
        $this->data['edit_id'] = $id;
        return view('admin.faq.create', $this->data);

    }

    public function store(Request $request)
    {
        $post = array();

        $validator = $this->validateFAQForm($request);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetFAQForm($post, $request);
        $post['created_by'] = Auth::id();
        $this->insertdata('faq', $post);

        return redirect()->back()->with("success","The faq was successfully created.");

    }

    public function update(Request $request, $id)
    {
        $post = array();

        $validator = $this->validateFAQForm($request, $id);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetFAQForm($post, $request, $id);
        $post['updated_by'] = Auth::id();
        $this->updatedata('faq', $post, $id);

        return redirect()->back()->with("success","The faq was successfully updated.");
    }

    public function getFaq(Request $request) {
        
        $data = $this->getFAQDataList($request->search); 
        return view('admin.faq.list',compact('data'));

    }



}
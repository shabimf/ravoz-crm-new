@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Spare Claim</h4>
            </div>
        </div>
        <div class="row  ">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'report.spare','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-spare']) }}
					<div class="form-row well">
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Date From')) }}
							{{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control','id'=>'datepicker' ]) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Date To')) }}
							{{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control', 'id' => 'dates']) }}
						</div>
                        <div class="form-group col-md-3">
                            {{ Form::label('name', __('Country')) }}
							{{ Form::select('country_id', $country, old('country_id',(isset($country_id)) ? $country_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Country','id' => 'country_id']) }}
						</div>
                        <div class="form-group col-md-3">
                            {{ Form::label('name', __('Branch')) }}
							{{ Form::select('branch_id', $branch, old('branch_id',(isset($branch_id)) ? $branch_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch','id' => 'branch_id']) }}
						</div>
						<div class="form-group col-md-3">
                            {{ Form::label('name', __('Company')) }}
							{{ Form::select('company_id', $company, old('company_id',(isset($company_id)) ? $company_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Company','id' => 'company_id']) }}
						</div>
						<div class="form-group col-md-3">
                            {{ Form::label('name', __('Currency')) }}
							{{ Form::select('currency_id', $currency, old('currency_id',(isset($currency_id)) ? $currency_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Currency','id' => 'currency_id']) }}
						</div>
						<div class="form-group col-md-3">
                            {{ Form::label('name', __('Model')) }}
							{{ Form::select('model_id', $model, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model','id' => 'model_id']) }}
						</div>
						<div class="form-group col-md-3">
                            {{ Form::label('name', __('Type')) }}
							{{ Form::select('type_id',['0'=>'Service','1'=>'Stock Service'] , old('type_id',(isset($type_id)) ? $type_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Type','id' => 'type_id']) }}
						</div>
						<div class="form-group col-md-3">
						  <button class="btn btn-primary search-btn">Search</button>
                          <a class="btn btn-primary reset-btn" href="{{route('report.spare')}}">Reset</a>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
				    <div class="dt-buttons float-right">
						<a class="btn btn-primary buttons-excel buttons-html5"  href="{{ url('report/spare/file-export?from='.$from.'&to='.$to.'&country_id='.$country_id.'&branch_id='.$branch_id.'&model_id='.$model_id.'&company_id='.$company_id.'&currency_id='.$currency_id.'&type_id='.$type_id.'') }}"><span>Excel</span></a>
					</div>
					  <div class="table-responsive">
						<table id="spare-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("report.spare-get") }}">
							<thead>
								<tr>
									<th width="5">#</th>
									<th>YEAR</th>
									<th>MONTH</th>
									<th>COUNTRY</th>
									<th>COMPANY</th>
									<th>BRANCH</th>
									<th>SPARE NAME</th>
									<th>SERVICE CALL </th>
                                    <th>AMOUNT</th>
								</tr>
							</thead>
						</table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
$(".reset-btn").click(function() {
    $("#branch_id").val(null).trigger("change");
    $("#country_id").val(null).trigger("change")
});
$(function() {
	$( 'input[name="from"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
	$( 'input[name="to"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
});

var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
var country_id =  getUrlParameter('country_id');
var branch_id =  getUrlParameter('branch_id');
var model_id =  getUrlParameter('model_id');
var company_id =  getUrlParameter('company_id');
var currency_id =  getUrlParameter('currency_id');
var type_id =  (getUrlParameter('type_id')?getUrlParameter('type_id'):0);

FTX.Utils.documentReady(function() {
    FTX.SPARE.list.init(from_date,to_date,country_id,branch_id,model_id,company_id,currency_id,type_id);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

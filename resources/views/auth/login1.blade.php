<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

</head>

<body>
    <div class="login-bg pt-5 pt-lg-0">
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100">
                <div class="col-lg-5 col-xl-3 d-lg-flex justify-content-center align-items-center">
                    <div class="card login bg-white py-5 px-5">
                        <div class="title">
                            <h3 class="text-dark">{{ __('Login') }}</h3>
                        </div>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror"
                                    name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-bottom">
                                <div class="custom-control custom-checkbox d-inline-block">
                                    <input class="custom-control-input" type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>

                                    <label class="custom-control-label text-dark" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </dAiv>
                                @if (Route::has('password.request'))
                                    <div class="forgot-pass">
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Password') }}
                                        </a>
                                    </div>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">
                                {{ __('Login') }}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

<?php

namespace App\Http\Controllers\Backend\Role;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Auth\Role;
use App\Http\Responses\RedirectResponse;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use Auth;
use DB;

class RoleController extends Controller
{

    public function __construct()
    {
        $module = $this->GetAllByStatus('module');
        
        foreach ($module as $m) {
            $res = array();
            $permissions = json_decode(json_encode($this->getModulePermission($m->id)),1);
            // echo "<pre>"; print_r($permissions); echo "</pre>"; exit;
            if(!empty($permissions))
            {
                $res['module_name'] = $m->name;
                $res['module_id'] = $m->id;
                $res['permissions'] = $permissions;
                $results[] = $res;
            }
            
        }
        $this->data['module'] = $results;
        View::share('js', ['roles','master']);
    }

    public function index()
    {
        return view('admin.roles.index');
    }


    public function create()
    {
        return view('admin.roles.create', $this->data);
    }

    public function edit($id)
    {
        // dd($id);

        $this->data['role'] = $this->GetByID('roles', $id);
        $this->data['edit_id'] = $id;
        $user_role = DB::table('user_roles') ->where('role_id', $id) ->get();
        $user_roles = json_decode(json_encode($user_role), 1);
        // echo "<pre>"; print_r($user_role); echo "</pre>"; exit;
        $userRole = [];
        if (count($user_roles) > 0)
        {
            foreach($user_roles as $ur)
            {
                $userRole[$ur['module_id']]=$ur;
            }
        }
        $this->data['user_role'] = $userRole;
        // echo "<pre>"; print_r($userRole); echo "</pre>"; exit;
        return view('admin.roles.create', $this->data);
    }



    public function store(Request $request)
    {
        // echo "<pre>";
        // print_r($request->input());
        // echo "</pre>";
        // exit;
        $post = array();
        $user_role = array();

        $validator = $this->validateRoleForm($request);
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post['name'] = $request['name'];
        $post['status'] = 1;
        $post['created_by'] = Auth::id();

        DB::beginTransaction();
        try {
            $role_id = $this->insertGetId('roles', $post);
            $user_role['role_id'] = $role_id;
            for ($i = 0; $i < count($request['module_id']); $i++) {
                if(isset($request['permission_id'][$i]) || isset($request['all'][$i]))
                {
                    $user_role['module_id'] = $request['module_id'][$i];
                    if(isset($request['all'][$i]) && $request['all'][$i]!='')
                    {
                        $user_role['all']=1;
                    }
                    else
                    {
                        $user_role['all']=0;
                    }
                    if(isset($request['permission_id'][$i]) && $request['permission_id'][$i]!='')
                    {
                        $user_role['permission_ids'] = implode(',', $request['permission_id'][$i]);
                    }
                    else
                    {
                        $user_role['permission_ids'] = "";
                    }
                    $this->insertdata('user_roles', $user_role);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors($e);
        }
        return redirect()->back()->with("success", "The role was successfully created.");
    }

    public function update(Request $request, $id)
    {
        
        // echo "<pre>"; print_r($request->input());  echo "</pre>"; exit;
        $validator = $this->validateRoleForm($request, $id);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = array();
        $user_role = array();

        $post['name'] = $request['name'];
        $post['updated_by'] = Auth::id();

        DB::beginTransaction();
        try {
            
            // $this->updatedata('roles', $post, $id);
            for ($i = 0; $i < count($request['module_id']); $i++) {
                $user_role['module_id'] = $request['module_id'][$i];
                if(isset($request['all'][$i]) && $request['all'][$i]!='')
                {
                    $user_role['all']=1;
                }
                else
                {
                    $user_role['all']=0;
                }
                if(isset($request['permission_id'][$i]) && $request['permission_id'][$i]!='')
                {
                    $user_role['permission_ids'] = implode(',', $request['permission_id'][$i]);
                }
                else
                {
                    $user_role['permission_ids'] = "";
                }
                if(isset($request['user_role_id'][$i]) && $request['user_role_id'][$i]!='')
                {
                    // echo "<pre>"; print_r($user_role);  echo "</pre>"; exit;
                    DB::table('user_roles')->where('id', $request['user_role_id'][$i])->update($user_role);
                }
                else
                {
                    $user_role['role_id'] = $id;
                    $this->insertdata('user_roles', $user_role);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors($e);
        }

        DB::beginTransaction();
        try {

            //$this->updatedata('user_roles', $user_role);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->withInput()->withErrors($e);
        }

        return redirect()->back()->with("success", "The Role was successfully updated.");
    }

    public function getRole()
    {
        //DB::statement(DB::raw('set @rownum=0'));
        $data = DB::table("roles")
            ->leftJoin('user_roles', 'roles.id', '=', 'user_roles.role_id')
            ->leftJoin('module', 'user_roles.module_id', '=', 'module.id')
            ->select('roles.*','user_roles.module_id as module_id', 'user_roles.permission_ids as permission_ids', 'user_roles.all as all', 'module.name as module')
            ->groupBy('user_roles.role_id')
            ->get();
            // echo "<pre>"; print_r($data); echo "</pre>"; exit;
        /*foreach ($data as $key => $d) {
            $permission = "";
            if ($d->permission_ids != NULL) {
                $permission_id = explode(',', $d->permission_ids);
                $permission_name=array();
                foreach ($permission_id as $p) {
                    $permission_name[] = DB::table('permissions')
                        ->where('id', $p)
                        ->first()->name;
                }
                $permission = implode("<br />", $permission_name);
            } else {
                $permissions = DB::table('permissions')
                    ->select('name')
                    ->where('module_id', $d->module_id)
                    ->get();
                    $permissionname=array();
                foreach ($permissions as $pn) {
                    $permissionname[] = $pn->name;
                }
                $permission = implode("<br />", $permissionname);
            }
            $data[$key]->permission = $permission;
        }*/
        return DataTables::of($data)
            ->escapeColumns(['name'])
            ->addColumn('actions', function ($data) {
                return '<a href="' . route('role.edit', $data->id) . '" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            })
            ->addColumn('status', function ($data) {
                if ($data->status == 1) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                return '<label class="switch "><input id="' . $data->id . '" data-id=' . $data->id . ' data-tb="roles" type="checkbox" value="' . $data->status . '" class="primary status_change" ' . $checked . ' ><span class="slider round"></span></label>';
            })
            ->addIndexColumn()
            ->editColumn('module', function ($data) {
                $sql = DB::table("user_roles")->leftJoin('module', 'user_roles.module_id', '=', 'module.id')->select('module.name as module','module.id as id','user_roles.permission_ids as permission_ids','user_roles.all as all')->where('user_roles.role_id', $data->id)->get();
                if(count($sql)>0) {
                foreach($sql as $k=>$v) {
                    if($v->permission_ids!="" && $v->all==0)
                    {
                        $permission = explode(',',$v->permission_ids);
                        $permissions = DB::table("permissions")->select('name')->whereIN('id',  $permission)->get();
                        foreach ($permissions as $key=>$value) {
                            $modules[$v->module][$key] =  $value->name;
                        }
                    }
                    if($v->all==1)
                    {
                        $modules[$v->module][] = "(all)";
                    }
                    if($v->permission_ids=="" && $v->all==0)
                    {
                        $modules[] = '';
                    }
                }
            } else {
                $modules[] = '';
            }
                
                $module = "";
                if(count($modules)>0){
                    foreach ($modules as $k=>$v) {
                        if($v) { 
                            $module.= $k ." : " .implode(", ",$v)."<br/>";
                        }
                    }
                }
                return $module;
            })->make(true);
    }

    public function getPermissions(Request $request)
    {
        $data = DB::table("permissions")
            ->where(['module_id' => $request->ref, 'status' => 1])
            ->get();
        $permissions = "";
        foreach ($data as $d) {
            $permissions .= '<option value="' . $d->id . '">' . $d->name . '</option>';
        }
        return response()->json(['permission' => $permissions]);
    }
}

<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use App\Http\Traits\CommonTrait;

class ForecastExport implements FromArray, WithHeadings, WithMapping
{
    protected $country_id,$model_id;

    use CommonTrait;

    function __construct($country_id, $model_id) {

        $this->data['country_id'] = $country_id;
        $this->data['model_id'] = $model_id;
        
    }

    public function array(): array
    {
        return $this->getForecastTableData($this->data);
    }

    public function headings(): array
    {
        return [
            
            'MODEL',
            'FOCUS CODE',
            'PRODUCT CODE',
            'PRODUCT DESCRIPTION',
            'ACTIVATION',
            'BALANCE QUANTITY OF HANDSET',
            'DATE OF FIRST ACTIVATION',
            'NO OF MONTHS FROM ACTIVATION',
            'USED SPARE COUNT IN WARRANTY',
            'USED SPARE COUNT IN OUT OF WARRANTY',
            'TOTAL USED SPARE',
            'EXPECTED MONTHLY AVERAGE USAGE IN WARRANTY',
            'EXPECTED MONTHLY AVERAGE USEAGE IN NON WARRANTY',
            'AVERAGE MONTHLY USAGE',
            'CURRENT STOCK',
            'DIFFERENCE'
        ];
    }
    
    public function map($forecast): array
    {
        return [
           
            $forecast['model'],
            $forecast['focus_code'],
            $forecast['part_code'],
            $forecast['description'],
            $forecast['activation_total'],
            $forecast['activation_balance'],
            $forecast['activation_date'],
            $forecast['activation_month'],
            $forecast['in_warranty_spare'],
            $forecast['out_warranty_spare'],
            $forecast['total_spare_used'],
            $forecast['average_usage_inwarranty'],
            $forecast['average_usage_outwarranty'],
            $forecast['total_average_usage'],
            $forecast['current_stock'],
            $forecast['difference'],
        ];
    }
}

<?php

namespace App\Http\Traits;
use DB;


trait PermissionTrait
{

    // public function GetId($table,$name){

    //     $row = DB::table($table)
    //             ->select("id")
    //             ->where("name", $name)
    //             ->first();
    //     return $row ? $row->id : null;

    // }
    
    public function GetPermission($role_id, $module_id){
        if($module_id){
            $result_type = DB::table('user_roles')->select("permission_ids", "all")->where("role_id", $role_id) ->where("module_id", "=", $module_id)->get()->first();
            if($result_type){
                return $result_type;
            }
        }

        return null;
        

    }

}
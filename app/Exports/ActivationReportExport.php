<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Http\Traits\CommonTrait;

class ActivationReportExport implements FromCollection, WithHeadings, WithMapping
{
    protected $from,$to;
    use CommonTrait;

    function __construct($from, $to) {

        $this->from = $from;
        $this->to = $to;
    }

    public function collection()
    {
        return $this->getActivationReportTableData($this->from, $this->to)->get();
    }

    public function headings(): array
    {
        return [
            'Sl',
            'Country',
            'Model',
            'IMEI-1',
            'IMEI-2',
            'SN',
            'Color',
            'RAM',
            'ROM',
            'Date Of Manufature',
            'PCB Serial No',
            'Act.Date',
            'Expiry Date'
        ];
    }
    
    public function map($activation): array
    {
        return [
            $activation->rownum,
            $activation->country_name,
            $activation->model_name,
            $activation->IMEI_no1,
            $activation->IMEI_no2,
            $activation->serial_no,
            $activation->color_name,
            $activation->ram_name,
            $activation->rom_name,
            ($activation->date_of_manufacture?date('d-m-Y',strtotime($activation->date_of_manufacture)):''),
            $activation->pcb_serial_no,
            ($activation->activation_date?date('d-m-Y',strtotime($activation->activation_date)):''),
            ($activation->expiry_date?date('d-m-Y',strtotime($activation->expiry_date)):'')
        ];
    }
}

<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Rules\MatchOldPassword;

trait GetTrait
{
    //Permission Form

    public function validatePermissionForm($request)
    {

        $rules = [
            'name' => ['required', 'max:191'],
            'module_id' => ['required'],
        ];

        $messsages = [
            'name.required' => 'Permission name must required',
            'name.max' => 'Permission name may not be greater than 191 characters.',
            'module_id.required' => 'Please select module',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetPermissionForm($post, $request, $id=NULL)
    {
        $post['name'] = $request->input('name');
        $post['module_id'] = $request->input('module_id');
        if ($id == NULL) {
            $post['status'] = 1;
        }
        return $post;
    }

    //Role Form

    public function validateRoleForm($request, $id=NULL)
    {
        // $permissions = '';
        // if(!$request->has('all') && !$request->has('permission_id'))
        // {
        //     $permissions = 'required';
        // }

        // $rules = [
        //     'name' => ['required', 'unique:roles,name,' . $id],
        //     'module_id' => ['required'],
        //     'permissions' => $permissions,
        // ];

        $rules = [
            'name' => ['required'],
        ];

        $messsages = [
            'name.required' => 'Role name must required',
            // 'module_id.required' => 'Please select module',
            // 'permissions.required' => 'You must select at least one permission for this role.',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetRoleForm($post, $request)
    {

        $input['status'] = $request->input('status');
        $all = $request->input('associated_permissions') == 'all' ? true : false;

        $post->name = $request->input('name');
        $post->all = $all;
        $post->sort = $request->input('sort');
        $post->status = (isset($input['status']) && $input['status'] == 1) ? 1 : 0;

        return $post;
    }

    //Master From

    public function validateMasterForm($request, $table, $id = null)
    {

        $rules = [
            'name' => ['required', 'max:191', 'unique:' . $table . ',name,' . $id],

        ];

        $messsages = [
            'name.required' => 'Permission name must required',
            'name.max' => 'Permission name may not be greater than 191 characters.',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetMasterForm($post, $request, $id = null)
    {
        $post['name'] = $request->input('name');
        if ($id == NULL) {
            $post['status'] = 1;
        }
        return $post;
    }



    // Currency From

    public function validateCurrencyForm($request, $id = null)
    {

        $rules = [
            'name' => ['required', 'max:191', 'unique:currency,name,' . $id],

            'rate' => ['required'],

        ];

        $messsages = [
            'name.required' => 'Currrency code must required',
            'name.max' => 'Currrency code may not be greater than 191 characters.',
            'rate.required' => 'Rate must required',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetCurrencyForm($post, $request, $id = null)
    {

        $post['name'] = $request->input('name');
        $post['rate'] = $request->input('rate');
        if ($id == NULL) {
            $post['status'] = 1;
        }
        return $post;
    }


    // Country From

    public function validateCountryForm($request, $id = null)
    {

        $rules = [
            'name' => ['required', 'unique:country,name,' . $id],
            'currency_id' => ['required'],
            'timezone' => ['required'],
            'warranty' => ['required'],
            'tax' => ['required'],
        ];

        $messsages = [
            'name.required' => 'Country code must required',
            'currency_id.required' => 'Currency must required',
            'timezone.required' => 'Time zone must required',
            'warranty.required' => 'Warranty must required',
            'tax.required' => 'Tax must required',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetCountryForm($post, $request, $id = null)
    {
        $post['name'] = $request->input('name');
        $post['currency_id'] = $request->input('currency_id');
        $post['time_zone'] = $request->input('timezone');
        $post['warranty'] = $request->input('warranty');
        $post['tax'] = $request->input('tax');
        if ($id == NULL) {
            $post['status'] = 1;
        }
        return $post;
    }




     // Spare From

     public function validateSpareForm($request, $id = null)
     {

         $rules = [
             'name' => ['required'],
            //  'partcode' => ['required', 'unique:spare,part_code,' . $id],
            //  'description' => ['required'],
             'focuscode' => ['required', 'unique:spare,focus_code,' . $id],
         ];

         $messsages = [
             'name.required' => 'Spare name must required',
            //  'partcode.required' => 'Part Code must required',
            //  'description.required' => 'Description must required',
             'focuscode.required' => 'Focus Code must required',
         ];

         $validator = Validator::make($request->all(), $rules, $messsages);
         return $validator;
     }
     public function GetSpareForm($post, $request, $id = null)
     {

         if($request->has('model_id'))
         {
            $post['model_id'] = implode(',',$request->input('model_id'));
         }
         $post['spare_name'] = $request->input('name');
         $post['part_code'] = $request->input('partcode');
        //  $post['description'] = $request->input('description');
         $post['focus_code'] = $request->input('focuscode');
         if($request->has('uniquecode'))
         {
            $post['unique_code'] = $request->input('uniquecode');
         }
         if ($id == NULL) {
             $post['status'] = 1;
         }
         return $post;
     }




    // Branch From

    public function validateBranchForm($request, $id = null)
    {

        $rules = [
            'name' => ['required', 'unique:branch,name,' . $id],
            'company_id' => ['required'],
            'code' => ['required', 'unique:branch,code,' . $id],
        ];

        $messsages = [
            'name.required' => 'Branch must required',
            'company_id.required' => 'Company must required',
            'code.required' => 'Code must required',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetBranchForm($post, $request, $id = null)
    {
        $post['name'] = $request->input('name');
        $post['company_id'] = $request->input('company_id');
        $post['country_id'] = $request->input('country_id');
        $post['code'] = $request->input('code');
        if ($id == NULL) {
            $post['status'] = 1;
        }
        return $post;
    }


    // Sofware Version From

    public function validateSoftwareVersionForm($request, $id = null)
    {

        $rules = [
            'name' => ['required'],
            'model_id' => ['required'],
        ];

        $messsages = [
            'name.required' => 'Version name must required',
            'model_id.required' => 'Model must required',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetSoftwareVersionForm($post, $request, $id = null)
    {
        $post['name'] = $request->input('name');
        $post['url'] = $request->input('url');
        $post['model_id'] = $request->input('model_id');
        if ($id == NULL) {
            $post['status'] = 1;
        }
        return $post;
    }

    // User From

    public function validateUserForm($request, $id = null)
    {
        $password = '';
        $username = '';
        if($id==NULL)
        {
            $username = 'required';
            $password = 'required';
        }

        $rules = [
            'branch_id' => ['required'],
            'role_id' => ['required'],
            'username' => $username,
            'name' => ['required'],

            //'email' => ['required', 'unique:users,email,'.$id],
            'mobile' => ['required', 'unique:users,phoneno,'.$id],
            'password' => $password,
        ];

        $messsages = [
            'branch_id.required' => 'Branch must required',
            'role_id.required' => 'Role must required',
            'username.required' => 'User name must required',
            'name.required' => 'Name must required',
            'email.required' => 'Email Id must required',
            'mobile.required' => 'Mobile must required',
            'password.required' => 'Password must required',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetUserForm($post, $request, $id = null)
    {
        $post['branch_id'] = $request->input('branch_id');
        $post['first_name'] = $request->input('name');
        $post['role_id'] = $request->input('role_id');
        $post['email'] = $request->input('email');
        $post['phoneno'] = $request->input('mobile');
        $post['username'] = $request->input('username');
        if ($id == NULL) {
            $post['status'] = 1;
        }
        return $post;
    }



    public function validateModelForm($request, $id = null)
    {

        $rules = [
            'name' => ['required', 'max:191', 'unique:models,name,'  . $id],
            'brand_id' => ['required'],
            'device_id' => ['required'],
            'factory_name' => ['required'],
            'addmore.*.color_id' => ['required'],
            // 'addmore.*.rom_id' => ['required'],
            // 'addmore.*.ram_id' => ['required'],
        ];

        $messsages = [
            'brand_id.required' => 'Please select brand',
            'device_id.required' => 'Please select device',
            'name.required' => 'Model name must required',
            'name.max' => 'Model name may not be greater than 191 characters.',
            'factory_name.required' => 'Factory name must required',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }


    public function GetModelForm($post, $request)
    {
        $post['name'] = $request->input('name');
        $post['brand_id'] = $request->input('brand_id');
        $post['device_id'] = $request->input('device_id');
        $post['factory_name'] = $request->input('factory_name');
        return $post;
    }


    public function validateLevelForm($request, $id = null){

        $rules = [
            'device_id' => ['required'],
            'name' => ['required', 'max:191'],
            //'arabic_name' => ['required', 'max:191', 'unique:levels,arabic_name,'  . $id],
            // 'rate' => ['required'],
        ];

        $messsages = [
            // 'arabic_name.required' => 'Level arabic name must required',
            'device_id.required' => 'Please select product type',
            // 'rate.required' => 'Rate must required',
            'name.required' => 'Level name must required',
            'name.max' => 'Model name may not be greater than 191 characters.',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetLevelForm($post, $request){
        $post->name = $request->input('name');
        $post->device_id = $request->input('device_id');
        $post->arabic_name = $request->input('arabic_name');
        // $post->rate = $request->input('rate');
        $post->service_type_id = ($request->input('service_type_id') ?? 0);
        return $post;
    }

    public function validateComplaintForm($request, $id = null){

        $rules = [
            'description' => ['required', 'unique:complaints,description,' . $id],
            //'arabic_description' => ['required'],
            // 'level_id' => ['required'],
        ];

        $messsages = [
            // 'arabic_description.required' => 'Complaint arabic description must required',
            'description.required' => 'Complaint description must required',
            // 'level_id.required' => 'Level must required',

        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }


    public function GetComplaintForm($post, $request){
        // $post->level_id = $request->input('level_id');
        $post->description = $request->input('description');
        // $post->arabic_description = $request->input('arabic_description');
        return $post;
    }

    public function validateAccessoriesForm($request, $id = null){

        $rules = [
            'type_id' => ['required',],
            //'arabic_description' => ['required'],
            'name' => ['required'],
        ];

        $messsages = [
            // 'arabic_description.required' => 'Complaint arabic description must required',
            'type_id.required' => 'Product Type must required',
            'name.required' => 'Name must required',

        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetAccessoriesForm($post, $request, $id = null){
        $post->product_type = $request->input('type_id');
        $post->name = $request->input('name');
        if ($id == NULL) {
            $post['status'] = 1;
        }
        return $post;
    }

    public function validateStockForm($request)
    {

        $rules = [
            'date' => ['required',],
            'branch_id' => ['required'],
            'addmore.*.spare_id' => ['required'],
            'addmore.*.qty' => ['required'],
        ];

        $messsages = [
            'date.required' => 'Please enter date',
            'branch_id.required' => 'Please select branch',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetServiceForm($post, $request, $id = null) {

       $accessories_id = (isset($request->accessories_id)?implode(",", $request->accessories_id):'');
        if($request->has('is_warranty'))
        {
            $post['is_warranty'] = $request->is_warranty;
        }

        $post['brand_id'] = $request->input('brand_id');
        $post['variant_id'] = $request->input('model_id');
        $post['sn_no'] = $request->input('sn_no');
        $post['sale_date'] = dateformat($request->input('sale_date'));
        $post['active_date'] = dateformat($request->input('active_date'));
        $post['manufacture_date'] = dateformat($request->input('manufacture_date'));
        $post['delivery_date'] = dateformat($request->input('delivery_date'));
        $post['version_id'] = $request->input('version_id');
        $post['technician_id'] = $request->input('technician_id');
        $post['security_code'] = $request->input('security_code');
        $post['remarks'] = $request->input('remarks');
        $post['accessories_id'] = $accessories_id;
        return $post;

    }

    public function GetTransferForm($post, $request) {
        $post['service_id'] = $request->service_id;
        $post['to_branch'] = $request->branch_id;
        $post['remarks'] = $request->transfer_remarks;
        return $post;
    }

    // Country From

    public function validateFAQForm($request, $id = null)
    {

        $rules = [
            'question' => ['required', 'unique:faq,question,' . $id],
            'answer' => ['required'],
        ];

        $messsages = [
            'question.required' => 'Question must required',
            'answer.required' => 'Answer must required',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function GetFAQForm($post, $request, $id = null)
    {
        $post['question'] = $request->input('question');
        $post['answer'] = $request->input('answer');
        if ($id == null) {
            $post['status'] = 1;
        }
        return $post;
    }


    public function GetDOAForm($post, $request, $id = null)
    {
        if($request->has('imei_id') && $request->input('imei_id')!='')
        {
            $post['imei_id'] = $request->input('imei_id');
        }
        else if($request->has('service_id') && $request->input('service_id')!='' && $request->input('service_id')!=0)
        {
            $post['variant_id']=$variant_id = $this->FieldByID("service","variant_id",$request->input('service_id'));
            $post['brand_id'] = $brand_id = $this->FieldByID("service","brand_id",$request->input('service_id'));
        }
        else
        {
            $post['variant_id'] = $request->input('model_id');
            $model_id = $this->FieldByID("variant_details","model_id",$request->input('model_id'));
            $post['brand_id'] = $this->FieldByID("models","brand_id",$model_id);
        }
        $post['remarks'] = $request->input('problem_remarks');
        $post['service_id'] = ($request->input('service_id')??0);
        $post['device_check'] = ($request->input('device_check')? implode(',',$request->input('device_check')):null);
        $post['is_special_request'] = ($request->input('is_special_request')??0);
        if ($id == NULL) {
            $post['status'] = 0;
        }
        return $post;
    }


    public function GetStockServiceForm($post, $request, $id = null)
    {
        if($request->has('defective_imei') && $request->input('defective_imei')!='')
        {
            if($request->input('stock_service_id')) {
                $post['new_version_id'] = $request->input('new_version');
            } else {
                $post['imei_id'] = $request->input('imei_id');
                $post['doa_id']  = $request->input('doa_id');
                $post['version_id'] = $request->input('version_id');
            }
        }
        else
        {
            $post['doa_id']  = $request->input('doa_id');
            $post['version_id'] = $request->input('version_id');
        }
        $post['remarks'] = $request->input('problem_remarks');
        $post['technician_id'] = $request->input('technician_id');
        $post['accessories_id'] = ($request->input('device_check')? implode(',',$request->input('device_check')):null);
        if ($id == NULL) {
            $post['status'] = 0;
        }
        return $post;
    }

    public function validateSpareSaleForm($request)
    {

        $rules = [
            'branch_id' => ['required'],
            'addmore.*.model_id' => ['required'],
            'addmore.*.spare_id' => ['required'],
            'addmore.*.qty' => ['required'],
            'addmore.*.price' => ['required'],
        ];

        $messsages = [
            'branch_id.required' => 'Please select branch',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }

    public function validateLevelRateForm($request){

        $rules = [
            'level_id' => ['required'],
            'company_id' => ['required'],
            'date' => ['required'],
            'rate' => ['required'],
        ];

        $messsages = [
            'level_id.required' => 'Please select level',
            'company_id.required' => 'Please select company',
            'date.required' => 'Please enter date',
            'rate.required' => 'Please enter rate',
        ];

        $validator = Validator::make($request->all(), $rules, $messsages);
        return $validator;
    }


    public function GetLevelRateForm($post, $request){
        $post->level_id = $request->input('level_id');
        $post->company_id = $request->input('company_id');
        $post->rate = $request->input('rate');
        $post->date = dateformat("01-".$request->input('date'));
        return $post;
    }


    public function validatePasswordForm($request){

        $rules = [
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ];

        $validator = Validator::make($request->all(), $rules);
        return $validator;

    }

    public function validateChangePasswordForm($request){

        $rules = [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['required','same:new_password'],
        ];

        $validator = Validator::make($request->all(), $rules);
        return $validator;

    }





}

@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Stock Report</h4>
            </div>
        </div>
        <div class="row">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'report.stock','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-stock']) }}
					<div class="form-row well">
						
                        <div class="form-group col-md-3">
                            {{ Form::label('name', __('Country')) }}
							{{ Form::select('country_id', $country, old('country_id',(isset($country_id)) ? $country_id:''), ['class' => 'form-control js-example-basic-single country', 'placeholder' => 'Select Country','id' => 'country_id']) }}
		
						</div>
                        <div class="form-group col-md-3">
                            {{ Form::label('name', __('Branch')) }}
							{{ Form::select('branch_id', $branch, old('branch_id',(isset($branch_id)) ? $branch_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch','id' => 'branch_id']) }}
						</div>
						<div class="form-group col-md-2">
						  <button class="btn btn-success search-btn">Search</button>
                          <a class="btn btn-success reset-btn" href="{{route('report.stock')}}">Reset</a>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
					  <div class="table-responsive">
                        <table id="stock-report-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("report.stock-get") }}">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                @if($branch_id || $country_id)
                                @foreach ($branch as $key=>$branch_name)
                                    @if($branch_id)
                                        @if($branch_id == $key)
                                          <th colspan="2">{{$branch_name}}</th>
                                        @endif
                                    @elseif($country_id)
                                        @if(array_key_exists($key,$country_branch_id)) 
                                          <th colspan="2">{{$branch_name}}</th>
                                        @endif
                                    @else
                                        <th colspan="2">{{$branch_name}}</th>
                                    @endif
                                @endforeach
                                @else 
                                    @foreach ($country as $key=>$country_name)
                                        <th colspan="2">{{$country_name}}</th>
                                    @endforeach
                                @endif
                                <th></th>
                            </tr>    
                            <tr>
                                <th>#</th>
                                <th>FOCUS CODE</th>
                                <th>PRODUCT CODE</th>
                                <th>PRODUCT DESCRIPTION</th>
                                @if($branch_id || $country_id)
                                    @foreach ($branch as $key=>$branch_name)
                                        @if($branch_id)
                                            @if($branch_id == $key)
                                            <th>QUANTITY</th>
                                            <th>VALUE</th>
                                            @endif
                                        @elseif($country_id)
                                            @if(array_key_exists($key,$country_branch_id)) 
                                            <th>QUANTITY</th>
                                            <th>VALUE</th>
                                            @endif
                                        @else
                                            <th>QUANTITY</th>
                                            <th>VALUE</th>
                                        @endif
                                    @endforeach
                                    @else
                                    @foreach ($country as $key=>$country_name)
                                        <th>QUANTITY</th>
                                        <th>VALUE</th>
                                    @endforeach
                                @endif
                                <th >TOTAL</th>
                            </tr>
                        </thead>
						</table>
                      
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
$(".reset-btn").click(function() {
    $("#branch_id").val(null).trigger("change");
    $("#country_id").val(null).trigger("change")
});
var branch_id =  getUrlParameter('branch_id');
var country_id =  getUrlParameter('country_id');
FTX.Utils.documentReady(function() {
    FTX.STOCK.list.init(country_id,branch_id);
});

</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

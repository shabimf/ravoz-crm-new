@extends('layouts.warranty')
@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                <h4 class="card-title-text">ACTIVATE WARRANTY</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('activation.list') }}"> <i
                            class="feather icon-list"></i> Activated IMEI List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['route' => 'activation.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-activation', 'enctype'=>"multipart/form-data"]) }}
            @csrf
            <div class="form-row">
                <div class="form-group col-md-12">
                    {{ Form::label('name', __('File')) }}
                    <input type="file" name="file" class="form-control" id="customFile">
                    <a class="pull-right" href="{{url('/uploads/format/activation.xlsx')}}"><small>Download Sample xlsx File <i class="fa fa-download"></i></small></a>
                </div>

            </div>
            <button class="btn btn-primary float-right">Import</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection


(function () {

    FTX.Levels = {

        list: {

            selectors: {
                level_table: $('#level-table'),
            },

            init: function () {

                this.selectors.level_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.level_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'device_type', name: 'device_type'},
                        { data: 'name', name: 'name' },
                        // { data: 'arabic_name', name: 'arabic_name' },
                        // { data: 'rate', name: 'rate' },
                        { data: 'service_type_id', name: 'service_type_id'},
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false },
                       
                      
                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();

@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
                <div class="add-new">
                    <a class="btn btn-primary" href="{{ route('model.create') }}"> <i class="feather icon-plus"></i> Create Model</a>
                </div>
            </div>
        </div>
        <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">Model Management</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="model-table" class="table table-striped table-bordered" data-ajax_url="{{ route("model.get") }}">
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <th>Brand</th>
					                <th>Model</th>
									<th>Factory Name</th>
									<th>Device</th>
									<th  width="100">Specifications</th>
					                <th  width="100">Actions</th>
									<th  width="100">Status</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Specifications</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	    <div id="model-data">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Models.list.init();
    });
	$(document).on('click', '#model-table tbody tr button', function() {
		var $this = $(this);
        id = $this.data("id");
		name = $this.data("name");
		brand = $this.data("brand");
		$.get("model/specification/"+id, function (data) {
           $('#model-data').html(data);
		   $("#exampleModal").modal("show");
		   $('.modal-title').text(name+'('+brand+') Specifications');
		});
    });
</script>
@endsection

@extends('layouts.app')

@section('content')
    {{-- <div class="main-panel">
        <div class="panel-hedding">
            <div class="row">
                 <div class="col-md-6 col-xl-6 col-xxl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-end">
                                <div class="mr-2">
                                    <h3 class="mb-2">456.23</h3>
                                    <span>Registered use</span>
                                    <p class="mb-0 mt-3"><i class="feather icon-trending-up pr-2 text-primary"></i> 5%
                                        higher than last week</p>
                                </div>
                                <button type="button" class="btn btn-lg btn-overlay-primary btn-icon ml-auto"> <i
                                        class="feather icon-user-check"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-6 col-xxl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-end">
                                <div class="mr-2">
                                    <h3 class="mb-2">200.45</h3>
                                    <span>Avg.Sessions</span>
                                    <p class="mb-0 mt-3"><i class="feather icon-trending-up pr-2 text-success"></i> 15%
                                        Bounce Rate Weekly</p>
                                </div>
                                <button type="button" class="btn btn-lg btn-overlay-success btn-icon ml-auto"> <i
                                        class="feather icon-user-check"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-6 col-xxl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-end">
                                <div class="mr-2">
                                    <h3 class="mb-2">265.32</h3>
                                    <span>Traffic Sources</span>
                                    <p class="mb-0 mt-3"><i class="feather icon-trending-up pr-2 text-info"></i> 5%
                                        higher than lst week</p>
                                </div>
                                <button type="button" class="btn btn-lg btn-overlay-info btn-icon ml-auto"> <i
                                        class="feather icon-user-check"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-6 col-xxl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-end">
                                <div class="mr-2">
                                    <h3 class="mb-2">658.26</h3>
                                    <span>Sessions</span>
                                    <p class="mb-0 mt-3"><i class="feather icon-trending-up pr-2 text-danger"></i>
                                        10.5% Completions Weekly</p>
                                </div>
                                <div id="statistics-sparkline-line-07" class="ml-auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-title">
                            <div class="card-title-left">
                                <h4 class="card-title-text">Manage Employees</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-centered table-hover mb-1">
                                    <thead>
                                        <tr>
                                            <th scope="col">Name </th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Office</th>
                                            <th scope="col">Phone</th>
                                            <th scope="col">Salary</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> Airi Satou</td>
                                            <td>airisatou@gmail.com</td>
                                            <td>802 Peninsula St. Madison, AL 35758</td>
                                            <td>(171) 555-2222 </td>
                                            <td> $162,700 </td>
                                            <td><a class="btn btn-overlay-primary text-primary btn-icon btn-sm mr-2 mb-1"
                                                    href="#"><i class="la la-edit"></i> </a><a
                                                    class="btn btn-overlay-danger text-danger btn-icon btn-sm mb-1"
                                                    href="#"><i class="fa fa-trash-o"></i> </a></td>
                                        </tr>
                                        <tr>
                                            <td> Brenden Wagner</td>
                                            <td>brendenwagner@gmail.com</td>
                                            <td>412 S. Green Hill St. Asheboro, NC 27205</td>
                                            <td>(313) 555-5735</td>
                                            <td> $5,56,700 </td>
                                            <td><a class="btn btn-overlay-primary text-primary btn-icon btn-sm mr-2 mb-1"
                                                    href="#"><i class="la la-edit"></i> </a><a
                                                    class="btn btn-overlay-danger text-danger btn-icon btn-sm mb-1"
                                                    href="#"><i class="fa fa-trash-o"></i> </a></td>
                                        </tr>
                                        <tr>
                                            <td> Caesar Vance </td>
                                            <td>caesarvance@gmail.com</td>
                                            <td>46 St Paul Ave. Dickson, TN 37055</td>
                                            <td>(503) 555-9931</td>
                                            <td> $56,700 </td>
                                            <td><a class="btn btn-overlay-primary text-primary btn-icon btn-sm mr-2 mb-1"
                                                    href="#"><i class="la la-edit"></i> </a><a
                                                    class="btn btn-overlay-danger text-danger btn-icon btn-sm mb-1"
                                                    href="#"><i class="fa fa-trash-o"></i> </a></td>
                                        </tr>
                                        <tr>
                                            <td> Vivian Harrell</td>
                                            <td>vivianharrell@gmail.com</td>
                                            <td>197 Hawthorne Rd. Beckley, WV 25801</td>
                                            <td>(204) 619-5731</td>
                                            <td> $13,465 </td>
                                            <td><a class="btn btn-overlay-primary text-primary btn-icon btn-sm mr-2 mb-1"
                                                    href="#"><i class="la la-edit"></i> </a><a
                                                    class="btn btn-overlay-danger text-danger btn-icon btn-sm mb-1"
                                                    href="#"><i class="fa fa-trash-o"></i> </a></td>
                                        </tr>
                                        <tr>
                                            <td> Airi Satou</td>
                                            <td>airisatou@gmail.com</td>
                                            <td>802 Peninsula St. Madison, AL 35758</td>
                                            <td>(171) 555-2222 </td>
                                            <td> $162,700 </td>
                                            <td><a class="btn btn-overlay-primary text-primary btn-icon btn-sm mr-2 mb-1"
                                                    href="#"><i class="la la-edit"></i> </a><a
                                                    class="btn btn-overlay-danger text-danger btn-icon btn-sm mb-1"
                                                    href="#"><i class="fa fa-trash-o"></i> </a></td>
                                        </tr>
                                        <tr>
                                            <td> Brenden Wagner</td>
                                            <td>brendenwagner@gmail.com</td>
                                            <td>412 S. Green Hill St. Asheboro, NC 27205</td>
                                            <td>(313) 555-5735</td>
                                            <td> $5,56,700 </td>
                                            <td><a class="btn btn-overlay-primary text-primary btn-icon btn-sm mr-2 mb-1"
                                                    href="#"><i class="la la-edit"></i> </a><a
                                                    class="btn btn-overlay-danger text-danger btn-icon btn-sm mb-1"
                                                    href="#"><i class="fa fa-trash-o"></i> </a></td>
                                        </tr>
                                        <tr>
                                            <td> Airi Satou</td>
                                            <td>airisatou@gmail.com</td>
                                            <td>802 Peninsula St. Madison, AL 35758</td>
                                            <td>(171) 555-2222 </td>
                                            <td> $162,700 </td>
                                            <td><a class="btn btn-overlay-primary text-sprimary btn-icon btn-sm mr-2"
                                                    href="#"><i class="la la-edit"></i> </a><a
                                                    class="btn btn-overlay-danger text-danger btn-icon btn-sm mb-1"
                                                    href="#"><i class="fa fa-trash-o"></i> </a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-title">
                            <div class="card-title-left">
                                <h4 class="card-title-text">Latest activity</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="activity">
                                <div class="activity-item">
                                    <div class="date">Sep 25</div>
                                    <div class="text">Responded to need <a href="single-need.html">“Volunteer
                                            opportunity”</a></div>
                                </div>
                                <div class="activity-item">
                                    <div class="date">Sep 24</div>
                                    <div class="text">Added an interest “Volunteer Activities”</div>
                                </div>
                                <div class="activity-item">
                                    <div class="date">Sep 23</div>
                                    <div class="text">Joined the group <a href="single-group.html">“Boardsmanship
                                            Forum”</a></div>
                                </div>
                                <div class="activity-item">
                                    <div class="date">Sep 21</div>
                                    <div class="text">Responded to need <a href="single-need.html">“In-Kind
                                            Opportunity”</a></div>
                                </div>
                                <div class="activity-item">
                                    <div class="date">Sep 18</div>
                                    <div class="text">Created need <a href="single-need.html">“Volunteer
                                            Opportunity”</a></div>
                                </div>
                                <div class="activity-item">
                                    <div class="date">Sep 24</div>
                                    <div class="text">Added an interest “Volunteer Activities”</div>
                                </div>
                                <div class="activity-item">
                                    <div class="date">Sep 17</div>
                                    <div class="text">Attending the event <a href="single-event.html">“Some New
                                            Event”</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-title">
                            <div class="card-title-left">
                                <h4 class="card-title-text">Maps world</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="world-map-markers" style="height: 320px;"></div>
                            <div class="row">
                                <div class="col mt-4">
                                    <div class="d-flex mb-2">
                                        <span>Completed</span>
                                        <span class="ml-auto">60%</span>
                                    </div>
                                    <div class="progress progress-h-5">
                                        <div class="progress-bar" role="progressbar" style="width: 60%;"
                                            aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="col mt-4">
                                    <div class="d-flex mb-2">
                                        <span>Completed</span>
                                        <span class="ml-auto">60%</span>
                                    </div>
                                    <div class="progress progress-h-5">
                                        <div class="progress-bar" role="progressbar" style="width: 60%;"
                                            aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="col mt-4">
                                    <div class="d-flex mb-2">
                                        <span>Completed</span>
                                        <span class="ml-auto">60%</span>
                                    </div>
                                    <div class="progress progress-h-5">
                                        <div class="progress-bar" role="progressbar" style="width: 60%;"
                                            aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-title">
                            <div class="card-title-left">
                                <h4 class="card-title-text">Statistics</h4>
                            </div>
                            <div class="card-title-right">
                                <span class="ml-auto">This months <i class="la la-angle-down"></i></span>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="d-flex border-bottom pb-3 mb-3 align-items-center">
                                <h6>Total visitors</h6>
                                <div class="ml-auto">
                                    <div id="statistics-sparkline-line"></div>
                                </div>
                                <div class="ml-4">
                                    <span class="d-block">1.2k</span>
                                    <span class="badge badge-overlay-success">+500</span>
                                </div>
                            </div>
                            <div class="d-flex border-bottom pb-3 mb-3 align-items-center">
                                <h6>Total Page</h6>
                                <div class="ml-auto">
                                    <div id="statistics-sparkline-line-02"></div>
                                </div>
                                <div class="ml-4">
                                    <span class="d-block">1.2k</span>
                                    <span class="badge badge-overlay-danger">+500</span>
                                </div>
                            </div>
                            <div class="d-flex border-bottom pb-3 mb-3 align-items-center">
                                <h6>Total page view</h6>
                                <div class="ml-auto">
                                    <div id="statistics-sparkline-line-03"></div>
                                </div>
                                <div class="ml-4">
                                    <span class="d-block">1.2k</span>
                                    <span class="badge badge-overlay-warning">+500</span>
                                </div>
                            </div>
                            <div class="d-flex border-bottom pb-3 mb-3 align-items-center">
                                <h6>Total Revenue</h6>
                                <div class="ml-auto">
                                    <div id="statistics-sparkline-line-04"></div>
                                </div>
                                <div class="ml-4">
                                    <span class="d-block">1.2k</span>
                                    <span class="badge badge-overlay-primary">+500</span>
                                </div>
                            </div>
                            <div class="d-flex border-bottom pb-3 mb-3 align-items-center">
                                <h6>Total sale</h6>
                                <div class="ml-auto">
                                    <div id="statistics-sparkline-line-05"></div>
                                </div>
                                <div class="ml-4">
                                    <span class="d-block">1.2k</span>
                                    <span class="badge badge-overlay-success">+500</span>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <h6>Total lose</h6>
                                <div class="ml-auto">
                                    <div id="statistics-sparkline-line-06"></div>
                                </div>
                                <div class="ml-4">
                                    <span class="d-block">1.2k</span>
                                    <span class="badge badge-overlay-info">+500</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-title">
                            <div class="card-title-left">
                                <h4 class="card-title-text">Weather today</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="d-flex mb-4">
                                <i class="wi wi-day-showers display-3 text-warning"></i>
                                <div class="ml-auto ">
                                    <h2 class="display-4">30°</h2>
                                    <span>San francisco, USA </span>
                                </div>
                            </div>
                            <p class="mb-5">Officia nam sed possimus repellat et, assumenda corporis velit The
                                secret of getting ahead is getting started.</p>
                            <div class="row mb-4">
                                <div class="col text-center mb-2">
                                    <h3 class="mb-1">31°</h3>
                                    <span>London UK</span>
                                </div>
                                <div class="col text-center mb-2">
                                    <h3 class="mb-1">02°</h3>
                                    <span>USA</span>
                                </div>
                                <div class="col text-center mb-2">
                                    <h3 class="mb-1">01°</h3>
                                    <span>Canada</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col text-center mb-2">
                                    <h3 class="mb-1">24°</h3>
                                    <span>India</span>
                                </div>
                                <div class="col text-center mb-2">
                                    <h3 class="mb-1">20°</h3>
                                    <span>Germany</span>
                                </div>
                                <div class="col text-center mb-2">
                                    <h3 class="mb-1">35°</h3>
                                    <span>France</span>
                                </div>
                            </div>
                            <a class="btn btn-light btn-block mt-4" href="#">View more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
@section('pagescript')
@include('inc.chart-js')
@endsection

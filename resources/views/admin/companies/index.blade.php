@extends('layouts.app')

@section('content')


<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
                <div class="add-new">
                    <a class="btn btn-primary" href="{{ url('admin/master/'.$module.'/create') }}"> <i class="feather icon-plus"></i> Create {{$module_name}}</a>
                </div>
            </div>
        </div>
        <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">{{$module_name}} Management</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="company-table" class="table table-striped table-bordered" data-ajax_url="{{ url('admin/master/'.$module.'/getData') }}">
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <th >Name</th>
                                    <th >Currency</th>
					                <th width="100">Actions</th>
                                    <th width="100">Status</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('pagescript')
@include('inc.datatables-js')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Company.list.init();
    });
</script>
@endsection

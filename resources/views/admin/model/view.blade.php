<table id="modaldata" class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
            <th>Color</th>
            <th>RAM</th>
            <th>ROM</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($model_details))
            @foreach($model_details as $k =>$v)
                <tr>
                  <td>{{$v->color_name}}</td>
                  <td>{{$v->ram_name}}</td>
                  <td>{{$v->rom_name}}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
@extends('layouts.service')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                <h4 class="card-title-text">View Purchase Order</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('request') }}"> <i
                            class="feather icon-list"></i> Purchase Order List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::model($list, ['route' => ['request.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-model']) }}
            @csrf
            <div class="form-row">
               <div class="form-group col-md-4">
                {{ Form::label('date', __('Date')) }}
                {{ Form::text('date', old('date',(isset($list)) ? date('d-m-Y', strtotime($list[0]->date)):''), ['class' => 'form-control', 'readonly' => 'readonly']) }}
               </div>
               <div class="form-group col-md-4">
                {{ Form::label('name', __('From Branch')) }}
                {{ Form::text('branch', old('branch',(isset($list)) ? $list[0]->from_name:''), ['class' => 'form-control', 'readonly' => 'readonly']) }}
               </div>
               <div class="form-group col-md-4">
                {{ Form::label('name', __('To Branch')) }}
                {{ Form::text('branch', old('branch',(isset($list)) ? $list[0]->to_name:''), ['class' => 'form-control', 'readonly' => 'readonly']) }}
               </div>
            </div>
            <table class="table table-bordered" id="dynamicTable">
                <thead>
                <tr>
                    <th>Model</th>
                    <th>Spare</th>
                    <th>Part Code</th>
                    <th>Focus Code</th>
                    <th>Quantity</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $row)
                  <tr>
                    <td>{{ ($row->model_name?$row->model_name:"Other") }}</td>
                    <td>{{ $row->spare_name }}</td>
                    <td>{{ $row->part_code }}</td>
                    <td>{{ $row->focus_code }}</td>
                    <td>
                      <input class="form-control" name="qty[{{$row->id}}]" type="number" value="{{(isset($row)) ? $row->qty:'0'}}" required>
                    </td>
                  </tr>
                @endforeach
                </tbody>
            </table>
            @if(isset($list))
              @if ($list[0]->status == 0)
                @if (userHasPermission('Service','purchase_order_convert') && auth()->user()->branch_id == $list[0]->to_branch)
                  <button type="submit" class="btn btn-primary float-right">Submit</button>
                @endif
              @endif
            @endif
            {{ Form::close() }}
        </div>
    </div>
@endsection


@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-6">
                <h4 class="card-title-text">{{((isset($user)) ? "Edit":'Create')}} User </h4>
            </div>
            <div class="col-md-6">
                <div class="add-new">
                    <a class="btn btn-primary" href="{{ route('user') }}"> <i class="feather icon-list"></i> User
                        List</a>
                </div>
            </div>
        </div>

        @include('inc.alert')
        @if (isset($edit_id))
        {{ Form::model($user, ['route' => ['user.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-role']) }}
        @else
        {{ Form::open(['route' => 'user.store','autocomplete' => 'nope','class' => 'form-horizontal','role' => 'form','method' => 'post','id' => 'create-role']) }}
        @endif
        @csrf
        <div class="row">
            <div class="col form-group">
                {{ Form::label('branch', __('Branch')) }}
                {{ Form::select('branch_id', $branch, old('branch_id', isset($user) ? $user->branch_id : ''), ['class' => 'form-control module','placeholder' => 'Select Branch','required' => 'required']) }}
            </div>
            <div class="col form-group">
                {{ Form::label('name', __('Name')) }}
                {{ Form::text('name', old('name', isset($user) ? $user->first_name : ''), ['class' => 'form-control','required' => 'required']) }}
                {{ Form::hidden('user_id', old('user_id', isset($user) ? $user->id : 0), ['class' => 'form-control','required' => 'required','id' => 'user_id']) }}
            </div>
        </div>
        @if(!isset($user))
        <div class="row">
            <div class="col form-group">
                {{ Form::label('username', __('User Name')) }}
                {{ Form::text('username',old('username', isset($user) ? $user->username : ' '), ['class' => 'form-control','required' => 'required', 'id' => 'username','autocomplete'=>'nope']) }}
            </div>
            <div class="col form-group">
                {{ Form::label('password', __('Password')) }}
                {{ Form::text('password','123456',['class' => 'form-control', 'required' => 'required']) }}
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col form-group">
                {{ Form::label('email', __('Email')) }}
                {{ Form::email('email', old('email', isset($user) ? $user->email : ''), ['class' => 'form-control', 'id' => 'email']) }}
            </div>
            <div class="col form-group">
                {{ Form::label('mobile', __('Mobile')) }}
                {{ Form::text('mobile', old('mobile', isset($user) ? $user->phoneno : ''), ['class' => 'form-control','required' => 'required', 'id' =>'phone']) }}
            </div>
        </div>

        <div class="row">
            <div class="col form-group">
                {{ Form::label('role', __('Role')) }}
                {{ Form::select('role_id', $roles, old('role_id', isset($user) ? $user->role_id : ''), ['class' => 'form-control module','placeholder' => 'Select Role','required' => 'required']) }}
            </div>
            @if(isset($user))
            <div class="col form-group">
                {{ Form::label('username', __('User Name')) }}
                {{ Form::text('username', old('username', isset($user) ? $user->username : ''), ['class' => 'form-control','required' => 'required', 'id' => 'username']) }}
            </div>
            @else
            <div class="col form-group">
            </div>
            @endif
        </div>
        @if(isset($user))
        @php
        $json = $user->multiple_access;
        $obj = json_decode($json,1);
        @endphp
        @endif

        <div class="row">
            <div class="col form-group">
                {{ Form::label('name', __('Multiple Access')) }}

                
                <div class="card-body">
                    <div class="custom-control custom-control-inline">
                        <input type="checkbox" id="customRadioInline1" name="multi_access" value="2" @if(isset($obj) && array_key_exists('country_id', $obj)) checked="checked" @endif class="custom-control-input chb">
                        <label class="custom-control-label" for="customRadioInline1">Country</label>
                    </div>
                    <div class="custom-control custom-control-inline">
                        <input type="checkbox" id="customRadioInline2" name="multi_access" value="3" @if(isset($obj) && array_key_exists('company_id', $obj)) checked="checked" @endif class="custom-control-input chb">
                        <label class="custom-control-label" for="customRadioInline2">Company</label>
                    </div>
                    <div class="custom-control custom-control-inline">
                        <input type="checkbox" id="customRadioInline3" name="multi_access" value="4" class="custom-control-input chb" @if(isset($obj) && array_key_exists('branch_id', $obj)) checked="checked" @endif @if(!isset($edit_id)) checked="checked" @endif  >
                        <label class="custom-control-label" for="customRadioInline3">Branch</label>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4 form-group access @if(isset($obj) && array_key_exists('country_id', $obj)) @else hide @endif" id="access2">
                {{ Form::label('country', __('Country')) }}
                {{ Form::select('country_id[]', $country, (isset($obj['country_id'])) ? $obj['country_id'] : "",  ['class' => 'form-control module js-example-basic-multiple', 'multiple' => 'multiple']) }}
            </div>
            <div class="col-md-4 form-group access @if(isset($obj) && array_key_exists('company_id', $obj)) @else hide @endif" id="access3">
                {{ Form::label('company', __('Company')) }}
                {{ Form::select('company_id[]', $company, (isset($obj['company_id'])) ? $obj['company_id'] : "",  ['class' => 'form-control module js-example-basic-multiple', 'multiple' => 'multiple']) }}
            </div>
            <div class="col-md-4 form-group access @if(isset($obj) && array_key_exists('branch_id', $obj)) @elseif(!isset($edit_id)) @else hide @endif" id="access4">
                {{ Form::label('branch', __('Branch')) }}
                {{ Form::select('multi_branch_id[]', $branch, (isset($obj['branch_id'])) ? $obj['branch_id'] : "",  ['class' => 'form-control module js-example-basic-multiple', 'multiple' => 'multiple']) }}
            </div>
        </div>
        <button type="submit" class="btn btn-success float-right">Submit</button>
        {{ Form::close() }}
    </div>
</div>
@endsection
@section('pagescript')

<!-- select2 -->
<script src="{{asset('js/select2/select2.min.js')}}"></script>

<!-- bloodhound -->
<script src="{{asset('js/select2/bloodhound.min.js')}}"></script>

<!-- Bootstrap tagsinput -->
<script src="{{asset('js/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>

<!-- tagsinput custom -->
<script src="{{asset('js/bootstrap-tagsinput/tagsinput.js')}}"></script>

<script>
    $(document).ready(function() {

        $("input[name$='multi_access']").click(function() {

            var test = $(this).val();

            $("div.access").hide();
            $("#access" + test).show();
        });
        $(".chb").change(function() {
            $(".chb").prop('checked', false);
            $(this).prop('checked', true);
        });

    });
</script>
<script type="text/javascript">
    var _commentForm = $( '#create-role' );
      var _validator = _commentForm.validate({
        rules: {
         branch_id: "required",
         username: {
            remote: {
                url: "{{ url('admin/user/varifyusername') }}",
                type: "GET",
                data: {
                    email: function () {
                        return $('#username').val();
                    },
                    user_id: function () {
                        return $('#user_id').val();
                    },
                }
            }
        },

         password:"required",
        //  email: {
        //     required: true,
        //     email: true,
        //     remote: {
        //         url: "{{ url('admin/user/varifyemail') }}",
        //         type: "GET",
        //         data: {
        //             email: function () {
        //                 return $('#email').val();
        //             },
        //             user_id: function () {
        //                 return $('#user_id').val();
        //             },
        //         }
        //     }
        // },

        mobile: {
            maxlength: 15,
            minlength: 10,
            remote: {
                url: "{{url('admin/user/varifycontact')}}",
                type: "GET",
                data: {
                    mobile: function () {
                        return $('#phone').val();
                    },
                    user_id: function () {
                        return $('#user_id').val();
                    },
                }
            }
        },
         role_id: "required",   
        },
		
        messages: {
            branch_id :  "Please select branch",
            name: "Please enter name",
            username:  {
                required: "Please enter user name",
                remote:"This username is already exists",
            },
            
            password : "Please enter password",
            // email: {
            //     required: "Please enter a valid email address",
            //     remote:"This email id is already exists",
            // },
           
            mobile:{
                required: "Please enter phone no",
                remote:"This phone number is already exists",
            },
            
            role_id :  "Please select role",
        }
      });

</script>
@endsection
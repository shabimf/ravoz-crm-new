<?php

namespace App\Http\Controllers\Backend\Model;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Models;
use App\Models\ModelDetails;
use Illuminate\Support\Facades\View;
use Auth;
use DB;

class ModelController extends Controller
{
    public function __construct()
    {
        $this->middleware('ajax-session-expired');
        $this->data['brand'] = $this->NameAndID('brands');
        $this->data['device'] = $this->NameAndID('product_type');
        $this->data['color'] = $this->NameAndID('color');
        $this->data['rom'] = $this->NameAndID('rom');
        $this->data['ram'] = $this->NameAndID('ram');
        $this->data['country'] = $this->NameAndID('country');
        View::share('js', ['model', 'master']);
    }
    public function index()
    {
        return view('admin.model.index');
    }
    public function getModelData()
    {
        $data = Models::leftJoin('brands', 'brands.id', '=', 'models.brand_id')
                ->leftJoin('product_type', 'product_type.id', '=', 'models.device_id')
                ->select(['models.id', 'models.name', 'models.status', 'models.factory_name', 'brands.name as brand', 'product_type.name as device'])
                ->get();
        return DataTables::of($data)->escapeColumns(['name'])->addColumn('actions', function ($data) {
            $action = '<a href="' . route('model.edit', $data->id) . '" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            return $action;
        })->addColumn('view', function ($data) {
            $view = '<button data-id=' . $data->id . ' data-name=' . $data->name . ' data-brand=' . $data->brand . '   data-toggle="tooltip" data-placement="top" title="View" class="btn btn-info btn-sm"><i class="feather icon-eye"></i></button>';
            return $view;
        })->addColumn('status', function ($data) {
            if ($data->status == 1) {
                $checked = "checked";
            } else {
                $checked = "";
            }
            return '<label class="switch "><input id="' . $data->id . '" data-id=' . $data->id . ' data-tb="models" type="checkbox" value="' . $data->status . '" class="primary status_change" ' . $checked . ' ><span class="slider round"></span></label>';
        })
        ->addIndexColumn()
        ->make(true);
    }
    public function create()
    {
        $this->data['edit_model_details'] = [];
        return view('admin.model.create', $this->data);
    }
    public function store(Request $request)
    {
        $validator = $this->validateModelForm($request, 0);
        if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);
        DB::beginTransaction();
        $country_id = '';
        if($request->has('country_id'))$country_id = implode(',',$request->input('country_id'));
        $id = DB::table('models')->insertGetId(['name' => $request->input('name'), 'brand_id' => $request->input('brand_id'), 'device_id' => $request->input('device_id'), 'factory_name' => $request->input('factory_name'), 'country_id' => $country_id, 'created_by' => Auth::id(),]);
        $results = $error = [];
        foreach ($request->addmore as $key => $value) {
            if (!empty($results[$value['color_id']][$value['rom_id']][$value['ram_id']])) {
                $error[] = "This specification already exists(color:" . $this->FieldByID('color', 'name', $value['color_id']) . ",RAM:" . $this->FieldByID('ram', 'name', $value['ram_id']) . ",ROM:" . $this->FieldByID('rom', 'name', $value['rom_id']) . ")";
            } else {
                $results[$value['color_id']][$value['rom_id']][$value['ram_id']] = $key;
            }
            $values['model_id'] = $id;
            DB::table('variant_details')->insertGetId(['model_id' => $id, 'color_id' => $value['color_id'], 'rom_id' => $value['rom_id'], 'ram_id' => $value['ram_id'], ]);
        }
        if (count($error) > 0) {
            return redirect()->back()->with("info", $error);
        } else {
            DB::commit();
            return redirect()->back()->with("success", "The model was successfully created.");
        }
    }
    public function update(Request $request, $id)
    {
        $validator = $this->validateModelForm($request, $id);
        if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);
        if (Models::where('name', $request['name'])->where('id', '!=', $id)->first()) {
            return redirect()->back()->with("error", "That model already exists. Please choose a different name.");
        }
        DB::beginTransaction();
        $country_id = '';
        if($request->has('country_id'))$country_id = implode(',',$request->input('country_id'));
        DB::table('models')->where('id', $id)->update(['name' => $request->input('name'), 'brand_id' => $request->input('brand_id'), 'device_id' => $request->input('device_id'), 'factory_name' => $request->input('factory_name'),'country_id'=>$country_id]);
        $results = $error = [];
        foreach ($request->addmore as $key => $value) {
            if (!empty($results[$value['color_id']][$value['rom_id']][$value['ram_id']])) {
                $error[] = "This specification already exists(color:" . $this->FieldByID('color', 'name', $value['color_id']) . ",RAM:" . $this->FieldByID('ram', 'name', $value['ram_id']) . ",ROM:" . $this->FieldByID('rom', 'name', $value['rom_id']) . ")";
            } else {
                $results[$value['color_id']][$value['rom_id']][$value['ram_id']] = $key;
            }
            $values['model_id'] = $id;
            if ($value['model_id'] > 0) {
                DB::table('variant_details')->where('id', $value['model_id'])->update(['model_id' => $id, 'color_id' => $value['color_id'], 'rom_id' => $value['rom_id'], 'ram_id' => $value['ram_id'], ]);
            } else {
                DB::table('variant_details')->insertGetId(['model_id' => $id, 'color_id' => $value['color_id'], 'rom_id' => $value['rom_id'], 'ram_id' => $value['ram_id'], ]);
            }
        }
        if (count($error) > 0) {
            return redirect()->back()->with("info", $error);
        } else {
            DB::commit();
            return redirect()->back()->with("success", "The model was successfully updated.");
        }
    }
    public function edit($id)
    {
        $this->data['edit_model'] = Models::find($id);
        $this->data['edit_model_details'] = $this->WhereModelByData($id);
        $this->data['edit_id'] = $id;
        return view('admin.model.create', $this->data);
    }
    public function getModelSpecifications($id)
    {
        $this->data['model_details'] = $this->WhereModelByData($id);
        return view('admin.model.view', $this->data);
    }
    public function destroy($id)
    {
        try {
            ModelDetails::whereId($id)->delete();
            return redirect()->back()->with('success', 'Specification deleted successfully');
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->back()->with('error', 'Cannot delete specification with associated records.');
        }
    }
}

@extends('layouts.service')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Stock List</h4>
            </div>
        </div>
        <div class="row  ">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'stock','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-service']) }}
					<div class="form-row well">
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Branch')) }}
							{{ Form::select('branch_id', $branch, old('branch_id',(isset($branch_id)) ? $branch_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch','id' => 'branch_id']) }}
						</div>
                        <div class="form-group col-md-3">
							{{ Form::label('name', __('Model')) }}
							{{ Form::select('model_id', $models, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model','id' => 'model_id']) }}
						</div>
						
						<div class="form-group col-md-3">
						  <button class="btn btn-success search-btn">Search</button>
                          <button class="btn btn-success reset-btn" type="reset">Reset</button>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="open-stock-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("stock.get") }}">
					        <thead>
					            <tr>
								    <th width="5">#</th>
					                <th width="80">Branch</th>
                                    <th>Model</th>
									<th>Spare</th>
									<th>Qty</th>
									<th>Average Price</th>
									
									<th>Action</th>
		
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
$(".reset-btn").click(function() {
    $("#branch_id").val(null).trigger("change");
});

var branch_id = getUrlParameter('branch_id');
var model_id =  getUrlParameter('model_id');
var visible = {{ (userHasPermission('Service','stock_edit')?1:0) }};
FTX.Utils.documentReady(function() {
    FTX.OPENSTOCK.list.init(branch_id,model_id,visible);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <style>
        .print_table th,
        .print_table td {
            padding: 6px;
            font-size: 12px;
        }

        .print_table th.bh {
            background: #E7E7E7;
            color: #000;
        }

        .btop {
            border-top: 1px solid #E7E7E7;
        }

        .fullwidth_ {
            width: 98%;
            margin: 0 auto;
        }

        .halfwidth_ {
            width: 49%;
            height: 100%;
            padding: 10px 5px;
            float: left;
            border-right: 0px dotted black;
        }
        .text-left {
  text-align: left;
}
.text-right {
  text-align: right;
}
.text-center {
  text-align: center;
}
.text-justify {
  text-align: justify;
}
.text-danger {
  color: #a94442;
}
a.text-danger:hover {
  color: #843534;
}
h4, h5, h6{
    font-weight: none;
}


th {
  text-align: left;
}
.table {
  width: 100%;
  max-width: 100%;
  margin-bottom: 20px;
}
.table > thead > tr > th,
.table > tbody > tr > th,
.table > tfoot > tr > th,
.table > thead > tr > td,
.table > tbody > tr > td,
.table > tfoot > tr > td {
  padding: 8px;
  line-height: 1.42857143;
  vertical-align: top;
  border-top: 1px solid #ddd;
}
.table > thead > tr > th {
  vertical-align: bottom;
  border-bottom: 2px solid #ddd;
}
.table > caption + thead > tr:first-child > th,
.table > colgroup + thead > tr:first-child > th,
.table > thead:first-child > tr:first-child > th,
.table > caption + thead > tr:first-child > td,
.table > colgroup + thead > tr:first-child > td,
.table > thead:first-child > tr:first-child > td {
  border-top: 0;
}
.table > tbody + tbody {
  border-top: 2px solid #ddd;
}
.table .table {
  background-color: #fff;
}
.table-condensed > thead > tr > th,
.table-condensed > tbody > tr > th,
.table-condensed > tfoot > tr > th,
.table-condensed > thead > tr > td,
.table-condensed > tbody > tr > td,
.table-condensed > tfoot > tr > td {
  padding: 5px;
}
.table-bordered {
  border: 1px solid #ddd;
}
.table-bordered > thead > tr > th,
.table-bordered > tbody > tr > th,
.table-bordered > tfoot > tr > th,
.table-bordered > thead > tr > td,
.table-bordered > tbody > tr > td,
.table-bordered > tfoot > tr > td {
  border: 1px solid #ddd;
}
.table-bordered > thead > tr > th,
.table-bordered > thead > tr > td {
  border-bottom-width: 2px;
}
.table-striped > tbody > tr:nth-of-type(odd) {
  background-color: #f9f9f9;
}
.table-hover > tbody > tr:hover {
  background-color: #f5f5f5;
}
table col[class*="col-"] {
  position: static;
  display: table-column;
  float: none;
}
table td[class*="col-"],
table th[class*="col-"] {
  position: static;
  display: table-cell;
  float: none;
}
.text-primary {
  color: #337ab7;
}
a.text-primary:hover {
  color: #286090;
}
.text-danger {
  color: #a94442;
}
a.text-danger:hover {
  color: #843534;
}
.text-success {
  color: #3c763d;
}
a.text-success:hover {
  color: #2b542c;
}
.pull-right {
  float: right !important;
}

    </style>
</head>

@php
$status = 0;
$country = 10;
$model_id = 0;
$ec = 1;
$rwarranty_provider = 1;
$tax_percentage = 0;
$accessories = '';
$row_bill['branch_id'] = 1;
$trn = "";
$row_bill['bill_remarks'] ="";
@endphp

<body>
    <div class="">

        <div class="">

            <div style="border-bottom:1px solid grey; ">
                <div style="width:30%; float:left;padding-top:20px;">
                    <img src="{{ asset('images/logo.png') }}" width="150"><br />
                </div>
                <div style="width:40%; float:left">
                    <br /><br /><br /><br />
                    @if ($status)
                        <h4 class="text-center">
                            Delivery Note
                        </h4>
                    @else
                        <h4 class="text-center">
                            @if ($tax_percentage > 0) @endif DELIVERY NOTE
                            @if ($country != 10) مذكرة التسليم
                                @if ($tax_percentage > 0) @endif
                            @endif
                        </h4>
                    @endif
                </div>
                <div style="width:30%; float:right; text-align:right;font-size:14px">
                    <br />
                    <p> <span class="text-danger">CBECBAD22051466</span> <br /> <small>
                            {{ date('d/m/Y', strtotime('2022/05/13')) }} </small>

                        <br /> <small>
                            @if ($country == 10) @php
                                /*echo "GSTIN No. : ".$trn;*/
                            @endphp
                            @elseif($country == 5)
                                @php echo "CRN NO : ".$trn; @endphp
                            @else
                                @php echo "TRN : ".$trn; @endphp
                            @endif
                        </small>


                    </p>
                </div>
            </div>
            <br />
            @if ($status)
                <h5>To : Sudhakaran </h5>
                <h6>{{$row_bill['bill_remarks']}}</h6>
                <table class="table  table-condensed print_table">
                    <tr>
                        <th class=" bh" colspan="2">Particulars</th>
                        <th width="80" class="text-right bh">Quantity </th>
                        <th width="80" class="text-right bh">Amount </th>
                        <th width="80" class="text-right bh">Total</th>
                    </tr>
                    <tr>
                        <th colspan="5"><small> Device : Realme8 ( REALME ) <br /> IMEI : </small> <br />
                            <small> Task : DISPLAY BROKEN <br />

                                @php
                                    $status = 0;
                                    $row_sp['spare_name'] = 'FLEX CABLE CAMERA NO4 7900 ORG';
                                @endphp
                                @if (!$status)
                                    @php
                                        echo 'Spare Used : ';
                                        echo "<br/><span style='font-style:italic; font-weight:normal'>FLEX CABLE CAMERA NO4 7900 ORG</span>";
                                    @endphp
                                @endif
                            </small>
                        </th>
                    </tr>
                    <tr>
                        <td align="right" width="30">1.</td>
                        <td>Service Charge</td>
                        <td class="text-right">1</td>
                        <td class="text-right">3,400.00</td>
                        <td class="text-right"> 3,400.00 </td>
                    </tr>
                    @php
                        $amount=0;
                    @endphp
                    @if ($status)
                        @if ($amount != '')
                            <tr>
                                <td align="right" width="30">2.</td>
                                <td>Advance Received</td>
                                <td align="right" width="30">1</td>
                                <td align="right" width="30">- {{ $amount }} </td>
                                <td align="right" width="30">- {{ $amount }} </td>
                            </tr>
                        @endif
                    @endif

                    <tr>
                        <td colspan="4" class="text-right btop  text-primary"> TOTAL</td>
                        <td class="text-right  text-primary btop">3400.00
                        </td>
                    </tr>
                    @if (!$status1)
                        @if ($tax_percentage > 0)
                            <tr>
                                <td colspan="4" class="text-right btop text-danger">
                                    @if ($country == 10)
                                        @php echo "GST"; @endphp
                                    @else
                                        @php echo "VAT"; @endphp ({{ $tax_percentage }}%)
                                    @endif
                                </td>
                                <td class="text-right text-danger btop">612.00</td>
                            </tr>
                        @endif
                    @endif
                    <tr>
                        <td colspan="4" class="text-right btop text-success"> <small>Rounded</small><strong> Net Total (In INR ) </strong> </td>
                        <td class="text-right  text-success btop">
                            <strong>4012.00</strong>
                        </td>
                    </tr>

                </table>
                <br />

                <div style="width: 49%" class="pull-right text-right">
                    <!--<p > <span > Cashier</span></p>-->
                </div>
            @else
                <h5>To الى : sudhakaran </h5>
                <h6>{{$row_bill['bill_remarks']}}</h6>
                <table class="table  table-condensed print_table">
                    <tr>
                        <th class=" bh" colspan="2">Particulars البيان</th>
                        <th width="80" class="text-right bh">Quantity العدد</th>
                        <th width="80" class="text-right bh">Amount السعر </th>
                        <th width="80" class="text-right bh">Total الإجمالى</th>
                    </tr>
                    <tr>
                        <th colspan="5"><small> Device الجهاز :Realme8 ( REALME )<br /> IMEI :
                                 </small> <br />
                            <small> Task المهمة : DISPLAY BROKEN الشاشة مكسورة <br />
                                @php
                                    $status = 1;
                                @endphp
                                @if (!$status)
                                    @php
                                        echo 'Spare Used :';
                                        echo "<br/><span style='font-style:italic; font-weight:normal'>FLEX CABLE CAMERA NO4 7900 ORG</span>";
                                    @endphp
                                @endif
                            </small>
                        </th>
                    </tr>

                    <tr>
                        <td align="right" width="30">1.</td>
                        <td>Service Charge  رسوم الخدمه </td>
                        <td class="text-right">1</td>
                        <td class="text-right">3400.00</td>
                        <td class="text-right"> 3400.00 </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="text-right btop  text-primary"> TOTAL الإجمالى</td>
                        <td class="text-right  text-primary btop">3400.00</td>
                    </tr>
                    @if ($tax_percentage > 0)
                        <tr>
                            <td colspan="4" class="text-right btop text-danger">
                                @if ($country == 10) @php echo "GST"; @endphp
                                @else
                                    @php echo "VAT"; @endphp ({{ $tax_percentage }}%)
                                @endif
                            </td>
                            <td class="text-right text-danger btop">612.00</td>
                        </tr>
                    @endif
                    <tr>
                        <td colspan="4" class="text-right btop text-success"> <small>Rounded</small><strong> Net Total
                                التقريبى الصافي الإجمالى( In INR ) </strong> </td>
                        <td class="text-right  text-success btop">
                            <strong>4012.00</strong>
                        </td>
                    </tr>
                </table>
                <br />
                <div style="width: 49%" class="pull-right text-right">
                    <p> <span> Cashier الكاشير </span></p>
                </div>
            @endif
        </div>
    </div>


    @if ($row_bill['branch_id'] == 173)


        <div style="width:  48% ; float:left;  ">
            <h6 style="font-size:10px">Terms and Conditions</h6>
            <ul style="font-size:8px; padding:0 ">
                <li>The device has warranty against the defects subject to the conditions given below </li>
                <li>The warranty included only the spare parts which has been replaced </li>
                <li>Delivery note is mandatory to claim the warranty</li>
            </ul>
            <br>
            <h6 style="font-size:9px">Warranty is void under the following circumstances:</h6>

            <ul style="font-size:8px; padding:0 ">
                <li>Device was misused, mishandled improperly tested neglected, altered or defected in anyway </li>
                <li>The device became defective resulting from the usage of non-approved accessories that were connected
                    to the device</li>
                <li>Device was serviced, repaired or altered by any person other than easycare service center and/or
                    technician </li>
                <li>Drop damage or damage caused by water or other liquid </li>
                <li>Deterioration due to natural were & tear, humidity, rest etc.</li>
                <li>Alteration Or tampering of the imei number or serial code on this device </li>
                <li>Standard warranty period -3 months from the date of purchase </li>
            </ul>
        </div>

        <div style="width:48%; float:right;text-align:right;  direction:rtl;">
            <h6 style="font-size:10px">الشروط و الأحكام </h6>
            <ul style="font-size:8px; padding:0 ">
                <li>يحمل هذا الجهاز ضمانا من الاعطـال حسب الشروط المنصوص عليها ادناه </li>
                <li> ويشمل الضمان قطع الغيار التي تم استبدالها </li>
                <li>من الضروري ان يتم استعراض امر التسليم عند المطالبة بحق الضمان </li>
            </ul>
            <br>
            <h6 style="font-size:9px"> ويعتبر الضمان لاغيا في الظروف التالية :</h6>

            <ul style="font-size:8px; padding:0 ">
                <li>إذا تمت إساءة استخدام الجهاز او تم فحصه والكشف عليه او تعديله </li>
                <li>اذا تضرر الجهاز جراء استخدام مستلزمات او اكسسوارات غير معتمدة تم توصيلها بالجهاز</li>
                <li>اذا تمت صيانة الجهاز واصلاحه او تغييره في أي مكان غير مكان الصيانة المعتمد والفنيين المعتمدين</li>
                <li>إذا تضرر بسبب سقوطه او غمره بالمياه او أي سائل آخر </li>
                <li>إذا اهترئ بسبب التقادم الاعتيادي او الرطوبة او الصدأ، الــخ</li>
                <li>تغيير او إزالة رقم التسلسل للجهاز </li>
                <li>يستمر ضمان الجهاز الاعتيادي لمدة 3 اشهر من تاريخ الشراء </li>
            </ul>
        </div>
        <div class="claearfix"></div>

        <div style="width:100%">
            <hr>
            <p> <b>------------------------------------<br /> Device Received <br /> Customer Signature </b></p><br />
        </div>

    @endif
     <!--<hr />


    <p style="font-size:10px; ">
        Electra Street, Behind Pink Building, Abudhabi, +971 2 6444021,+971 05 64820676
    </p> -->
</body>

</html>

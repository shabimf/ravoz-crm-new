(function() {
    FTX.SERVICE = {

        list: {

            selectors: {
                Service_table: $('#service-table'),
            },

            init: function(branch_id, model_id, from, to, nums, serial_num, status, status_id, warranty) {
                this.selectors.Service_table.dataTable({
                    processing: false,
                    serverSide: true,
                    cache: false,
                    iDisplayLength: 100,
                    ajax: {
                        url: this.selectors.Service_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.branch_id = branch_id,
                                d.model_id = model_id,
                                d.nums = nums,
                                d.serial_num = serial_num,
                                d.status = status,
                                d.status_id = status_id,
                                d.warranty = warranty
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'branch_name', name: 'branch_name' },
                        { data: 'customer_name', name: 'customer_name' },
                        // { data: 'customer_phone', name: 'customer_phone'},
                        { data: 'is_warranty', name: 'is_warranty', searchable: false },
                        { data: 'brand_name', name: 'brand_name' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'status', name: 'status', searchable: false, sortable: false },
                        { data: 'is_approved', name: 'is_approved', searchable: false, sortable: false }
                    ],
                    order: [
                        [1, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
    FTX.APPROVE = {

        list: {

            selectors: {
                Approve_request_table: $('#approve-request-table'),
            },

            init: function() {
                this.selectors.Approve_request_table.dataTable({
                    processing: false,
                    serverSide: true,
                    cache: false,
                    ajax: {
                        url: this.selectors.Approve_request_table.data('ajax_url'),
                        type: 'post'
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'branch_name', name: 'branch_name' },
                        { data: 'customer_name', name: 'customer_name' },
                        { data: 'customer_phone', name: 'customer_phone' },
                        { data: 'is_warranty', name: 'is_warranty', searchable: false },
                        { data: 'brand_name', name: 'brand_name' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [
                        [1, "desc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();
<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;

class InvoiceController extends Controller
{
    public function generateInvoicePDF() {
        $pdf = PDF::loadView('admin.invoice.pdf');
        return $pdf->stream('invoice.pdf');
    }

    public function generateServicePDF() {
        $pdf = PDF::loadView('admin.invoice.service');
        return $pdf->stream('service.pdf');
    }
}

@extends('layouts.service')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Stock Service</h4>
            </div>
			<div class="col-md-3">
				<div class="add-new">
				  <a class="btn btn-primary" data-toggle="modal" data-target="#stocksaleModal">
					<i class="feather icon-plus"></i> Add Stock Service
				  </a>
				</div>
            </div>

        </div>
        <div class="row  ">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'stock_service.index','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-service']) }}
					<div class="form-row well">

						<div class="form-group col-md-2">
							{{ Form::label('name', __('Date From')) }}
							{{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control','id'=>'datepicker' ]) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Date To')) }}
							{{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control', 'id' => 'dates']) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Dead IMEI')) }}
							{{ Form::text('imei', old('imei',(isset($imei)) ? $imei:''), ['class' => 'form-control']) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Branch')) }}
							{{ Form::select('branch_id', $branch, old('branch_id',(isset($branch_id)) ? $branch_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch','id' => 'branch_id']) }}
						</div>

						<div class="form-group col-md-2">
						  <button class="btn btn-success search-btn">Search</button>
                          <button class="btn btn-success reset-btn" type="reset">Reset</button>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="stock-service-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("stock_service.get") }}">
					        <thead>
					            <tr>
								    <th width="5">#</th>
					                <th width="80">Date</th>
									<th>Serial Num.</th>
									<th>Branch</th>
					                <th>Device</th>
									<th>IMEI</th>
                                    <th>Serial No</th>
									<th>Status</th>
									<th></th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
<div class="modal fade" id="stocksaleModal" tabindex="-1" role="dialog" aria-labelledby="stocksaleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
	<div class="modal-header">
        <h5 class="modal-title">STOCK SERVICE</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body" id="doa_area">
	    <div class="alert alert-success alert-dismissible doa_msg hide">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			Stock Service <strong>successfully created!</strong>
        </div>
      <form action="" method="post" id="doa_form" name="doa_form">
        <table class="table table-bordered table-condensed doa_table">
          <tbody>
            <tr>
              <th class="success imei-det"> {{ Form::label('name', __('IMEI/SL')) }}</th>
              <th class="success model-det d-none">{{ Form::label('name', __('DOA Reference')) }}</th>
              <td colspan="3">
                <div class="imei-det">
                    <div class=" input-group">
                        {{ Form::text('defective_imei', '', ['class' => 'form-control imei_add', 'id' => 'imei_selected']) }}
                        {{ Form::hidden('imei_id', '', ['class' => 'form-control', 'id' => 'imei_id']) }}
                        {{ Form::hidden('doa_id', '', ['class' => 'form-control', 'id' => 'doa_id']) }}
                        <div class="input-group-append">
                            {{ Form::button('Check', ['class' => 'btn btn-overlay-dark btns check_replace_imei cp', 'id' => 'search']) }}
                        </div>
                    </div>
                    <a href="javascript:void(0);" id="doa_ref">Enter DOA Reference</a>
                </div>
                <div class="model-det d-none">
                    <div class=" input-group">
                        {{ Form::text('defective_device', '', ['class' => 'form-control imei_add', 'id' => 'doa_reference']) }}
                        <div class="input-group-append">
                            {{ Form::button('Check', ['class' => 'btn btn-overlay-dark btns check_doa cp', 'id' => 'doa_search']) }}
                        </div>
                    </div>
                    <a href="javascript:void(0);" id="imei_ref">Enter IMEI/SL</a>
                </div>
              </td>
            </tr>
			<tr >
              <th class="success" width="20%">IMEI-1</th>
              <td id="imei_no_1" width="30%"></td>
			  <th class="success" width="20%">IMEI-2</th>
              <td id="imei_no_2" width="30%"></td>
            </tr>
            <tr>
              <th class="success" width="20%">Model No</th>
              <td id="model_name" width="30%"></td>
			  <th class="success" width="20%">Status</th>
              <td id="billed_status" width="30%"> </td>
            </tr>
			<tr>
              <th class="success">Technician</th>
              <td colspan="3">
			    {{ Form::select('technician_id', $users, old('technician_id'), ['class' => 'js-example-basic-single', 'placeholder' => 'Select Technician', 'id' => 'technician_id']) }}
			  </td>
            </tr>
            <tr>
                <th class="success">Software Version</th>
                <td colspan="3">
                    <select class="form-control js-example-basic-single" id="version" name="version_id"
                            placeholder="Select a Version">  </select>
                </td>
            </tr>
			<tr>
              <th class="success">Complaint</th>
              <td colspan="3">
			  <select class="js-example-basic-multiple select2-hidden-accessible" name="complaint[]" multiple="">
				@foreach ($levels as $key => $complaint)
					<optgroup label="{{ $key }}">
						@foreach ($complaint as $k => $v)
							<option value="{{ $k }}">{{ $v }}</option>
						@endforeach
					</optgroup>
				@endforeach
			  </select>
			  </td>
            </tr>
			<tr>
              <th class="success">Remarks</th>
              <td colspan="3">
			   {{ Form::textarea('problem_remarks', '', ['class' => 'form-control', 'id' => 'problem_remarks']) }}
              </td>
            </tr>
			<tr>
                <th width="25%" class="success">Check List</th>
                <td colspan="3">
					@foreach ($access as $k => $v)
						<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" id="accessories_{{ $k }}"
								value="{{ $k }}" name="device_check[]"
								class="custom-control-input form-control">
							<label class="custom-control-label"
								for="accessories_{{ $k }}">{{ $v }}</label>
						</div>
					@endforeach
                </td>
            </tr>
          </tbody>
        </table>
        <hr>
		<input id="stock_service_id" type="hidden" name="stock_service_id" value="0"/>
        <input type="submit" class="btn btn-success imei_success pull-right" name="issue_doa" value="Register">
        <div class="clearfix"></div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
$(".reset-btn").click(function() {
    $("#branch_id").val(null).trigger("change");
});
$(function() {
	$( 'input[name="from"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
	$( 'input[name="to"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
});

var branch_id = getUrlParameter('branch_id');
var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
var imei =  getUrlParameter('imei');
var is_approved = getUrlParameter('is_approved');
FTX.Utils.documentReady(function() {
    FTX.STOCKSERVICE.list.init(branch_id,from_date,to_date,imei,is_approved);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

@extends('layouts.warranty')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                <h4 class="card-title-text">Create Sale</h4>
                </div>
                <div class="col-md-6 {{(userHasPermission('Warranty','sales_list')?'':'hide')}}">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('sale') }}"> <i
                            class="feather icon-list"></i> Sales - List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['route' => 'sale.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-sales', 'enctype'=>"multipart/form-data"]) }}
            @csrf
            <div class="form-row">
               {{-- <div class="form-group col-md-6">
                    {{ Form::label('name', __('Branch')) }}
                    {{ Form::select('branch_id', $branch, old('branch_id',Auth::user()->branch_id), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch', 'required' => 'required']) }}
                </div> --}}
                <div class="form-group col-md-12">
                    {{ Form::label('name', __('File')) }}
                    <input type="file" name="file" class="form-control" id="customFile">
                    <a class="pull-right" href="{{url('/uploads/format/customer_sale.xlsx')}}"><small>Download Sample xlsx File <i class="fa fa-download"></i></small></a>
                </div>

            </div>
            <button class="btn btn-primary float-right">Import</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/select2/bloodhound.min.js')}}"></script>
@endsection

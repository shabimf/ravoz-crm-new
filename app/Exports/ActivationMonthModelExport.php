<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use App\Http\Traits\CommonTrait;

class ActivationMonthModelExport implements FromArray
{
    protected $from,$to,$country_id,$model_id;

    use CommonTrait;

    function __construct($from, $to, $country_id, $model_id) {
        $this->data['from'] = $from;
        $this->data['to'] = $to;
        $this->data['country_id'] = $country_id;
        $this->data['model_id'] = $model_id;

    }

    public function array(): array
    {
        
        return $this->getMonthWiseActivationTableData($this->data);
    }


}

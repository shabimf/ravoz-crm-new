@extends('layouts.warranty')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
	    <div class="row mb-4">
            <div class="col-md-12 text-right">
			    <a class="btn btn-primary {{(userHasPermission('Warranty','imei_list')?'':'hide')}}" href="{{ route('devices.imeilist') }}">
					<i class="feather icon-list"></i> Uploaded IMEI List
				</a>
				<a class="btn btn-primary ml-2 {{(userHasPermission('Warranty','import_imei')?'':'hide')}}" href="{{ route('devices.create') }}">
				   <i class="feather icon-plus"></i> Import Warranty Devices
				</a>
            </div>
        </div>

        <div class="row">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'devices','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-imei']) }}
					<div class="form-row well">
					    <div class="form-group col-md-3">
							{{ Form::label('name', __('Country')) }}
							{{ Form::select('country_id', ['-1'=>'Freezone'] +$country , old('country_id',(isset($country_id)) ? $country_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Country']) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Model')) }}
							{{ Form::select('model_id', $model, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model']) }}
						</div>
						<div class="form-group col-md-6">
						   <button class="btn btn-primary search-btn">Search</button>
                           <a class="btn btn-primary reset-btn"  href="{{route('devices')}}">Reset</a>
						   <a class="btn btn-primary float-right reset-btn" href="{{ url('warranty/devices/export?country_id='.$country_id.'&model_id='.$model_id.'') }}">Excel</a>
                        </div>
					</div>
				{{ Form::close() }}
				
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">IMEI List</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="device-table" class="table table-striped table-bordered" data-ajax_url="{{ route("devices.get") }}">
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <th>Country</th>
									<th>Model</th>
					                <th>IMEI-1</th>
									<th>IMEI-2</th>
									<th>SN</th>
									<th>Color</th>
									<th>RAM</th>
									<th>ROM</th>
									<th>Date Of Manufature</th>
									<th>PCB Serial No</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('pagescript')
@include('inc.datatables-js')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script>
	var para = getUrlParameter('id');
	var country_id = getUrlParameter('country_id');
	var model_id = getUrlParameter('model_id');
    FTX.Utils.documentReady(function() {
        FTX.Devices.list.init(para,country_id,model_id);
    });
</script>
@endsection

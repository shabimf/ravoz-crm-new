<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Http\Traits\CommonTrait;

class IMEIExport implements FromCollection, WithHeadings, WithMapping
{
    protected $country_id,$model_id;

    use CommonTrait;

    function __construct($country_id,$model_id) {
        $this->data['country_id'] = $country_id;
        $this->data['model_id'] = $model_id;
    }

    public function collection()
    {
        
        return $this->getIMEIDataList($this->data)->get();
    }

    public function headings(): array
    {
        return [
            'SL NO',
            'COUNTRY',
            'MODEL',
            'IMEI-1',
            'IMEI-2',
            'SN',
            'COLOR',
            'RAM',
            'ROM',
            'DATE OF MANUFATURE',
            'PCB SERIAL NO'
        ];
    }
    
    public function map($imei): array
    {
        return [
            $imei->rownum,
            $imei->country_name,
            $imei->model_name,
            $imei->IMEI_no1,
            $imei->IMEI_no2,
            $imei->serial_no,
            $imei->color_name,
            $imei->ram_name,
            $imei->rom_name,
            $imei->date_of_manufacture,
            $imei->pcb_serial_no,
        ];
    }
}

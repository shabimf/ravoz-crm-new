<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Backend\Role\RoleController;
use App\Http\Controllers\Backend\Permission\PermissionController;
use App\Http\Controllers\Backend\Master\MasterController;
use App\Http\Controllers\Backend\ModuleController;
use App\Http\Controllers\Backend\SoftwareversionController;
use App\Http\Controllers\Backend\Model\ModelController;
use App\Http\Controllers\Backend\AjaxController;
use App\Http\Controllers\Backend\Level\LevelController;
use App\Http\Controllers\Backend\Complaint\ComplaintController;
use App\Http\Controllers\Backend\Accessories\AccessoriesController;
use App\Http\Controllers\Backend\CurrencyController;
use App\Http\Controllers\Backend\CountryController;
use App\Http\Controllers\Backend\Device\DeviceController;
use App\Http\Controllers\Backend\Sales\SalesController;
use App\Http\Controllers\Backend\BranchController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\InvoiceController;
use App\Http\Controllers\Backend\SpareController;
use App\Http\Controllers\Backend\Sales\ActivationController;
use App\Http\Controllers\Backend\Sales\SalesReturnController;
use App\Http\Controllers\Backend\Sales\SalesRequestController;
use App\Http\Controllers\Backend\Stock\StockController;
use App\Http\Controllers\Backend\Stock\TransferController;
use App\Http\Controllers\Backend\Stock\PurchaseRequestController;
use App\Http\Controllers\Backend\ServiceController;
use App\Http\Controllers\Backend\WarrantyController;
use App\Http\Controllers\Backend\Transfer\IMEITransferController;
use App\Http\Controllers\Backend\ServiceTransferController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\SpareSaleController;
use App\Http\Controllers\Backend\FAQController;
use App\Http\Controllers\Backend\DOAController;
use App\Http\Controllers\Backend\StockServiceController;
use App\Http\Controllers\Backend\ReportController;
use App\Http\Controllers\Backend\LevelRateController;
use App\Http\Controllers\Backend\ChangePasswordController;
use App\Http\Controllers\DropdownController;
use App\Http\Controllers\Backend\BomController;
use App\Http\Controllers\Backend\SoftwareController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/complete-registration', [HomeController::class, 'completeRegistration'])->name('complete.registration');

Route::get('/google_auth', [HomeController::class, 'googleAuthRegistration'])->name('google.auth');

// Route::group(['middleware' => 'auth'], function () {
Route::middleware(['auth', '2fa'])->group(function () {
    Route::post('/2fa', function () {
        return redirect(route('dashboard'));
    })->name('2fa');
    Route::get('logout', [LoginController::class, 'logout'])->name('logout');
    Route::get('change-password', [ChangePasswordController::class, 'index']);
    Route::post('change-password', [ChangePasswordController::class, 'store'])->name('change.password');
    Route::get('/', [HomeController::class, 'index'])->name('dashboard');
    // Route::group(['middleware' => ['AdminAccess:settings|user_access']], function () {
    //
    // });
    Route::group(['middleware' => ['AdminAccess']], function () {
        Route::group(['prefix' => 'admin'], function () {
            Route::group(['middleware' => ['AdminAccess:settings']], function () {
                Route::group(['prefix' => 'master/{table}'], function () {
                    Route::get('index', [MasterController::class, 'index']);
                    Route::get('create/{id?}', [MasterController::class, 'create']);
                    Route::post('store/{id?}', [MasterController::class, 'store']);
                    Route::post('getData', [MasterController::class, 'getMasterData']);
                });
                Route::group(['prefix' => 'settings'], function () {
                    Route::resource('currency', CurrencyController::class);
                    Route::post('currency/get', [CurrencyController::class, 'getCurrency'])->name('currency.get');
                    Route::resource('country', CountryController::class);
                    Route::post('country/get', [CountryController::class, 'getCountry'])->name('country.get');
                    Route::resource('branch', BranchController::class);
                    Route::post('branch/get', [BranchController::class, 'getBranchList'])->name('branch.get');
                    Route::resource('softwareversion', SoftwareversionController::class);
                    Route::post('softwareversion/get', [SoftwareversionController::class, 'getSoftwareVersion'])->name('softwareversion.get');
                    Route::resource('spare', SpareController::class, ['except' => ['show']]);
                    Route::post('spare/get', [SpareController::class, 'getSpare'])->name('spare.get');
                    Route::get('spare/import', [SpareController::class, 'import'])->name('spare.import');
                    Route::post('spare/import', [SpareController::class, 'import_store'])->name('spare.import_store');
                    //FAQ
                    Route::resource('faqs', FAQController::class);
                    Route::post('faqs/get', [FAQController::class, 'getFAQData'])->name('faq.get');

                });


                Route::post('level/get', [LevelController::class, 'getLevelData'])->name('level.get');
                Route::resource('level', LevelController::class, ['except' => ['show']]);

                Route::post('level/rate/get', [LevelRateController::class, 'getLevelRateData'])->name('level.rate.get');
                Route::resource('level/rate', LevelRateController::class, ['except' => ['show'] ,'names' => 'level.rate']);


                Route::resource('complaint', ComplaintController::class, ['except' => ['show']]);
                Route::post('complaint/get', [ComplaintController::class, 'getComplaintData'])->name('complaint.get');
                Route::get('complaint/import', [ComplaintController::class, 'import'])->name('complaint.import');
                Route::post('complaint/import', [ComplaintController::class, 'import_store'])->name('complaint.import_store');

                Route::resource('accessories', AccessoriesController::class, ['except' => ['show']]);
                Route::post('accessories/get', [AccessoriesController::class, 'getAccessoriesData'])->name('accessories.get');

                Route::resource('model', ModelController::class, ['except' => ['show']]);
                Route::get('model/specification/{id}', [ModelController::class, 'getModelSpecifications'])->name('model.specification');
                Route::post('model/delete/{id}', [ModelController::class, 'destroy'])->name('model.destroy');
                Route::post('model/get', [ModelController::class, 'getModelData'])->name('model.get');
            });
            Route::group(['middleware' => ['AdminAccess:user_access']], function () {
                Route::get('module/create', [ModuleController::class, 'create'])->name('module.create');
                Route::get('module', [ModuleController::class, 'index'])->name('module');
                Route::get('permission/create', [PermissionController::class, 'create'])->name('permission.create');
                Route::get('permission', [PermissionController::class, 'index'])->name('permission');
                Route::get('role/create', [RoleController::class, 'create'])->name('role.create');
                Route::get('role', [RoleController::class, 'index'])->name('role');
                Route::get('user/create', [UserController::class, 'create'])->name('user.create');
                Route::get('user', [UserController::class, 'index'])->name('user');
                Route::get('user/{id}/password/change', [UserController::class, 'changePassword'])->name('user.password.change');
                Route::PATCH('user/{id}/password/change', [UserController::class, 'updatePassword'])->name('user.password.update');
            });


            Route::resource('module', ModuleController::class, ['except' => ['create', 'index']]);
            Route::post('module/get', [ModuleController::class, 'getModule'])->name('module.get');
            Route::resource('role', RoleController::class, ['except' => ['show', 'create', 'index']]);
            Route::post('role/get', [RoleController::class, 'getRole'])->name('role.get');
            Route::post('role/getpermission', [RoleController::class, 'getPermissions'])->name('role.get.permission');
            Route::resource('permission', PermissionController::class, ['except' => ['show', 'create', 'index']]);
            Route::post('permission/get', [PermissionController::class, 'getPermissionList'])->name('permission.get');
            Route::resource('user', UserController::class, ['except' => ['show', 'create', 'index']]);
            Route::post('user/get', [UserController::class, 'getUser'])->name('user.get');
            Route::get('user/varifyemail', [UserController::class, 'varifyemail']);
            Route::get('user/varifycontact', [UserController::class, 'varifycontact']);
            Route::get('user/varifyusername', [UserController::class, 'varifyusername']);
            Route::get('generate/invoice', [InvoiceController::class, 'generateInvoicePDF'])->name('generate.invoice.pdf');
            Route::get('generate/service', [InvoiceController::class, 'generateServicePDF'])->name('generate.service.pdf');
            /* shabi */
            Route::post('activation', [AjaxController::class, 'Activation']);
            Route::post('ga_activation', [AjaxController::class, 'GoogleAuthActivation']);
            Route::post('ga_rest_activation', [AjaxController::class, 'GoogleRestAuthActivation']);
            /* shabi */
        });
    });
    Route::group(['middleware' => ['ServiceAccess']], function () {
        Route::group(['prefix' => 'service'], function () {

            Route::get('updateBrandVariant', [ServiceController::class, 'updateBrandVariant']);
            //service
            Route::post('/getDeviceType', [ServiceController::class, 'getDeviceType']);
            Route::post('/getIMEIData', [ServiceController::class, 'getIMEIData']);
            Route::get('/getversion/{id}', [ServiceController::class, 'getSoftwareVersion']);
            Route::get('/getModel/{id}', [ServiceController::class, 'getModels']);
            Route::post('/getCustomerData', [ServiceController::class, 'getCustomerData']);
            Route::post('customer/update', [ServiceController::class, 'updateCustomerData'])->name('customer.update');
            Route::post('/customer/checkPhone', [ServiceController::class, 'checkPhoneExists'])->name('customer.checkPhone');
            Route::post('/customer/checkEmail', [ServiceController::class, 'checkEmailExists'])->name('customer.checkEmail');

            Route::group(['middleware' => ['ServiceAccess:service_create']], function () {
              Route::get('create', [ServiceController::class, 'create'])->name('service.create');
            });

            Route::group(['middleware' => ['ServiceAccess:service_list']], function () {
              Route::get('/list', [ServiceController::class, 'getlist'])->name('service.list');
            });

            Route::post('get', [ServiceController::class, 'getServiceRequest'])->name('service.get');
            Route::post('appeal', [ServiceController::class, 'sentAppealRequest'])->name('service.appeal');
            Route::get('{id}/appeal/get', [ServiceController::class, 'getAppeal'])->name('appeal.get');
            Route::post('appeal/{id}', [ServiceController::class, 'addAppeal'])->name('appeal.add');
            Route::post('update/appel', [ServiceController::class, 'updateAppeal'])->name('appeal.status');


            //DOA
            Route::middleware(['ServiceAccess:doa_list'])->prefix('doa')->group(function() {
                Route::get('', [DOAController::class, 'index'])->name('doa.index');
                Route::get('pdf/{id}', [DOAController::class, 'getPDF']);
                Route::post('imei/replace', [DOAController::class, 'updateReplaceIMEI']);
                Route::post('get', [DOAController::class, 'getDOAData'])->name('doa.get');
                Route::post('status', [StockServiceController::class, 'doaStatusUpdate'])->name('doa.status');
            });

            Route::post('doa/update1', [DOAController::class, 'doaUpdate']);
            Route::post('doa/invoice/delete', [DOAController::class, 'doaInvoiceDelete']);
            Route::post('doa/image/delete', [DOAController::class, 'doaImageDelete']);
            Route::resource('doa', DOAController::class, ['except' => ['index']]);


            //Purchase
            Route::post('purchase/get', [StockController::class, 'getStockData'])->name('purchase.get');
            Route::group(['middleware' => ['ServiceAccess:purchase_create']], function () {
                Route::get('purchase/create', [StockController::class, 'create'])->name('purchase.create');
                Route::get('purchase/import', [StockController::class, 'import'])->name('purchase.import');
                Route::post('purchase/import/store', [StockController::class, 'importStore'])->name('purchase.import.store');
            });
            Route::group(['middleware' => ['ServiceAccess:purchase_list']], function () {
                Route::get('purchase', [StockController::class, 'index'])->name('purchase');
            });
            Route::resource('purchase', StockController::class, ['except' => ['create', 'index']]);

            Route::post('stock/get', [StockController::class, 'getStockListData'])->name('stock.get');
            Route::group(['middleware' => ['ServiceAccess:stock_list']], function () {
              Route::get('stock', [StockController::class, 'getStockList'])->name('stock');
            });
            Route::group(['middleware' => ['ServiceAccess:stock_edit']], function () {
                Route::get('stock/{id}/edit', [StockController::class, 'edit'])->name('stock.edit');
                Route::patch('stock/{id}', [StockController::class, 'update'])->name('stock.update');
                Route::post('stock/update', [StockController::class, 'updateItems'])->name('stock.update.items');
            });
            //transfer
            Route::group(['middleware' => ['ServiceAccess:transfer_create']], function () {
                Route::get('sales/transfer/create', [TransferController::class, 'create'])->name('transfer.create');
            });
            Route::group(['middleware' => ['ServiceAccess:transfer_list']], function () {
                Route::get('sales/transfer', [TransferController::class, 'index'])->name('transfer');
            });
            Route::resource('sales/transfer', TransferController::class, ['except' => ['create', 'index']]);
            Route::post('sales/transfer/get', [TransferController::class, 'getStockTransferData'])->name('transfer.get');
            //request
            Route::group(['middleware' => ['ServiceAccess:purchase_order_create']], function () {
                Route::get('request/create', [PurchaseRequestController::class, 'create'])->name('request.create');
            });
            Route::group(['middleware' => ['ServiceAccess:purchase_order_list']], function () {
                Route::get('request', [PurchaseRequestController::class, 'index'])->name('request');
            });
            Route::resource('request', PurchaseRequestController::class, ['except' => ['create', 'index']]);
            Route::post('request/get', [PurchaseRequestController::class, 'getPurchaseRequest'])->name('request.get');

            //sparesale
            Route::group(['middleware' => ['ServiceAccess:spare_sale_create']], function () {
                Route::get('sparesale/create', [SpareSaleController::class, 'create'])->name('sparesale.create');
            });
            Route::group(['middleware' => ['ServiceAccess:spare_sale_list']], function () {
                Route::get('sparesale', [SpareSaleController::class, 'index'])->name('sparesale');
            });
            Route::resource('sparesale', SpareSaleController::class, ['except' => ['create', 'index']]);
            Route::post('sparesale/get', [SpareSaleController::class, 'getSpareSale'])->name('sparesale.get');

            //sales Request
            Route::get('sales/request', [SalesRequestController::class, 'index'])->name('salesrequest.index');
            Route::get('sales/request/{id}', [SalesRequestController::class, 'show']);
            Route::post('sales/request/get', [SalesRequestController::class, 'getSalesRequest'])->name('salesrequest.get');
            Route::post('sales/request/update', [SalesRequestController::class, 'getStatusUpdate'])->name('salesrequest.update');
            //Sale Request Resend
            Route::post('sales/request/resend', [SalesRequestController::class, 'saleRequestResend'])->name('salerequest.resend');
            //Add Complaint
            Route::post('add/complaint', [ServiceController::class, 'addServiceComplaint'])->name('service.complaint.add');
            Route::post('update/complaint-status', [ServiceController::class, 'UpdateServiceComplaintStatus'])->name('service.complaint.status');
            Route::get('receipt/{id}', [ServiceController::class, 'getServiceReceipt'])->name('service.receipt');
            Route::post('image/delete', [ServiceController::class, 'removeImage'])->name('service.image.delete');
            Route::post('stock_service/add/complaint', [StockServiceController::class, 'addStockServiceComplaint'])->name('stock.service.complaint.add');
            //Remove Complaint
            Route::post('complaint/remove', [ServiceController::class, 'removeComplaint'])->name('remove.complaint');
            //service Transfer
            Route::put('servicetransfer', [ServiceController::class, 'transferCreate'])->name('service.transfer');
            Route::resource('servicetransfer', ServiceTransferController::class, ['except' => ['create']]);
            Route::post('transfer/get', [ServiceTransferController::class, 'getTransferData'])->name('servicetransfer.get');
            Route::post('transfer/status', [ServiceTransferController::class, 'updateTransferStatus'])->name('servicetransfer.status');
            Route::post('transfer/type', [ServiceTransferController::class, 'updateTransferType'])->name('servicetransfer.type');
            //Add Spare
            Route::post('add/spare', [ServiceController::class, 'addServiceSpare'])->name('service.spare.add');
            Route::post('get/spare/price', [ServiceController::class, 'getSparePrice'])->name('get.price.spare');
            Route::post('stock_service/add/spare', [StockServiceController::class, 'addStockServiceSpare'])->name('stock.service.spare.add');
            //Remove Spare
            Route::post('spare/remove', [ServiceController::class, 'removeServiceSpare'])->name('remove.service.spare');
            //Add Service Charge
            Route::post('add/servicecharge', [ServiceController::class, 'addServiceCharge'])->name('service_charge.add');
            //Service Edit Complete
            Route::post('status', [ServiceController::class, 'serviceStatusUpdate'])->name('service.status');
            //Service Bill
            Route::get('bill/{id}', [ServiceController::class, 'serviceBill'])->name('service.bill');
            Route::post('regenerate/bill/{id}', [ServiceController::class, 'serviceBillRegenerate'])->name('service.bill.regenerate');
            //Service Bill View
            Route::get('viewbill/{id}', [ServiceController::class, 'serviceViewBill'])->name('service.view.bill');

            Route::get('dashboard', [DashboardController::class, 'index']);
            Route::post('/getworkstatus', [DashboardController::class, 'getWorkStatusData']);
            //
            Route::post('update/version', [ServiceController::class, 'updateVersion'])->name('service.updateversion');
            Route::post('update/warranty', [ServiceController::class, 'updateWarranty'])->name('service.updatewarranty');
            Route::post('update/compliant/description', [ServiceController::class, 'updateDescription'])->name('service.compliant.description');
            Route::post('replace/pcbno', [ServiceController::class, 'replacePCBNo'])->name('service.replace.pcbno');


            //Stock Service
            Route::post('/getDoaIMEIData', [StockServiceController::class, 'getDOAIMEIData']);
            Route::post('/getDOAData', [StockServiceController::class, 'getDOAData']);
            Route::post('stock_service/replaceIMEI', [StockServiceController::class, 'replaceIMEI'])->name('replaceIMEI.update');
            Route::resource('stock_service', StockServiceController::class);
            Route::post('stock_service/get', [StockServiceController::class, 'getStockServiceData'])->name('stock_service.get');
            Route::get('stock_service/{id}/appeal/get', [StockServiceController::class, 'getAppeal'])->name('stock_service.appeal.get');
             //Service Cancel
            Route::get('cancel/{id}',[ServiceController::class, 'serviceCancel'])->name('service.cancel');

            Route::get('level/rate/{id}/{serviceid}', [LevelRateController::class, 'getRate'])->name('company.get.rate');

            //faq
            Route::get('faq', [FAQController::class, 'getFaq']);
            //bom
            Route::group(['middleware' => ['ServiceAccess:bom_list']], function () {
                Route::post('bom/get', [BomController::class, 'get'])->name('bom.get');
                Route::get('bom/export', [BomController::class, 'export'])->name('bom.export');
                Route::resource('bom', BomController::class, ['except' => ['create', 'show']]);
            });

            //software
            Route::group(['middleware' => ['ServiceAccess:software_list']], function () {
                Route::resource('software', SoftwareController::class, ['except' => ['create', 'show']]);
                Route::post('software/get', [SoftwareController::class, 'get'])->name('software.get');
            });

            Route::group(['middleware' => ['ServiceAccess:appeal_request_approval']], function () {
                Route::group(['prefix' => 'approve/request'], function () {
                    Route::post('get', [ServiceController::class, 'approveListGet'])->name('approve.request.get');
                    Route::get('', [ServiceController::class, 'approveList'])->name('approve.request.list');
                });
            });
        });
        Route::group(['middleware' => ['ServiceAccess:purchase_list|service_list|doa_list|transfer_list|purchase_order_list|spare_sale_list|service_transfer_list|sales_request_list|stock_service_list|stock_list']], function () {
           Route::resource('service', ServiceController::class, ['except' => ['create', 'getlist']]);
        });
    });

    Route::group(['middleware' => ['WarrantyAccess']], function () {
        Route::group(['prefix' => 'warranty'], function () {
            //sales
            Route::group(['middleware' => ['WarrantyAccess:sales_create']], function () {
                Route::get('sale/create', [SalesController::class, 'create'])->name('sale.create');
            });
            Route::group(['middleware' => ['WarrantyAccess:sales_list']], function () {
                Route::get('sale', [SalesController::class, 'index'])->name('sale');
            });
            Route::resource('sale', SalesController::class, ['except' => ['create', 'index']]);
            Route::post('sale/get', [SalesController::class, 'getSaleData'])->name('sale.get');
            //activation
            Route::group(['prefix' => 'activation','middleware' => ['WarrantyAccess:import_activation']], function () {
                Route::get('', [ActivationController::class, 'index'])->name('activation');
                Route::get('list', [ActivationController::class, 'list'])->name('activation.list');
                Route::post('get', [ActivationController::class, 'getActivationData'])->name('activation.get');
                Route::post('update', [ActivationController::class, 'updateActivation']);
                Route::get('add', [ActivationController::class, 'add'])->name('activation.add');
                Route::get('export', [ActivationController::class, 'exportData'])->name('activation.report.export');
            });
            Route::resource('activation', ActivationController::class, ['except' => ['index','show']]);
            //sales Return
            Route::group(['middleware' => ['WarrantyAccess:activation_return_create']], function () {
                Route::get('return/create', [SalesReturnController::class, 'create'])->name('return.create');
                Route::get('return/activation', [SalesReturnController::class, 'activation'])->name('return.activation');

                Route::post('return/activation/store', [SalesReturnController::class, 'activationStore'])->name('return.activation.store');
            });
            Route::group(['middleware' => ['WarrantyAccess:activation_return_list']], function () {
                Route::get('return', [SalesReturnController::class, 'index'])->name('return');
            });
            Route::resource('return', SalesReturnController::class, ['except' => ['create', 'index']]);
            Route::post('return/get', [SalesReturnController::class, 'getSaleReturnData'])->name('return.get');
            //IMEI
            Route::group(['middleware' => ['WarrantyAccess:import_imei']], function () {
                Route::get('devices/create', [DeviceController::class, 'create'])->name('devices.create');
            });
            Route::group(['middleware' => ['WarrantyAccess:imei_list']], function () {
                Route::get('devices', [DeviceController::class, 'index'])->name('devices');
            });
            Route::resource('devices', DeviceController::class, ['except' => ['show', 'create', 'index']]);

            Route::get('devices/export', [DeviceController::class, 'getIMEIExport'])->name('devices.export');
            Route::post('devices/import', [DeviceController::class, 'import'])->name('devices.import');
            Route::post('devices/get', [DeviceController::class, 'getDeviceData'])->name('devices.get');
            Route::get('devices/imei_list', [DeviceController::class, 'getIMEIList'])->name('devices.imeilist');
            Route::post('devices/imei/get', [DeviceController::class, 'getIMEIData'])->name('devices.imeiget');

            //IMEITransfer
            Route::group(['middleware' => ['WarrantyAccess:import_imei_transfer']], function () {
                Route::get('IMEITransfer/create', [IMEITransferController::class, 'create'])->name('IMEITransfer.create');
            });
            Route::group(['middleware' => ['WarrantyAccess:imei_transfer_list']], function () {
                Route::get('IMEITransfer', [IMEITransferController::class, 'index'])->name('IMEITransfer');
            });
            Route::resource('IMEITransfer', IMEITransferController::class, ['except' => ['show', 'create', 'index']]);
            Route::post('IMEITransfer/import', [IMEITransferController::class, 'import'])->name('IMEITransfer.import');
            Route::post('IMEITransfer/get', [IMEITransferController::class, 'getIMEITransferData'])->name('IMEITransfer.get');
            Route::get('IMEITransfer/imei_list', [IMEITransferController::class, 'getIMEITransferList'])->name('IMEITransfer.imeilist');
            Route::post('IMEITransfer/imei/get', [IMEITransferController::class, 'getIMEIData'])->name('IMEITransfer.imeiget');
            Route::get('dashboard', [DashboardController::class, 'index']);
        });
        Route::group(['middleware' => ['WarrantyAccess:sales_list|activation_return_list|imei_list|imei_transfer_list']], function () {
          Route::resource('warranty', WarrantyController::class);
        });
    });

    Route::group(['prefix' => 'report'], function () {
        //DOA Report
        Route::middleware(['ServiceAccess:doa_report'])->prefix('doa')->group(function() {
            Route::get('', [ReportController::class, 'getDoaList'])->name('report.doa');
            Route::post('get', [ReportController::class, 'getDoaListData'])->name('report.doa-get');
            Route::get('file-export', [ReportController::class, 'fileDoaExport'])->name('report.doa-file-export');
        });
        //Activation Report
        Route::middleware(['ServiceAccess:activation_report'])->prefix('activation')->group(function() {
            Route::get('', [ReportController::class, 'getActivationList'])->name('report.activation');
            Route::post('get', [ReportController::class, 'getActivationListData'])->name('report.activation-get');
            Route::get('file-export', [ReportController::class, 'fileActivationExport'])->name('report.activation-file-export');
            Route::get('file-model-export', [ReportController::class, 'fileModelActivationExport'])->name('report.activation-file-model-export');
            Route::get('file-model-month-export', [ReportController::class, 'fileModelMonthActivationExport'])->name('report.activation-file-model-month-export');
        });
        //Stock Report
        Route::middleware(['ServiceAccess:stock_report'])->prefix('stock')->group(function() {
            Route::get('', [ReportController::class, 'getStockList'])->name('report.stock');
            Route::post('get', [ReportController::class, 'getStockListData'])->name('report.stock-get');
        });

        //CONSUMPTION Report
        Route::middleware(['ServiceAccess:consumption_report'])->prefix('consumption')->group(function() {
            Route::get('', [ReportController::class, 'getConsumptionList'])->name('report.consumption');
            Route::post('get', [ReportController::class, 'getConsumptionListData'])->name('report.consumption-get');
            Route::get('file-export', [ReportController::class, 'fileConsumptionExport'])->name('report.consumption-file-export');
        });
        //spare cliam Report

        Route::middleware(['ServiceAccess:spare_claim_report'])->prefix('spare')->group(function() {
            Route::get('', [ReportController::class, 'getSpareCliamList'])->name('report.spare');
            Route::post('get', [ReportController::class, 'getSpareCliamListData'])->name('report.spare-get');
            Route::get('file-export', [ReportController::class, 'filespareExport'])->name('report.spare-file-export');
        });
        //Forecast Report

        Route::middleware(['ServiceAccess:forecast_report'])->prefix('forecast')->group(function() {
            Route::get('', [ReportController::class, 'getForecastList'])->name('report.forecast');
            Route::post('get', [ReportController::class, 'getForecastListData'])->name('report.forecast-get');
            Route::get('file-export', [ReportController::class, 'fileforecastExport'])->name('report.forecast-file-export');
        });

        //service claim report
        Route::middleware(['ServiceAccess:service_claim_report'])->prefix('service')->group(function() {
            Route::get('claim', [ReportController::class, 'getServiceClaimList'])->name('report.service.claim');
            Route::post('claim/get', [ReportController::class, 'getServiceClaimListData'])->name('report.service.claim.get');
            Route::get('claim/file-export', [ReportController::class, 'fileServiceClaimExport'])->name('report.service-claim-file-export');
        });
        Route::middleware(['ServiceAccess:complaint_report'])->prefix('service')->group(function() {
            Route::get('compliant', [ReportController::class, 'getServiceCompliantList'])->name('report.service.compliant');
            Route::post('compliant/get', [ReportController::class, 'getServiceCompliantListData'])->name('report.service.compliant.get');
            Route::get('compliant/file-export', [ReportController::class, 'fileServiceCompliantExport'])->name('report.service-compliant-file-export');
        });

        //
        Route::middleware(['ServiceAccess:stock_service_claim_report'])->prefix('stock_service')->group(function() {
            Route::get('claim', [ReportController::class, 'getStockServiceClaimList'])->name('report.stock_service.claim');
            Route::post('claim/get', [ReportController::class, 'getStockServiceListData'])->name('report.stock_service.claim.get');
            Route::get('claim/file-export', [ReportController::class, 'fileStockServiceClaimExport'])->name('report.stock_service-claim-file-export');
        });
        //model wise complaint report
        Route::middleware(['ServiceAccess:model_wise_complaint_report'])->prefix('complaint')->group(function() {
            Route::get('', [ReportController::class, 'getComplaintsList'])->name('report.complaint');
            Route::post('get', [ReportController::class, 'getComplaintsData'])->name('report.complaint.get');
            Route::get('file-export', [ReportController::class, 'fileComplaintsExport'])->name('report.complaint-file-export');
        });

        Route::middleware(['ServiceAccess:price_comparison_report'])->prefix('price')->group(function() {
            Route::get('', [ReportController::class, 'getPriceList'])->name('report.price');
            Route::post('get', [ReportController::class, 'getPriceData'])->name('report.price.get');
            Route::get('file-export', [ReportController::class, 'filePriceExport'])->name('report.price-file-export');
        });

        Route::middleware(['ServiceAccess:level_claim_report'])->prefix('level')->group(function() {
            Route::get('', [ReportController::class, 'getLevelClaimList'])->name('report.level.claim');
        });
    });
    Route::get('getSpare',[DropdownController::class, 'getSpare'])->name('getSpare');
    Route::post('getCurrency',[DropdownController::class, 'getCurrency'])->name('getCurrency');
    Route::get('getBranch',[DropdownController::class, 'get_by_branch'])->name('getBranch');
    Route::get('varifyIMEI', [DropdownController::class, 'varifyIMEI'])->name('varifyIMEI');
    Route::get('varifyPCBNo', [DropdownController::class, 'varifyPCBNo'])->name('varifyPCBNo');
});

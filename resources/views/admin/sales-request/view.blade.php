@extends('layouts.service')
@section('content')

<div class="main-panel">
   <div class="panel-hedding">
      <div class="row mb-4">
         <div class="col-md-6">
            <h1>Sales Request</h1>
         </div>
         <div class="col-md-6">
            <div class="add-new">
               <a class="btn btn-primary" href="{{ route('salesrequest.index') }}">
               <i class="feather icon-list"></i> Sales - Request List
               </a>
            </div>
         </div>
      </div>
   </div>
   @include('inc.alert')
   <div class="row">
      <div class="col-lg-12">
         <div class="card">
            <div class="card-body p-0">
               <div class="row pb-5 p-5">
                  <div class="col-md-4">
                     <h5 class="text-primary">Service No: {{$request->serial_no}}</h5>
                     <p>Date to: {{date("F j, Y",strtotime($request->created_at))}}</p>
                  </div>
                  <div class="col-md-8 text-md-right mt-4 mt-md-0">
                     <h4 class="mb-4">Client Information</h4>
                     <p class="mb-1">{{$request->customer_name}}</p>
                     <p class="mb-1">{{$request->address}},</p>
                     <p class="mb-1">{{$request->customer_phone}}</p>
                     <a href="#"> {{$request->customer_email}}</a>
                     <p class="mb-1"><span class="text-muted">Status: </span>
                     @if ($request->status == 0)
                     <a href="#" class="badge badge-warning">Pending</a>
                     @elseif ($request->status == 1)
                     <a href="#" class="badge badge-success">Accepted</a>
                     @else
                     <a href="#" class="badge badge-danger">Rejected</a>
                     @endif
                     </p>
                  </div>
               </div>
               <hr>
               <div class="row p-5">
                  <div class="col-md-12">
                     <h4 class="mb-4">Device Information</h4>
                     <div class="table-responsive">
                        <table class="table table-condensed device_table  table-bordered table-hover no-margin sf">
                           <tbody>
                              <tr>
                                 <th> IMEI NO1 </th>
                                 <td><strong>{{$request->IMEI_no1}}</strong></td>
                                 <th> IMEI NO2 </th>
                                 <td><strong>{{$request->IMEI_no2}}</strong></td>
                                 <th> Serial Num </th>
                                 <td>{{$request->serial_no}}</td>
                                 <th> Warranty </th>
                                 <td>
                                    <strong>
                                      @if ($request->is_warranty == 1) Yes @else No @endif
                                    </strong>
                                 </td>
                              </tr>
                              <tr>
                                 <th> Brand </th>
                                 <td>{{$request->brand}}</td>
                                 <th> Model </th>
                                 <td>{{$request->model}}
                                    (
                                    @if($request->color){{$request->color}}@endif
                                    @if($request->ram),{{$request->ram}}@endif
                                    @if($request->rom),{{$request->rom}}@endif
                                    )
                                 </td>
                                 <th> Technician </th>
                                 <td><strong>{{$request->technician}}</strong></td>
                                 <th> Security Code </th>
                                 <td>{{$request->security_code}}</td>
                              </tr>
                              <tr>
                                 <th> Sale Date </th>
                                 <td> @if($request->sale_date) {{ date('d-m-Y', strtotime($request->sale_date))}} @else - @endif</td>
                                 <th> Active Date </th>
                                 <td> @if($request->active_date) {{ date('d-m-Y', strtotime($request->active_date))}}  @else - @endif</td>
                                 <th> Date of Manufacture </th>
                                 <td> @if($request->manufacture_date) {{ date('d-m-Y', strtotime($request->manufacture_date))}} @else - @endif</td>
                                 <th> Delivery Date </th>
                                 <td> @if($request->delivery_date) {{ date('d-m-Y', strtotime($request->delivery_date))}} @else - @endif</td>
                              </tr>
                              <tr>
                                 <th> Software Version </th>
                                 <td> {{$request->version}} </td>
                                 <th> Images </th>
                                 <td>
                                    @if ($request->images)
                                      @foreach ($request->images as $k=>$file)
                                      <a href="{{ asset('uploads/service/'.$file) }}" target="_blank">Image {{$k+1}}</a><br/>
                                      @endforeach
                                    @endif
                                 </td>
                                 <th> Invoice </th>
                                 <td>
                                    @if ($request->invoice)
                                      @foreach ($request->invoice as $k=>$file)
                                      <a href="{{ asset('uploads/invoice/'.$file) }}" target="_blank">Invoice {{$k+1}}</a><br/>
                                      @endforeach
                                    @endif
                                 </td>
                                 <th> Accessories </th>
                                 <td> @if ($request->accessories_id) {{get_accessories($request->accessories_id)}} @endif</td>
                              </tr>
                              @if($request->status == 0 && userHasPermission('Service','sales_request_approval'))
                              <tr>
                                 <td colspan="8">
  						                  <button class="btn btn-overlay-secondary btn-sm float-right btn-accept" data-id="{{$request->id}}" data-serviceId="{{$request->service_id}}" data-type="2">Reject</button>
                                    <button class="btn btn-overlay-primary btn-sm float-right btn-accept" data-id="{{$request->id}}" data-serviceId="{{$request->service_id}}" data-type="1"> Accept </button>
                                 </td>
                              </tr>

                              @endif
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
   $( ".btn-accept" ).click(function() {
     var type = $(this).attr("data-type");
     var request_id = $(this).attr("data-id");
     var service_id = $(this).attr("data-serviceId");
      $.ajax({
         type: 'post',
         url: "{{route('salesrequest.update')}}",
         data: {
            'type': type,
            'request_id': request_id,
            'service_id': service_id
         },success: function (data) {
            window.location.replace("/service/sales/request?status=0");
         },
         error: function () {

         }
      });
   });
</script>
@endsection

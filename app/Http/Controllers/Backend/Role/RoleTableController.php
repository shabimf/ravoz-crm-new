<?php

namespace App\Http\Controllers\Backend\Role;

use App\Http\Controllers\Controller;
use App\Models\Auth\Role;
use DB;
use Yajra\DataTables\Facades\DataTables;

class RoleTableController extends Controller
{
    public function __invoke()
    {
        return Datatables::of(Role::leftJoin('permission_role', 'permission_role.role_id', '=', 'roles.id')
            ->leftJoin('permissions', 'permission_role.permission_id', '=', 'permissions.id')
            ->select([
                'roles.id',
                'roles.name',
                'roles.all',
                'roles.sort',
                'roles.status',
                DB::raw("GROUP_CONCAT( DISTINCT permissions.name SEPARATOR '<br/>') as permission_name"),
                DB::raw('(SELECT COUNT(users.id) FROM users WHERE users.role_id = roles.id AND users.deleted_at IS NULL) AS userCount'),
            ])
            ->groupBy('roles.id'))
            ->escapeColumns(['name', 'sort'])
            ->addColumn('permissions', function ($role) {
                if ($role->all) {
                    return '<span class="label label-success">All</span>';
                }

                return ucwords(str_replace('-',' ',$role->permission_name));
            })
            ->addColumn('users', function ($role) {
                return $role->userCount;
            })
            ->addColumn('actions', function ($role) {
                return $role->action_buttons;
            })
            ->make(true);
    }
}

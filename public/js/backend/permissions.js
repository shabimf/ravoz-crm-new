(function () {

    FTX.Permissions = {

        list: {

            selectors: {
                permissions_table: $('#permissions-table'),
            },

            init: function () {

                this.selectors.permissions_table.dataTable({

                    processing: false,
                    serverSide: true,

                    ajax: {
                        url: this.selectors.permissions_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'name', name: 'name' },
                        { data: 'module', name: 'module' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();

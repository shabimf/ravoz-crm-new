<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class IMEITransferItems extends Model
{
    use HasFactory;
    protected $table = "imei_transfer_items";  
    public $timestamps = false;
    protected $guarded = array();
}

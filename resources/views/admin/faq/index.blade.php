@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
                <div class="add-new">
                    <a class="btn btn-primary" href="{{ route('faqs.create') }}"> <i class="feather icon-plus"></i> Create FAQ</a>
                </div>
            </div>
        </div>
        <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">FAQ Management</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="faq-table" class="table table-striped table-bordered" data-ajax_url="{{ route("faq.get") }}">
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <th>Question</th>
									<th>Answer</th>
					                <th  width="100">Actions</th>
									<th  width="100">Status</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('pagescript')
@include('inc.datatables-js')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Faqs.list.init();
    });
</script>
@endsection

@extends('layouts.app')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                  <h4 class="card-title-text">{{((isset($edit_level)) ? "Edit":'Create')}} Level </h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ url('admin/level') }}"> <i
                            class="feather icon-list"></i> Level List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($edit_level, ['route' => ['level.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-model']) }}
            @else
                {{ Form::open(['route' => 'level.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-model']) }}
            @endif
            @csrf
            
            <div class="form-row">  
                <div class="form-group col-md-4">
                    {{ Form::label('name', __('Product Type')) }}
                    {{ Form::select('device_id', $device, old('device_id',(isset($edit_level)) ? $edit_level->device_id:'0'), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Product Type', 'required' => 'required', 'id' => 'device_id']) }}
                </div>
                <div class="form-group col-md-4">
                    {{ Form::label('name', __('Level Name')) }}
                    {{ Form::text('name', old('name',(isset($edit_level)) ? $edit_level->name:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!-- <div class="form-group col-md-4">
                    {{ Form::label('name', __('Level Name Arabic')) }}
                    {{ Form::text('arabic_name', old('arabic_name',(isset($edit_level)) ? $edit_level->arabic_name:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div> -->
                <!-- <div class="form-group col-md-4">
                    {{ Form::label('name', __('Rate')) }}
                    {{ Form::text('rate', old('rate',(isset($edit_level)) ? $edit_level->rate:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div> -->
                <div class="form-group col-md-4">
                    <label>&nbsp;</label>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="service_type_id" name="service_type_id" value="1" {!! !empty($edit_level->service_type_id) ? 'checked' : '' !!}>
                        <label class="custom-control-label" for="service_type_id"> Stock Service</label>
                    </div>  
                </div>
            </div>
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection
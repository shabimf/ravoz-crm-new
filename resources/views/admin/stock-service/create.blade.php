@extends('layouts.service')
@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-12">
                    <h4 class="card-title-text device">STOCK SERVICE</h4>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-service', 'auto-complete' => 'off', 'enctype' => 'multipart/form-data']) }}
            @csrf
            <div class="row">
                <div class="col-md-4">
                </div>

                <div class="col-md-4 form-group imeicls">
                    {{ Form::label('name', __('IMEI/SL')) }}
                    <div class=" input-group">
                        {{ Form::text('imei_no', '', ['class' => 'form-control', 'id' => 'imei_no']) }}
                        {{ Form::hidden('imeiId', '', ['class' => 'form-control', 'id' => 'imeiId']) }}


                        <div class="input-group-append">
                            {{ Form::button('Search', ['class' => 'btn btn-overlay-dark btns', 'id' => 'search' ,'onclick' => 'return searchIMEI();']) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-4"> </div>
            </div>
            <div class="col-sm-6 col-sm-offset-3 text-center scan_result hide ap" style="z-index: 1110; top: 65px;">
                <div class="jumbotron " style="border-top-left-radius:0 ;border-top-right-radius:0 ; padding-top:20px;">
                </div>
            </div>
            <div class="col-sm-12  full_box rp   block_div hide" style=" padding-bottom:20px; font-size:11px">

                <div class="row">
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Factory IMEI/SN Model')) }}
                        {{ Form::hidden('country_id', old('country_id',(isset($country_id)) ? $country_id:''), ['class' => 'form-control', 'id' => 'country_id']) }}
                        {{ Form::text('sn_no', '', ['class' => 'form-control txt_imei_id', 'id' => 'sn_no', 'readonly']) }}
                    </div>
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Sale Date')) }}
                        {{ Form::text('sale_date', '', ['class' => 'form-control txt_sales_date', 'id' => 'sale_date', 'disabled']) }}
                    </div>
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Active Date')) }}
                        {{ Form::text('active_date', '', ['class' => 'form-control txt_active_date', 'id' => 'active_date', 'disabled']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Date of Manufacture')) }}
                        {{ Form::text('manufacture_date', '', ['class' => 'form-control txt_active_date', 'id' => 'manufacture_date', 'disabled']) }}
                    </div>
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Software Version')) }}
                        <select class="form-control js-example-basic-single" id="version" name="version_id"
                            placeholder="Select a Version"> </select>
                    </div>
                    <div class="col-md-4 form-group"> {{ Form::label('technician_id', __('Technician')) }} <span
                            style="color: red">*</span>
                        {{ Form::select('technician_id', $users, old('technician_id'), ['class' => 'js-example-basic-single', 'placeholder' => 'Select Technician', 'id' => 'technician_id']) }}
                        <label id="technician_id-error" class="error" for="technician_id"></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group"> {{ Form::label('name', __('Delivery Date')) }} <span
                            style="color: red">*</span>{{ Form::text('delivery_date', '', ['class' => 'form-control', 'id' => 'delivery_date']) }}
                    </div>
                    <div class="col-md-4 form-group"> {{ Form::label('name', __('Security Code')) }}
                        {{ Form::text('security_code', '', ['class' => 'form-control', 'id' => 'code']) }} </div>

                </div>


                <div class="row">
                    <div class="col-md-12 form-group">
                        {{ Form::label('name', __('Accessories')) }}
                        <div class="search-box">
                            <div class="search">
                                @foreach ($access as $k => $v)
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" id="accessories_{{ $k }}"
                                            value="{{ $k }}" name="accessories_id[]"
                                            class="custom-control-input form-control">
                                        <label class="custom-control-label"
                                            for="accessories_{{ $k }}">{{ $v }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>


                <div class="row">
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Complaint')) }}<span style="color: red">*</span>
                        <div class="search-box">
                            <div class="search">
                                <select class="js-example-basic-multiple select2-hidden-accessible" name="complaint[]"
                                    multiple="">
                                    @foreach ($levels as $key => $complaint)
                                        <optgroup label="{{ $key }}">
                                            @foreach ($complaint as $k => $v)
                                                <option value="{{ $k }}">{{ $v }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                                <label id="complaint[]-error" class="error" for="complaint[]"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form-group"> {{ Form::label('name', __('Remarks')) }} {!! Form::text('remarks', null, ['class' => 'form-control', 'id' => 'remarks']) !!}
                    </div>
                </div>
                <input id="stock_service_id" type="text" name="stock_service_id" value="0"/>
                <button type="submit" class="btn btn-sm btn-primary float-right" id='btnAddRequest'>Submit</button>
                {{ Form::close() }}
            </div>

        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/select2/bloodhound.min.js')}}"></script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
@endsection

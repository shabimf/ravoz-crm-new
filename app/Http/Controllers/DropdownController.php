<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class DropdownController extends Controller
{

    public function getSpare(Request $request)
    {
      $spare = DB::table("spare")
                  ->leftjoin("models",DB::raw("FIND_IN_SET(models.id,spare.model_id)"),">",DB::raw("'0'"))
                  ->whereRaw("find_in_set('".$request->model_id."',spare.model_id)")
                  ->pluck("spare.spare_name","spare.id");
      return response()->json($spare);
    }

    public function getCurrency(Request $request) {
      return $this->getCompanyCurrency($request->id);
    }
    public function get_by_branch(Request $request)
    {
     
      if (!$request->country_id) {
          $html = '<option value="">Please Select</option>';
      } else {
          $html = '';
          $branches = $this->getBranches(Session::get('sessi_branch_id'), $request->country_id);
          $html = '<option value="">Please Select</option>';
          foreach ($branches as $key => $val) {
            $html .= '<option value="'.$key.'">'.$val.'</option>';
          }
      }
      return response()->json(['html' => $html]);
    }

    public function varifyIMEI(Request $request)
    {
      $imei_no = trim($request->imei_no);
      $query = DB::table("imei_details")->where('imei_details.IMEI_no1', '=', $imei_no)->orWhere('imei_details.IMEI_no2', '=', $imei_no);
      $query->get();
      if($query->count()> 0)
      {
        echo json_encode(false);
      } else { 
        echo json_encode(true);
      }
    }

    public function varifyPCBNo(Request $request)
    {
      $pcb_serial_no = trim($request->pcb_serial_no);
      $query = DB::table("imei_details")->where('pcb_serial_no', '=', $pcb_serial_no)->whereNotNull('expiry_date');
      $query->get();
      if($query->count()> 0)
      {
        echo json_encode(false);
      } else { 
        echo json_encode(true);
      }
    }
    
}

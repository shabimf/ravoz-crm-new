(function() {
    var i = 1;
    FTX.SALERETURN = {

        list: {

            selectors: {
                Sale_return_table: $('#sale-return-table'),
            },

            init: function(country_id, model_id, from, to) {
                this.selectors.Sale_return_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.Sale_return_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.country_id = country_id,
                                d.model_id = model_id
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'return_date', name: 'return_date' },
                        { data: 'country_name', name: 'country_name' },
                        { data: 'activation_date', name: 'activation_date' },
                        { data: 'IMEI_no1', name: 'IMEI_no1' },
                        { data: 'IMEI_no2', name: 'IMEI_no2' },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'invoice_no', name: 'invoice_no' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'user_name', name: 'user_name' },



                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();
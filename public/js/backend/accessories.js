FTX.Accessories = {

    list: {

        selectors: {
            accessories_table: $('#accessories-table'),
        },

        init: function () {

            this.selectors.accessories_table.dataTable({

                processing: false,
                serverSide: true,

                ajax: {
                    url: this.selectors.accessories_table.data('ajax_url'),
                    type: 'post',
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                    { data: 'product_type', name: 'product_type' },
                    { data: 'name', name: 'name' },
                    { data: 'actions', name: 'actions', searchable: false, sortable: false },
                    { data: 'status', name: 'status', searchable: false, sortable: false }
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                "createdRow": function (row, data, dataIndex) {
                    FTX.Utils.dtAnchorToForm(row);
                }
            });
        }
    },
}
$(".imei_success").click(function(e) {
    var _validator = $("form[name='doa_form']").validate({
        rules: {
            problem_remarks: "required"
        },
        messages: {
            problem_remarks: "Please enter remarks"
        },
        submitHandler: function(form) {
            var formData = new FormData(form);
            formData.append('service_id', service_id)
            var token = $('meta[name="csrf-token"]').attr('content');
            $(".imei_success").addClass('hide');
            var service_id = 0;
            if ($("input[name='service_id']").val()) {
                service_id = $("input[name='service_id']").val();
            }
            var type = 0;
            formData.append('service_id', service_id);
            formData.append('type', type);
            // var datastring = $(form).serialize() + '&_token=' + token + '&type=0' + '&service_id=' + service_id;
            $.ajax({
                url: "/service/doa/update1",
                type: "POST",
                header: {
                    'X-CSRF-TOKEN': token,
                },
                contentType: false,
                processData: false,
                data: formData,
                cache: false,
                success: function(dataResult) {
                    if (dataResult) {
                        $(".doa_msg").slideDown();
                        setTimeout(function() {
                            $(".imei_success").removeClass('hide');
                            $('#doaModal').modal('hide');
                            window.location.reload();
                        }, 4000);
                    }
                }
            });
        }
    });
});

function getProductType(query) {
    $.ajax({
        url: "/service/getDeviceType",
        type: "POST",
        data: { query: query },
        cache: false,
        dataType: 'json',
        success: function(data) {
            return data;
        }
    });
}


$(".check_replace_imei").click(function(e) {
    var query = $("#imei_selected").val();
    var product_type = getProductType(query);
    var country_id = $("#country_id").val();
    $.ajax({
        url: "/service/getIMEIData",
        type: "POST",
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            query: query,
            product_type: product_type
        },
        cache: false,
        dataType: 'json',
        success: function(dataResult) {
            console.log(dataResult);
            var resultData = dataResult.data;
            if (resultData.length > 0) {
                $.each(resultData, function(index, row) {
                    var device = row.model_name;
                    device += "(";
                    device += row.ram_name + "," + row.rom_name + "," + row.color;
                    device += ")";
                    $("#model_name").html(device);
                    $("#billed_").val(1);
                    $("#imei_id").val(row.id);
                    $("#billed_status").html('Not Sold');
                    $(".imei_success").addClass('hide');


                    if ($('#brand_name').length) {
                        $("#brand_name").html(row.brand_name);
                        $("#manufature_date").html(row.date_of_manufacture);
                        $("#pcb").html(row.pcb_serial_no);
                        $(".imei_replace_success").addClass('hide');
                    }
                    if (country_id != row.country_id) {
                        $("#billed_status").html('Device not in same country');
                    } else if (row.expiry_date) {
                        $("#billed_status").html('Sold');
                    } else if (row.doa_status == 0 && row.is_doa > 0) {
                        $("#billed_status").html('This device already exist in DOA');
                        $(".imei_success").addClass('hide');
                    } else if (row.is_doa > 0) {
                        $("#billed_status").html('This device already exist in DOA');
                    } else if (row.sales_request_id > 0 && row.service_status == 0) {
                        $("#billed_status").html('IMEI/SL No Already exists in sales request');
                    } else {
                        $("#billed_").val(0);
                        $(".imei_success").removeClass('hide');
                        if ($('#brand_name').length) {
                            $(".imei_replace_success").removeClass('hide');
                        }
                    }

                })
            } else {
                alert("IMEI Number Not Found in Database");
                $("#model_name").html("");
                $("#billed_status").html('');
                $(".imei_success").addClass('hide');
                if ($('#brand_name').length) {
                    $("#brand_name").html('');
                    $("#manufature_date").html('');
                    $("#pcb").html('');
                    $(".imei_replace_success").addClass('hide');
                }
            }
        }
    });
});

$(".reset-btn").click(function() {
    $("#branch_id").val(null).trigger("change");
});
$(function() {
    $('input[name="from"]').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
        locale: {
            format: 'DD-MM-YYYY'
        }
    }).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
    $('input[name="to"]').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
        locale: {
            format: 'DD-MM-YYYY'
        }
    }).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
});

$(".imei_replace_success").click(function(e) {
    var token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("data-id");
    var datastring = $("#doa_replace_form").serialize() + '&_token=' + token + '&id=' + id;
    $.ajax({
        url: "/service/doa/imei/replace",
        type: "POST",
        header: {
            'X-CSRF-TOKEN': token
        },
        data: datastring,
        cache: false,
        success: function(dataResult) {
            if (dataResult) {
                $('#IMEIModal').modal('hide');
                window.location.reload();
            }
        }
    });
});


$("#choose_model").click(function() {
    $(".model-det").removeClass('d-none');
    $(".imei-det").addClass('d-none');
    $(".imei_success").removeClass('hide');
});
$("#enter_manually").click(function() {
    $(".imei-det").removeClass('d-none');
    $(".model-det").addClass('d-none');
    $(".imei_success").addClass('hide');
});

$(".imei_add").change(function() {  
    $(".input-group-append").removeClass('d-none');
});

function removeimage(val) {
    var img_id = $("#doaimage_id_" + val).val();
    $.ajax({
        url: "/service/doa/image/delete",
        type: "POST",
        data: {
            'id': img_id,
        },
        success: function(data) {
            window.location.reload();
        },
        error: function() {

        }
    });
}
$("#removeImage").click(function(e) {
    var invoice_id = $("#doa_invoice_id").val();
    $.ajax({
        url: "/service/doa/invoice/delete",
        type: "POST",
        data: {
            'id': invoice_id,
        },
        success: function(data) {
            window.location.reload();
        },
        error: function() {

        }
    });
});
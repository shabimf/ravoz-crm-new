<?php

namespace App\Http\Controllers\Backend\Accessories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Accessories;
use Illuminate\Support\Facades\View;
use Auth;


class AccessoriesController extends Controller
{

    public function __construct()
    {
        $this->data['product_type'] = $this->NameAndID('product_type');
        View::share('js', ['accessories', 'accessories','master']);
    }

    public function index()
    {
        return view('admin.accessories.index');
    }

    public function getAccessoriesData()
    {
        $data = Accessories::leftJoin('product_type', 'product_type.id', '=', 'accessories.product_type')
                    ->select([
                        'accessories.id',
                        'accessories.name',
                        'accessories.status',
                        'product_type.name as product_type'
                    ])
                    ->get();

        return DataTables::of($data)
            ->escapeColumns(['name'])
            ->addColumn('actions', function($data) {
                $action = '<a href="'.route('accessories.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
                return $action;
            })
            ->addIndexColumn()
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="accessories" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->make(true);
    }

    public function create()
    {
        return view('admin.accessories.create', $this->data);
    }


    public function store(Request $request)
    {

        $post = new Accessories();

        $validator = $this->validateAccessoriesForm($request, 0);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetAccessoriesForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The Accessories was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateAccessoriesForm($request, $id);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = Accessories::find($id);
        $post = $this->GetAccessoriesForm($post, $request);
        if ($post->save()){
            return redirect()->back()->with("success","The Accessories was successfully updated.");
        }

    }

    public function edit($id)
    {
        $this->data['edit_accessories'] = Accessories::find($id);
        $this->data['edit_id'] = $id;
        return view('admin.accessories.create', $this->data);
    }




}

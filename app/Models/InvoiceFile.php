<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceFile extends Model
{
    use HasFactory;
    protected $table = 'invoice_file';
    public $timestamps = false;
    protected $guarded = array();
}

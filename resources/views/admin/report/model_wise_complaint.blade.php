@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Model Wise Complaint Report</h4>
            </div>
        </div>
        <div class="row ">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'report.complaint','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-complaint']) }}
					<div class="form-row well">
						<div class="form-group col-md-3">
                            {{ Form::label('name', __('Model')) }}
							{{ Form::select('model_id', $model, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model','id' => 'model_id']) }}
						</div>
						<div class="form-group col-md-3">
						  <button class="btn btn-primary search-btn">Search</button>
                          <a class="btn btn-primary reset-btn" href="{{url('report/complaint')}}">Reset</a>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
				    <div class="dt-buttons float-right">  
						<a class="btn btn-primary buttons-excel buttons-html5"  href="{{ url('report/complaint/file-export?model_id='.$model_id.'') }}"><span>Excel</span></a>
					</div>
					  <div class="table-responsive">
						<table id="model-com-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("report.complaint.get") }}">
							<thead>
								<tr>
									<th width="5">#</th>
									<th>Complaint</th>
                                    <th>Warranty</th>
                                    <th>Without Warranty</th>
									<th>Total</th>
								</tr>
							</thead>
						</table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
var model_id =  getUrlParameter('model_id');
FTX.Utils.documentReady(function() {
    FTX.SERVICEMODCOM.list.init(model_id);
});
</script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

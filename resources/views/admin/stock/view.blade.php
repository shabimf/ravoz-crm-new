@extends('layouts.service')
@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                <h4 class="card-title-text">View Purchase Items</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('purchase') }}"> <i
                            class="feather icon-list"></i> Purchase Spare List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            <div class="form-row">
               <div class="form-group col-md-6">
                {{ Form::label('date', __('Date')) }}
                {{ Form::text('date', old('date',(isset($list)) ? date('d-m-Y', strtotime($list[0]->date)):''), ['class' => 'form-control', 'readonly' => 'readonly']) }}
               </div>
               <div class="form-group col-md-6">
                {{ Form::label('name', __('Branch')) }}
                {{ Form::text('branch', old('branch',(isset($list)) ? $list[0]->branch_name:''), ['class' => 'form-control', 'readonly' => 'readonly']) }}
               </div>
               
            </div>
            <table class="table table-bordered" id="dynamicTable">  
                <thead>
                <tr>
                    <th>Model</th>
                    <th>Spare</th>
                    <th>Part Code</th>
                    <th>Focus Code</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th class="{{(userHasPermission('Service','stock_edit')?'':'hide')}}">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $row)
                  <tr>
                    <td>{{ ($row->model_name?$row->model_name:"Other") }}</td>
                    <td>{{ $row->spare_name }}{{ $row->spare_id }}</td>
                    <td>{{ $row->part_code }}</td>
                    <td>{{ $row->focus_code }}</td>
                    <td>
                        @if(userHasPermission('Service','stock_edit'))
                        <input type="text" value="{{$row->qty}}" class="form-control" id="qty_{{$row->id}}">
                        @else
                        {{$row->qty}}
                        @endif
                    </td>
                    <td>
                        @if(userHasPermission('Service','stock_edit') && checkPurchaseItemLastRecord($row->spare_id,$list[0]->branch_id) == $row->id)
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">{{ $row->currency }}</span>
                            </div>
                            <input type="text"  id="price_{{$row->id}}" value="{{number_format($row->local_price,2)}}" class="form-control">
                        </div>
                        @else
                        {{ $row->currency }} {{number_format($row->local_price,2)}}
                        @endif
                    </td>
                    <td  class="{{(userHasPermission('Service','stock_edit')?'':'hide')}}"><button class="btn btn-primary updatecls" type="button" data-spare-id="{{$row->spare_id}}" data-id="{{$row->id}}" data-branch-id="{{$list[0]->branch_id}}" data-qty="{{$row->qty}}">Update</button></td>
                  </tr>
                @endforeach
                </tbody>
            </table> 
        </div>
    </div>
@endsection
@section('pagescript')
<style>
     .customTextfield input{
 border:unset;
  
}
.customTextfield input:focus{
  outline:unset;
}
.customTextfield{
  border:1px solid #000;
  padding:2px;
}
    </style>
<script>
$(".updatecls").on('click', function(event){
    var item_id = $(this).attr("data-id");
    var qty = $('#qty_'+item_id).val().trim();
    var price = 0;
    if( $('#price_'+item_id).length > 0) {
      price = $('#price_'+item_id).val().trim();
    }   
    
    var branch_id = $(this).attr("data-branch-id");
    var oldqty = $(this).attr("data-qty").trim();
    var spare_id = $(this).attr("data-spare-id").trim();
    var formData = 'item_id='+item_id+'&branch_id='+branch_id+'&spare_id='+spare_id+'&qty='+qty+'&price='+price+'&oldqty='+oldqty;
    $.ajax({
        type: "POST",
        url: "{{ route('stock.update.items') }}",
        data: formData,
        success: function(data) {
            window.location.reload();
        }
    });
});
</script>
@endsection


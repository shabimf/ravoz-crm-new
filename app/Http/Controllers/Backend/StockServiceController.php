<?php
namespace App\Http\Controllers\Backend;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use DB;
use Auth;
use Session;
class StockServiceController extends Controller {
    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->data['model'] = $this->NameAndID('models');
            $this->data['access'] = $this->NameAndID('accessories');
            $this->data['branch'] = $this->getBranches(Session::get('sessi_branch_id'));
            $this->data['brands'] = $this->NameAndID('brands');
            return $next($request);
        });
        View::share('js', ['master', 'stock_service']);
    }
    public function index(Request $request) {
        $this->data['users'] = $this->GetUsers(Auth::user()->branch_id);
        $level = $this->GetComplaints();
        $levels = array();
        foreach ($level as $v) {
            if (!isset($levels[$v->name])) {
                $levels[$v->name] = array();
            }
            $levels[$v->name][$v->id] = $v->description;
        }
        $this->data['levels'] = $levels;
        $this->data['branch_id'] = $request->branch_id ? $request->branch_id : 0;
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['imei'] = $request->imei ? $request->imei : "";
        return view('admin.stock-service.index', $this->data);
    }
    public function create() {
        return view('admin.stock-service.create', $this->data);
    }
    public function getDOAIMEIData(Request $request) {
        return $this->checkDOAData($request);
    }
    public function getDOAData(Request $request) {
        $name = $request['query'];
        $query = array();
        $doa_det=DB::table("doa")->where('doa.serial_no', '=', $name)->first();
        if(empty($doa_det))
        {
            return 0;
        }
        if($doa_det->imei_id!='')
        {
            $data = DB::table("doa")
            ->leftJoin('imei_details', 'doa.imei_id', '=', 'imei_details.id')
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('brands', 'brands.id', '=', 'doa.brand_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.ram_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('stock_service', 'doa.id', '=', 'stock_service.doa_id')
            ->select(
                "doa.*",
                "models.name as model_name",
                "brands.name as brand_name",
                "brands.id as brand_id",
                "models.factory_name",
                "models.id as model_id",
                "ram.name as ram_name",
                "rom.name as rom_name",
                "color.name as color",
                "variant_details.id as variant_id",
                "stock_service.status as stock_status",
                "stock_service.id as stock_service_id",
                "imei_details.IMEI_no1",
                "imei_details.IMEI_no2")
            ->where('doa.serial_no', '=', $name);
        }
        else
        {
            $data = DB::table("doa")
            ->leftJoin('variant_details', 'variant_details.id', '=', 'doa.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('brands', 'brands.id', '=', 'doa.brand_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.ram_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('stock_service', 'doa.id', '=', 'stock_service.doa_id')
            ->leftJoin('imei_details', 'doa.imei_id', '=', 'imei_details.id')
            ->select(
                "doa.*",
                "models.name as model_name",
                "brands.name as brand_name",
                "brands.id as brand_id",
                "models.factory_name",
                "models.id as model_id",
                "ram.name as ram_name",
                "rom.name as rom_name",
                "color.name as color",
                "variant_details.id as variant_id",
                "stock_service.status as stock_status",
                "stock_service.id as stock_service_id",
                "imei_details.IMEI_no1",
                "imei_details.IMEI_no2")
            ->where('doa.serial_no', '=', $name);
        }
        $query = json_decode(json_encode($data->orderBy('doa.id', 'DESC')->get()),1);
        $query = $data->orderBy('doa.id', 'DESC')->get();
        // echo "<pre>"; print_r($query); echo "</pre>"; exit;
        return $query
            ? json_encode(array(
                'data' => $query
            )) : [];
    }
    public function update(Request $request, $id = NULL) {
        $post = $postdata = array();
        $post = $this->GetStockServiceForm($post, $request);
        if ($request->stock_service_id) {
            $stock_service_id = $request->stock_service_id;
            $this->updatedata('stock_service', $post, $stock_service_id);
        } else {
            $post['created_by'] = Auth::id();
            $post['branch_id'] = Auth::user()->branch_id;
            $post['serial_no'] = "JOB" . $this->serialNumber("stock_service", $post['branch_id']);
            //var_dump($post);exit;
            $stock_service_id = $this->insertGetId('stock_service', $post);
            $complaint = (isset($request->complaint) ? implode(",", $request->complaint) : '');
            if ($complaint) {
                foreach ($request->complaint as $complaint_id) {
                    $postdata['branch_id'] = Auth::user()->branch_id;
                    $postdata['stock_service_id'] = $stock_service_id;
                    $postdata['complaint_id'] = $complaint_id;
                    $this->insertGetId('service_complaint', $postdata);
                }
            }
        }
        return $stock_service_id;
    }
    public function getStockServiceData(Request $request) {
        $data = DB::table('stock_service')
                ->leftJoin('branch', 'branch.id', '=', 'stock_service.branch_id')
                ->leftJoin('imei_details', 'imei_details.id', '=', 'stock_service.imei_id')
                ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
                ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
                ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
                ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
                ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
                ->select(['stock_service.*', 'branch.name as branch_name', 'models.name as model_name', 'color.name as color', 'ram.name as ram', 'rom.name as rom', 'imei_details.IMEI_no1', 'imei_details.IMEI_no2', 'imei_details.serial_no as imei_sn']);
        if (!empty($request->branch_id)) {
            $data->where("stock_service.branch_id", $request->branch_id);
        } else {
            if (Session::get('sessi_branch_id')) {
                $data->whereIn('stock_service.branch_id', explode(',', Session::get('sessi_branch_id')));
            }
        }
        if (!empty($request->imei)) {
            $data->where('imei_details.IMEI_no1', '=', $request->imei)->orWhere('imei_details.IMEI_no2', '=', $request->imei);
        }
        if (isset($request->is_approved)) {
            $data->where("stock_service.is_approved", 0);
        }
        $from = $request->from ? date('Y-m-d', strtotime($request->from)) : "";
        $to = $request->to ? date('Y-m-d', strtotime($request->to)) : "";
        if (!empty($from) && !empty($to)) {
            $data->whereBetween('stock_service.created_at', [$from, $to]);
        }
        $data->get();
        return DataTables::of($data)->escapeColumns(['created_at', 'branch_name'])->filterColumn('created_at', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(stock_service.created_at,'%d-%m-%Y'))") . ' like ?', ["%{$keyword}%"]);
        })->filterColumn('branch_name', function ($query, $keyword) {
            $query->whereRaw('branch.name like ?', ["%{$keyword}%"]);
        })->filterColumn('IMEI_no1', function ($query, $keyword) {
            $query->whereRaw('imei_details.IMEI_no1 like ?', ["%{$keyword}%"]);
        })->filterColumn('model_name', function ($query, $keyword) {
            $query->whereRaw('models.name like ?', ["%{$keyword}%"])->orWhere('color.name', 'LIKE', "%{$keyword}%")->orWhere('rom.name', 'LIKE', "%{$keyword}%")->orWhere('rom.name', 'LIKE', "%{$keyword}%");
        })->editColumn('created_at', function ($data) {
            return date('d-m-Y', strtotime($data->created_at));
        })->editColumn('model_name', function ($data) {
            if($data->model_name=='')
            {
                $res=DB::table('doa')
                    ->leftJoin('variant_details', 'variant_details.id', '=', 'doa.variant_id')
                    ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
                    ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
                    ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
                    ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
                    ->select([
                        'models.name as model_name',
                        'color.name as color',
                        'ram.name as ram',
                        'rom.name as rom',
                    ])->where("doa.id", '=', $data->doa_id)->first();
                    $model = "";
                    if ($res->model_name) {
                        $model = $res->model_name . "(" . $res->color;
                        if ($res->ram) {
                            $model .= "," . $res->ram;
                        }
                        if ($res->rom) {
                            $model .= "," . $res->rom;
                        }
                        $model .= ")";
                    }
                    return $model;
            }
            else
            {
                $model = "";
                if ($data->model_name) {
                    $model = $data->model_name . "(" . $data->color;
                    if ($data->ram) {
                        $model .= "," . $data->ram;
                    }
                    if ($data->rom) {
                        $model .= "," . $data->rom;
                    }
                    $model .= ")";
                }
                return $model;
            }
            // $model = "";
            // if ($data->model_name) {
            //     $model = $data->model_name . "(" . $data->color;
            //     if ($data->ram) {
            //         $model.= "," . $data->ram;
            //     }
            //     if ($data->rom) {
            //         $model.= "," . $data->rom;
            //     }
            //     $model.= ")";
            // }
            // return $model;
        })->editColumn('status', function ($data) {
            if ($data->status == 0) {
                $status = '<i class="fa fa-circle text-warning mr-2"></i> Pending';
            } elseif ($data->status == 1) {
                $status = '<i class="fa fa-circle text-success mr-2"></i> Repaired';
            } elseif ($data->status == 4) {
                $status = '<i class="fa fa-circle text-success mr-2"></i> Completed';
            }
            return $status;
        })->editColumn('serial_no', function ($data) {
            return '<a href="' . url('service/stock_service/' . $data->id) . '">' . $data->serial_no . '</a>';
        })->addColumn('actions', function ($data) {
            return '<a href="' . url('service/stock_service/' . $data->id) . '"  class="btn btn-primary btn-xs">View</a>';
        })->addIndexColumn()->make(true);
    }
    public function show($id) {
        $this->data['service'] = $this->getStockService($id);
        $this->data['approve_level'] = $this->getLevels($id, 1);
        if (isset($this->data['service']->expiry_date) && $this->data['service']->expiry_date != "") {
            $this->data['service']->editstatus = "Sold";
        } else {
            $this->data['service']->editstatus = "Not Sold";
        }
        // $accessories_id = array();
        // $accessories_id = explode(',', $this->data['service']->accessories_id);
        // $this->data['service']->accessories_id=$accessories_id;
        // $this->data['service']->accessories_id=$this->data['service']->accessories_id;
        $branch_id = Auth::user()->branch_id;
        $level = $this->GetComplaints();
        $levels = array();
        foreach ($level as $v) {
            if (!isset($levels[$v->name])) {
                $levels[$v->name] = array();
            }
            $levels[$v->name][$v->id] = $v->description;
        }
        $this->data['levels'] = $levels;
        $this->data['get_complaints'] = json_decode(json_encode(DB::table('complaints')->leftJoin('service_complaint', 'service_complaint.complaint_id', '=', 'complaints.id')->select('complaints.*', 'service_complaint.status', 'service_complaint.id as service_comp_id')->where('service_complaint.stock_service_id', $id)->get()), 1);
        $this->data['getSpareDetails'] = json_decode(json_encode(DB::table('service_spare')->leftJoin('spare', 'service_spare.spare_id', '=', 'spare.id')->select('service_spare.*', 'spare.spare_name', 'spare.part_code', 'spare.focus_code')->where('service_spare.stock_service_id', $id)->get()), 1);
        $models = $this->GetSpares($this->data['service']->model_id);
        $model = array();
        foreach ($models as $v) {
            if (!isset($model[$this->data['service']->model])) {
                $model[$this->data['service']->model] = array();
            }
            if (!isset($v->name)) $v->name = "Others";
            else $v->name = $this->data['service']->model;
            $model[$v->name][$v->id] = $v->spare_name;
        }
        $this->data['models'] = $model;
        $this->data['version'] = $this->getNameByID('software_version', 'model_id', $this->data['service']->model_id);
        $this->data['users'] = $this->GetUsers(Auth::user()->branch_id);
        $country_id = $this->FieldByID("branch", "country_id", Auth::user()->branch_id);
        $this->data['transfer_branches'] = $this->getBranch($country_id, Auth::user()->branch_id);
        $this->data['currency'] = $this->getCompanyCurrency(NULL);
        // echo "<pre>"; print_r($this->data); echo "</pre>"; exit;
        return view('admin.stock-service.view', $this->data);
    }
    public function addStockServiceComplaint(Request $request) {
        $stock_service_id = $request->input('stock_service_id');
        $complaints = $request->input('complaint');
        $branch_id = Auth::user()->branch_id;
        $technician_id = Auth::user()->id;
        foreach ($complaints as $comp) {
            if ($this->GetRowCount('service_complaint', array("stock_service_id" => $stock_service_id, "complaint_id" => $comp), 'id') == 0) {
                DB::table('service_complaint')->insertGetId(["stock_service_id" => $stock_service_id, "complaint_id" => $comp, "branch_id" => $branch_id, "technician_id" => $technician_id, "status" => 0]);
            }
        }
        return 1;
    }
    public function addStockServiceSpare(Request $request) {
        $spare_id = $request->input('spare');
        $stock_service_id = $request->input('stock_service');
        //$branch_id = Auth::user()->branch_id;
        $branch_id = $this->FieldByID('stock_service', 'branch_id', $stock_service_id);
        $currency = $this->GetCurrencyRate($branch_id);
        $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        $stock = DB::table('stock')->where(['spare_id' => $spare_id, "branch_id" => $branch_id])->first();
        $stock = json_decode(json_encode($stock), 1);
        $quantity = $request->input('qty');
        if (empty($stock)) {
            return 'NA';
        }
        if ($quantity > $stock['qty']) {
            return 'NA';
        } else {
            $stock_qty = $stock['qty'] - $quantity;
        }
        $price = $request->input('price');
        $warranty = $request->input('warranty');
        $insdata = array();
        $insdata['spare_id'] = $spare_id;
        $insdata['stock_service_id'] = $stock_service_id;
        $insdata['is_warranty'] = 1;
        $insdata['price'] = DB::table('stock')->where('spare_id', $spare_id)->first()->price;
        $insdata['local_price'] = $price;
        $insdata['quantity'] = $quantity;
        $insdata['branch_id'] = $branch_id;
        $insdata['created_by'] = Auth::user()->id;
        if (isset($stock_qty)) {
            $updateData['qty'] = $stock_qty;
            $this->updatedata('stock', $updateData, $stock['id']);
        }
        $servicespare = DB::table('service_spare')->where(['spare_id' => $spare_id, "stock_service_id" => $stock_service_id])->first();
        $servicespare = json_decode(json_encode($servicespare), 1);
        if (!empty($servicespare)) {
            $updatespare['quantity'] = $servicespare['quantity'] + $quantity;
            return $this->updatedata('service_spare', $updatespare, $servicespare['id']);
        } else {
            return DB::table('service_spare')->insertGetId($insdata);
        }
    }
    public function doaStatusUpdate(Request $request) {
        // echo "<pre>"; print_r($request->input()); echo "</pre>"; exit;
        $stock_service_id = $request->input('stock_service');
        $where_total = array("stock_service_id" => $stock_service_id);
        $where_pending = array("stock_service_id" => $stock_service_id, "status" => '0');
        $total_complaint = $this->GetRowCount('service_complaint', $where_total, 'id');
        $pending_complaint = $this->GetRowCount('service_complaint', $where_pending, 'id');
        if ($pending_complaint != 0) {
            return 2;
        }
        $stock_service = $this->GetFirst('stock_service', array("id" => $stock_service_id));
        $doa_id = $stock_service->doa_id;
        $imei_id = $stock_service->imei_id;
    
        if($stock_service->status != 4) {
            $udata['completion_date'] = date('Y-m-d H:i:s');
            $this->updatedata('stock_service', $udata, $stock_service_id);
        }
        $this->updateStatus('stock_service', $stock_service_id, '4', 'status');
        Session::flash('success', 'Updated successfully');
        return $this->updateStatus('imei_details', $imei_id, '0', 'is_doa');
    }
    public function updateComplaintStatus(Request $request) {
        $id = $request->input('ref');
        $status = $request->input('sta');
        $data['status'] = $status;
        if ($status == 1) {
            $branch_id = Auth::user()->branch_id;
            $technician_id = Auth::user()->id;
            $currency = $this->GetCurrencyRate($branch_id);
            $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
            $complaint_id = $this->GetFirst('service_complaint', array("id" => $id))->complaint_id;
            $level_id = $this->GetFirst('complaints', array("id" => $complaint_id))->level_id;
            $complaint_rate = $this->GetFirst('levels', array("id" => $level_id))->rate;
            $local_price = ($complaint_rate * $currency_rate);
            $data['price'] = $complaint_rate;
            $data['local_price'] = $local_price;
        }
        return $update = $this->updatedata('service_complaint', $data, $id);
    }
    public function sentAppealRequest(Request $request) {
        if ($request->service_type == 1) {
            $post['level_id'] = $request->level_id;
            $post['level_price'] = ($request->rate??0);
            $currency = $this->GetCurrencyRate(Auth::user()->branch_id);
            $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
            $post['level_local_price'] = ($post['level_price'] * $currency_rate);
            $post['is_approved'] = 1;
            $post['is_approved_by'] = Auth::user()->id;
            $post['is_approved_at'] = date("Y-m-d H:i:s");
            $this->updatedata("stock_service", $post, $request->service_id);
            return true;
        } else {
            $post['service_id'] = $request->service_id;
            $post['serial_no'] = "APPE" . $this->serialNumber("appeal_master", Auth::user()->branch_id);
            $branch = $this->GetFirst("stock_service", array("service_id" => $post['service_id']));
            $post['from_branch'] = Auth::user()->branch_id;
            $post['to_branch'] = $branch->branch_id;
            $post_data['parent_id'] = $this->insertGetId("appeal_master", $post);
            $post_data['msg'] = $request->appeal_remarks;
            $post_data['created_by'] = Auth::user()->id;
            $this->insertdata("appeal_rel_comments", $post_data);
            return true;
        }
    }
    public function getAppeal($id) {

        $this->data['comments'] = DB::table('appeal_master')->select('appeal_master.id', 'appeal_rel_comments.msg', 'appeal_rel_comments.file', 'appeal_rel_comments.created_at', 'appeal_master.serial_no', 'users.first_name', 'appeal_master.status')->leftJoin('appeal_rel_comments', 'appeal_master.id', '=', 'appeal_rel_comments.parent_id')->leftJoin('users', 'users.id', '=', 'appeal_rel_comments.created_by')->where('appeal_master.stock_service_id', '=', $id)->orderBy('appeal_rel_comments.id', 'DESC')->get();
        return view('admin.service.appeal', $this->data);
    }

    public function replaceIMEI(Request $request) {
       $imei_id = $this->FieldByID('stock_service','imei_id',$request->service_id);
       if($imei_id) {
        $imei = $this->GetFirst('imei_details',['id'=>$imei_id]);
        $data['service_id'] =  $request->service_id;
        $data['imei_id'] =  $imei_id;
        $data['imei_no1'] = $udatah['imei_no1'] = $imei->IMEI_no1;
        $data['imei_no2'] = $udatah['imei_no2'] = $imei->IMEI_no2;
        $udata['IMEI_no1'] = $request->imei_no_1;
        $udata['IMEI_no2'] = $request->imei_no_2;
        if ($checkExist = $this->GetFirst('stock_service_imei_history',['service_id'=>$data['service_id'],'imei_id'=>$data['imei_id']])) {
            $id = $checkExist->id;
            $this->updatedata('stock_service_imei_history',$udatah,$id);
        } else {
            $id = $this->insertGetId('stock_service_imei_history', $data);
        }
        $this->updatedata('imei_details',$udata,$imei_id);
        return $id;
       }
    }
}

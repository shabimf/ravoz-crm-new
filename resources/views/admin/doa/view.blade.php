@extends('layouts.service')
@section('content')
<style>
    .rmv {
        cursor: pointer;
        color: #fff;
        border-radius: 30px;
        border: 1px solid #fff;
        display: inline-block;
        background: rgba(255, 0, 0, 1);
        margin: -5px -10px;
    }

    .rmv:hover {
        background: rgba(255, 0, 0, 0.5);
    }
</style>
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-6">
                <h1>DOA</h1>
            </div>
            <div class="col-md-6 {{(userHasPermission('Service','doa_list')?'':'hide')}}">
                <div class="add-new">
                    <a class="btn btn-primary" href="{{ route('doa.index') }}">
                        <i class="feather icon-list"></i> DOA - List
                    </a>
                </div>
            </div>
        </div>
    </div>
    @include('inc.alert')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row pb-5 p-5">
                        <div class="col-md-4">
                            <h5 class="text-primary">Serial Num: {{$doa->serial_no}}</h5>
                            <p>Date to: {{date("F j, Y",strtotime($doa->created_at))}}</p>
                        </div>
                        <div class="col-md-8 text-md-right mt-4 mt-md-0">
                            <p class="mb-1"><span class="text-muted">Branch: </span>
                                {{$doa->branch_name}}
                            <p class="mb-1"><span class="text-muted">Status: </span>
                                @if ($doa->status == 0)
                                <a href="#" class="badge badge-warning">Pending</a>
                                @elseif ($doa->status == 1)
                                <a href="#" class="badge badge-success">Accepted</a>
                                @else
                                <a href="#" class="badge badge-danger">Rejected</a>
                                @endif
                            </p>
                        </div>
                    </div>
                    <hr>
                    <div class="row p-5">
                        <div class="col-md-12">
                            @if($doa->issue_doa==0 && $doa->status!=2 && $doa->service_id==0)
                            <a class="edit_work text-primary text-right pull-right cp" data-id="{{ $doa->id }}"
                                data-toggle="modal" data-target="#doaModal">
                                <i class="la la-edit edit"></i>
                            </a>
                            @endif
                            <div class="clearfix"></div>
                            <div class="table-responsive">
                                <table
                                    class="table table-condensed device_table  table-bordered table-hover no-margin sf">
                                    <tbody>
                                        <tr>
                                            <th> IMEI NO1 </th>
                                            <td><strong>@if(isset($doa->IMEI_no1)){{$doa->IMEI_no1}}@endif</strong></td>
                                            <th> IMEI NO2 </th>
                                            <td><strong>@if(isset($doa->IMEI_no2)){{$doa->IMEI_no2}}@endif</strong></td>
                                        </tr>
                                        <tr>
                                            <th> Serial No </th>
                                            <td colspan="3">@if(isset($doa->sn_no)) {{$doa->sn_no}} @endif</td>
                                        </tr>
                                        <tr>
                                            <th> Model </th>
                                            <td>{{$doa->model_name}}
                                                (
                                                @if($doa->color){{$doa->color}}@endif
                                                @if($doa->ram),{{$doa->ram}}@endif
                                                @if($doa->rom),{{$doa->rom}}@endif

                                                )
                                            </td>
                                            <th> Sold Date </th>
                                            <td> @if(isset($doa->activation_date)){{date('d-m-Y',strtotime($doa->activation_date))}}
                                                @else ($doa->sales_date?date('d-m-Y',strtotime($doa->sales_date)):" ")
                                                @endif </td>
                                        </tr>
                                        <tr>
                                            <th> Service Date </th>
                                            <td> {{($doa->service_date?date('d-m-Y',strtotime($doa->service_date)):"")}}
                                            </td>
                                            <th> Special Request </th>
                                            <td> {{($doa->is_special_request?"Yes":"No")}} </td>
                                        </tr>
                                        <tr>
                                            <th> Problem Remarks </th>
                                            <td colspan="3"> {{$doa->remarks}} </td>
                                        </tr>

                                        @if($doa->access)
                                        <tr>
                                            <th> Check List </th>
                                            <td colspan="3"> {{$doa->access}}</td>

                                        </tr>
                                        @endif
                                        @if($doa->status == 0 && userHasPermission('Service','doa_status_change'))
                                        <tr>
                                            <td colspan="8">
                                                <button class="btn btn-overlay-secondary btn-sm float-right btn-accept"
                                                    data-id="{{$doa->id}}" data-imeiid="{{$doa->imei_id}}"
                                                    data-type="2">Reject</button>
                                                <button class="btn btn-overlay-primary btn-sm float-right btn-accept"
                                                    data-id="{{$doa->id}}" data-imeiid="{{$doa->imei_id}}"
                                                    data-type="1"> Accept </button>
                                            </td>
                                        </tr>
                                        @elseif(userHasPermission('Service','doa_issue') && $doa->replace_imei_id == ""
                                        && $doa->issue_doa == 0 && $doa->status == 1)
                                        <tr>
                                            <td colspan="8">
                                                <a class="btn btn-primary btn-sm float-right"
                                                    href="{{url('service/doa/pdf/'.$doa->id)}}" target="_blank">Issue
                                                    DOA</a>
                                                @if(isset($doa->IMEI_no1) && $doa->IMEI_no1!='' && isset($doa->IMEI_no2)
                                                && $doa->IMEI_no2!='')
                                                <button class="btn btn-primary btn-sm float-right" data-toggle="modal"
                                                    data-target="#IMEIModal">Replace IMEI</button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="IMEIModal" tabindex="-1" role="dialog" aria-labelledby="IMEIModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">IMEI Replacement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="doa_area">
                <div class="alert alert-success alert-dismissible doa_msg hide">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    DOA details <strong>successfully created!</strong>
                </div>
                <form action="" method="post" id="doa_replace_form" name="doa_replace_form">
                    <table class="table table-bordered table-condensed doa_table">
                        <tbody>
                            <tr>
                                <th class="success"> {{ Form::label('name', __('IMEI/SL')) }}</th>
                                <td colspan="3">
                                    <div class=" input-group">
                                        {{ Form::text('defective_imei', '', ['class' => 'form-control imei_add', 'id' =>
                                        'imei_selected']) }}
                                        {{ Form::hidden('imei_id', '', ['class' => 'form-control', 'id' => 'imei_id'])
                                        }}
                                        <div class="input-group-append">
                                            {{ Form::button('Check', ['class' => 'btn btn-overlay-dark btns
                                            check_replace_imei cp', 'id' => 'search']) }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="success">Model No</th>
                                <td id="model_name" width="30%"></td>
                                <th class="success">Brand</th>
                                <td id="brand_name" width="30%"></td>
                            </tr>
                            <th class="success">Date Of Manufature</th>
                            <td id="manufature_date" width="30%"></td>
                            <th class="success">PCB Serial No</th>
                            <td id="pcb" width="30%"></td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <input type="button" class="btn btn-success imei_replace_success pull-right hide"
                        data-id="{{$doa->id}}" name="issue_doa" value="Replace">
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="doaModal" tabindex="-1" role="dialog" aria-labelledby="doaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">DOA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @php
            $doa_invoice = DB::table('doa_invoice')->select('name', 'id')->where('doa_id', $doa->id)->first();
            $doa_images = DB::table('doa_attachments')->select('name', 'id')->where('doa_id', $doa->id)->get();
            // echo "<pre>"; print_r($doa); echo "</pre>"; exit;
            @endphp
            <div class="modal-body" id="doa_area">
                <div class="alert alert-success alert-dismissible doa_msg hide">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    DOA details <strong>successfully updated!</strong>
                </div>
                <form action="" method="post" id="doa_form" name="doa_form" enctype="multipart/form-data">
                    @csrf
                    <table class="table table-bordered table-condensed doa_table">
                        <tbody>
                            <tr>
                                <th class="success imei-det @if($doa->imei_id=="") d-none @endif"> {{ Form::label('name', __('IMEI/SL')) }}</th>
                                <th class="success model-det @if($doa->variant_id=="") d-none @endif">{{ Form::label('name', __('Model')) }}</th>
                                <td>
                                    {{ Form::hidden('doa_id', old('doa_id',(isset($doa->id)) ?
                                            $doa->id:''), ['class' => 'form-control', 'id' => 'doa_id']) }}
                                    <div class="imei-det  @if($doa->imei_id=="") d-none @endif">
                                        <div class="input-group">
                                            {{ Form::text('defective_imei', old('imei_id',(isset($doa->IMEI_no1)) ?
                                            $doa->IMEI_no1:''), ['class' => 'form-control imei_add', 'id' => 'imei_select']) }}
                                            {{ Form::hidden('doa_imei_id', '', ['class' => 'form-control', 'id' =>
                                            'doa_imei_id']) }}
                                            {{ Form::hidden('country_id', old('country_id',(isset($country_id)) ?
                                            $country_id:''), ['class' => 'form-control', 'id' => 'country_id']) }}
                                            <div class="input-group-append d-none">
                                                {{ Form::button('Check', ['class' => 'btn btn-overlay-dark btns
                                                doa_replace_imei cp', 'id' => 'search']) }}
                                            </div>
                                        </div>
                                        <a href="javascript:void(0);" id="choose_model">Choose Model</a>
                                    </div>
                                    <div class="model-det @if($doa->variant_id=="") d-none @endif">
                                        <div class="input-group">
                                            {{ Form::select('model_id', $model, old('model_id',(isset($doa->variant_id)) ?
                                            $doa->variant_id:""), ['class' => 'form-control js-example-basic-single',
                                            'placeholder' => 'Select Model']) }}
                                        </div>
                                        <a href="javascript:void(0);" id="enter_manually">Enter Manually</a>
                                    </div>
                                </td>
                            </tr>
                            <tr class="imei-det @if($doa->imei_id=="") d-none @endif">
                                <th class="success">Model No</th>
                                <td id="doa_model_name">{{ $doa->model_name .'( '.$doa->color.', '.$doa->ram.',
                                    '.$doa->rom.' )' }}</td>
                            </tr>
                            <tr class="imei-det @if($doa->imei_id=="") d-none @endif">
                                <th class="success">Status</th>
                                <td id="billed_status">Not Sold </td>
                                {{ Form::hidden('billed', '0', ['class' => 'form-control', 'id' => 'billed_']) }}
                            </tr>
                            <tr>
                                <th class="success">Remarks</th>
                                <td>
                                    {{ Form::textarea('problem_remarks', old('problem_remarks', (isset($doa->remarks)) ?
                                    $doa->remarks:''), ['class' => 'form-control', 'id' =>
                                    'problem_remarks']) }}
                                </td>
                            </tr>
                            <tr>
                                <th width="25%" class="success">Check List</th>
                                <td>
                                    @php
                                    if ($doa->access) {
                                    $accessories_array = explode(',', $doa->access);
                                    }
                                    @endphp
                                    @foreach ($access as $k => $v)
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" id="accessories_{{ $k }}" value="{{ $k }}"
                                            name="device_check[]" @if ($doa->access) @if (in_array($v,
                                        $accessories_array)) checked @endif @endif class="custom-control-input
                                        form-control">
                                        <label class="custom-control-label" for="accessories_{{ $k }}">{{ $v }}</label>
                                    </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th class="success">Special Request</th>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="is_special_request"
                                            value="1" name="is_special_request" @if($doa->is_special_request==1) checked
                                        @endif >
                                        <label class="custom-control-label" for="is_special_request"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="success">Invoice Upload</th>
                                <td><input type="file" class="form-control" id="invoice_upload" name="invoice_upload">
                                    @if(!empty($doa_invoice))
                                    <div><input type="hidden" id="doa_invoice_id" value="{{$doa_invoice->id}}" />
                                        <img src="{{ asset('uploads/doa/'.$doa_invoice->name)}}" width="50%"
                                            height="50%"><input type="button" id="removeImage" value="x" class="rmv" /><br />
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th class="success">Images Upload</th>
                                <td><input type="file" class="form-control" id="image_upload" name="image_upload[]"
                                        multiple>
                                    @if(!empty($doa_images))
                                        @foreach($doa_images as $key => $img)
                                        <div><input type="hidden" id="doaimage_id_{{$key}}" value="{{$img->id}}" />
                                            <img src="{{ asset('uploads/doa/'.$img->name)}}" width="35%"
                                                height="35%"><input type="button" id="removeImage_{{$key}}" onclick="removeimage('{{$key}}')" value="x" class="rmv" />
                                        </div>
                                        @endforeach
                                    @endif
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <hr>
                    <input type="submit" class="btn btn-success imei_success pull-right" name="issue_doa"
                        value="Update">
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('pagescript')
<script>

    $(".doa_replace_imei").click(function(e) {
        var query = $("#imei_select").val();
        var product_type = getProductType(query);
        var country_id = $("#country_id").val();
        $.ajax({
            url: "/service/getIMEIData",
            type: "POST",
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                query: query,
                product_type: product_type
            },
            cache: false,
            dataType: 'json',
            success: function(dataResult) {
                console.log(dataResult);
                var resultData = dataResult.data;
                if (resultData.length > 0) {
                    $.each(resultData, function(index, row) {
                        var device = row.model_name;
                        device += "(";
                        device += row.ram_name + "," + row.rom_name + "," + row.color;
                        device += ")";
                        $("#doa_model_name").html(device);
                        $("#billed_").val(1);
                        $("#doa_imei_id").val(row.id);
                        $("#billed_status").html('Not Sold');
                        $(".imei_success").addClass('hide');

                        if (country_id != row.country_id) {
                            $("#billed_status").html('Device not in same country');
                        } else if (row.expiry_date) {
                            $("#billed_status").html('Sold');
                        } else if (row.is_doa > 0) {
                            $("#billed_status").html('This device already exist in DOA');
                        } else if (row.sales_request_id > 0 && row.service_status == 0) {
                            $("#billed_status").html('IMEI/SL No Already exists in sales request');
                        } else {
                            $("#billed_").val(0);
                            $(".imei_success").removeClass('hide');
                        }
                    })
                }
                else
                {
                    alert("IMEI Number Not Found in Database");
                    $("#doa_model_name").html("");
                    $("#billed_status").html('');
                    $(".imei_success").addClass('hide');
                }
            }
        });
    });
    $( ".btn-accept" ).click(function() {
     var type = $(this).attr("data-type");
     var id = $(this).attr("data-id");
     var imei_id = $(this).attr("data-imeiid");
     var token = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
            url: "/service/doa/update",
			type: "PUT",
            data: {
                'type': type,
                'id': id,
                '_token' : token,
                'imei_id' :imei_id
            },success: function (data) {
               window.location.reload();
            },
            error: function () {

            }
      });
   });
</script>
@endsection

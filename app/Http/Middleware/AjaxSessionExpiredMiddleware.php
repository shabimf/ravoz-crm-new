<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class AjaxSessionExpiredMiddleware
{
    public function handle(Request $request, Closure $next)
    {
      
        if ($request->ajax() && empty(Auth::user())) { 
            return response()->json(['message' => 'Session expired'], 403);
        }

        return $next($request);
    }
}

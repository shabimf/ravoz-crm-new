<?php
use Illuminate\Support\Str;
use App\Http\Traits\PermissionTrait;
/**
 * Henerate UUID.
 *
 * @return uuid
 */
function generateUuid()
{
    return Str::uuid();
}
if (!function_exists("homeRoute"))
{
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function homeRoute()
    {
        if (access()->allow("view-backend"))
        {
            return "admin.dashboard";
        }
        elseif (auth()
            ->check())
        {
            return "frontend.index";
        }
        return "frontend.index";
    }
}
// Global helpers file with misc functions.
if (!function_exists("app_name"))
{
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config("app.name");
    }
}
if (!function_exists("access"))
{
    /**
     * Access (lol) the Access:: facade as a simple function.
     */
    function access()
    {
        return app("access");
    }
}
if (!function_exists("history"))
{
    /**
     * Access the history facade anywhere.
     */
    function history()
    {
        return app("history");
    }
}
if (!function_exists("gravatar"))
{
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app("gravatar");
    }
}
if (!function_exists("escapeSlashes"))
{
    /**
     * Access the escapeSlashes helper.
     */
    function escapeSlashes($path)
    {
        $path = str_replace("\\", DIRECTORY_SEPARATOR, $path);
        $path = str_replace("//", DIRECTORY_SEPARATOR, $path);
        $path = trim($path, DIRECTORY_SEPARATOR);
        return $path;
    }
}
if (!function_exists("getRouteUrl"))
{
    /**
     * Converts querystring params to array and use it as route params and returns URL.
     */
    function getRouteUrl($url, $url_type = "route", $separator = "?")
    {
        $routeUrl = "";
        if (!empty($url))
        {
            if ($url_type == "route")
            {
                if (strpos($url, $separator) !== false)
                {
                    $urlArray = explode($separator, $url);
                    $url = $urlArray[0];
                    parse_str($urlArray[1], $params);
                    $routeUrl = route($url, $params);
                }
                else
                {
                    $routeUrl = route($url);
                }
            }
            else
            {
                $routeUrl = $url;
            }
        }
        return $routeUrl;
    }
}
if (!function_exists("active_class"))
{
    /**
     * Get the active class if the condition is not falsy.
     *
     * @param        $condition
     * @param string $activeClass
     * @param string $inactiveClass
     *
     * @return string
     */
    function active_class($condition, $activeClass = "active", $inactiveClass = "")
    {
        return $condition ? $activeClass : $inactiveClass;
    }
}
if (!function_exists("set_active"))
{
    function set_active($route)
    {
        if (is_array($route))
        {
            return in_array(Request::path() , $route) ? "active" : "";
        }
        return Request::path() == $route ? "active" : "";
    }
}
if (!function_exists("get_accessories"))
{
    function get_accessories($ids)
    {
        if ($ids)
        {
            $id_arr = explode(",", $ids);
            $data = DB::table("accessories")->whereIn("id", $id_arr)->pluck("name")
                ->implode(", ");
        }
        return $data;
    }
}
if (!function_exists("getComplaintsCount"))
{
    function getComplaintsCount($service_id)
    {
        $complaintCount = DB::table("service_complaint")->where("service_id", "=", $service_id)->count();
        $complaintStatusCount = DB::table("service_complaint")->where("status", 0)
            ->where("service_id", "=", $service_id)->count();
        return $complaintCount == $complaintStatusCount ? 1 : 0;
    }
}
if (!function_exists("dateformat"))
{
    function dateformat($date)
    {
        return date("Y-m-d", strtotime($date));
    }
}

if (!function_exists("isFinished"))
{
    function isFinished($service_branch_id, $transfer_id = null, $received_back_user = null, $status = null)
    {
        if ($transfer_id == "")
        {
            return true;
        }
        else
        {
            if ($transfer_id > 0 && $service_branch_id == Auth::user()->branch_id && $received_back_user > 0)
            {
                return true;
            }
            elseif ($transfer_id > 0 && $service_branch_id == Auth::user()->branch_id && $status == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
if (!function_exists("isPending"))
{
    function isPending($service_id)
    {
        return DB::table("service")->where("id", $service_id)->where("status", "!=", 5)->count();
    }
}

if (!function_exists("isDOA"))
{
    function isDOA($doa_id, $status = null)
    {
        if ($doa_id > 0)
        {
            if ($status == 0)
            {
                return true;
            }
            return false;
        }
        return false;
    }
}

if (!function_exists("userHasPermission"))
{
    function userHasPermission($module, $permission)
    {
        $row = DB::table("module")->select("id")->where("name", $module)->first();
        $module_id = $row ? $row->id : 0;
        if ($module_id > 0)
        {
            $permission_result = DB::table("user_roles")->select("permission_ids", "all")
                                    ->where("role_id", Auth::user()
                                    ->role_id)
                                    ->where("module_id", "=", $module_id)->get()
                                    ->first();
            if ($permission_result)
            {
                $permission_array = explode(",", $permission_result->permission_ids);
                if ($permission_result->all == 1)
                {
                    return true;
                }
                else
                {
                    $permision = DB::table("permissions")->select("id")->where("name", $permission)->first();
                    $permision_id = $permision ? $permision->id : 0;
                    if ($permision_id > 0 && $permission_result->permission_ids)
                    {
                        if (in_array($permision_id, $permission_array))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        else
        {
            return false;
        }
    }
}

if (!function_exists("buttonHide"))
{
    function buttonHide($status = null)
    {   
        if(userHasPermission('Service','service_edit')) {
            return false;
        }
        elseif ($status == 4 || $status == 5)
        {
            return "hide";
        } 
        return false;
    }
}

if (!function_exists("stockServiceButtonHide"))
{
    function stockServiceButtonHide($status = null)
    {   
        if(userHasPermission('Service','stock_service_edit')) {
            return false;
        }
        elseif ($status == 4)
        {
            return "hide";
        } 
        return false;
    }
}

if (!function_exists("checkPurchaseItemLastRecord"))
{
    function checkPurchaseItemLastRecord($spare_id,$branch_id)
    {   
        $last_record =  DB::table('purchase_master')->select('purchase_items.id')
                            ->leftJoin('purchase_items', 'purchase_master.id', '=', 'purchase_items.parent_id')
                            ->where([
                                'purchase_items.spare_id' => $spare_id,
                                'purchase_master.branch_id' => $branch_id,
                            ])
                            ->orderBy('purchase_items.id', 'DESC')->take(1)
                            ->get();  
        return ($last_record?$last_record[0]->id:0);
    }
}
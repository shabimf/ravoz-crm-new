<?php
namespace App\Http\Controllers\Backend\Sales;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportActivation;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use App\Exports\ActivationReportExport;
use DB;
class ActivationController extends Controller {
    public function __construct() {
        View::share('js', ['master']);
    }
    public function index() {
        return view('admin.activation.index');
    }
    public function store(Request $request) {
        $this->validate($request, ['file' => 'required|file|mimes:xls,xlsx,csv|max:10240']);
        if ($request->file('file')->isValid()) {
            Excel::import(new ImportActivation, request()->file('file'));
            $data = Excel::toCollection(new ImportActivation(), $request->file('file'));
            // echo "<pre>"; print_r($data); echo "</pre>"; exit;
            $imei_arr = $im_d = $error = $date_valid = $im_ex = $im_sale = $country_ex = $im_doa_ex = [];
            $expire_exist = "";
            if (count($data) > 0) {
                $record_count = 0;
                DB::beginTransaction();
                foreach ($data[0] as $key => $value) {
                    if (trim($value['imeisn'])) {
                        // if ($this->transformDate($value['date']) == 1)
                        // {
                        //     $date_valid[] = $value['imei'];
                        // }
                        if (in_array($value['imeisn'], $imei_arr)) {
                            $im_d[] = $value['imeisn'];
                        }
                        $imei_check = DB::table('imei_details')->where('IMEI_no1', '=', $value['imeisn'])->orWhere('IMEI_no2', '=', $value['imeisn'])->orWhere('serial_no', '=', trim($value['imeisn']))->first();
                        $imei_id = ($imei_check ? $imei_check->id : 0);
                        $is_doa = ($imei_check ? $imei_check->is_doa : 1);
                        if ($imei_id == 0) {
                            $im_ex[] = $value['imeisn'];
                        } elseif ($is_doa == 1) {
                            $im_doa_ex[] = $value['imeisn'];
                        } else {
                            $expire_exist = DB::table('imei_details')->where('id', $imei_id)->whereNotNull('activation_date')->count();
                            if ($expire_exist > 0) {
                                $im_sale[] = $value['imeisn'];
                            } else {
                                $country_id = ($imei_check ? $imei_check->country_id : 0);
                                if ($country_id == 0) {
                                    $country_ex[] = $value['imeisn'];
                                } else {
                                    $country = $this->GetByID('country', $country_id);
                                    $warranty = ($country ? $country->warranty : 0);
                                    if ($this->transformDate($value['date']) != 1) {
                                        $activation_date = $this->transformDate($value['date']);
                                        $international_warranty = date('Y-m-d', strtotime(" + 1 years", strtotime($activation_date)));
                                        $expiry_date = date('Y-m-d', strtotime("+" . $warranty . " months", strtotime($activation_date)));
                                        $last_update_on_activated_date = date('Y-m-d H:i:s');
                                        if (count($im_d) == 0 && count($im_ex) == 0 && count($im_sale) == 0 && count($country_ex) == 0) {
                                            DB::table('imei_details')->where('id', $imei_id)->update(['activation_date' => $activation_date, 'expiry_date' => $expiry_date, 'last_update_on_activated_date' => $last_update_on_activated_date, 'international_warranty' => $international_warranty]);
                                            $record_count = $record_count + 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (count($im_d) > 0) {
                $error[] = "Duplication entry of IMEI NO / Serial NO " . implode(', ', array_unique($im_d));
            }
            if (count($date_valid) > 0) {
                $error[] = "Not valid date enter in IMEI NO / Serial NO " . implode(', ', array_unique($date_valid));
            }
            if (count($im_ex) > 0) {
                $error[] = "Not exists IMEI NO / Serial NO " . implode(', ', array_unique($im_ex));
            }
            if (count($im_doa_ex) > 0) {
                $error[] = "This IMEI NOs / Serial NOs already exists in DOA list:  " . implode(', ', array_unique($im_doa_ex));
            }
            if (count($im_sale) > 0) {
                $error[] = "Already activated this IMEI NOs / Serial NOs " . implode(', ', array_unique($im_sale));
            }
            if (count($country_ex) > 0) {
                $error[] = "Kindly transfer IMEI / Serial NO " . implode(', ', array_unique($country_ex)) . "to any country";
            }
            if (count($error) > 0) {
                return redirect()->back()->with("info", array_unique($error));
            } else {
                DB::commit();
                return redirect()->back()->with("success", $record_count . " Records Uploaded Successfully!");
            }
        }
    }
    public function list(Request $request) {
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        return view('admin.activation.list', $this->data);
    }
    public function getActivationData(Request $request) {
        $data = $this->getActivationReportTableData($request->from, $request->to);
        return Datatables::of($data)->removeColumn('id')->editColumn('date_of_manufacture', function ($data) {
            return date('d-m-Y', strtotime($data->date_of_manufacture));
        })->filterColumn('country_name', function ($query, $keyword) {
            $query->whereRaw('country.name like ?', ["%{$keyword}%"]);
        })->filterColumn('model_name', function ($query, $keyword) {
            $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
        })->filterColumn('color_name', function ($query, $keyword) {
            $query->whereRaw('color.name like ?', ["%{$keyword}%"]);
        })->filterColumn('ram_name', function ($query, $keyword) {
            $query->whereRaw('ram.name like ?', ["%{$keyword}%"]);
        })->filterColumn('rom_name', function ($query, $keyword) {
            $query->whereRaw('rom.name like ?', ["%{$keyword}%"]);
        })->filterColumn('date_of_manufacture', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(imei_details.date_of_manufacture,'%d-%m-%Y'))") . ' like ?', ["%{$keyword}%"]);
        })->filterColumn('activation_date', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(imei_details.activation_date,'%d-%m-%Y'))") . ' like ?', ["%{$keyword}%"]);
        })->filterColumn('expiry_date', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(imei_details.expiry_date,'%d-%m-%Y'))") . ' like ?', ["%{$keyword}%"]);
        })->editColumn('activation_date', function ($data) {
            return date('d-m-Y', strtotime($data->activation_date));
        })->editColumn('expiry_date', function ($data) {
            return ($data->expiry_date ? date('d-m-Y', strtotime($data->expiry_date)) : "");
        })->addIndexColumn()->make(true);
    }
    // public function report(Request $request) {
    //     $this->data['from'] = $request->from ? $request->from : "";
    //     $this->data['to'] = $request->to ? $request->to : "";
    //     return view('admin.activation.report', $this->data);
    // }
    // public function getListData(Request $request) {
    //     $data = $this->getActivationReportTableData($request->from, $request->to);
    //     return Datatables::of($data)->removeColumn('id')->editColumn('date_of_manufacture', function ($data) {
    //         return date('d-m-Y', strtotime($data->date_of_manufacture));
    //     })->editColumn('activation_date', function ($data) {
    //         return date('d-m-Y', strtotime($data->activation_date));
    //     })->editColumn('expiry_date', function ($data) {
    //         return ($data->expiry_date ? date('d-m-Y', strtotime($data->expiry_date)) : "");
    //     })->addIndexColumn()->make(true);
    // }
    public function exportData(Request $request) {
        $from = $request->from ? date('Y-m-d', strtotime($request->from)) : "";
        $to = $request->to ? date('Y-m-d', strtotime($request->to)) : "";
        return Excel::download(new ActivationReportExport($from, $to), 'activation.xlsx');
    }
    public function add() {
        return view('admin.activation.create');
    }
    public function updateActivation(Request $request) {

        DB::beginTransaction();
        $error = $im_ex = $im_sale = $country_ex = $im_doa_ex = [];
        $expire_exist = "";
        if (trim($request->imei)) {
            $imei_check = DB::table('imei_details')->where('IMEI_no1', '=', $request->imei)->orWhere('IMEI_no2', '=', $request->imei)->orWhere('serial_no', '=', $request->imei)->first();
            $imei_id = ($imei_check ? $imei_check->id : 0);
            $is_doa = ($imei_check ? $imei_check->is_doa : 1);
            if ($imei_id == 0) {
                $im_ex[] = $request->imei;
            } elseif ($is_doa == 1) {
                $im_doa_ex[] = $request->imei;
            } else {
                $expire_exist = DB::table('imei_details')->where('id', $imei_id)->whereNotNull('activation_date')->count();
                if ($expire_exist > 0) {
                    $im_sale[] = $request->imei;
                } else {
                    $country_id = ($imei_check ? $imei_check->country_id : 0);
                    if ($country_id == 0) {
                        $country_ex[] = $request->imei;
                    } else {
                        $country = $this->GetByID('country', $country_id);
                        $warranty = ($country ? $country->warranty : 0);
                        $activation_date =  date('Y-m-d', strtotime($request->date));
                        $international_warranty = date('Y-m-d', strtotime(" + 1 years", strtotime($activation_date)));
                        $expiry_date = date('Y-m-d', strtotime("+" . $warranty . " months", strtotime($activation_date)));
                        $last_update_on_activated_date = date('Y-m-d H:i:s');
                        if (count($im_ex) == 0 && count($im_sale) == 0 && count($country_ex) == 0) {
                            DB::table('imei_details')->where('id', $imei_id)->update(['activation_date' => $activation_date, 'expiry_date' => $expiry_date, 'last_update_on_activated_date' => $last_update_on_activated_date, 'international_warranty' => $international_warranty]);
                        }

                    }
                }
            }
        }
        if (count($im_ex) > 0) {
            $error[] = "Not exists IMEI NO / Serial NO " . implode(', ', array_unique($im_ex));
        }
        if (count($im_doa_ex) > 0) {
            $error[] = "This IMEI / Serial NO nos already exists in DOA list:  " . implode(', ', array_unique($im_doa_ex));
        }
        if (count($im_sale) > 0) {
            $error[] = "Already activated this IMEI NOs / Serial NOs " . implode(', ', array_unique($im_sale));
        }
        if (count($country_ex) > 0) {
            $error[] = "Kindly transfer IMEI / Serial NO " . implode(', ', array_unique($country_ex)) . "to any country";
        }
        if (count($error) > 0) {
            return redirect()->back()->with("info", array_unique($error));
        } else {
            DB::commit();
            return redirect()->back()->with("success", " Records added Successfully!");
        }
    }
}

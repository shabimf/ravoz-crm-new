(function () {

    FTX.IMEITransfer = {

        list: {

            selectors: {
                IMEITransfer_table: $('#IMEITransfer-table'),
            },

            init: function () {

                this.selectors.IMEITransfer_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.IMEITransfer_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        {
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'created_at', name: 'created_at'},
                        { data: 'from_country_name', name: 'from_country_name' },
                        { data: 'to_country_name', name: 'to_country_name' },
                        { data: 'IMEI_no1', name: 'IMEI_no1'},
                        { data: 'IMEI_no2', name: 'IMEI_no2'},
                        { data: 'serial_no', name: 'serial_no'},
                        { data: 'brand_name', name: 'brand_name'},
                        { data: 'model_name', name: 'model_name'}
                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();

@extends('layouts.warranty')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Activation Report</h4>
            </div>
        </div>
        <div class="row  ">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'activation.report','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-doa']) }}
					<div class="form-row well">
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Date From')) }}
							{{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control','id'=>'datepicker' ]) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Date To')) }}
							{{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control', 'id' => 'dates']) }}
						</div>

						<div class="form-group col-md-2">
						  <button class="btn btn-success search-btn">Search</button>
                          <a class="btn btn-success reset-btn" href="{{ url('warranty/activation/list') }}">Reset</a>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
				    <div class="dt-buttons float-right">
					<a class="btn btn-primary buttons-excel buttons-html5"  href="{{ url('warranty/activation/export?from='.$from.'&to='.$to.'') }}"><span>Export Excel</span></a>

					</div>
					  <div class="table-responsive">
						<table id="activation-report-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("activation.report.get") }}">
                            <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <th>Country</th>
									<th>Model</th>
					                <th>IMEI-1</th>
									<th>IMEI-2</th>
									<th>SN</th>
									<th>Color</th>
									<th>RAM</th>
									<th>ROM</th>
									<th>Date Of Manufature</th>
									<th>PCB Serial No</th>
                                    <th>Act.Date</th>
									<th>Expiry Date</th>
					            </tr>
					        </thead>
						</table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>

$(function() {
	$( 'input[name="from"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
	$( 'input[name="to"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
});

var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
FTX.Utils.documentReady(function() {
    FTX.ActivationReport.list.init(from_date,to_date);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
@endsection

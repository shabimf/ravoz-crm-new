@extends('layouts.app')

@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                    <h4 class="card-title-text">Country</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('country.index') }}"> <i
                            class="feather icon-list"></i> Country List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($country, ['route' => ['country.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-company']) }}
            @else
                {{ Form::open(['route' => 'country.store','class' => 'form-horizontal','role' => 'form','method' => 'post','id' => 'create-company', 'auto-complete' => 'off']) }}
            @endif
            @csrf
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('name', __('Name')) }}
                    {{ Form::text('name', old('name',(isset($country)) ? $country->name:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <div class="col form-group">
                    {{ Form::label('currency', __('Currency')) }}
                    {{ Form::select('currency_id', $currency, old('currency_id',(isset($country)) ? $country->currency_id:''), ['class' => 'form-control', 'placeholder' => 'Select Currency', 'required' => 'required']) }}
                </div>
            </div>
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('timezone', __('Timezone')) }}
                    {{ Form::text('timezone', old('timezone',(isset($country)) ? $country->time_zone:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <div class="col form-group">
                    {{ Form::label('warranty', __('Warranty')) }}
                    {{ Form::number('warranty', old('warranty',(isset($country)) ? $country->warranty:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
            </div>
            <div class="row">
               <div class="col-md-6 form-group">
                {{ Form::label('tax', __('Tax')) }}
                {{ Form::number('tax', old('tax',(isset($country)) ? $country->tax:''), ['class' => 'form-control', 'required' => 'required']) }}
               </div>
            </div>
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection


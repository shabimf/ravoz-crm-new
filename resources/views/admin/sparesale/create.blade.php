@extends('layouts.service')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                <h4 class="card-title-text">Spare Sale</h4>
                </div>
                <div class="col-md-6 {{(userHasPermission('Service','spare_sale_list')?'':'hide')}}">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('sparesale') }}"> <i class="feather icon-list"></i> Spare Sale List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['route' => 'sparesale.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-sparesale', 'enctype'=>"multipart/form-data"]) }}
            @csrf
            <div class="form-row">
               <div class="form-group col-md-12">
                {{ Form::label('name', __('Branch')) }}
                {{ Form::select('branch_id', $branch, old('branch_id',Auth::user()->branch_id), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch', 'required' => 'required']) }}
               </div>
            </div>
            <table class="table table-bordered" id="dynamicTable">
                <thead>
                <tr>
                    <th>Model</th>
                    <th>Spare</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <button type="submit" class="btn btn-success float-right g-btn">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/select2/bloodhound.min.js')}}"></script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script>
$(function() {
	$( 'input[name="date"]').daterangepicker( {
		singleDatePicker: true,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	});
});
$(document).ready(function(){
   var count =  0;
   if(count == 0) dynamic_field(count);
    function dynamic_field(number)
    {

        html = '<tr>';
        html += '<td><select  class="form-control js-example-basic-single" name="addmore[' + number + '][model_id]" required="required" onchange="return getSpare(this.value,'+number+')"><option value="">--select--</option>@foreach ($models as $key => $value)<option value="{{ $key }}">{{ $value }}</option>@endforeach</select></td>';
        html += '<td><select  class="form-control js-example-basic-single" name="addmore[' + number + '][spare_id]" id="spare_id_' + number + '"><option value="">Select Spare</option></select></td>';
        html += '<td><input type="number" class="form-control" name="addmore[' + number + '][qty]" required="required"></td>';
        html += '<td><input type="text" class="form-control" name="addmore[' + number + '][price]" required="required"></td>';

        if(number > 0)
        {
            html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
            $('tbody').append(html);
        }
        else
        {
            html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
            $('tbody').append(html);
        }
        $( '.js-example-basic-single').select2();
    }

    $(document).on('click', '#add', function(){
        count++;
        dynamic_field(count);
    });
    $(document).on('click', '.remove', function(){
        $(this).closest("tr").remove();
    });
});
</script>
@endsection

@extends('layouts.service')

@section('content')
<div class="main-panel">
   <div class="panel-hedding">
      <div class="row mb-4">
         <div class="col-md-6">
            <h1>FAQS</h1>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12">
         <div class="card">
            <div class="card-title">
               <div class="card-title">
                  <div class="card-title-left">
                     <h4 class="card-title-text"></h4>
                  </div>
               </div>
               <div class="card-title-right">
                  <form name="add-search-form" id="add-search-form" method="get" action="{{url('/service/faq')}}">
                    <div class="search-box">
                        <div class="search">
                            <input class="form-control border-0" type="search" placeholder="How can ywe halp you..." aria-label="Search" name="search" value="{{ Request::get('search') }}">
                            <a href="javascript:;" onclick="document.getElementById('add-search-form').submit();">	<i class="la la-search"></i> </a>
                        </div>
                    </div>
                  </form>
               </div>
            </div>
            <div class="card-body p-4 p-sm-5">
               <div class="row">
                    <div class="col-md-12">
                     <div class="accordion accordion-box" id="accordionExample">
                    @if(!empty($data) && $data->count())
                        @foreach($data as $key => $value)
                        <div class="card">
                           <div class="card-header" id="headingOne-{{ $value->id }}">
                              <div class="accordion-title @if($key != 0) collapsed @endif" data-toggle="collapse" data-target="#collapseOne-{{ $value->id }}">
                                 <h5 class="mb-0"> <i class="zmdi zmdi-help-outline text-primary mr-3"></i> {{ $value->question }} </h5>
                              </div>
                           </div>
                           <div id="collapseOne-{{ $value->id }}" class="collapse @if($key == 0) show @endif" aria-labelledby="headingOne-{{ $value->id }}" data-parent="#accordionExample">
                              <div class="card-body">
                                 <p>{{ $value->answer }}</p>
                              </div>
                           </div>
                        </div>
                        @endforeach
                        @else
                        <p>
                           There are no data.
                        </p>
                    @endif
                    {{ $data->appends(['search' => Request::get('search') ])->links() }}
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('pagescript')
@include('inc.datatables-js')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Faqs.list.init();
    });
</script>
@endsection

@extends('layouts.service')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                <h4 class="card-title-text">View Spare Sale</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('sparesale') }}"> <i
                            class="feather icon-list"></i> Spare Sale List</a>
                    </div>
                </div>
            </div>
          
            <div class="form-row">
             
               <div class="form-group col-md-6">
                {{ Form::label('name', __('Serial No')) }}
                {{ Form::text('branch', old('branch',(isset($list)) ? $list[0]->serial_no:''), ['class' => 'form-control', 'readonly' => 'readonly']) }}
               </div>
               <div class="form-group col-md-6">
                {{ Form::label('name', __('Branch')) }}
                {{ Form::text('branch', old('branch',(isset($list)) ? $list[0]->branch_name:''), ['class' => 'form-control', 'readonly' => 'readonly']) }}
               </div>
            </div>
            <table class="table table-bordered" id="dynamicTable">
                <thead>
                <tr>
                    <th>Model</th>
                    <th>Spare</th>
                    <th>Part Code</th>
                    <th>Focus Code</th>
                    <th>Quantity</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $row)
                  <tr>
                    <td>{{ ($row->model_name?$row->model_name:"Other") }}</td>
                    <td>{{ $row->spare_name }}</td>
                    <td>{{ $row->part_code }}</td>
                    <td>{{ $row->focus_code }}</td>
                    <td>{{ $row->qty }}</td>
                    <td>{{ $row->price }}</td>
                  </tr>
                @endforeach
                </tbody>
            </table>
           
           
        </div>
    </div>
@endsection


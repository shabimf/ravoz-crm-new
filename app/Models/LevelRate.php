<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LevelRate extends Model
{
    use HasFactory;
    protected $table = 'level_rate';
    public $timestamps = false;
    protected $guarded = array();
}

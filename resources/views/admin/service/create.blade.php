@extends('layouts.service')
@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-12">
                    <h4 class="card-title-text device">Device Information</h4>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['route' => 'service.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-service', 'auto-complete' => 'off', 'enctype' => 'multipart/form-data']) }}
            @csrf
            <div class="row" style="float: right;position: absolute;right: 30px;width: 42%;">

                <div class="col-md-6 form-group">{{ Form::label('name', __('Product Type')) }}
                    {{ Form::select('product_type', $product_type, old('product_type'), ['class' => 'form-control', 'id' => 'product_type']) }}
                </div>
                <div class="col-md-6 form-group imeicls">
                    {{ Form::label('name', __('IMEI/SL')) }}
                    <div class=" input-group">
                        {{ Form::text('imei_no', '', ['class' => 'form-control', 'id' => 'imei_no']) }}
                        {{ Form::hidden('imeiId', '', ['class' => 'form-control', 'id' => 'imeiId']) }}
                        <div class="input-group-append">
                            {{ Form::button('Search', ['class' => 'btn btn-overlay-dark btns', 'id' => 'search' ,'onclick' => 'return searchIMEI();']) }}
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-6 col-sm-offset-3 text-center scan_result hide ap" style="z-index: 1110; top: 65px;">
                <div class="jumbotron " style="border-top-left-radius:0 ;border-top-right-radius:0 ; padding-top:20px;">
                </div>
            </div>
            <div class="col-sm-12  full_box rp block_div hide" style="padding-bottom:90px; font-size:11px;top: 100px;">
                <!-- <div class="overlay "></div> -->

                <div class="row">

                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Warranty')) }}<span style="color: red">*</span>
                        {{ Form::select('is_warranty', ['1' => 'Is Warranty', '0' => 'Non-Warranty'], old('is_warranty'), ['class' => 'form-control', 'id' => 'is_warranty']) }}
                    </div>
                    <div class="col-md-4 form-group">
                        {{ Form::label('brand_id', __('Brand')) }} <span style="color: red">*</span>
                        {{ Form::select('brand_id', $brands, old('brand_id'), ['class' => 'js-example-basic-single txt_brand_id', 'id' => 'brand_id', 'placeholder' => 'Select Brand', 'onchange=getBrand(this.value,0)']) }}
                        <label id="brand_id-error" class="error" for="brand_id"></label>
                    </div>
                    <div class="col-md-4 form-group">
                        {{ Form::label('model_id', __('Model')) }}
                        {{ Form::select('model_id', [], old('model_id'), ['class' => 'form-control js-example-basic-single txt_model_id', 'id' => 'model_id', 'onchange=getVersion(this.value)']) }}
                        <label id="model_id-error" class="error" for="model_id"></label>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Factory IMEI/SN Model')) }}
                        {{ Form::hidden('country_id', old('country_id',(isset($country_id)) ? $country_id:''), ['class' => 'form-control', 'id' => 'country_id']) }}
                        {{ Form::text('sn_no', '', ['class' => 'form-control txt_imei_id', 'id' => 'sn_no', 'readonly']) }}
                    </div>
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Sale Date')) }}
                        {{ Form::text('sale_date', '', ['class' => 'form-control txt_sales_date', 'id' => 'sale_date', 'disabled']) }}
                    </div>
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Active Date')) }}
                        {{ Form::text('active_date', '', ['class' => 'form-control txt_active_date', 'id' => 'active_date', 'disabled']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Date of Manufacture')) }}
                        {{ Form::text('manufacture_date', '', ['class' => 'form-control txt_manufature', 'id' => 'manufacture_date', 'disabled']) }}
                    </div>
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Software Version')) }}
                        <select class="form-control js-example-basic-single" id="version" name="version_id"
                            placeholder="Select a Version"> </select>
                    </div>
                    <div class="col-md-4 form-group"> {{ Form::label('technician_id', __('Technician')) }} <span
                            style="color: red">*</span>
                        {{ Form::select('technician_id', $users, old('technician_id'), ['class' => 'js-example-basic-single', 'placeholder' => 'Select Technician', 'id' => 'technician_id']) }}
                        <label id="technician_id-error" class="error" for="technician_id"></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group"> {{ Form::label('name', __('Delivery Date')) }}
                        {{ Form::text('delivery_date', '', ['class' => 'form-control', 'id' => 'delivery_date']) }}
                    </div>
                    <div class="col-md-4 form-group"> {{ Form::label('name', __('Security Code')) }}
                        {{ Form::text('security_code', '', ['class' => 'form-control', 'id' => 'code']) }} </div>
                    <div class="col-md-4 form-group"> {{ Form::label('name', __('Submission Category')) }}
                        {{ Form::select('submission_cat_id', $submission, old('submission_cat_id'), ['class' => 'js-example-basic-single', 'id' => 'submission_cat_id']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group"> {{ Form::label('name', __('Images')) }} <input type="file"
                            name="filename[]" multiple class="form-control"> </div>
                    <div class="col-md-4 form-group"> {{ Form::label('name', __('Invoice')) }} <input type="file"
                            name="invoice[]" multiple class="form-control"> </div>
                    <div class="col-md-4 form-group block_request_div hide">
                    {{ Form::label('name', __('Shop Name')) }}<span style="color: red">*</span>
                    {{ Form::text('shop_name','', ['class' => 'form-control', 'id' =>'shop_name']) }}
                </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group block_request_div hide">
                        {{ Form::label('name', __('Invoice No')) }}<span style="color: red">*</span>
                        {{ Form::text('invoice_no','', ['class' => 'form-control', 'id' =>'invoice_no']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        {{ Form::label('name', __('Accessories')) }}
                        <div class="search-box">
                            <div class="search">
                                @foreach ($access as $k => $v)
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" id="accessories_{{ $k }}"
                                            value="{{ $k }}" name="accessories_id[]"
                                            class="custom-control-input form-control">
                                        <label class="custom-control-label"
                                            for="accessories_{{ $k }}">{{ $v }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel-hedding cus_block" style="padding-top: 30px;">
                    <div class="row mb-4">
                        <div class="col-md-12">
                            <h4 class="card-title-text device">Customer Information</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            {{ Form::label('name', __('Customer Name')) }}<span style="color: red">*</span>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    {{ Form::select('gender', ['Mr.' => 'Mr.', 'Mrs.' => 'Mrs.','Miss' => 'Miss','Ms.' => 'Ms.','Dr.' => 'Dr.'], old('gender'), ['class' => 'form-control', 'id' => 'gender']) }}
                                    <!-- </div>
                      <div> -->
                                    {{ Form::text('customer_name', '', ['class' => 'form-control btns', 'id' => 'customer_name']) }}
                                    <label id="customer_name-error" class="error" for="customer_name"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            {{ Form::label('name', __('Customer Phone Number')) }}<span style="color: red">*</span>
                            <div class="search-box">
                                <div class="search">
                                    {{ Form::text('customer_phone', '', ['class' => 'form-control btns col-md-12', 'id' => 'customer_phone']) }}
                                    <a href="javascript:void(0)" class="cus_search" data-id="1"> <i
                                            class="la la-search"></i> </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4"> {{ Form::label('name', __('Customer Email Address')) }}
                            {{ Form::text('customer_email', '', ['class' => 'form-control btns col-md-12', 'id' => 'customer_email']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group"> {{ Form::label('name', __('Customer Address')) }}
                            {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address']) !!} </div>
                        <div class="col-md-4 form-group"> {{ Form::label('name', __('Postal Code')) }}
                            {!! Form::text('postal_code', null, ['class' => 'form-control', 'id' => 'postal_code']) !!} </div>
                        <div class="col-md-4 form-group"> {{ Form::label('name', __('Backup Number')) }}
                            {!! Form::text('backup_no', null, ['class' => 'form-control', 'id' => 'backup_no']) !!} </div>
                    </div>
                </div>
                <div class="panel-hedding dealer_block hide" style="padding-top: 30px;">
                    <div class="row mb-4">
                        <div class="col-md-12">
                            <h4 class="card-title-text device">Dealer Information</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group"> {{ Form::label('name', __('Dealer Name')) }}<span
                                style="color: red">*</span>
                            {{ Form::text('dealer_name', '', ['class' => 'form-control btns', 'id' => 'dealer_name']) }}
                        </div>
                        <div class="col-md-4"> {{ Form::label('name', __('Dealer Phone No')) }}<span
                                style="color: red">*</span>
                            <div class="search-box">
                                <div class="search">
                                    {{ Form::text('dealer_phone', '', ['class' => 'form-control btns col-md-12', 'id' => 'dealer_phone']) }}
                                    <a href="javascript:void(0)" class="cus_search" data-id="2"> <i
                                            class="la la-search"></i> </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4"> {{ Form::label('name', __('Dealer Contact')) }}
                            {{ Form::text('dealer_contact', '', ['class' => 'form-control btns col-md-12', 'id' => 'dealer_contact']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group"> {{ Form::label('name', __('Address')) }}
                            {!! Form::text('dealer_address', null, ['class' => 'form-control', 'id' => 'dealer_address']) !!} </div>
                    </div>
                </div>
                <div class="panel-hedding salesman_block hide" style="padding-top: 30px;">
                    <div class="row mb-4">
                        <div class="col-md-12">
                            <h4 class="card-title-text device">Salesman Information</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            {{ Form::label('name', __('Salesman Name')) }}<span style="color: red">*</span>
                            <div class="input-group">
                                {{ Form::text('salesman_name', '', ['class' => 'form-control btns', 'id' => 'salesman_name']) }}
                            </div>
                        </div>
                        <div class="col-md-4 form-group"> {{ Form::label('name', __('Salesman Phone No')) }}<span
                                style="color: red">*</span>
                            <div class="search-box">
                                <div class="search">
                                    {{ Form::text('salesman_contact', '', ['class' => 'form-control btns col-md-12', 'id' => 'salesman_contact']) }}
                                    <a href="javascript:void(0)" class="cus_search" data-id="3"> <i
                                            class="la la-search"></i> </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4"> {{ Form::label('name', __('Salesman District')) }}
                            {{ Form::text('salesman_district', '', ['class' => 'form-control btns col-md-12', 'id' => 'salesman_district']) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group">
                        {{ Form::label('name', __('Complaint')) }}<span style="color: red">*</span>
                        <div class="search-box">
                            <div class="search">
                                <select class="js-example-basic-multiple select2-hidden-accessible" name="complaint[]"
                                    multiple="">
                                    @foreach ($levels as $key => $complaint)
                                        <optgroup label="{{ $key }}">
                                            @foreach ($complaint as $k => $v)
                                                <option value="{{ $k }}">{{ $v }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                                <label id="complaint[]-error" class="error" for="complaint[]"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form-group"> {{ Form::label('name', __('Remarks')) }} {!! Form::text('remarks', null, ['class' => 'form-control', 'id' => 'remarks']) !!}
                    </div>
                </div>
                <button type="submit" class="btn btn-sm btn-primary float-right" id='btnAddRequest'>Submit</button>
                {{ Form::close() }}
            </div>

        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/select2/bloodhound.min.js')}}"></script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script type="text/javascript">
  var access_all= '{{$access_all}}';
  jQuery("#technician_id").on("change", function() {
        var returnvalue;
        if(jQuery("select[name=technician_id]").val() == 0) {
            jQuery("label#technician_id-error").show(); // show Warning 
            jQuery("select#technician_id").focus();  // Focus the select box      
            returnvalue=false;   
        } else {
            jQuery("label#technician_id-error").hide();
            jQuery("label#technician_id-error").html('');
            returnvalue=true;    
        }
        return returnvalue;
    });
</script>
@endsection

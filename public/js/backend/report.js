(function() {
    var oldExportAction = function(self, e, dt, button, config) {
        if (button[0].className.indexOf('buttons-excel') >= 0) {
            if ($.fn.dataTable.ext.buttons.excelHtml5.available(dt, config)) {
                $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config);
            } else {
                $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
            }
        } else if (button[0].className.indexOf('buttons-print') >= 0) {
            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
        }
    };
    $("#country_id").change(function() {
        $.ajax({
            url: "/getBranch?country_id=" + $(this).val(),
            method: 'GET',
            success: function(data) {
                $('#branch_id').html(data.html);
            }
        });
    });

    var newExportAction = function(e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;

        dt.one('preXhr', function(e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = 2147483647;

            dt.one('preDraw', function(e, settings) {
                // Call the original action function
                oldExportAction(self, e, dt, button, config);

                dt.one('preXhr', function(e, s, data) {
                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                    // Set the property to what it was before exporting.
                    settings._iDisplayStart = oldStart;
                    data.start = oldStart;
                });

                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                setTimeout(dt.ajax.reload, 0);

                // Prevent rendering of the full data to the DOM
                return false;
            });
        });

        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    };
    FTX.DOA = {

        list: {

            selectors: {
                doa_table: $('#doa-table'),
            },

            init: function(from, to, model_id) {
                this.selectors.doa_table.dataTable({
                    processing: false,
                    serverSide: true,
                    "bFilter": false,
                    ajax: {
                        url: this.selectors.doa_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.model_id = model_id
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'year', name: 'year' },
                        { data: 'month', name: 'month' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'status', name: 'status' },
                        { data: 'IMEI_no1', name: 'IMEI_no1' },
                        { data: 'IMEI_no2', name: 'IMEI_no2' },
                        { data: 'replace_imei1', name: 'replace_imei1' },
                        { data: 'replace_imei2', name: 'replace_imei2' },
                        { data: 'remarks', name: 'remarks' },
                        { data: 'is_special_request', name: 'is_special_request', searchable: false, sortable: false },
                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
    FTX.ACTIVATION = {

        list: {

            selectors: {
                activation_table: $('#activation-table'),
            },

            init: function(from, to, country_id, model_id) {
                this.selectors.activation_table.dataTable({
                    processing: false,
                    serverSide: true,
                    "bFilter": false,
                    stateSave: false,
                    ajax: {
                        url: this.selectors.activation_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.country_id = country_id,
                                d.model_id = model_id

                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        // { data: 'year', name: 'year'},
                        // { data: 'month', name: 'month'},
                        { data: 'model', name: 'model' },
                        { data: 'color', name: 'color' },
                        { data: 'ram', name: 'ram' },
                        { data: 'rom', name: 'rom' },
                        { data: 'country_name', name: 'country_name' },
                        { data: 'total_dispatch_qty', name: 'total_dispatch_qty' },
                        { data: 'total_activated_qty', name: 'total_activated_qty' },
                        { data: 'balance', name: 'balance' },
                    ],
                    order: [
                        [1, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
    FTX.STOCK = {

        list: {

            selectors: {
                stock_table: $('#stock-report-table'),
            },

            init: function(country_id, branch_id) {
                this.selectors.stock_table.dataTable({
                    processing: false,
                    serverSide: true,
                    "bFilter": false,
                    dom: 'lBfrtip',
                    buttons: [{
                        extend: 'excelHtml5',
                        action: newExportAction,
                        title: 'stock_report'
                    }],
                    ajax: {
                        url: this.selectors.stock_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.country_id = country_id,
                                d.branch_id = branch_id
                        },
                    },
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    },
                });
            }
        },

    }

    FTX.CONSUMPTION = {

        list: {

            selectors: {
                consumption_table: $('#consumption-table'),
            },

            init: function(from, to, country_id, branch_id, model_id) {
                this.selectors.consumption_table.dataTable({
                    processing: false,
                    serverSide: true,
                    cache: false,
                    "bFilter": false,
                    ajax: {
                        url: this.selectors.consumption_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.country_id = country_id,
                                d.branch_id = branch_id,
                                d.model_id = model_id
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'spare_name', name: 'spare_name' },
                        { data: 'purchase_count', name: 'purchase_count' },
                        { data: 'warranty_count', name: 'warranty_count' },
                        { data: 'nonwarranty_count', name: 'nonwarranty_count' },
                        { data: 'sale_count', name: 'sale_count' },
                        { data: 'consumption_qty', name: 'consumption_qty' },
                        { data: 'balance_qty', name: 'balance_qty' }
                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    },
                });
            }
        },

    }
    FTX.SPARE = {

        list: {

            selectors: {
                spare_table: $('#spare-table'),
            },

            init: function(from, to, country_id, branch_id, model_id, company_id, currency_id, type_id) {
                this.selectors.spare_table.dataTable({
                    processing: false,
                    serverSide: true,
                    "bFilter": false,
                    cache: false,
                    ajax: {
                        url: this.selectors.spare_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.country_id = country_id,
                                d.branch_id = branch_id,
                                d.model_id = model_id,
                                d.company_id = company_id,
                                d.currency_id = currency_id,
                                d.type_id = type_id
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'year', name: 'year' },
                        { data: 'month', name: 'month' },
                        { data: 'country_name', name: 'country_name' },
                        { data: 'company_name', name: 'company_name' },
                        { data: 'branch_name', name: 'branch_name' },
                        { data: 'spare_name', name: 'spare_name' },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'price', name: 'price' },

                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    },
                });
            }
        },

    }

    FTX.FORECAST = {

        list: {

            selectors: {
                forecast_table: $('#forecast-table'),
            },

            init: function(country_id, model_id) {
                this.selectors.forecast_table.dataTable({
                    processing: false,
                    serverSide: true,
                    "bFilter": false,
                    cache: false,
                    ajax: {
                        url: this.selectors.forecast_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.country_id = country_id,
                                d.model_id = model_id
                        },
                    },
                    columns: [

                        { data: 'model', name: 'model' },
                        { data: 'focus_code', name: 'focus_code' },
                        { data: 'part_code', name: 'part_code' },
                        { data: 'description', name: 'description' },
                        { data: 'activation_total', name: 'activation_total' },
                        { data: 'activation_balance', name: 'activation_balance' },
                        { data: 'activation_date', name: 'activation_date' },
                        { data: 'activation_month', name: 'activation_month' },
                        { data: 'in_warranty_spare', name: 'in_warranty_spare' },
                        { data: 'out_warranty_spare', name: 'out_warranty_spare' },
                        { data: 'total_spare_used', name: 'total_spare_used' },
                        { data: 'average_usage_inwarranty', name: 'average_usage_inwarranty' },
                        { data: 'average_usage_outwarranty', name: 'average_usage_outwarranty' },
                        { data: 'total_average_usage', name: 'total_average_usage' },
                        { data: 'current_stock', name: 'current_stock' },
                        { data: 'difference', name: 'difference' },

                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    },
                });
            }
        },

    }
    FTX.SERVICECLAIM = {

        list: {

            selectors: {
                service_claim_table: $('#service-claim-table'),
            },

            init: function(from, to, country_id, branch_id, model_id, company_id, currency_id) {
                this.selectors.service_claim_table.dataTable({
                    processing: false,
                    serverSide: true,
                    "bFilter": false,
                    cache: false,
                    ajax: {
                        url: this.selectors.service_claim_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.country_id = country_id,
                                d.branch_id = branch_id,
                                d.model_id = model_id,
                                d.company_id = company_id,
                                d.currency_id = currency_id
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'approved_date', name: 'approved_date' },
                        { data: 'IMEI_no1', name: 'IMEI_no1' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'company_name', name: 'company_name' },
                        { data: 'branch_name', name: 'branch_name' },
                        { data: 'service_complaints', name: 'service_complaints' },
                        { data: 'claim_amount', name: 'claim_amount' },
                        { data: 'customer_amount', name: 'customer_amount' },
                        { data: 'status', name: 'status' },
                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    },
                });
            }
        },

    }
    FTX.STOCKSERVICECLAIM = {

        list: {

            selectors: {
                stock_service_claim_table: $('#stock-service-claim-table'),
            },

            init: function(from, to, country_id, branch_id, model_id, company_id, currency_id) {
                this.selectors.stock_service_claim_table.dataTable({
                    processing: false,
                    serverSide: true,
                    "bFilter": false,
                    cache: false,
                    ajax: {
                        url: this.selectors.stock_service_claim_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.country_id = country_id,
                                d.branch_id = branch_id,
                                d.model_id = model_id,
                                d.company_id = company_id,
                                d.currency_id = currency_id
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'IMEI_no1', name: 'IMEI_no1' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'company_name', name: 'company_name' },
                        { data: 'branch_name', name: 'branch_name' },
                        { data: 'service_complaints', name: 'service_complaints' },
                        { data: 'claim_amount', name: 'claim_amount' },
                        { data: 'status', name: 'status' },
                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    },
                });
            }
        },

    }
    FTX.SERVICECOM = {

        list: {

            selectors: {
                service_com_table: $('#service-com-table'),
            },

            init: function(from, to, country_id, branch_id, model_id) {
                this.selectors.service_com_table.dataTable({
                    processing: false,
                    serverSide: true,
                    "bFilter": false,
                    cache: false,
                    ajax: {
                        url: this.selectors.service_com_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.country_id = country_id,
                                d.branch_id = branch_id,
                                d.model_id = model_id
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'IMEI_no1', name: 'IMEI_no1' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'branch_name', name: 'branch_name' },
                        { data: 'service_complaints', name: 'service_complaints' },
                        { data: 'spare_name', name: 'spare_name' },
                        { data: 'status', name: 'status' },
                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    },
                });
            }
        },

    }
    FTX.SERVICEMODCOM = {

        list: {

            selectors: {
                model_com_table: $('#model-com-table'),
            },

            init: function(model_id) {
                this.selectors.model_com_table.dataTable({
                    processing: false,
                    serverSide: true,
                    "bFilter": false,
                    cache: false,
                    "bStateSave": false,
                    ajax: {
                        url: this.selectors.model_com_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.model_id = model_id
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'description', name: 'description' },
                        { data: 'warranty', name: 'warranty' },
                        { data: 'nonwarranty', name: 'nonwarranty' },
                        { data: 'total', name: 'total' },
                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    },
                });
            }
        },

    }

    FTX.PRICECOM = {

        list: {

            selectors: {
                price_table: $('#price-report-table'),
            },

            init: function() {
                this.selectors.price_table.dataTable({
                    processing: false,
                    serverSide: true,
                    "bFilter": false,
                    cache: false,
                    ajax: {
                        url: this.selectors.price_table.data('ajax_url'),
                        type: 'post'
                    },
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    },
                });
            }
        },

    }


})();
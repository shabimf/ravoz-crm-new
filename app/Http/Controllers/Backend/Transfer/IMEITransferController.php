<?php
namespace App\Http\Controllers\Backend\Transfer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportIMEITransfer;
use Yajra\DataTables\Facades\DataTables;
use App\Models\IMEITransfer;
use App\Models\IMEITransferItems;
use Illuminate\Support\Facades\View;
use DB;
use Auth;


class IMEITransferController  extends Controller
{

    public function __construct()
    {
        $this->data['country'] = $this->NameAndID('country');
        View::share('js', ['master','IMEITransfer','IMEITransferList']);
    }

    public function index()
    {
        return view('warranty.IMEITransfer.index');
    }

    public function create()
    {
        return view('warranty.IMEITransfer.create',$this->data);
    }

    public function import(Request $request)
    {

        $this->validate($request, ['file' => 'required|file|mimes:xls,xlsx,csv|max:10240']);

        if ($request->file('file')
            ->isValid())
        {
            $file = $request->file('file');

            Excel::import(new ImportIMEITransfer, request()
                ->file('file'));
            $data = Excel::toCollection(new ImportIMEITransfer() , $request->file('file'));
            // echo "<pre>"; print_r($data); echo "</pre>"; exit;
            $imei_arr = $error = [];

            if (count($data) > 0)
            {
                $record_count = 0;
                DB::beginTransaction();
                $path = $file->store('IMEITransfer', ['disk' => 'my_files']);
                $group = IMEITransfer::Create(['file' => $path, 'created_by' => Auth::id() ]);
                $country = $this->GetByID('country', $request->country_id);
                $warranty = ($country ? $country->warranty : 0);
                $im_d = $im_ex =  $im_sale = $im_cou = [];
                $expire_exist = "";
                foreach ($data[0] as $key => $value)
                {
                    if (in_array($value['imeisn'], $imei_arr))
                    {
                        $im_d[] = $value['imeisn'];
                    }
                    $imei_check = DB::table('imei_details')->where('IMEI_no1', '=', trim($value['imeisn']))->orWhere('IMEI_no2', '=', trim($value['imeisn']))->orWhere('serial_no', '=', trim($value['imeisn']))->first();
                    $imei_id = ($imei_check ? $imei_check->id : 0);
                    $transfer_country = ($imei_check ? $imei_check->country_id : NULL);
                    if ($imei_id == 0) {
                        $im_ex[] = $value['imeisn'];
                    } else {
                        $expire_exist = DB::table('imei_details')->where('id', $imei_id)->whereNotNull('expiry_date')->first();
                        $country_exist = DB::table('imei_details')->where('id', $imei_id)->where('country_id', $request->country_id)->first();
                        // if($expire_exist) {
                        //   $im_sale[] = $value['imei'];
                        // }
                        if($country_exist) {
                            $im_cou[] = $value['imeisn'];
                        }
                    }
                    $imei_arr[] = trim($value['imeisn']);
                    IMEITransferItems::Create(['imei_id' => $imei_id, 'group_id' => $group->id, 'from_country' => $transfer_country, 'to_country'=>$request->country_id]);
                    DB::table('imei_details')->where('id', $imei_id)->update(['country_id' => $request->country_id,'is_transferred' => 1]);
                    if($expire_exist) {
                        $expiry_date = date('Y-m-d', strtotime("+" . $warranty . " months", strtotime($imei_check->activation_date)));
                        DB::table('imei_details')->where('id', $imei_id)->update(['activated_by' => Auth::user()->id, 'expiry_date' => $expiry_date]);
                    }
                    $record_count = $record_count+1;
                }
            }
            if (count($im_d) > 0)
            {
                $error[] = "Duplication entry of IMEI NO / SN " . implode(', ', array_unique($im_d));
            }
           if (count($im_ex) > 0)
            {
                $error[] = "Not exists IMEI NO / SN " . implode(', ', array_unique($im_ex));
            }
            // if (count($im_sale) > 0)
            // {
            //     $error[] = "Already Sale this IMEI NOs " . implode(', ', array_unique($im_sale));
            // }
            if (count($im_cou) > 0)
            {
                $error[] = "You can't transfer within same country(" . implode(', ', array_unique($im_cou)).")";
            }
            if (count($error) > 0)
            {
                return redirect()->back()
                    ->with("info", array_unique($error));
            }
            else
            {
                DB::commit();
                return redirect()->back()->with("success", $record_count." Records Uploaded Successfully!");
            }
        }
    }

    public function getIMEITransferData(Request $request)
    {
        $data = IMEITransferItems::leftJoin('imei_transfer', 'imei_transfer.id', '=', 'imei_transfer_items.group_id')
                ->leftJoin('country as from_country', 'imei_transfer_items.from_country', '=', 'from_country.id')
                ->leftJoin('country as to_country', 'imei_transfer_items.to_country', '=', 'to_country.id')
                ->leftJoin('imei_details', 'imei_transfer_items.imei_id', '=', 'imei_details.id')
                ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
                ->leftJoin('models', 'variant_details.model_id', '=', 'models.id')
                ->leftJoin('brands', 'models.brand_id', '=', 'brands.id')
                ->leftJoin('color', 'variant_details.color_id', '=', 'color.id')
                ->leftJoin('ram', 'variant_details.ram_id', '=', 'ram.id')
                ->leftJoin('rom', 'variant_details.rom_id', '=', 'rom.id')
                ->select(['imei_details.IMEI_no1', 'imei_details.IMEI_no2', 'imei_details.serial_no', 'imei_transfer.created_at','models.name as model_name','brands.name as brand_name','color.name as color_name','ram.name as ram_name','rom.name as rom_name','from_country.name as from_country_name','to_country.name as to_country_name']);

        if (!empty($request->get('id'))) {
            $data->where("imei_transfer_items.group_id", $request->get('id'));
        }
        return Datatables::of($data)
        ->editColumn('model_name', function($data){
            return $data->model_name."(".$data->color_name.",".$data->rom_name.",".$data->ram_name.")";
        })
        ->editColumn('from_country_name', function ($data) {
            return ($data->from_country_name?$data->from_country_name:"Freezone");
        })
        ->filterColumn('from_country_name', function ($query, $keyword) {
            if (str_contains('freezone', strtolower($keyword))) {
              $query->whereNull('imei_transfer_items.from_country');
            } else {
              $query->whereRaw('from_country.name like ?', ["%{$keyword}%"]);
            }
        })
        ->editColumn('created_at', function ($data)
        {
            return date('d-m-Y', strtotime($data->created_at) );
        })->filterColumn('to_country_name', function ($query, $keyword) {
            $query->whereRaw('to_country.name like ?', ["%{$keyword}%"]);
        })->filterColumn('IMEI_no1', function ($query, $keyword) {
            $query->whereRaw('imei_details.IMEI_no1 like ?', ["%{$keyword}%"]);
        })->filterColumn('IMEI_no2', function ($query, $keyword) {
            $query->whereRaw('imei_details.IMEI_no2 like ?', ["%{$keyword}%"]);
        })->filterColumn('serial_no', function ($query, $keyword) {
            $query->whereRaw('imei_details.serial_no like ?', ["%{$keyword}%"]);
        })->filterColumn('brand_name', function ($query, $keyword) {
            $query->whereRaw('brands.name like ?', ["%{$keyword}%"]);
        })->filterColumn('model_name', function ($query, $keyword) {
            $query->whereRaw('models.name like ?', ["%{$keyword}%"])
            ->orWhere('color.name', 'LIKE', "%{$keyword}%")
            ->orWhere('rom.name', 'LIKE', "%{$keyword}%")
            ->orWhere('rom.name', 'LIKE', "%{$keyword}%");
        })->filterColumn('created_at', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(imei_transfer.created_at,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
        })->addIndexColumn()->make(true);
    }

    public function getIMEITransferList(Request $request)
    {
        $this->data['from'] = ($request->from?$request->from:date('01-m-Y'));
        $this->data['to'] = ($request->to?$request->to:date('t-m-Y'));
        return view('warranty.IMEITransfer.imei-list', $this->data);
    }

    public function getIMEIData(Request $request) {

        $data = IMEITransfer::leftJoin('imei_transfer_items', 'imei_transfer.id', '=', 'imei_transfer_items.group_id')
                ->leftJoin('country as from_country', 'imei_transfer_items.from_country', '=', 'from_country.id')
                ->leftJoin('country as to_country', 'imei_transfer_items.to_country', '=', 'to_country.id')
                ->leftJoin('imei_details', 'imei_transfer_items.imei_id', '=', 'imei_details.id')
                ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
                ->leftJoin('models', 'variant_details.model_id', '=', 'models.id')
                ->leftJoin('brands', 'models.brand_id', '=', 'brands.id')
                ->select(['imei_transfer.id','from_country.name as from_country_name','to_country.name as to_country_name','models.name as model_name','brands.name as brand_name','imei_transfer.created_at','imei_transfer.file', DB::raw('count(imei_transfer_items.group_id) as IMEICount')])
                ->groupBy('imei_transfer_items.group_id');
        $from = ($request->from?date('Y-m-d',strtotime($request->from)):date('Y-m-01'));
        $to = ($request->to?date('Y-m-d',strtotime($request->to)):date('Y-m-t'));
        $data->whereBetween('imei_transfer.created_at', [$from." 00:00:00", $to." 23:59:59"]);

        return Datatables::of($data)->escapeColumns(['model_name','from_country_name'])->editColumn('created_at', function ($data)
        {
            return date('d-m-Y', strtotime($data->created_at) )." (".$data->IMEICount.")";
        })->addColumn('actions', function ($data) {
            $action = '<a href="' . url('uploads/'.$data->file) . '" data-toggle="tooltip" data-placement="top" title="Download" class="btn btn-primary btn-sm">Download Excel</a>  <a href="' . url('warranty/IMEITransfer?id='.$data->id) . '"  class="view btn btn-primary btn-sm">View</a>';
            return $action;
        })->addIndexColumn()->make(true);
    }

}


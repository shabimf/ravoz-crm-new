@extends('layouts.app')

@section('content')


<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-12 text-right">
                <a class="btn btn-overlay-dark" href="{{ route('spare.import') }}"> <i class="feather icon-log-in"></i> Import</a>
				<a class="btn btn-primary" href="{{ route('spare.create') }}"> <i class="feather icon-plus"></i> Create Spare</a>
            </div>
        </div>
      
        <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">Spare Management</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="spare-table" class="table table-striped table-bordered" data-ajax_url="{{ route('spare.get') }}">
					        <thead>
					            <tr>
                                    <th width="5">Sl</th>
					                <th>Model</th>
                                    <th>Spare Name</th>
                                    <th>Part Code</th>
                                    <th>Focus Code</th>
                                    <th>Unique Code</th>
					                <th width="100">Actions</th>
                                    <th width="100">Status</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('pagescript')
@include('inc.datatables-js')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Spare.list.init();
    });
</script>
@endsection

<!DOCTYPE html>
<html>
  <title>DOA Certificate</title>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
      td,
      th {
        padding: 5px;
      }

      th {
        color: #6E0C0E;
      }

      body {
        font-family: 'sans-serif';
      }

      .text-left {
        text-align: left;
      }

      .text-right {
        text-align: right;
      }

      /* .text-center {
            text-align: center;
        } */
      .text-justify {
        text-align: justify;
      }

      .table-bordered {
        border: 1px solid #ddd;
      }

      table {
        border-spacing: 0;
        border-collapse: collapse;
      }

      table {
        background-color: transparent;
      }

      caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #777;
        text-align: left;
      }

      th {
        text-align: left;
      }

      .table {
        width: 100%;
        max-width: 100%;
        /* margin-bottom: 20px; */
        margin-top: 20px;
      }

      .table>thead>tr>th,
      .table>tbody>tr>th,
      .table>tfoot>tr>th,
      .table>thead>tr>td,
      .table>tbody>tr>td,
      .table>tfoot>tr>td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
      }

      .table>thead>tr>th {
        vertical-align: bottom;
        border-bottom: 2px solid #ddd;
      }

      .table>caption+thead>tr:first-child>th,
      .table>colgroup+thead>tr:first-child>th,
      .table>thead:first-child>tr:first-child>th,
      .table>caption+thead>tr:first-child>td,
      .table>colgroup+thead>tr:first-child>td,
      .table>thead:first-child>tr:first-child>td {
        border-top: 0;
      }

      .table>tbody+tbody {
        border-top: 2px solid #ddd;
      }

      .table .table {
        background-color: #fff;
      }

      .table-condensed>thead>tr>th,
      .table-condensed>tbody>tr>th,
      .table-condensed>tfoot>tr>th,
      .table-condensed>thead>tr>td,
      .table-condensed>tbody>tr>td,
      .table-condensed>tfoot>tr>td {
        padding: 5px;
      }

      .table-bordered {
        border: 1px solid #ddd;
      }

      .table-bordered>thead>tr>th,
      .table-bordered>tbody>tr>th,
      .table-bordered>tfoot>tr>th,
      .table-bordered>thead>tr>td,
      .table-bordered>tbody>tr>td,
      .table-bordered>tfoot>tr>td {
        border: 1px solid #ddd;
      }

      .table-bordered>thead>tr>th,
      .table-bordered>thead>tr>td {
        border-bottom-width: 2px;
      }

      .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #f9f9f9;
      }

      .table-responsive {
        min-height: .01%;
        overflow-x: auto;
      }

      .table-bordered th,
      .table-bordered td {
        border: 1px solid #ddd !important;
      }

      .table-hover>tbody>tr:hover {
        background-color: #f5f5f5;
      }

      table col[class*="col-"] {
        position: static;
        display: table-column;
        float: none;
      }

      table td[class*="col-"],
      table th[class*="col-"] {
        position: static;
        display: table-cell;
        float: none;
      }

      .clearfix:before,
      .clearfix:after {
        display: table;
        content: " ";
      }

      .clearfix:after {
        clear: both;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div>
        <div style="width:40%; float:left;">
          <img src="{{ asset('images/logo.png') }}" width="100" style="float:left; ">
          <div style="clear:both"></div>
          <p style="font-size:7px;margin-bottom:15px;">ARTC - RASCOTEC - ABUDHABI</p>
        </div>
        <div style="width:40%; float:left;">
          <h4 class="text-center" style="font-weight:bold; font-size:16px">DOA Certificate</h4>
        </div>
        <div style="width:20%; float:right; text-align:right;">
          <p style=" padding:0; margin:0;font-size:12px;">
            <span class="text-danger" style=" color:#a94442;"> {{$doa->serial_no}} </span>
            <br />
            <small> {{date("d/m/Y",strtotime($doa->created_at))}} </small>
          </p>
        </div>
      </div>
      <div class="clearfix"></div>
      <hr />
      <table class="table table-bordered" style="font-size:10px">
        <tr>
          <th> Defective From Branch </th>
          <td colspan="3" align="left">{{$doa->branch_name}}</td>
        </tr>
      </table>
      <div style="width:100%;float:left">
        <table class="table table-bordered" style="font-size:9px">
          <tr>
            <th>Model</th>
            <td align="left">{{$doa->model_name}}
                                        (
                                            @if($doa->color){{$doa->color}}@endif
                                            @if($doa->ram),{{$doa->ram}}@endif
                                            @if($doa->rom),{{$doa->rom}}@endif

                                        )
            </td>
            @if(isset($doa->IMEI_no1))
            <th>IMEI </th>
            <td align="left">{{$doa->IMEI_no1}}</td>
            @endif
          </tr>
          <tr>
            <th>Status</th>
            <td colspan="3" align="left">@if($doa->service_id) Sold @else Not Sold @endif</td>
          </tr>
          <tr>
            <th>Problem Remarks </th>
            <td colspan="3" align="left"> {{$doa->remarks}}  </td>
          </tr>
          <tr>
            <th>Check List </th>
            <td colspan="3" align="left">{{$doa->access}}</td>
          </tr>
        </table>
      </div>
      <hr />
      <table class="table " style="font-size:10px;">
        <tr>
          <td>
            <h3>Declaration</h3>
          </td>
        </tr>
        <tr>
          <td>
            <p>This is to certify that the above mentioned details are correct. The unit is under warranty and is covered under the Branch DOA policy of Company.</p>
          </td>
        </tr>
      </table>
      <table class="table " style="font-size:10px;margin-top:6%;">
        <tr>
          <td width="40%">
            <p>
              <b>------------------------------------ <br /> Technician’s Signature </b>
            </p>
            <br />
          </td>
      </table>
      @if($doa->replace_imei_id)
      <table class="table " style="font-size:10px;">
        <tr>
          <td>
            <u><h3>Replacement Unit Detail & acknoledgement</h3></u>
          </td>
        </tr>
        <tr>
          <td>
            <p>I/ We have received the full box replacement unit for the above mentioned defective device</p>
          </td>
        </tr>
      </table>
      <table class="table table-bordered" style="font-size:10px">
        <tr>
          <th> Replaced IMEI-1  </th>
          <td align="left">{{$doa->replace_imei1}}</td>
          <th> Replaced IMEI-2 </th>
          <td align="left">{{$doa->replace_imei2}}</td>
        </tr>
      </table>
      <table class="table " style="font-size:10px;margin-top:6%;">
        <tr>
          <td width="40%">
            <p>
              <b>------------------------------------ <br /> Receiver Name & Signature </b>
            </p>{{date('d/m/Y')}}
            <br />
          </td>
      </table>
      @endif
  </body>
</html>

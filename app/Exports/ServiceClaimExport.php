<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use App\Http\Traits\CommonTrait;

class ServiceClaimExport implements FromArray, WithHeadings, WithMapping
{
    protected $from,$to,$country_id,$branch_id,$model_id,$branch_ids,$company_id,$currency_id;

    use CommonTrait;

    function __construct($from, $to, $country_id, $branch_id, $model_id, $branch_ids,$company_id,$currency_id) {
        $this->data['from'] = $from;
        $this->data['to'] = $to;
        $this->data['country_id'] = $country_id;
        $this->data['branch_id'] = $branch_id;
        $this->data['model_id'] = $model_id;
        $this->data['branch_ids'] = $branch_ids;
        $this->data['company_id'] = $company_id;
        $this->data['currency_id'] = $currency_id;
    }

    public function array(): array
    {

        return $this->getServiceClaimTableData($this->data);
    }

    public function headings(): array
    {
        return [
            'Sl No',
            'Company',
            'Repair Order No',
            'Activation date',
            'Purchase Date',
            'Approved Date',
            'Receive Date',
            'Receive time',
            'Region/Country',
            'SC Code',
            'SC Name',
            'Order Status',
            'device status',
            'Repair type',
            'Brand',
            'Product Type',
            'Marketing Model',
            'Internal Model',
            'Color',
            'RAM',
            'ROM',
            'International Warranty Service',
            'IMEI/SN',
            'Factory IMEI/SN',
            'Board serial No',
            'Submission Method',
            'Customer name',
            'Customer phone no',
            'Customer Address',
            'Dealer Name',
            'Dealer Type',
            'Salesman name',
            'Date Of Manufacture',
            'Repaired Date',
            'Repaired Time',
            'Handover Date',
            'Handover Time',
            'Repair duration (hours)',
            'Old Software Version',
            'New Software Version',
            'Receiver',
            'Engineer',
            'Customer Fault Description',
            'Fault Cause',
            'Remark',
            'Special Description',
            'Good Material Code',
            'Defective Material Code',
            'Good Material Consumed Qty',
            'Good material name',
            'Remark for Replace Materials',
            'Have defective materials been taken away or not',
            'Local currency',
            'Unit Price',
            'Receivable price',
            'Net Charge',
            'Customer Charged amount',
            'Level',
            'Claim Amount'
        ];
    }

    public function map($claim): array
    {
        $repair_hours = '';
        $receive_time = explode(" ",$claim['created_at']);
        $approved_date = explode(" ",$claim['approved_date']);
        if ($claim['bill_date']) {
            $date1 = strtotime($claim['created_at']);
            $date2 = strtotime($claim['bill_date']);
            $diff = abs($date2 - $date1);
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24)/ (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            $hours  = floor(($diff - $years * 365*60*60*24- $months*30*60*60*24 - $days*60*60*24)/ (60*60));
            $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
            $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
            $repair_hours =  $years . " years, " . $months." months, ".$days." days , ".$hours." hours , ".$minutes." minutes , ".$seconds." seconds ";

        }
        return [
            $claim['rownum'],
            $claim['company_name'],
            $claim['serial_no'],
            $claim['active_date'],
            $claim['sale_date'],
            $approved_date[0],
            $claim['created_at'],
            $receive_time[1],
            $claim['country_name'],
            $claim['branch_code'],
            $claim['branch_name'],
            $claim['status'],
            NULL,
            NULL,
            $claim['brand_name'],
            $claim['device_name'],
            $claim['model_name'],
            $claim['model_name'],
            $claim['color'],
            $claim['ram'],
            $claim['rom'],
            NULL,
            $claim['IMEI_no1'],
            $claim['IMEI_no1'],
            $claim['pcb_serial_no'],
            NULL,
            $claim['customer_name'],
            $claim['customer_phone'],
            $claim['address'],
            $claim['customer_name'],
            NULL,
            $claim['customer_name'],
            $claim['date_of_manufacture'],
            $claim['bill_date'],
            $claim['bill_time'],
            $claim['bill_date'],
            $claim['bill_time'],
            $repair_hours,
            $claim['old_software'],
            $claim['old_software'],
            $claim['technician_name'],
            $claim['technician_name'],
            $claim['complaint_name'],
            $claim['service_complaints'],
            $claim['remarks'],
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            $claim['customer_amount'],
            $claim['level'],
            $claim['claim_amount']
        ];
    }
}

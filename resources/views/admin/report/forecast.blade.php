@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Forecast</h4>
            </div>
        </div>
        <div class="row  ">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'report.forecast','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-forecast']) }}
					<div class="form-row well">
                        <div class="form-group col-md-3">
                            {{ Form::label('name', __('Country')) }}
							{{ Form::select('country_id', $country, old('country_id',(isset($country_id)) ? $country_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Country','id' => 'country_id']) }}
						</div>
						<div class="form-group col-md-3">
                            {{ Form::label('name', __('Model')) }}
							{{ Form::select('model_id', $model, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model','id' => 'model_id']) }}
						</div>
						<div class="form-group col-md-3">
						  <button class="btn btn-primary search-btn">Search</button>
                          <a class="btn btn-primary reset-btn" href="{{route('report.forecast')}}">Reset</a>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
				    <div class="dt-buttons float-right">
						<a class="btn btn-primary buttons-excel buttons-html5"  href="{{ url('report/forecast/file-export?country_id='.$country_id.'&model_id='.$model_id.'') }}"><span>Excel</span></a>
					</div>
					  <div class="table-responsive">
						<table id="forecast-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("report.forecast-get") }}">
							<thead>
								<tr>

									<th>Model</th>
									<th>Focus Code</th>
									<th>Product Code</th>
									<th>Product Description</th>
									<th>Activation</th>
									<th>Balance Quantity OF Handset</th>
                                    <th>Date Of First Activation</th>
                                    <th>No Of Months From Activation</th>
                                    <th>Used Spare Count In Warranty</th>
                                    <th>Used Spare Count IN Out Of Warranty</th>
                                    <th>Total Used Spare</th>
                                    <th>Expected Monthly Average Usage In Warranty</th>
                                    <th>Expected Monthly Average Usage In Non Warranty</th>
                                    <th>Average Monthly Usage</th>
                                    <th>Current Stock</th>
                                    <th>Difference</th>
								</tr>
							</thead>
						</table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
$(".reset-btn").click(function() {
    $("#country_id").val(null).trigger("change")
	$("#model_id").val(null).trigger("change")
});
$(function() {
	$( 'input[name="from"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
	$( 'input[name="to"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
});

var country_id =  getUrlParameter('country_id');
var model_id =  getUrlParameter('model_id');
FTX.Utils.documentReady(function() {
    FTX.FORECAST.list.init(country_id,model_id);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

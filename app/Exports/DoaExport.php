<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Http\Traits\CommonTrait;

class DoaExport implements FromCollection, WithHeadings, WithMapping
{
    protected $from,$to,$model_id;
    use CommonTrait;
    function __construct($from, $to, $model_id) {
        $this->from = $from;
        $this->to = $to;
        $this->model_id = $model_id;
    }

    public function collection()
    {
        return $this->getDoaTableData($this->from, $this->to, $this->model_id)->get();
    }

    public function headings(): array
    {
        return [
            'SL NO',
            'YEAR',
            'MONTH',
            'DATE',
            'DOA NO',
            'DOA TYPE',
            'IMEI 1 OLD PIECE',
            'IMEI 2 OLD PIECE',
            'IMEI 1 REPLACED',
            'IMEI 2 REPLACED',
            'COMPLAINT',
            'SPECIAL REQUEST'
        ];
    }
    
    public function map($doa): array
    {
        if ($doa->issue_doa == 1) {
            $status ='DOA CERTIFICATE ISSUED';
        } elseif ($doa->replace_imei_id > 0 ) {
            $status = 'REPLACED';
        } else {
            $status = 'PENDING';
        }

        return [
            $doa->rownum,
            $doa->year,
            date("F", mktime(0, 0, 0, $doa->month, 10)),
            date("d/m/Y", strtotime($doa->created_at)),
            $doa->serial_no,
            $status,
            $doa->IMEI_no1,
            $doa->IMEI_no2,
            $doa->replace_imei1,
            $doa->replace_imei2,
            $doa->remarks,
            ($doa->is_special_request?"YES":"NO"),
        ];
    }
}

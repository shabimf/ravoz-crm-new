@extends('layouts.app')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                   <h4 class="card-title-text">{{((isset($edit_model)) ? "Edit":'Create')}} Model</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ url('admin/model') }}"> <i
                            class="feather icon-list"></i> Model List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($edit_model, ['route' => ['model.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-model']) }}
            @else
                {{ Form::open(['route' => 'model.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-model']) }}
            @endif
            @csrf
            
            <div class="form-row">
                <div class="form-group col-md-6">
                    {{ Form::label('name', __('Brand')) }}
                    {{ Form::select('brand_id', $brand, old('brand_id',(isset($edit_model)) ? $edit_model->brand_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Brand', 'required' => 'required']) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('name', __('Device')) }}
                    {{ Form::select('device_id', $device, old('device_id',(isset($edit_model)) ? $edit_model->device_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Device', 'required' => 'required']) }}
                </div>
                <div class="form-group col-md-4">
                    {{ Form::label('name', __('Model Name')) }}
                    {{ Form::text('name', old('name',(isset($edit_model)) ? $edit_model->name:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <div class="form-group col-md-4">
                    {{ Form::label('name', __('Factory Name')) }}
                    {{ Form::text('factory_name', old('factory_name',(isset($edit_model)) ? $edit_model->factory_name:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                @php
                $selected = [];
                if (isset($edit_model)) {
                    $selected = explode(',',$edit_model->country_id);
                }
                @endphp
                <div class="form-group col-md-4">
                    {{ Form::label('name', __('Country')) }}
                    <select name="country_id[]" multiple="multiple" class="js-example-basic-multiple select2-hidden-accessible" id="country_id">
                        @foreach($country as $k => $v)
                            <option value="{{ $k }}" {{ (in_array($k, $selected)) ? 'selected' : '' }}>{{ $v}}</option>
                        @endforeach
                    </select>
                </div>
                <table class="table table-bordered" id="dynamicTable">  
                <thead>
                <tr>
                    <th>Color</th>
                    <th>RAM</th>
                    <th>ROM</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($edit_model_details))
                  @foreach($edit_model_details as $k =>$v)
                 
                    <tr>
                        <td>
                         <select  class="form-control js-example-basic-single" name="addmore[{{ ++$k }}][color_id]" required="required">@foreach ($color as $key => $value)<option value="{{ $key }}" @if($v->color_id == $key) selected @endif>{{ $value }}</option>@endforeach</select></td>
                        </td>
                        <td>
                         <select  class="form-control js-example-basic-single" name="addmore[{{ $k }}][ram_id]"><option value="">--select--</option>@foreach ($ram as $key => $value)<option value="{{ $key }}" @if($v->ram_id == $key) selected @endif>{{ $value }}</option>@endforeach</select>
                        </td>
                        <td>
                         <select  class="form-control js-example-basic-single" name="addmore[{{ $k }}][rom_id]"><option value="">--select--</option>@foreach ($rom as $key => $value)<option value="{{ $key }}" @if($v->rom_id == $key) selected @endif>{{ $value }}</option>@endforeach</select>
                         <input value="{{(isset($edit_model_details)) ? $v->id:'0'}}" name="addmore[{{ $k }}][model_id]" type="hidden">
                        </td>
                        <td>
                            
                            @if ($k == 1)
                            <button type="button" name="add" id="add" class="btn btn-success">Add</button> 
                            @else
                            <a href="{{ route('model.destroy', $v->id) }}" data-method="delete" class="btn btn-danger">Remove</a>
                            @endif
                        </td>
                    </tr>
                  @endforeach
                @endif
               </tbody>
              
              </table> 
            </div>
            
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/select2/bloodhound.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
   var count =  $('#dynamicTable tbody tr').length;
   if(count == 0) dynamic_field(count);
    
    function dynamic_field(number)
    {
        html = '<tr>';
        html += '<td><select  class="form-control js-example-basic-single select2" name="addmore[' + number + '][color_id]" required="required"><option value="">--select--</option>@foreach ($color as $key => $value)<option value="{{ $key }}">{{ $value }}</option>@endforeach</select></td>';
        html += '<td><select  class="form-control js-example-basic-single select2" name="addmore[' + number + '][ram_id]" ><option value="">--select--</option>@foreach ($ram as $key => $value)<option value="{{ $key }}">{{ $value }}</option>@endforeach</select></td>';
        html += '<td><select  class="form-control js-example-basic-single select2" name="addmore[' + number + '][rom_id]" ><option value="">--select--</option>@foreach ($rom as $key => $value)<option value="{{ $key }}">{{ $value }}</option>@endforeach</select><input value="0" name="addmore[' + number + '][model_id]" type="hidden"></td>';
        
        if(number > 0)
        { 
            html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">Remove</button></td></tr>';
            $('tbody').append(html);
        }
        else
        { 
            html += '<td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td></tr>';
            $('tbody').append(html);
        }
        $(".js-example-basic-single").select2({
            placeholder: "--select--",
        });
    }

    $(document).on('click', '#add', function(){
        count++;
        dynamic_field(count);
    });
    $(document).on('click', '.remove', function(){
        //count--;
        $(this).closest("tr").remove();
    });
});

</script>
@endsection

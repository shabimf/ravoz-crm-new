@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Service Transfer List</h4>
            </div>
			<div class="col-md-3">
				<div class="add-new">
				  <!-- <a class="btn btn-primary" href="{{ route('service.create') }}">
					<i class="feather icon-plus"></i> Service Create
				  </a> -->
				</div>
            </div>

        </div>
        <div class="row  ">
			<div class="col-lg-12">
			    {{ Form::open(['url' => 'service/transfer','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-service']) }}
					<div class="form-row well">
					    <!-- <div class="form-group col-md-3">
							{{ Form::label('name', __('Phone Number')) }}
							{{ Form::text('nums', old('nums',(isset($nums)) ? $nums:''), ['class' => 'form-control']) }}
						</div> -->
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Date From')) }}
							{{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control', 'id'=>'datepicker' ]) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Date To')) }}
							{{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control','id' => 'dates']) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('SERIAL NUM.')) }}
							{{ Form::text('serial_num', old('serial_num',(isset($serial_num)) ? $serial_num:''), ['class' => 'form-control']) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('From Branch')) }}
							{{ Form::select('from_branch_id', $branch, old('from_branch_id',(isset($from_branch_id)) ? $from_branch_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch','id' => 'from_branch_id']) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('To Branch')) }}
							{{ Form::select('to_branch_id', $branch, old('to_branch_id',(isset($to_branch_id)) ? $to_branch_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch','id' => 'to_branch_id']) }}
                        </div>
						<div class="form-group col-md-9 two-btn">
						  <button class="btn btn-success search-btn">Search</button>
                          <button class="btn btn-success reset-btn" type="reset">Reset</button>
                        </div>

					</div>
				{{ Form::close() }}
				<div class="card">
				    <div class="card-body">
					    <div class="table-responsive">
						    <table id="transfer-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("servicetransfer.get") }}">
								<thead>
									<tr>
										<th width="5">#</th>
										<th width="80">Transfer.Date</th>
										<th>From Branch</th>
										<th>To Branch</th>
										<th>Service.Num</th>
										<th>Customer</th>
										<th>Phone.Num</th>
										<th>Warranty</th>
										<th>Brand</th>
										<th>Model</th>
										<!-- <th>Remarks</th> -->
										<th>Actions</th>
									</tr>
								</thead>
							</table>
					    </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
<div class="modal fade" id="myModalTechnician" tabindex="-1" role="dialog" aria-labelledby="myModalTechnicianLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalTechnicianLabel">Assign To Technician</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" name="assign_tech" id="assign_tech">
            <div class="modal-body">
                <div class="alert alert-success alert-dismissible technician_msg hide">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Technician assigned <strong>successfully!</strong>
                </div>
                <div class="row">
					<div class="col-md-12 form-group">
						{{ Form::label('name', __('Technician')) }}
						{{ Form::select('technician_id', $users, old('technician_id'), ['class' => 'js-example-basic-single', 'id' => 'technician_id', 'placeholder' => 'Select Technician','required'=>'required']) }}
					</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success assign_btn">Save</button>
            </div>
            </form>
            </div>
        </div>
    </div>
	<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
		    <div class="modal-header">
                <h5 class="modal-title" id="myModalTechnicianLabel">Are you sure?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
			<div class="modal-body">
				<p>Do you really want to delete these records? This process cannot be undone.</p>
			</div>
			<div class="modal-footer justify-content-center">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger reject_btn">Reject</button>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="status"/>
<input type="hidden" id="id"/>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
$(".reset-btn").click(function() {
    $("#from_branch_id").val(null).trigger("change");
    $("#to_branch_id").val(null).trigger("change")
});
$(function() {
	$( 'input[name="from"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
	$( 'input[name="to"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
});

var from_branch_id = getUrlParameter('from_branch_id');
var to_branch_id =  getUrlParameter('to_branch_id');
var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
var serial_num =  getUrlParameter('serial_num');
FTX.Utils.documentReady(function() {
    FTX.TRANSFER.list.init(from_branch_id,to_branch_id,from_date,to_date,serial_num);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

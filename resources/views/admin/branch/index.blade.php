@extends('layouts.app')

@section('content')


<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
                <div class="add-new">
                    <a class="btn btn-primary" href="{{ route('branch.create') }}"> <i class="feather icon-plus"></i> Create Branch</a>
                </div>
            </div>
        </div>
        <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">Branch Management</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="branch-table" class="table table-striped table-bordered" data-ajax_url="{{ route('branch.get') }}">
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <th>Name</th>
                                    <th>Code</th>
                                    <th>Country</th>
                                    <th>Company</th>
					                <th width="100">Actions</th>
                                    <th width="100">Status</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('pagescript')
@include('inc.datatables-js')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Branch.list.init();
    });
</script>
@endsection

<?php

namespace App\Http\Controllers\Backend\Complaint;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Complaint;
use App\Imports\ImportComplaint;
use Illuminate\Support\Facades\View;
use Auth;
use DB;

class ComplaintController extends Controller
{

    public function __construct()
    {
        $this->data['level'] = $this->NameAndID('levels');
        View::share('js', ['complaint', 'master']);
    }

    public function index()
    {
        return view('admin.complaint.index');
    }

    public function getComplaintData()
    {
        $data = Complaint::select([
                        'complaints.id',
                        'complaints.description',
                        'complaints.arabic_description',
                        'complaints.status'
                        
                    ])
                    ->get();

        return DataTables::of($data)
            ->escapeColumns(['name'])
            ->addColumn('actions', function($data) {
                $action = '<a href="'.route('complaint.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
                return $action;
            })
            ->addIndexColumn()
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="complaints" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->make(true);
    }

    public function create()
    {
        return view('admin.complaint.create', $this->data);
    }


    public function store(Request $request)
    {

        $post = new Complaint();

        $validator = $this->validateComplaintForm($request, 0);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetComplaintForm($post, $request);
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The Complaints was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateComplaintForm($request, $id);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = Complaint::find($id);
        $post = $this->GetComplaintForm($post, $request);
        if ($post->save()){
            return redirect()->back()->with("success","The Complaints was successfully updated.");
        }

    }

    public function edit($id)
    {
        $this->data['edit_complaint'] = Complaint::find($id);
        $this->data['edit_id'] = $id;
        return view('admin.complaint.create', $this->data);
    }

    function import(Request $request)
    {
        return view('admin.complaint.import', $this->data);
    }

    function import_store(Request $request)
    {

        $this->validate($request, ['file' => 'required|file|mimes:xls,xlsx,csv|max:10240']);
        if ($request->file('file')
            ->isValid())
        {
            Excel::import(new ImportComplaint, request()->file('file'));
            $data = Excel::toCollection(new ImportComplaint() , $request->file('file'));
            $name_arr = $name_d = $error = $complaint_ex = [];
            if (count($data) > 0)
            {
                DB::beginTransaction();
                $record_count = 0; 
                foreach ($data[0] as $key => $value)
                {
                    if (in_array($value['complaint_name'], $name_arr))
                    {
                        $name_d[] = $value['complaint_name'];
                    } else {
                        $is_exits_complaint = $this->isModelRecordExist('complaints', 'description', trim($value['complaint_name']));
                        if ($is_exits_complaint > 0 && $value['complaint_name'])
                        {
                            $complaint_ex[] = $value['complaint_name'];
                        }
                    }

                  

                    $name_arr[] = trim($value['complaint_name']);

                    if (count($name_d) == 0 &&  count($complaint_ex) == 0)
                    {  
                        $id = Complaint::Create(['description' => $value['complaint_name'] , 'status' => 1, 'created_by' => Auth::id() ]);
                        $record_count = $record_count+1;
                    }
                }
            }
           
            if (count($name_d) > 0)
            {
                $error[] = "Duplication entry of complaint names " . implode(',', array_unique($name_d));
            }
            if (count($complaint_ex) > 0)
            {
                $error[] = "Already exists complaint names " . implode(',', array_unique($complaint_ex));
            }
            if (count($error) > 0)
            {
                return redirect()->back()
                    ->with("info", array_unique($error));
            }
            else
            {
                DB::commit();
                return redirect()->back()->with("success", $record_count." Records Uploaded Successfully!");
            }
           
        }
    }



}

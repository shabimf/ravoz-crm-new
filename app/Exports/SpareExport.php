<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Http\Traits\CommonTrait;

class SpareExport implements FromCollection, WithHeadings, WithMapping
{
    protected $model_id;

    use CommonTrait;

    function __construct($model_id) {   
       $this->data['model_id'] = $model_id;
    }

    public function collection()
    {
        
        return $this->getSpareTableData($this->data['model_id'])->get();
    }

    public function headings(): array
    {
        return [
            'Model',
            'Spare Name',
            'Part Code',
            'Focus Code',
            'Unique Code'
        ];
    }
    
    public function map($spare): array
    {
        return [
            $spare->model_name,
            $spare->spare_name,
            $spare->part_code,
            $spare->focus_code,
            $spare->unique_code,
           
        ];
    }
}

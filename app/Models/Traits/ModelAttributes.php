<?php

namespace App\Models\Traits;

trait ModelAttributes
{
    /**
     * @return string
     */
    public function getEditButtonAttribute($permission, $route)
    {
        //if (access()->allow($permission)) {
            return '<a href="'.route($route, $this).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm">
                    <i class="feather icon-edit"></i>
                    </a>';
        //}
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute($permission, $route)
    {
        // if (access()->allow($permission)) {
            return '<a href="'.route($route, $this).'" 
                    class="btn btn-primary btn-danger btn-sm" 
                    data-method="delete"
                    data-trans-button-cancel="Cancel"
                    data-trans-button-confirm="Delete"
                    data-trans-title="Are you sure you want to do this?">
                        <i data-toggle="tooltip" data-placement="top" title="Delete" class="feather icon-trash"></i>
                </a>';
        //}
    }
}

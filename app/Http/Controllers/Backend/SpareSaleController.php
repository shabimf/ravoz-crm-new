<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Session;
use DB;
use Auth;

class SpareSaleController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next)
        {
            $this->data['branch'] = $this->getBranches(Session::get('sessi_branch_id'));
            $this->data['models'] = $this->NameAndID('models');
            return $next($request);
        });
        View::share('js', ['master']);
    }

    public function index(Request $request)
    {
        $this->data['from'] = ($request->from ? $request->from : date('01-m-Y'));
        $this->data['to'] = ($request->to ? $request->to : date('t-m-Y'));
        return view('admin.sparesale.index', $this->data);
    }

    public function getSpareSale(Request $request)
    {
        $data = DB::table('spare_sale_master')->leftJoin('users', 'users.id', '=', 'spare_sale_master.created_by')
            ->leftJoin('branch', 'branch.id', '=', 'spare_sale_master.branch_id')
            ->select(['spare_sale_master.id', 'spare_sale_master.created_at', 'branch.name as branch_name', 'spare_sale_master.serial_no', 'users.first_name']);

        if (!empty($request->branch_id))
        {
            $data->where("spare_sale_master.branch_id", $request->branch_id);
        }
        else
        {
            if (Session::get('sessi_branch_id'))
            {
                $data->orWhereIn('spare_sale_master.branch_id', explode(',', Session::get('sessi_branch_id')));
            }
        }
        $from = ($request->from ? date('Y-m-d', strtotime($request->from)) : date('Y-m-01'));
        $to = ($request->to ? date('Y-m-d', strtotime($request->to)) : date('Y-m-t'));

        $data->whereBetween('spare_sale_master.created_at', [$from, $to]);
        $data->orderBy('spare_sale_master.id', 'DESC')
            ->get();

        return DataTables::of($data)->escapeColumns(['branch_name'])->filterColumn('created_at', function ($query, $keyword)
        {
            $query->whereRaw(DB::raw("(DATE_FORMAT(spare_sale_master.created_at,'%d-%m-%Y'))") . ' like ?', ["%{$keyword}%"]);
        })->filterColumn('branch_name', function ($query, $keyword)
        {
            $query->whereRaw('branch.name like ?', ["%{$keyword}%"]);
        })->filterColumn('first_name', function ($query, $keyword)
        {
            $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
        })->filterColumn('serial_no', function ($query, $keyword)
        {
            $query->whereRaw('spare_sale_master.serial_no like ?', ["%{$keyword}%"]);
        })->addColumn('actions', function ($data)
        {
            $view = '<a  href="' . route('sparesale.show', $data->id) . '" class="btn btn-info btn-sm"><i class="feather icon-eye"></i></a>';
            return $view;
        })->editColumn('created_at', function ($data)
        {
            return date('d-m-Y', strtotime($data->created_at));
        })
            ->addIndexColumn()
            ->make(true);
    }

    public function create(Request $request)
    {
        return view('admin.sparesale.create', $this->data);
    }

    public function store(Request $request)
    {
        $validator = $this->validateSpareSaleForm($request);
        if ($validator->fails()) return redirect()
            ->back()
            ->withInput()
            ->withErrors($validator);
        DB::beginTransaction();
        $spare_arr = $error = [];
        $serial_no = $this->serialNumber('spare_sale_master', $request->branch_id);
        $currency = $this->GetCurrencyRate($request->branch_id);
        $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        $id = DB::table('spare_sale_master')->insertGetId(['branch_id' => $request->branch_id, 'created_by' => Auth::id() , 'serial_no' => $serial_no]);
        foreach ($request->addmore as $key => $value)
        {
            if (isset($spare_arr[$value['spare_id']]))
            {
                $spare_arr[$value['spare_id']] = (int)$spare_arr[$value['spare_id']] + $value['qty'];
            }
            else
            {
                $spare_arr[$value['spare_id']] = (int)$value['qty'];
            }
            $current_stock = $this->getAvailableStock($request->branch_id, $value['spare_id']);
            if ($spare_arr[$value['spare_id']] > $current_stock)
            {
                $error[] = "This Spare (" . $this->FieldByID('spare', 'spare_name', $value['spare_id']) . ") available stock is " . $current_stock;
            }
            $local_price = ($value['price'] * $currency_rate);
            DB::table('spare_sale_items')->insertGetId(['parent_id' => $id, 'spare_id' => $value['spare_id'], 'qty' => $value['qty'], 'price' => $value['price'], 'local_price' => $local_price]);

            $stock_data['spare_id'] = $value['spare_id'];
            $stock_data['qty'] = $value['qty'];
            $stock_data['branch_id'] = $request->branch_id;
            $this->updateStock($stock_data);
        }
        if (count($error) > 0)
        {
            return redirect()->back()
                ->with("info", array_unique($error));
        }
        else
        {
            DB::commit();
            return redirect()->back()
                ->with("success", "Spare sale added successfully.");
        }

    }

    public function show($id)
    {
        $this->data['edit_id'] = $id;
        $this->data['list'] = DB::table("spare_sale_items as item")
                                ->leftJoin(
                                    "spare_sale_master as parent",
                                    "item.parent_id",
                                    "=",
                                    "parent.id"
                                )
                                ->leftjoin("spare", "item.spare_id", "=", "spare.id")
                                ->leftjoin("models", "spare.model_id", "=", "models.id")
                                ->leftjoin("users", "users.id", "=", "parent.created_by")
                                ->leftjoin("branch", "parent.branch_id", "=", "branch.id")
                                ->select([
                                    \DB::raw('
                                                spare.spare_name,
                                                spare.part_code,
                                                spare.focus_code,
                                                models.name as model_name,
                                                parent.created_at,
                                                parent.serial_no,
                                                item.id,
                                                branch.name as branch_name,
                                                item.*'),
                                ])
                                ->where("item.parent_id", $id)
                                ->get();
        return view('admin.sparesale.view', $this->data);
    }

}


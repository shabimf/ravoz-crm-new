<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('css/plugins/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

    <link href="{{ asset('css/login.css') }}" rel="stylesheet">


</head>

<body>
    <div id="particles-js">

        <div class="text login-page">

            <div class="container">

                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ asset('images/ravoz-logo.png') }}" alt="ravoz-logo" class="login-logo">
                    </div>
                    <div class="col-sm-6 log-form">

                        <h5 class="text-right login-title" style="color:#9c8fbe">SERVICE CRM.</h5>

                        <div class="login_form">

                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="textbox">
                                    <input type="text" class="input-box" placeholder="user" name="username" />
                                </div>
                                <div class="textbox mt-4">
                                    <input type="password" class="input-box" placeholder="password" name="password" />
                                </div>
                                <div class="textbox mt-4">
                                    <button class="btn btn-success btn-block text-right login_btn"> LOGIN</button>
                                </div>

                            </form>


                        </div>
                        <p class="copyright-text">copyright &#169; ravoz digital hk ltd.</p>


                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src='https://cldup.com/S6Ptkwu_qA.js'></script>

    <script>
        // ParticlesJS Config.
        particlesJS("particles-js", {
            "particles": {
                "number": {
                    "value": 80,
                    "density": {
                        "enable": true,
                        "value_area": 700
                    }
                },


                "color": {
                    "value": "#ffffff"
                },

                "shape": {
                    "type": "circle",
                    "stroke": {
                        "width": 0,
                        "color": "#000000"
                    },

                    "polygon": {
                        "nb_sides": 5
                    }
                },


                "opacity": {
                    "value": 0.5,
                    "random": false,
                    "anim": {
                        "enable": false,
                        "speed": 0.1,
                        "opacity_min": 0.1,
                        "sync": false
                    }
                },


                "size": {
                    "value": 3,
                    "random": true,
                    "anim": {
                        "enable": false,
                        "speed": 10,
                        "size_min": 0.1,
                        "sync": false
                    }
                },


                "line_linked": {
                    "enable": true,
                    "distance": 150,
                    "color": "#00ffb3",
                    "opacity": 0.4,
                    "width": 1
                },

                "move": {
                    "enable": true,
                    "speed": 2,
                    "direction": "none",
                    "random": false,
                    "straight": false,
                    "out_mode": "out",
                    "bounce": false,
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 1200
                    }
                }
            },



            "interactivity": {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                        "enable": true,
                        "mode": "grab"
                    },

                    "onclick": {
                        "enable": true,
                        "mode": "push"
                    },

                    "resize": true
                },

                "modes": {
                    "grab": {
                        "distance": 140,
                        "line_linked": {
                            "opacity": 1
                        }
                    },


                    "bubble": {
                        "distance": 400,
                        "size": 40,
                        "duration": 2,
                        "opacity": 8,
                        "speed": 3
                    },

                    "repulse": {
                        "distance": 200,
                        "duration": 0.4
                    },

                    "push": {
                        "particles_nb": 4
                    },

                    "remove": {
                        "particles_nb": 2
                    }
                }
            },



            "retina_detect": true
        });



    </script>

</body>

</html>
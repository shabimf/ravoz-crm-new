<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" href="{{asset('images/ravoz_logo.svg')}}" type="image/x-icon">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->
   
    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend.css') }}" rel="stylesheet">
    <style>
        .hidden {
            display: none !important;
        }
    </style>
</head>

<body>
    <div id="app">
        <!-- **********  HEADER  ********** -->
        @include('inc.header')

        <!-- **********  HEADER  ********** -->
        <main class="py-4">
            <!-- **********  WRAPPER  ********** -->
            <div class="wrapper">
                @include('inc.nav')
                
                @yield('content')
                @if(URL::current() == URL::to(''))
                @include('inc.dashboard')
                @endif 
            </div>
            <!-- **********  WRAPPER  ********** -->
        </main>
    </div>
    <!-- **********  JAVASCRIPT  ********** -->

    <!-- jquery -->
    <script src="{{ asset('js/jquery-3.5.1.min.js')}}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- jquery mask -->
   <script src="{{ asset('js/jquery-validation/jquery.validate.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('js/bootstrap/bootstrap.bundle.min.js')}}"></script>

    <!-- jquery-nicescroll -->
    <script src="{{ asset('js/jquery-nicescroll/jquery.nicescroll.min.js')}}"></script>


    <!-- custom -->
    <script src="{{ asset('js/custom.js')}}"></script>
    <script src="{{ asset('js/backend/common.js')}}"></script>

    @isset($js)
    @foreach($js as $j)
    <script src="{{asset('js/backend/'. $j. '.js')}}"></script>
    @endforeach
    @endif

    @yield('pagescript')
</body>

</html>

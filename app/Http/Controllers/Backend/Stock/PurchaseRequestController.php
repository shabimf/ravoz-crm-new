<?php
namespace App\Http\Controllers\Backend\Stock;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use DB;
use Auth;
use Session;

class PurchaseRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['branch'] = $this->NameAndID('branch');
            $this->data['models'] = $this->NameAndID('models');
            $this->data['spare'] = $this->GetSpareData();
            $this->data['from_branch'] = $this->getBranches(Session::get('sessi_branch_id'));
            return $next($request);
        });
        View::share('js', ['transfer','master']);
    }

    public function index(Request $request)
    {
        $this->data['branch_id'] = ($request->branch_id?$request->branch_id:0);
        $this->data['from'] = ($request->from?$request->from:"");
        $this->data['to'] = ($request->to?$request->to:"");
        return view('admin.request.index', $this->data);
    }

    public function getPurchaseRequest(Request $request) {
        $data = DB::table('order')
                ->leftJoin('users', 'users.id', '=', 'order.created_by')
                ->leftJoin('branch as from', 'from.id', '=', 'order.from_branch')
                ->leftJoin('branch as to', 'to.id', '=', 'order.to_branch')
                ->select(['order.id', 'order.date','from.name as from_branch','order.to_branch as to_branch_id','order.serial_no','to.name as to_branch','users.first_name','order.status'
                ]);


        if (!empty($request->branch_id)) {
            $data->where("order.to_branch", $request->branch_id);
        }else {
            if (Session::get('sessi_branch_id')) {
                $data->orWhere(function($subquery){
                    $subquery->WhereIn('order.from_branch', explode(',',Session::get('sessi_branch_id')));
                    $subquery->WhereIn('order.to_branch', explode(',',Session::get('sessi_branch_id')));
                });
                // $data->orWhereIn('order.from_branch', explode(',',Session::get('sessi_branch_id')));
                // $data->orWhereIn('order.to_branch', explode(',',Session::get('sessi_branch_id')));
            }
        }
        if (isset($request->status)) {
            $data->where("order.status", "=", $request->status);
        }

        $from = ($request->from?date('Y-m-d',strtotime($request->from)):"");
        $to = ($request->to?date('Y-m-d',strtotime($request->to)):"");
        if (!empty($request->from) && !empty($request->to)) {
           $data->whereBetween('order.date', [$from, $to]);
        }
       // $data->orderBy('order.id', 'DESC')->get();

        return DataTables::of($data)->filterColumn('date', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(order.date,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
        })->filterColumn('from_branch', function ($query, $keyword) {
            $query->whereRaw('from.name like ?', ["%{$keyword}%"]);
        })->filterColumn('to_branch', function ($query, $keyword) {
            $query->whereRaw('to.name like ?', ["%{$keyword}%"]);
        })->filterColumn('first_name', function ($query, $keyword) {
            $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
        })->filterColumn('serial_no', function ($query, $keyword) {
            $query->whereRaw('order.serial_no like ?', ["%{$keyword}%"]);
        })->escapeColumns(['from_branch'])->addColumn('actions', function ($data) {
            if($data->status == 1) {
                $viewtxt = '<i class="feather icon-eye"></i>';
            } elseif ($data->to_branch_id != Auth::user()->branch_id) {
                $viewtxt = '<i class="feather icon-eye"></i>';
            } else {
                $viewtxt = "Convert To Stock Transfer";
            }
            $view = '<a  href="' .route('request.edit', $data->id). '" class="btn btn-info btn-sm">'.$viewtxt.'</a>';
            return $view;
        })->addColumn('status', function ($data) {
            if($data->status == 1) {
                $status = '<button type="button" class="btn btn-sm btn-success">Completed</button>';
            } else {
                $status = '<button type="button" class="btn btn-sm btn-warning">Pending</button>';
            }
            return $status;
        })->editColumn('date', function ($data)
        {
        return date('d-m-Y', strtotime($data->date) );
        })->addIndexColumn()->make(true);
    }

    public function create()
    {
        return view('admin.request.create', $this->data);
    }

    public function store(Request $request)
    {
        $validator = $this->validateStockForm($request, 0);
        if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);
        DB::beginTransaction();
        $serial_no = $this->serialNumber('order',$request->branch_id);
        $id = DB::table('order')->insertGetId(['from_branch' =>  $request->from_branch ,'to_branch' => $request->branch_id, 'date' => date('Y-m-d', strtotime($request->date)), 'created_by' => Auth::id(), 'status' => 0,'serial_no'=>$serial_no]);
        foreach ($request->addmore as $key => $value) {
            DB::table('order_items')->insertGetId(['parent_id' => $id, 'spare_id' => $value['spare_id'], 'qty' => $value['qty']]);
        }
        DB::commit();
        return redirect()->back()->with("success", "Purchase order added successfully.");
    }

    public function edit($id)
    {
        $this->data['edit_id'] = $id;
        $this->data['list'] = $this->viewItems("order",$id);
        return view('admin.request.view', $this->data);
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $items = $this->viewItems("order",$id);
        $tserial_no = $this->serialNumber('transfer',$items[0]->from_branch);
        $pserial_no = $this->serialNumber('purchase_master',$items[0]->from_branch);
        $currency = $this->GetCurrencyRate($items[0]->from_branch);
        $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        $tid = DB::table('transfer')->insertGetId(['from_branch' => $items[0]->to_branch,'to_branch' => $items[0]->from_branch, 'date' =>  date('Y-m-d'), 'created_by' => Auth::id(), 'status' => 1, 'serial_no'=> $tserial_no]);
        $pid = DB::table('purchase_master')->insertGetId(['branch_id' => $items[0]->from_branch, 'date' => date('Y-m-d'), 'created_by' => Auth::id(), 'tansfer_id' => $tid, 'serial_no'=> $pserial_no]);
        $spare_arr = $error = [];
        foreach ($items as $value) {
            $qty = ($request['qty'][$value->id]?$request['qty'][$value->id]:0);
            if ($qty > 0) {

                $cost = $this->GetAverageCost($items[0]->to_branch, $value->spare_id);
                $cost = ($cost?$cost->price:0);
                $local_price = ($cost*$currency_rate);

                $usd_price = $this->GetAverageCost($items[0]->from_branch, $value->spare_id);
                $usd_price = ($usd_price?$usd_price->price:0);

                $stock = $this->getAvailableStock($items[0]->from_branch, $value->spare_id);


                DB::table('transfer_items')->insertGetId(['parent_id' => $tid, 'spare_id' => $value->spare_id, 'qty' => $qty, 'price' => $cost, 'local_price' => $local_price]);
                DB::table('purchase_items')->insertGetId(['parent_id' => $pid, 'spare_id' => $value->spare_id, 'qty' => $qty, 'price' => $cost, 'local_price' => $local_price]);
                $is_stock = $this->getStock($items[0]->from_branch,$value->spare_id);

                if(isset($spare_arr[$value->spare_id])) {
                    $spare_arr[$value->spare_id] = (int)$spare_arr[$value->spare_id] + $qty;
                } else {
                    $spare_arr[$value->spare_id] = (int)$qty;
                }

                $current_stock = $this->getAvailableStock($items[0]->to_branch,$value->spare_id);

                if ($spare_arr[$value->spare_id] > $current_stock) {
                    $error [] = "This Spare (".$this->FieldByID('spare', 'spare_name', $value->spare_id).") available stock is ".$current_stock;
                }

                if($current_stock > 0) {
                    DB::table('stock')->where('branch_id', $items[0]->to_branch)->where('spare_id', $value->spare_id)->update(['qty' => DB::raw('qty - '.$qty), 'updated_by' => Auth::id()]);
                }

                if ($is_stock > 0 ) {
                    DB::table('stock')->where('branch_id', $items[0]->from_branch)->where('spare_id', $value->spare_id)->update(['qty' => DB::raw('qty + '.$qty), 'updated_by' => Auth::id(),'price'=> $cost, 'local_price'=>$local_price]);
                } else {
                    DB::table('stock')->insertGetId(['branch_id' => $items[0]->from_branch, 'spare_id' => $value->spare_id, 'qty' => $qty, 'created_by' => Auth::id(), 'updated_by' => Auth::id(),'price'=> $cost, 'local_price'=>$local_price]);
                }
            }
        }
        if (count($error) > 0)
        {
            return redirect()->back()
                ->with("info", array_unique($error));
        }
        else
        {
            DB::table('order')->where('id', $id)->update(['status' => 1]);
            DB::commit();
            return redirect()->back()->with("success", "Stock transfer successfully.");
        }
    }



}

(function () {
    var i = 1;
    FTX.Transfer = {
       
        list: {
        
            selectors: {
                Transfer_table: $('#transfer-table'),
            },
        
            init: function (branch_id,from,to,status) {
               
                this.selectors.Transfer_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.Transfer_table.data('ajax_url'),
                        type: 'post',
                        data: function (d) {
                            d.from = from,
                            d.to = to,
                            d.branch_id = branch_id,
                            d.status = status
                        },
                    },
                    columns: [
                        {
                            "data": 'DT_RowIndex',
                            orderable: false, 
                            searchable: false
                        },
                        { data: 'date', name: 'date' },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'from_branch', name: 'from_branch' },
                        { data: 'to_branch', name: 'to_branch' },
                        { data: 'first_name', name: 'first_name' },
                        { data: 'status', name: 'status', searchable: false, sortable: false },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();
(function () {
    FTX.STOCK = {
       
        list: {
        
            selectors: {
                Stock_table: $('#stock-table'),
            },
        
            init: function (branch_id,from,to) {
                this.selectors.Stock_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.Stock_table.data('ajax_url'),
                        type: 'post',
                        data: function (d) {
                            d.from = from,
                            d.to = to,
                            d.branch_id = branch_id
                        },
                    },
                    columns: [
                        {
                            "data": 'DT_RowIndex',
                            orderable: false, 
                            searchable: false
                        },
                        { data: 'date', name: 'date' },
                        { data: 'serial_no', name: 'serial_no'},
                        { data: 'from_branch', name: 'from_branch'},
                        { data: 'first_name', name: 'first_name'},
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }

    FTX.OPENSTOCK = {
       
        list: {
        
            selectors: {
                Stock_table: $('#open-stock-table'),
            },
        
            init: function (branch_id,model_id,show) {

                var visible = false;
                if (show == 1) {
                  visible = false;
                } 
                
                this.selectors.Stock_table.dataTable({
                    processing: false,
                    serverSide: true,
                    cache: false,
                    ajax: {
                        url: this.selectors.Stock_table.data('ajax_url'),
                        type: 'post',
                        data: function (d) {
                            d.branch_id = branch_id,
                            d.model_id  = model_id
                        },
                    },
                    columns: [
                        {
                            "data": 'DT_RowIndex',
                            orderable: false, 
                            searchable: false
                        },
                        { data: 'branch_name', name: 'branch_name' },
                        { data: 'model_name', name: 'model_name'},
                        { data: 'spare_name', name: 'spare_name'},
                        { data: 'qty', name: 'qty'},
                        { data: 'local_price', name: 'local_price'},
                        { data: 'actions', name: 'actions', searchable: false, sortable: false , visible: visible},
                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();
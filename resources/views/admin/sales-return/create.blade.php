@extends('layouts.warranty')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                <h4 class="card-title-text">Activation/Sales Return</h4>
                </div>
                <div class="col-md-6 {{(userHasPermission('Warranty','activation_return_list')?'':'hide')}}">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('return') }}"> <i
                            class="feather icon-list"></i> Activation/Sales - Return List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['route' => 'return.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-sales-return', 'enctype'=>"multipart/form-data"]) }}
            @csrf
            <div class="form-row">

                <div class="form-group col-md-12">
                    {{ Form::label('name', __('File')) }}
                    <input type="file" name="file" class="form-control" id="customFile">
                    <a class="pull-right" href="{{url('/uploads/format/sales_return.xlsx')}}"><small>Download Sample xlsx File <i class="fa fa-download"></i></small></a>
                </div>

            </div>
            <button class="btn btn-primary float-right">Import</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/select2/bloodhound.min.js')}}"></script>
@endsection

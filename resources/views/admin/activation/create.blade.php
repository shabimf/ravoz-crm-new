@extends('layouts.warranty')
@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                <h4 class="card-title-text">Activation</h4>
                </div>
                <div class="col-md-6 {{(userHasPermission('Warranty','import_activation')?'':'hide')}}">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('activation.list') }}"> <i
                            class="feather icon-list"></i> Activation List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['url'=>'warranty/activation/update','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-activation']) }}
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    {{ Form::label('name', __('Date')) }}
                    {{ Form::text('date','', ['class' => 'form-control', 'id' =>'date','required']) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('name', __('IMEI / SN')) }}
                    {{ Form::text('imei','', ['class' => 'form-control', 'id' =>'imei','required']) }}
                </div>
            </div>
            <button class="btn btn-primary float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script>
$(function() {
	$( 'input[name="date"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
});
</script>
@endsection


<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Http\Traits\CommonTrait;

class SpareClaimExport implements FromCollection, WithHeadings, WithMapping
{
    protected $from,$to,$country_id,$branch_id,$branch_ids,$currency_id,$company_id,$type_id;

    use CommonTrait;

    function __construct($from, $to, $country_id, $branch_id, $branch_ids, $company_id,$currency_id,$type_id) {

        $this->data['from'] = $from;
        $this->data['to'] = $to;
        $this->data['country_id'] = $country_id;
        $this->data['branch_id'] = $branch_id;
        $this->data['branch_ids'] = $branch_ids;
        $this->data['currency_id'] = $currency_id;
        $this->data['company_id'] = $company_id;
        $this->data['type_id'] = $type_id;
        
    }

    public function collection()
    {
        return $this->getSpareCliamTableData($this->data)->get();
    }

    public function headings(): array
    {
        return [
            'SL NO',
            'YEAR',
            'MONTH',
            'COUNTRY',
            'COMPANY',
            'BRANCH',
            'SPARE NAME',
            'SERVICE CALL',
            'AMOUNT'
        ];
    }
    
    public function map($claim): array
    {
        if (!empty($this->data['currency_id'])) {
            $currency_convert =  $this->GetByID('currency',$claim->currency_id);
            $curr  = $this->FieldByID('currency','name',$this->data['currency_id']);
            $currency_convert_d =  $this->GetByID('currency',$this->data['currency_id']);
            $amt = number_format($claim->price/$currency_convert->rate,2);
            if ($curr == "USD") $price  = $curr." ".$amt;
            else $price  = $curr." ".number_format($amt*$currency_convert_d->rate,2);
        }
        else {
            $price = $claim->currency ." ".$claim->price;
        }
        return [
            $claim->rownum,
            $claim->year,
            date("F", mktime(0, 0, 0, $claim->month, 10)),
            $claim->country_name,
            $claim->company_name,
            $claim->branch_name,
            $claim->spare_name,
            $claim->serial_no,
            $price
        ];
    }
}

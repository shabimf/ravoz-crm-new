<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportSpare;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Spare;
use DB;
use Auth;

class SpareController extends Controller
{
    public function __construct()
    {
        $this->data['model'] = $this->NameAndID('models');
        View::share('js', ['master']);
    }

    public function index()
    {
        return view('admin.spare.index');
    }

    public function create()
    {
        return view('admin.spare.create', $this->data);
    }

    public function edit($id)
    {
        $this->data['spare'] = $this->GetByID('spare', $id);
        $this->data['edit_id'] = $id;
        return view('admin.spare.create', $this->data);
    }

    public function store(Request $request)
    {
        $post = array();
        $validator = $this->validateSpareForm($request);
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);
        $post = $this->GetSpareForm($post, $request);
        $post['created_by'] = Auth::id();
        if (DB::table('spare')->where('spare_name', $request->name)->where('model_id', '=', $request->model_id)->first()) {
            return redirect()->back()->with("error", "That model already exists. Please choose a different name.");
        }
        $this->insertdata('spare', $post);
        return redirect()->back()->with("success", "spare was successfully created.");
    }

    public function update(Request $request, $id)
    {
        $post = array();
        $validator = $this->validateSpareForm($request, $id);
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);
        $post = $this->GetSpareForm($post, $request, $id);
        $post['updated_by'] = Auth::id();
        if (DB::table('spare')->where([['spare_name', "=", $request->name], ['model_id', "=", $request->model_id], ['id', "!=", $id]])->first()) {
            return redirect()->back()->with("error", "That model already exists. Please choose a different name.");
        }
        $this->updatedata('spare', $post, $id);
        return redirect()->back()->with("success", "Spare was successfully updated.");
    }


    public function getSpare()
    {
        $data = DB::table("spare")
                    ->leftjoin("models",DB::raw("FIND_IN_SET(models.id,spare.model_id)"),">",DB::raw("'0'"))
                    ->groupBy('spare.id')
                    ->get([
                        'spare.*',
                        DB::raw("GROUP_CONCAT(models.name) as model_name")
                    ]);
        foreach ($data as $key => $d) {
            if ($d->model_id == 0) {
                $model_name = 'Others';
                $data[$key]->model_name = $model_name;
            }
        }
        return DataTables::of($data)

            ->escapeColumns(['model', 'name', 'partcode', 'focuscode', 'uniquecode'])
            ->addColumn('actions', function ($data) {
                return '<a href="' . route('spare.edit', $data->id) . '" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            })
            ->addColumn('status', function ($data) {
                if ($data->status == 1) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                return '<label class="switch "><input id="' . $data->id . '" data-id=' . $data->id . ' data-tb="spare" type="checkbox" value="' . $data->status . '" class="primary status_change" ' . $checked . ' ><span class="slider round"></span></label>';
            })
            ->addIndexColumn()
            ->make(true);
    }

    function import(Request $request)
    {
        return view('admin.spare.import', $this->data);
    }
    function import_store(Request $request)
    {

        $this->validate($request, ['file' => 'required|file|mimes:xls,xlsx,csv|max:10240']);
        if ($request->file('file')
            ->isValid())
        {
            $file = $request->file('file');
            Excel::import(new ImportSpare, request()->file('file'));
            $data = Excel::toCollection(new ImportSpare() , $request->file('file'));
            $name_arr = $part_arr = $desc_arr = $focus_arr = $uniq_arr = $error = [];

            if (count($data) > 0)
            {
                DB::beginTransaction();
                $name_d = $part_d = $focus_d = $name_ex = $part_ex = $focus_ex = [];
                $record_count = 0; 
                foreach ($data[0] as $key => $value)
                {
                    if (in_array($value['spare_name'], $name_arr))
                    {
                        $name_d[] = $value['spare_name'];
                    }
                    // if (in_array($value['part_code'], $part_arr))
                    // {
                    //     $part_d[] = $value['part_code'];
                    // }
                    if (in_array($value['focus_code'], $focus_arr))
                    {
                        $focus_d[] = $value['focus_code'];
                    }
                    $is_exits_spare = DB::table('spare')->where([['spare_name', "=", $value['spare_name']], ['model_id', "=", $request->model_id]])->get() ;

                    if ($is_exits_spare->count() > 0 && $value['spare_name'])
                    {
                        $name_ex[] = $value['spare_name'];
                    }
                    // $is_exits_part = $this->isModelRecordExist('spare', 'part_code', trim($value['part_code']));
                    // if ($is_exits_part > 0 && $value['part_code'])
                    // {
                    //     $part_ex[] = $value['part_code'];
                    // }
                    $is_exits_focus = $this->isModelRecordExist('spare', 'focus_code', trim($value['focus_code']));
                    if ($is_exits_focus > 0 && $value['focus_code'])
                    {
                        $focus_ex[] = $value['focus_code'];
                    }

                    $name_arr[] = trim($value['spare_name']);
                    // $part_arr[] = trim($value['part_code']);
                    $focus_arr[] = trim($value['focus_code']);

                    $model_ids = '';
                    if ($request->model_id) {
                        $model_ids = implode(',',$request->model_id);
                    }

                    if (count($name_d) == 0 &&  count($part_d) == 0 &&  count($focus_d) == 0 &&  count($name_ex) == 0 &&  count($focus_ex) == 0)
                    {
                        $group = Spare::Create(['model_id' => $model_ids, 'spare_name' => $value['spare_name'], 'part_code' => $value['part_code'], 'focus_code' => $value['focus_code'], 'unique_code' => $value['unique_code'], 'status' => 1, 'created_by' => Auth::id() ]);
                        $record_count = $record_count+1;
                    }
                }
            }
            if (count($name_d) > 0)
            {
                $error[] = "Duplication entry of Spare Name " . implode(',', array_unique($name_d));
            }
            if (count($part_d) > 0)
            {
                $error[] = "Duplication entry of Part Code " . implode(',', array_unique($part_d));
            }
            if (count($focus_d) > 0)
            {
                $error[] = "Duplication entry of Focus Code " . implode(',', array_unique($focus_d));
            }
            if (count($name_ex) > 0)
            {
                $error[] = "Already exists Spare Name " . implode(',', array_unique($name_ex));
            }
            // if (count($part_ex) > 0)
            // {
            //     $error[] = "Already exists Part Code " . implode(',', array_unique($part_ex));
            // }
            if (count($focus_ex) > 0)
            {
                $error[] = "Already exists Focus Code " . implode(',', array_unique($focus_ex));
            }
            if (count($error) > 0)
            {
                return redirect()->back()
                    ->with("info", array_unique($error));
            }
            else
            {
                DB::commit();
                return redirect()->back()->with("success", $record_count." Records Uploaded Successfully!");
            }
        }
    }


}

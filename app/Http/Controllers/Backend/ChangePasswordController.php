<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Models\Auth\User;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        View::share('js', ['master']);
    }
   
    public function index()
    {
        return view('auth.passwords.changepassword');
    } 
   
    public function store(Request $request)
    {
        $validator = $this->validateChangePasswordForm($request);
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);
   
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
   
        return redirect()->back()->with("success","Password change successfully.");
    }
}
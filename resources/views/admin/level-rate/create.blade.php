@extends('layouts.app')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                  <h4 class="card-title-text">{{((isset($edit_level)) ? "Edit":'Create')}} Level Rate </h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ url('admin/level/rate') }}"> <i
                            class="feather icon-list"></i> Level Rate List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['route' => 'level.rate.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-level-rate']) }}
            @csrf
            <div class="form-row">
                <div class="form-group col-md-3">
                    {{ Form::label('name', __('Date')) }}
                    {{ Form::text('date', old('date'), ['class' => 'form-control', 'required' => 'required','id'=>'id']) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('name', __('Company')) }}
                    {{ Form::select('company_id', $companies, old('company_id'), ['class' => 'form-control js-example-basic-single company_id', 'placeholder' => 'Select Company', 'required' => 'required', 'id' => 'company_id']) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('name', __('Level')) }}
                    {{ Form::select('level_id', $level, old('level_id'), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Level', 'required' => 'required', 'id' => 'level_id']) }}
                </div>
                <div class="form-group col-md-3">
                    <span class="currcls"></span>
                    {{ Form::label('name', __('Rate')) }}
                    {{ Form::text('rate', old('rate'), ['class' => 'form-control', 'required' => 'required']) }}
                </div>

            </div>
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script>
$(function() {
    $( 'input[name="date"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM-YYYY'));
    });
});
$( "#company_id" ).change(function() {
    $.ajax({
        type: 'post',
        url: "{{ route('getCurrency') }}",
        data: {
            'id': $(this).val()
        },
        success: function (data) {
          $(".currcls").html(data);
        }
    });
});
</script>
@endsection

@extends('layouts.warranty')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
	    <div class="row mb-4">
            <div class="col-md-12 text-right">
			    <a class="btn btn-primary {{(userHasPermission('Warranty','imei_transfer_list')?'':'hide')}}" href="{{ route('IMEITransfer.imeilist') }}">
					<i class="feather icon-list"></i> Uploaded IMEI Transfer List
				</a>
				<a class="btn btn-primary ml-2 {{(userHasPermission('Warranty','import_imei_transfer')?'':'hide')}}" href="{{ route('IMEITransfer.create') }}">
				   <i class="feather icon-plus"></i> Import IMEI Transfer
				</a>
            </div>
        </div>

        <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">IMEI Transfer List</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="IMEITransfer-table" class="table table-striped table-bordered" data-ajax_url="{{ route("IMEITransfer.get") }}">
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
									<th>Date</th>
					                <th>From Country</th>
									<th>To Country</th>
					                <th>IMEI-1</th>
									<th>IMEI-2</th>
									<th>SN</th>
									<th>Brand</th>
									<th>Model</th>
								
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('pagescript')
@include('inc.datatables-js')
<script>
	var para = getUrlParameter('id');
    FTX.Utils.documentReady(function() {
        FTX.IMEITransfer.list.init(para);
    });
</script>
@endsection

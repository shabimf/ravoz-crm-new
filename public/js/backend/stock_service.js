$(".check_replace_imei").click(function(e) {

    var query = $("#imei_selected").val();
    $.ajax({
        url: "/service/getDoaIMEIData",
        type: "POST",
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            query: query,
            product_type: 1
        },
        cache: false,
        dataType: 'json',
        success: function(dataResult) {

            console.log(dataResult);
            // return false;
            var resultData = dataResult.data;
            if (resultData.length > 0) {
                $.each(resultData, function(index, row) {
                    var device = row.model_name;
                    device += "(";
                    device += row.ram_name + "," + row.rom_name + "," + row.color;
                    device += ")";
                    $("#model_name").html(device);
                    $("#imei_no_1").html(row.IMEI_no1);
                    $("#imei_no_2").html(row.IMEI_no2);
                    $("#imei_id").val(row.id);
                    $("#billed_status").html('Not Sold');
                    $(".imei_success").addClass('hide');
                    $("#doa_id").val(row.doa_id);
                    if (row.model_id) {
                        getVersion(row.variant_id);
                    }
                    // if (row.expiry_date) {
                    // 	$("#billed_status").html('Sold');
                    // } else
                    if (row.is_doa == 0) {
                        $("#billed_status").html('This IMEI No not in DOA');
                    } else if (row.stock_service_id > 0 && row.stock_status == 0) {
                        $("#billed_status").html('This IMEI No alreay in stock service');
                    } else if (row.is_doa > 0 && row.status == 1) {
                        if (row.issue_doa > 0 || row.replace_imei_id > 0) {
                            $(".imei_success").removeClass('hide');
                        }
                    }
                })
            } else {
                alert("IMEI Number Not Found in Database");
                $("#model_name").html("");
                $("#imei_no_1").html("");
                $("#imei_no_2").html("");
                $("#billed_status").html('');
                $(".imei_success").addClass('hide');

            }
        }
    });
});

$(".check_doa").click(function() {
    var query = $("#doa_reference").val();
    $.ajax({
        url: "/service/getDOAData",
        type: "POST",
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            query: query
        },
        cache: false,
        dataType: 'json',
        success: function(dataResult) {
            console.log(dataResult);
            if (dataResult == 0) {
                alert("DOA Not Found");
                $("#model_name").html("");
                $("#imei_no_1").html("");
                $("#imei_no_2").html("");
                $("#billed_status").html('');
                $(".imei_success").addClass('hide');
            }
            var resultData = dataResult.data;
            if (resultData.length > 0) {
                $.each(resultData, function(index, row) {
                    var device = row.model_name;
                    device += "(";
                    device += row.ram_name + "," + row.rom_name + "," + row.color;
                    device += ")";
                    if (row.imei_id != '') {
                        $("#imei_no_1").html(row.IMEI_no1);
                        $("#imei_no_2").html(row.IMEI_no2);
                        $("#imei_id").val(row.imei_id);
                    }
                    $("#model_name").html(device);
                    $("#billed_status").html('Not Sold');
                    $(".imei_success").addClass('hide');
                    $("#doa_id").val(row.id);
                    if (row.model_id) {
                        getVersion(row.variant_id);
                    }
                    if (row.stock_service_id > 0 && row.stock_status == 0) {
                        $("#billed_status").html('Alreay in stock service');
                    } else if (row.status == 1) {
                        if (row.issue_doa > 0) {
                            $(".imei_success").removeClass('hide');
                        }
                    }
                })
            } else {
                alert("IMEI Number Not Found in Database");
                $("#model_name").html("");
                $("#imei_no_1").html("");
                $("#imei_no_2").html("");
                $("#billed_status").html('');
                $(".imei_success").addClass('hide');
            }
        }
    });
});

$(".imei_success").click(function(e) {
    var _validator = $("form[name='doa_form']").validate({
        rules: {
            technician_id: "required",
            problem_remarks: "required",
            'complaint[]': "required"
        },
        messages: {
            problem_remarks: "Please enter remarks",
            technician_id: "Please select technician",
            'complaint[]': "Please select complaint"
        },
        submitHandler: function(form) {
            var datastring = $(form).serialize();
            $(".imei_success").addClass('hide');
            $.ajax({
                url: "/service/stock_service/update",
                type: "PUT",
                data: datastring,
                cache: false,
                success: function(dataResult) {
                    console.log(dataResult);
                    if (dataResult) {
                        $(".doa_msg").slideDown();
                        setTimeout(function() {
                            $(".imei_success").removeClass('hide');
                            $('#stocksaleModal').modal('hide');
                            window.location.reload();
                        }, 4000);
                    }
                }
            });
        }
    });
});

$("#doa_ref").click(function() {
    $(".model-det").removeClass('d-none');
    $(".imei-det").addClass('d-none');
});
$("#imei_ref").click(function() {
    $(".imei-det").removeClass('d-none');
    $(".model-det").addClass('d-none');
});
@extends('layouts.app')

@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                    <h4 class="card-title-text">Spare</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('spare.index') }}"> <i
                            class="feather icon-list"></i> Spare List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($spare, ['route' => ['spare.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-company']) }}
            @else
                {{ Form::open(['route' => 'spare.store','class' => 'form-horizontal','role' => 'form','method' => 'post','id' => 'create-spare', 'auto-complete' => 'off']) }}
            @endif
            @csrf
            @php
            $selected = [];
            if (isset($spare)) {
                $selected = explode(',',$spare->model_id);
            }
            @endphp
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('Model', __('Model')) }}
                    <select  name="model_id[]" multiple="multiple" class="js-example-basic-multiple select2-hidden-accessible" id="model_id">
                        @foreach($model as $k => $v)
                            <option value="{{ $k }}" {{ (in_array($k, $selected)) ? 'selected' : '' }}>{{ $v}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col form-group">
                    {{ Form::label('name', __('Spare Name')) }}
                    {{ Form::text('name', old('name',(isset($spare)) ? $spare->spare_name:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
            </div>
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('focuscode', __('Focus Code')) }}
                    {{ Form::text('focuscode', old('focuscode',(isset($spare)) ? $spare->focus_code:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <div class="col form-group">
                    {{ Form::label('partcode', __('Part Code')) }}
                    {{ Form::text('partcode', old('partcode',(isset($spare)) ? $spare->part_code:''), ['class' => 'form-control']) }}
                </div>
                <!-- <div class="col form-group">
                    {{ Form::label('description', __('Description')) }}
                    {!! Form::text('description', old('partcode',(isset($spare)) ? $spare->description:''), ['class'=>'form-control', 'required' => 'required']) !!}
                </div> -->
               
            </div>
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('uniquecode', __('Unique Code')) }}
                    {{ Form::text('uniquecode', old('uniquecode',(isset($spare)) ? $spare->unique_code:''), ['class' => 'form-control']) }}
                </div>
                <div class="col form-group">
                </div>
            </div>
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/select2/bloodhound.min.js')}}"></script>
@endsection
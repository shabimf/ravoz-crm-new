@extends('layouts.warranty')
@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            {{ Form::open(['route' => 'IMEITransfer.imeilist','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-imei']) }}
                <div class="form-row">
                    <div class="form-group col-md-3">
                        {{ Form::label('name', __('From Date')) }}
                        {{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control', 'required' => 'required','id'=>'datepicker' ]) }}
                    </div>
					<div class="form-group col-md-3">
                        {{ Form::label('name', __('To Date')) }}
                        {{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control', 'required' => 'required' , 'id' => 'dates']) }}
                    </div>

					<div class="form-group col-md-2">
                      <button class="btn btn-primary search-btn">Search</button>
					</div>
                </div>
            {{ Form::close() }}
            <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">Uploaded IMEI Transfer  List</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	    <table id="IMEITransfer-table" class="table table-striped table-bordered" data-ajax_url="{{ route('IMEITransfer.imeiget') }}" >
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <th>Date</th>
									<th>From Country</th>
                                    <th>To Country</th>
					                <th>Brand</th>
                                    <th>Model</th>
									<th>Action</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

@endsection
@section('pagescript')
@include('inc.datatables-js')

<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/datepicker/bootstrap-timepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script>
$(function() {
	$( 'input[name="from"]').daterangepicker( {
		singleDatePicker: true,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	});
	$( 'input[name="to"]').daterangepicker( {
		singleDatePicker: true,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	});
});
// var model_id = getUrlParameter('model_id');
var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
FTX.Utils.documentReady(function() {
    FTX.IMEITransferList.list.init(from_date,to_date);
});
</script>
@endsection

<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;

class ImportPurchaseSpare implements ToCollection, WithHeadingRow
{

    public function collection(Collection $rows)
    {
        Validator::make($rows->toArray(), [
            '*.focus_code' => 'required',
            '*.unit_price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            '*.quantity' => 'integer|required'
        ])->validate();
    }

    

}

<!-- datatables -->
<script src="{{ asset('js/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('js/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('js/datatables/buttons.flash.min.js')}}"></script>
<script src="{{ asset('js/datatables/jszip.min.js')}}"></script>
<script src="{{ asset('js/datatables/pdfmake.min.js')}}"></script>
<script src="{{ asset('js/datatables/vfs_fonts.js')}}"></script>
<script src="{{ asset('js/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ asset('js/datatables/buttons.print.min.js')}}"></script>

<!-- fixedHeader -->
<script src="{{ asset('js/datatables/dataTables.fixedHeader.min.js')}}"></script>

<!-- dataTables select -->
<script src="{{ asset('js/datatables/dataTables.select.min.js')}}"></script>

<!-- dataTables.fixedColumns.min -->
<script src="{{ asset('js/datatables/dataTables.fixedColumns.min.js')}}"></script>

<!-- dataTables.rowGroup.min -->
<script src="{{ asset('js/datatables/dataTables.rowGroup.min.js')}}"></script>
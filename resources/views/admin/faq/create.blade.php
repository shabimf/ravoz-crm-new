@extends('layouts.app')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                  <h4 class="card-title-text">{{((isset($edit_faq)) ? "Edit":'Create')}} FAQ </h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ url('admin/settings/faqs') }}"> <i
                            class="feather icon-list"></i> FAQ List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($edit_faq, ['route' => ['faqs.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-model']) }}
            @else
                {{ Form::open(['route' => 'faqs.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-model']) }}
            @endif
            @csrf
            
            <div class="form-row">
                <div class="form-group col-md-6">
                    {{ Form::label('name', __('Question')) }}
                    {{ Form::text('question', old('name',(isset($edit_faq)) ? $edit_faq->question:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('name', __('Answer')) }}
                    {{ Form::text('answer', old('rate',(isset($edit_faq)) ? $edit_faq->answer:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
            </div>
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection

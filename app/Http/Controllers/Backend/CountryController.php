<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use DB;
use Auth;


class CountryController extends Controller
{
    public function __construct()
    {
        $this->data['currency'] = $this->NameAndID('currency');
        View::share('js', ['master']);
    }

    public function index()
    {
        return view('admin.country.index');
    }

    public function create()
    {
        return view('admin.country.create', $this->data);
    }

    public function edit($id)
    {

        $this->data['country'] = $this->GetByID('country', $id);
        $this->data['edit_id'] = $id;
        return view('admin.country.create', $this->data);

    }

    public function store(Request $request)
    {
        $post = array();

        $validator = $this->validateCountryForm($request);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetCountryForm($post, $request);
        $post['created_by'] = Auth::id();
        $this->insertdata('country', $post);

        return redirect()->back()->with("success","The country was successfully created.");

    }

    public function update(Request $request, $id)
    {
        $post = array();

        $validator = $this->validateCountryForm($request, $id);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetCountryForm($post, $request, $id);
        $post['updated_by'] = Auth::id();
        $this->updatedata('country', $post, $id);

        return redirect()->back()->with("success","The country was successfully updated.");
    }


    public function getCountry()
    {
        $data=DB::table("country")
              ->leftJoin('currency', 'country.currency_id', '=', 'currency.id')
              ->select('country.*', 'currency.name as currency')
              ->get();
        //$data = $this->GetAll('country');
        return DataTables::of($data)
            ->escapeColumns(['name','rate'])
            ->addColumn('actions', function($data) {
                return '<a href="'.route('country.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            })
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="country" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->addIndexColumn()
            ->make(true);
    }
}

@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
	    <div class="row mb-4">
            <div class="col-md-12 text-right">
                <a class="btn btn-overlay-dark" href="{{ route('complaint.import') }}"> <i class="feather icon-log-in"></i> Import</a>
				<a class="btn btn-primary" href="{{ route('complaint.create') }}"> <i class="feather icon-plus"></i> Create Complaint</a>
            </div>
        </div>
       
        <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">Complaint Management</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="complaint-table" class="table table-striped table-bordered" data-ajax_url="{{ route("complaint.get") }}">
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <!-- <th>Level</th> -->
					                <th>Complaint Details</th>
									<!-- <th>Complaint Arabic Details</th> -->
					                <th  width="100">Actions</th>
									<th  width="100">Status</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('pagescript')
@include('inc.datatables-js')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Complaints.list.init();
    });
</script>
@endsection

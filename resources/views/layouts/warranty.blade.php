<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" href="{{asset('images/ravoz_logo.svg')}}" type="image/x-icon">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend.css') }}" rel="stylesheet">
    <style>
        .hidden {
            display: none !important;
        }
    </style>
</head>

<body>
    <div id="app">
        <!-- **********  HEADER  ********** -->
        @include('inc.header')

        <!-- **********  HEADER  ********** -->
        <main class="py-4">
            <!-- **********  WRAPPER  ********** -->
            <div class="wrapper">





            <div class="sidebar-panel nicescrollbar sidebar-panel-light">
                <ul class="sidebar-menu">
                    <li class="menu-expand">
                        <a class="side-toggle" href="javascript:void(0);">
                        <span></span> <span class="sidebar-badge"> </span><i class="la la-compress"></i>
                        </a>
                    </li>
                    <li class="{{ active_class(request()->is('warranty')) }}">
                        <a href="{{ route('warranty.index') }}">
                           <span>Dashboard</span>  <i class="la la-home"></i> 
                        </a>
                    </li>
                    @if (userHasPermission('Warranty','imei_list') || userHasPermission('Warranty','imei_transfer_list'))
                    <li class = "{{ set_active(['warranty/devices','warranty/devices/create','warranty/devices/imei_list','warranty/IMEITransfer','warranty/IMEITransfer/create','warranty/IMEITransfer/imei_list']) }}
                    {{ active_class(request()->is('warranty/model/*/edit')) }}">
                        <a href="#">
                        <i class="fa fa-caret-down down-arrow"></i> <span class="menu-left">IMEI </span> <i class="la la-barcode"></i> 
                        </a>
                        <ul class="sidebar-submenu">
                            <li class="{{(userHasPermission('Warranty','imei_list')?'':'hide')}}">
                                <a href="{{ route('devices') }}" class="nav-link {{ request()->is('warranty/devices*') ? 'active font-weight-bolder' : '' }}"> 
                                    <span> IMEI Management </span> 
                                </a>
                            </li>
                            <!-- <li class="{{(userHasPermission('Warranty','imei_list')?'':'hide')}}">
                                <a href="{{ route('devices') }}" class="nav-link {{ request()->is('warranty/activation/list*') ? 'active font-weight-bolder' : '' }}"> 
                                    <span> Activated IMEI </span> 
                                </a>
                            </li> -->
                            <li class="{{(userHasPermission('Warranty','imei_transfer_list')?'':'hide')}}">
                                <a href="{{ route('IMEITransfer') }}" class="nav-link {{ request()->is('warranty/IMEITransfer*') ? 'active font-weight-bolder' : '' }}"> <span> IMEI Transfer </span> 
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if (userHasPermission('Warranty','sales_list') || userHasPermission('Warranty','import_activation') || userHasPermission('Warranty','activation_return_list'))
                    <li class = "{{ active_class(request()->is('warranty/sale*')) }}
                        {{ active_class(request()->is('warranty/activation*')) }}
                        {{ active_class(request()->is('warranty/return*')) }}">
                        <a href="#">
                        <i class="fa fa-caret-down down-arrow"></i> <span class="menu-left">Sales </span><i class="la la-get-pocket"></i> 
                        </a>
                        <ul class="sidebar-submenu">
                            <li class="{{(userHasPermission('Warranty','sales_list')?'':'hide')}}"><a href="{{ route('sale') }}" class="nav-link {{ request()->is('warranty/sale*') ? 'active font-weight-bolder' : '' }}"> <span> Sales </span> </a></li>
                            <li class="{{(userHasPermission('Warranty','import_activation')?'':'hide')}}"><a href="{{ route('activation.list') }}" class="nav-link {{ active_class(request()->is('warranty/activation*')) ? 'active font-weight-bolder' : '' }}"> <span> Activated IMEI </span> </a></li>
                            <li class="{{(userHasPermission('Warranty','activation_return_list')?'':'hide')}}"><a href="{{ route('return') }}" class="nav-link {{ request()->is('warranty/return*') ? 'active font-weight-bolder' : '' }}"> <span> Activation/Sales Return </span> </a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
        </div>





                @yield('content')

            </div>
            <!-- **********  WRAPPER  ********** -->
        </main>
    </div>
    <!-- **********  JAVASCRIPT  ********** -->

    <!-- jquery -->
    <script src="{{ asset('js/jquery-3.5.1.min.js')}}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- jquery mask -->
   <script src="{{ asset('js/jquery-validation/jquery.validate.min.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('js/bootstrap/bootstrap.bundle.min.js')}}"></script>

    <!-- jquery-nicescroll -->
    <script src="{{ asset('js/jquery-nicescroll/jquery.nicescroll.min.js')}}"></script>


    <!-- custom -->
    <script src="{{ asset('js/custom.js')}}"></script>
    <script src="{{ asset('js/backend/common.js')}}"></script>

    @isset($js)
    @foreach($js as $j)
    <script src="{{asset('js/backend/'. $j. '.js')}}"></script>
    @endforeach
    @endif

    @yield('pagescript')
</body>

</html>

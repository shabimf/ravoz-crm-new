<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Http\Traits\CommonTrait;

class ActivationModelExport implements FromCollection, WithHeadings, WithMapping
{
    protected $from,$to,$country_id,$model_id;

    use CommonTrait; 

    function __construct($from, $to, $country_id, $model_id) {
        $this->data['from'] = $from;
        $this->data['to'] = $to;
        $this->data['country_id'] = $country_id;
        $this->data['model_id'] = $model_id;

    }

    public function collection()
    {
        return $this->getActivationModelTableData($this->data)->get();
    }

    public function headings(): array
    {
        return [
            'SL NO',
            'MODEL',
            'COLOR',
            'RAM',
            'ROM',
            'ACTIVATED QUANTITY'
        ];
    }
    
    public function map($activation): array
    {
        return [
            $activation->rownum,
            $activation->model,
            $activation->color,
            $activation->ram,
            $activation->rom,
            $activation->total_activated_qty
        ];

    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelDetails extends Model
{
    use HasFactory;
    protected $table = 'variant_details';
    public $timestamps = false;
    protected $guarded = array();
}

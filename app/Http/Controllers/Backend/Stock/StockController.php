<?php
namespace App\Http\Controllers\Backend\Stock;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use App\Imports\ImportPurchaseSpare;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Auth;
use Session;
class StockController extends Controller {
    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->data['models'] = $this->NameAndID('models');
            $this->data['spare'] = $this->GetSpareData();
            $this->data['branch'] = $this->getBranches(Session::get('sessi_branch_id'));
            return $next($request);
        });
        View::share('js', ['stock', 'master']);
    }
    public function index(Request $request) {
        $this->data['branch_id'] = ($request->branch_id ? $request->branch_id : 0);
        $this->data['from'] = ($request->from ? $request->from : date('01-m-Y'));
        $this->data['to'] = ($request->to ? $request->to : date('t-m-Y'));
        return view('admin.stock.index', $this->data);
    }
    public function getStockData(Request $request) {
        $data = DB::table('purchase_master')->leftJoin('purchase_items', 'purchase_master.id', '=', 'purchase_items.parent_id')->leftJoin('users', 'users.id', '=', 'purchase_master.created_by')->leftJoin('branch as from', 'from.id', '=', 'purchase_master.branch_id')->select(['purchase_master.id', 'purchase_master.date', 'purchase_master.serial_no', 'from.name as from_branch', 'users.first_name', ]);
        if (!empty($request->branch_id)) {
            $data->where("purchase_master.branch_id", $request->branch_id);
        } else {
            if (Session::get('sessi_branch_id')) {
                $data->whereIn('purchase_master.branch_id', explode(',', Session::get('sessi_branch_id')));
            }
        }
        $from = ($request->from ? date('Y-m-d', strtotime($request->from)) : date('Y-m-01'));
        $to = ($request->to ? date('Y-m-d', strtotime($request->to)) : date('Y-m-t'));
        $data->whereBetween('purchase_master.date', [$from, $to]);
        $data->where('purchase_items.parent_id','>', 0);
        $data->GroupBy('purchase_items.parent_id');
        return DataTables::of($data)->escapeColumns(['from_branch'])->filterColumn('date', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(purchase_master.date,'%d-%m-%Y'))") . ' like ?', ["%{$keyword}%"]);
        })->filterColumn('from_branch', function ($query, $keyword) {
            $query->whereRaw('from.name like ?', ["%{$keyword}%"]);
        })->filterColumn('first_name', function ($query, $keyword) {
            $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
        })->filterColumn('serial_no', function ($query, $keyword) {
            $query->whereRaw('purchase_master.serial_no like ?', ["%{$keyword}%"]);
        })->addColumn('actions', function ($data) {
            $view = '<a  href="' . url('service/purchase', $data->id) . '" class="btn btn-info btn-sm"><i class="feather icon-eye"></i></a>';
            return $view;
        })->editColumn('date', function ($data) {
            return date('d-m-Y', strtotime($data->date));
        })->addIndexColumn()->make(true);
    }
    public function create() {
        return view('admin.stock.create', $this->data);
    }
    public function store(Request $request) {
        $validator = $this->validateStockForm($request, 0);
        if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);
        DB::beginTransaction();
        $serial_no = $this->serialNumber('purchase_master', $request->branch_id);
        $currency = $this->GetCurrencyRate($request->branch_id);
        $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        $id = DB::table('purchase_master')->insertGetId(['branch_id' => $request->branch_id, 'date' => date('Y-m-d', strtotime($request->date)), 'created_by' => Auth::id(), 'serial_no' => $serial_no]);
        $post['parent_id'] = $id;
        $post['branch_id'] = $request->branch_id;
        foreach ($request->addmore as $value) {
            $post['price'] = round(($value['price']/$currency_rate),2);
            $post['local_price'] =  $value['price'];
            $post['spare_id'] = $value['spare_id'];
            $post['qty'] = $value['qty'];
            $this->insertPurchaseSpare($post);
        }
        DB::commit();
        return redirect()->back()->with("success", "Stock added successfully.");
    }
    public function show($id) {
        $this->data['list'] = DB::table("purchase_items as item")
        ->leftJoin("purchase_master as parent", "item.parent_id", "=", "parent.id")
        ->leftjoin("spare", "item.spare_id", "=", "spare.id")
        ->leftjoin("models",DB::raw("FIND_IN_SET(models.id,spare.model_id)"),">",DB::raw("'0'"))
        ->leftjoin("users", "users.id", "=", "parent.created_by")
        ->leftjoin("branch as from", "parent.branch_id", "=", "from.id")
        ->leftJoin('country', 'country.id', '=', 'from.country_id')
        ->leftJoin('currency', 'currency.id', '=', 'country.currency_id')
        ->select([\DB::raw('
                                                spare.spare_name,
                                                spare.part_code,
                                                spare.focus_code,
                                                models.name as model_name,
                                                parent.date,
                                                parent.branch_id,
                                                item.id,
                                                from.name as branch_name,
                                                currency.name as currency,
                                                item.*'), ])->where("item.parent_id", $id)->get();
        return view('admin.stock.view', $this->data);
    }
    public function getStockList(Request $request) {
        $this->data['model_id'] = ($request->model_id ? $request->model_id : 0);
        $this->data['branch_id'] = ($request->branch_id ? $request->branch_id : 0);
        return view('admin.stock.list', $this->data);
    }
    public function getStockListData(Request $request) {
        $data = DB::table('stock')->leftJoin('branch', 'branch.id', '=', 'stock.branch_id')->leftJoin('country', 'country.id', '=', 'branch.country_id')->leftJoin('currency', 'currency.id', '=', 'country.currency_id')->leftJoin('spare', 'spare.id', '=', 'stock.spare_id')->leftJoin('models', 'models.id', '=', 'spare.model_id')->select(['stock.id', 'stock.qty', 'stock.price', 'stock.local_price', 'branch.name as branch_name', 'spare.spare_name', 'models.name as model_name', 'currency.name as currency']);
        if (!empty($request->branch_id)) {
            $data->where("stock.branch_id", $request->branch_id);
        } else {
            if (Session::get('sessi_branch_id')) {
                $data->whereIn('stock.branch_id', explode(',', Session::get('sessi_branch_id')));
            }
        }
        if (!empty($request->model_id)) {
            $data->whereRaw("find_in_set('".$request->model_id."',spare.model_id)");
            //$data->where("spare.model_id", $request->model_id);
        }
        //$data->orderBy('stock.id', 'DESC')->get();
        return DataTables::of($data)->escapeColumns(['branch_name'])->filterColumn('branch_name', function ($query, $keyword) {
            $query->whereRaw('branch.name like ?', ["%{$keyword}%"]);
        })->filterColumn('spare_name', function ($query, $keyword) {
            $query->whereRaw('spare.spare_name like ?', ["%{$keyword}%"]);
        })->filterColumn('model_name', function ($query, $keyword) {
            $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
        })->filterColumn('qty', function ($query, $keyword) {
            $query->whereRaw('stock.qty like ?', ["%{$keyword}%"]);
        })->editColumn('local_price', function ($data) {
            return ($data->currency) . " " . number_format($data->local_price, 2);
        })->addColumn('actions', function ($data) {
            $action = '<a href="' . route('stock.edit', $data->id) . '" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            return $action;
        })->addIndexColumn()->make(true);
        $this->data['data'] = $data;
    }
    public function import() {
        return view('admin.stock.import', $this->data);
    }
    public function importStore(Request $request) {
        $this->validate($request, ['file' => 'required|file|mimes:xls,xlsx,csv|max:10240']);
        if ($request->file('file')->isValid()) {
            $focus_ex = $error = [];
            Excel::import(new ImportPurchaseSpare, request()->file('file'));
            $data = Excel::toCollection(new ImportPurchaseSpare(), $request->file('file'));
            if (count($data) > 0) {
                $record_count = 0;
                DB::beginTransaction();
                $post['branch_id'] = $request->branch_id;
                $serial_no = $this->serialNumber('purchase_master', $post['branch_id']);
                $post['created_by'] = Auth::id();
                $post['serial_no'] = $serial_no;
                $post['date'] = date('Y-m-d', strtotime($request->date));
                $post['parent_id'] = $this->insertGetId('purchase_master', $post);
                $currency = $this->GetCurrencyRate($post['branch_id']);
                $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
                foreach ($data[0] as $key => $value) {
                    $is_exits_focus = $this->isModelRecordExist('spare', 'focus_code', trim($value['focus_code']));
                    if ($is_exits_focus == 0 && $value['focus_code']) {
                        $focus_ex[] = $value['focus_code'];
                    }
                    $post['price'] = round(($value['unit_price']/$currency_rate),2);
                    $post['local_price'] = $value['unit_price'];
                    $post['spare_id'] = ($is_exits_focus??0);
                    $post['qty'] = $value['quantity'];
                    $this->insertPurchaseSpare($post);
                    $record_count = $record_count + 1;
                }
            }
            if (count($focus_ex) > 0) {
                $error[] = "Not exists Focus Code " . implode(',', array_unique($focus_ex));
            }
            if (count($error) > 0) {
                return redirect()->back()->with("info", array_unique($error));
            } else {
                DB::commit();
                return redirect()->back()->with("success", $record_count . " Records Uploaded Successfully!");
            }
        }
    }
    public function edit($id) {
        $this->data['stock'] = DB::table("stock")->leftJoin('spare', 'spare.id', '=', 'stock.spare_id')->leftJoin('branch', 'branch.id', '=', 'stock.branch_id')->leftJoin('country', 'country.id', '=', 'branch.country_id')->leftJoin('currency', 'currency.id', '=', 'country.currency_id')->select('stock.*', 'spare.spare_name', 'branch.name as branch_name', 'currency.name as currency')->where('stock.id', $id)->first();
        $this->data['edit_id'] = $id;
        return view('admin.stock.edit', $this->data);
    }
    public function update(Request $request, $id) {
        $currency = $this->GetCurrencyRate($request->branch_id);
        $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        $data['qty'] = $request->qty;
        $data['local_price'] = $request->price;
        $data['price'] = ($data['local_price'] / $currency_rate);
        $this->updatedata("stock", $data, $id);
        return redirect()->back()->with("success", "Stock updated successfully!");
    }

    public function updateItems(Request $request) {
        $stock_det = $this->GetFirst('stock', array("branch_id" => $request->branch_id, "spare_id" => $request->spare_id));
        $is_stock = $stock_det->qty;
        $currency = $this->GetCurrencyRate($request->branch_id);
        $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        $item_id = $request->item_id;
        $qty = $request->qty;
        $old_qty = $request->oldqty;
        $price = $request->price;
        $new_qty = 0;
        $curr_stock = 0;
        if($qty >= $old_qty) {
          $new_qty = $qty - $old_qty;
          $curr_stock = $is_stock + $new_qty;
        } elseif($old_qty > $qty) {
          $new_qty = $old_qty - $qty ;
          $curr_stock = $is_stock - $new_qty;
        }
        if ($curr_stock>0) {
            $updatedata['qty'] =  $qty;
            if ( $price > 0 ) {
                $updatedata['price'] = round(($price/$currency_rate),2);
                $updatedata['local_price'] = $price;
                if ($this->getLastPurchaseItem($request->spare_id,$request->branch_id) == $item_id) {
                    $data['local_price'] = $updatedata['local_price'];
                    $data['price'] = $updatedata['price'];
                }
            }
            $this->updatedata("purchase_items", $updatedata, $item_id);
            $stock = $this->GetFirst("stock",["branch_id"=>$request->branch_id,"spare_id"=>$request->spare_id]);  
            $data['qty'] = $curr_stock;
            $stock_id = $stock->id;
            
            $this->updatedata("stock", $data, $stock_id);
            Session::flash('success', 'Stock data successfully updated.'); 
            return true;
        } else {
            Session::flash('error', 'Error occurred in stock data'); 
            return false;
        }
      
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use App\Exports\DoaExport;
use App\Exports\ActivationExport;
use App\Exports\ConsumptionExport;
use App\Exports\SpareClaimExport;
use App\Exports\ForecastExport;
use App\Exports\ServiceClaimExport;
use App\Exports\StockServiceClaimExport;
use App\Exports\ServiceComplaintExport;
use App\Exports\ActivationModelExport;
use App\Exports\ActivationMonthModelExport;
use App\Exports\PriceComparisonExport;
use App\Exports\ModelComplaintExport;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DB;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['country'] = $this->getCountries(Session::get('sessi_branch_id'));
            $this->data['branch'] = $this->getBranches(Session::get('sessi_branch_id'));
            $this->data['model'] = $this->NameAndID('models');
            $this->data['company'] = $this->NameAndID('companies');
            $this->data['currency'] = $this->NameAndID('currency');
            return $next($request);
        });
        View::share('js', ['master','report']);
    }

    public function getDoaList(Request $request)
    {
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        return view('admin.report.doa', $this->data);
    }

    public function getDoaListData(Request $request)
    {

        $data = $this->getDoaTableData($request->from, $request->to, $request->model_id );
        return DataTables::of($data)
            ->escapeColumns(['created_at'])
            ->editColumn('created_at', function ($data) {
                return date('d-m-Y', strtotime($data->created_at));
            })->editColumn('status', function ($data) {
                if ($data->issue_doa == 1) {
                    $status ='DOA CERTIFICATE ISSUED';
                } elseif ($data->replace_imei_id > 0 ) {
                    $status = 'REPLACED';
                } else {
                    $status = 'PENDING';
                }
                return $status;
            })->editColumn('month', function ($data) {
                return date("F", mktime(0, 0, 0, $data->month, 10));
            })->editColumn('is_special_request', function ($data) {
                return ($data->is_special_request?"YES":"NO");
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function fileDoaExport(Request $request) {
        $from = $request->from? date('Y-m-d', strtotime($request->from)): "";
        $to = $request->to? date('Y-m-d', strtotime($request->to)): "";
        $model_id = $request->model_id ? $request->model_id : 0;
        return Excel::download(new DoaExport($from , $to, $model_id), 'doa.xlsx');
    }

    public function getActivationList (Request $request)
    {
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['country_id'] = $request->country_id ? $request->country_id : 0;
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        $imei = DB::table('imei_details')->orderBy('last_update_on_activated_date','desc')->first();
        $this->data['last_updated_on'] = date("d-M-Y h:i:s A", strtotime($imei->last_update_on_activated_date));
        return view('admin.report.activation', $this->data);
    }

    public function getActivationListData(Request $request)
    {
        $data = $this->getActivationTableData($request->all());
        return DataTables::of($data)
            ->escapeColumns(['model'])
            ->editColumn('month', function ($data) {
                return date("F", mktime(0, 0, 0, $data->month, 10));
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function fileActivationExport(Request $request) {
        return Excel::download(new ActivationExport($request->from,$request->to,$request->country_id,$request->model_id), 'activation.xlsx');
    }

    public function fileModelActivationExport(Request $request) {
        return Excel::download(new ActivationModelExport($request->from,$request->to,$request->country_id,$request->model_id), 'model_activation.xlsx');
    }

    public function getStockList (Request $request)
    {
        $this->data['country_id'] = $request->country_id ? $request->country_id : 0;
        $this->data['branch_id'] = $request->branch_id ? $request->branch_id : 0;
        $this->data['country_branch_id'] = ($request->country_id?array_flip($this->getBranchIds($request->country_id)):[]);
        return view('admin.report.stock', $this->data);
    }

    public function getStockListData(Request $request) {
        $request['branch_ids'] = Session::get('sessi_branch_id');
        $data = $this->getStockTableData($request->all());
        return DataTables::of($data)
            ->escapeColumns(['spare_name'])
            ->addIndexColumn()
            ->make(true);  
    }

    public function getConsumptionList(Request $request) {
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['country_id'] = $request->country_id ? $request->country_id : 0;
        $this->data['branch_id'] = $request->branch_id ? $request->branch_id : 0;
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        return view('admin.report.consumption', $this->data);   
    }

    public function getConsumptionListData(Request $request) {
        $request['branch_ids'] = Session::get('sessi_branch_id');
        $data = $this->getConsumptionTableData($request->all());
        return DataTables::of($data)
                    ->escapeColumns(['model_name'])
                    ->editColumn('model_name', function ($data) {
                        return ($data->model_name?$data->model_name:"Others");
                    })
                    ->addColumn('consumption_qty', function ($data) {
                        return ($data->warranty_count+$data->nonwarranty_count+$data->sale_count);
                    })
                    ->addColumn('balance_qty', function ($data) {
                        return ($data->purchase_count-($data->warranty_count+$data->nonwarranty_count+$data->sale_count));
                    })
                    ->addIndexColumn()
                    ->make(true);
    }

    public function fileConsumptionExport(Request $request) {
        $request->branch_ids = Session::get('sessi_branch_id');
        return Excel::download(new ConsumptionExport($request->from,$request->to,$request->country_id,$request->branch_id,$request->model_id,$request->branch_ids), 'consumption.xlsx');
        
    }

    public function getSpareCliamList(Request $request) {
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['country_id'] = $request->country_id ? $request->country_id : 0;
        $this->data['branch_id'] = $request->branch_id ? $request->branch_id : 0;
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        $this->data['company_id'] = $request->company_id ? $request->company_id : 0;
        $this->data['currency_id'] = $request->currency_id ? $request->currency_id : 0;
        $this->data['type_id'] = $request->type_id ? $request->type_id : 0;
        return view('admin.report.spare', $this->data);   
    }

    public function getSpareCliamListData(Request $request) {
        $request['branch_ids'] = Session::get('sessi_branch_id');
        $data = $this->getSpareCliamTableData($request->all());
        return DataTables::of($data)
                    ->escapeColumns(['spare_name'])
                    ->editColumn('month', function ($data) {
                        return date("F", mktime(0, 0, 0, $data->month, 10));
                    })
                    ->editColumn('price', function ($data) use ($request) {
                        if (!empty($request->currency_id)) {
                            $currency_convert =  $this->GetByID('currency',$data->currency_id);
                            $curr  = $this->FieldByID('currency','name',$request->currency_id);
                            $currency_convert_d =  $this->GetByID('currency',$request->currency_id);
                            $amt = number_format($data->price/$currency_convert->rate,2);
                            if ($curr == "USD") $price  = $curr." ".$amt;
                            else $price  = $curr." ".number_format($amt*$currency_convert_d->rate,2);
                            return $price;
                        } else {
                            return $data->currency." ".$data->price;
                        }
                    })
                    ->addIndexColumn()
                    ->make(true);
    }

    public function filespareExport(Request $request) {
        $request->branch_ids = Session::get('sessi_branch_id');
        return Excel::download(new SpareClaimExport($request->from,$request->to,$request->country_id,$request->branch_id,$request->branch_ids,$request->company_id,$request->currency_id,$request->type_id), 'spare_cliam.xlsx');
        
    }

    public function getForecastList(Request $request) {
        $this->data['country_id'] = $request->country_id ? $request->country_id : 0;
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        return view('admin.report.forecast', $this->data);    
    }

    public function getForecastListData(Request $request) {
        $data = $this->getForecastTableData($request->all());
        return DataTables::of($data)
                ->escapeColumns(['focus_code'])
                ->addIndexColumn()
                ->make(true);
    }


    public function fileforecastExport (Request $request) {
        return Excel::download(new ForecastExport($request->country_id, $request->model_id), 'forcecast.xlsx'); 
    }

    public function getServiceClaimList (Request $request) {
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['country_id'] = $request->country_id ? $request->country_id : 0;
        $this->data['branch_id'] = ($request->branch_id ? $request->branch_id : 0);
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        $this->data['company_id'] = ($request->company_id ? $request->company_id : 0);
        $this->data['currency_id'] = $request->currency_id ? $request->currency_id : 0;
        return view('admin.report.service', $this->data);
    }


    public function getServiceClaimListData (Request $request) {
        $request['branch_ids'] = Session::get('sessi_branch_id');
        $data = $this->getServiceClaimTableData($request->all());
        return DataTables::of($data)
                ->escapeColumns(['serial_no'])
                ->addIndexColumn()
                ->make(true);
    }

    public function fileServiceClaimExport (Request $request) {
        $request->branch_ids = Session::get('sessi_branch_id');
        return Excel::download(new ServiceClaimExport($request->from,$request->to,$request->country_id, $request->branch_id,$request->model_id,$request->branch_ids,$request->company_id,$request->currency_id), 'serviceclaim.xlsx'); 
    }
    

    public function getServiceCompliantList(Request $request) {
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['country_id'] = $request->country_id ? $request->country_id : 0;
        $this->data['branch_id'] = $request->branch_id ? $request->branch_id : 0;
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        return view('admin.report.complaint', $this->data); 
    }

    public function getServiceCompliantListData (Request $request) {
        $request['branch_ids'] = Session::get('sessi_branch_id');
        $request['is_approved'] = 'all';
        $data = $this->getServiceComplaintTableData($request->all());
        return DataTables::of($data)
                ->escapeColumns(['serial_no'])
                ->addIndexColumn()
                ->make(true);
    }

    public function fileServiceCompliantExport (Request $request) {
        $request->branch_ids = Session::get('sessi_branch_id');
        return Excel::download(new ServiceComplaintExport($request->from,$request->to,$request->country_id, $request->branch_id,$request->model_id,$request->branch_ids), 'complaint.xlsx'); 
    }

    public function fileModelMonthActivationExport (Request $request) {
        return Excel::download(new ActivationMonthModelExport($request->from,$request->to,$request->country_id,$request->model_id), 'month_model_activation.xlsx');
    } 

    public function getComplaintsList(Request $request) {
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        return view('admin.report.model_wise_complaint', $this->data);     
    }

    public function getComplaintsData (Request $request) {
        $data = $this->getModelComplaintTableData($request->all());
        return DataTables::of($data)
                ->escapeColumns(['description'])
                ->addIndexColumn()
                ->make(true);
    }

    public function fileComplaintsExport (Request $request) {
        return Excel::download(new ModelComplaintExport($request->model_id), 'model_complaint_report.xlsx');
    }
    
    public function getPriceList (Request $request) {
        return view('admin.report.price_comparison', $this->data);    
    }

    public function getPriceData(Request $request)
    {
        $request['branch_ids'] = Session::get('sessi_branch_id');
        $data = $this->getPriceComparisonTableData($request->all());
        return DataTables::of($data)
            ->escapeColumns(['spare_name'])
            ->addIndexColumn()
            ->make(true);  
    }

    public function filePriceExport (Request $request) {
        $request->branch_ids = Session::get('sessi_branch_id');
        return Excel::download(new PriceComparisonExport($request->branch_ids), 'price_comparison_report.xlsx'); 
    }

    public function getStockServiceClaimList (Request $request) {
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['country_id'] = $request->country_id ? $request->country_id : 0;
        $this->data['branch_id'] = ($request->branch_id ? $request->branch_id : 0);
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        $this->data['company_id'] = ($request->company_id ? $request->company_id : 0);
        $this->data['currency_id'] = $request->currency_id ? $request->currency_id : 0;
        return view('admin.report.stock_service', $this->data);
    }

    public function getStockServiceListData (Request $request) {
        $request['branch_ids'] = Session::get('sessi_branch_id');
        $data = $this->getStockServiceClaimTableData($request->all());
        return DataTables::of($data) 
                ->escapeColumns(['serial_no'])
                ->addIndexColumn()
                ->make(true); 
    }

    public function fileStockServiceClaimExport (Request $request) {
        $request->branch_ids = Session::get('sessi_branch_id');
        return Excel::download(new StockServiceClaimExport($request->from,$request->to,$request->country_id, $request->branch_id,$request->model_id,$request->branch_ids,$request->company_id,$request->currency_id), 'stockserviceclaim.xlsx'); 
    }

    public function getLevelClaimList (Request $request) {
        $this->data['company_id'] = ($request->company_id ? $request->company_id : 0);
        $this->data['type_id'] = ($request->type_id ? $request->type_id : 1);
        $this->data['year'] = ($request->year ? $request->year : date('Y'));
        $this->data['months'] = $this->data['levels'] = $this->data['cliam'] = [];
        if ($this->data['company_id']) {
            $data = $this->getLevelClaimTableData($request->all()); 
            $new_array = $month_array = $level_array = array();
            foreach($data as $d){
                $new_array[$d->period][$d->level_name] = $d; 
                if(!isset($month_array[$d->period])) {
                    $month_array[$d->period] = $d->month;
                }
                if(!isset($level_array[$d->id])) {
                    $level_array[$d->id] = $d->level_name;
                }
            } 
           //echo "<pre>";print_r($new_array); exit;
            $this->data['months'] = $month_array;
            $this->data['levels'] = $level_array;
            $this->data['cliam'] = $new_array;
        }
        return view('admin.report.level_claim', $this->data);
    }

}

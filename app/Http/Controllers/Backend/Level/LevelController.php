<?php

namespace App\Http\Controllers\Backend\Level;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Level;
use Illuminate\Support\Facades\View;
use Auth;


class LevelController extends Controller
{

    public function __construct()
    {
        $this->data['device'] = $this->NameAndID('product_type');
        View::share('js', ['level', 'master']);
    }

    public function index()
    {
        return view('admin.level.index');
    }

    public function getLevelData()
    {   
        $data = Level::leftJoin('product_type', 'product_type.id', '=', 'levels.device_id')
                        ->select([
                            'levels.id',
                            'levels.name',
                            'levels.status',
                            'levels.rate',
                            'levels.arabic_name',
                            'product_type.name as device_type',
                            'levels.service_type_id'
                        ])->get();

        return DataTables::of($data)
            ->escapeColumns(['name'])
            ->addColumn('actions', function($data) {
                $action = '<a href="'.route('level.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
                return $action;
            })
            ->addIndexColumn()
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="levels" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            }) 
            ->editColumn('service_type_id', function ($data) {
                $status = "No";
                if ($data->service_type_id == 1) {
                    $status = "Yes";
                }
                return $status;
            })
            ->make(true);
    }

    public function create()
    {
        return view('admin.level.create',$this->data);
    }


    public function store(Request $request)
    {

        $post = new Level();

        $validator = $this->validateLevelForm($request, 0);
        
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetLevelForm($post, $request);
        if (Level::where('name', $request['name'])->where('service_type_id',$post->service_type_id)->first()) {
            return redirect()->back()->with("error","That level already exists. Please choose a different name.");
        }
        $post->created_by = Auth::id();

        if ($post->save()){
            return redirect()->back()->with("success","The level was successfully created.");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = $this->validateLevelForm($request, $id);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);
        $post = Level::find($id);
        $post = $this->GetLevelForm($post, $request);
        if (Level::where('name', $request['name'])->where('service_type_id',$post->service_type_id)->where('id', '!=', $id)->first()) {
            return redirect()->back()->with("error","That level already exists. Please choose a different name.");
        }
        if ($post->save()){
            return redirect()->back()->with("success","The level was successfully updated.");
        }

    }

    public function edit($id)
    {
        $this->data['edit_level'] = Level::find($id);
        $this->data['edit_id'] = $id;
        return view('admin.level.create', $this->data);
    }




}

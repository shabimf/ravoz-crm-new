@extends('layouts.service')
@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="container layout full_width">

                <div class="col-sm-12">
                @include('inc.alert')
            <div class="panel panel-primary rp" id="complaint{{ $service->id }}" style="border:none; !important">

                <div class="panel-heading work_panel_head"> Device Information </div>
                <div class="panel-body ">
                    <div class="col-sm-12 rp">
                        <style>
                            .device_table {}

                            .device_table th {
                                color: #337AB7
                            }

                            .device_table th,
                            .device_table td {
                                width: 100px;
                            }

                            .card-task {
                                width: 445px;
                            }

                            .card-title-text {
                                padding: 5px 0px 0px 15px;
                                font-weight: 600;
                            }

                            .status-btn {
                                padding: 4px 4px 4px 4px;
                                font-size: 12px;
                                border-radius: 10px;
                                margin: 0px 15px 3px 13px;
                            }

                            .status-btn:hover {
                                color: #fff;
                            }
                        </style>
                        <br />


                        <a class="edit_work text-primary text-right pull-right cp {{stockServiceButtonHide($service->status)}}" data-id="{{ $service->id }}"
                            data-toggle="modal" data-target="#stocksaleModal">
                            <i class="la la-edit edit"></i>
                        </a>


                        <div class="clearfix"></div>
                        <div class="table-responsive">
                            <table class="table table-condensed device_table  table-bordered table-hover no-margin sf">
                                <tbody>
                                    <tr>
                                        <th> Technician </th>
                                        <td><strong>{{ $service->technician }}</strong></td>
                                        <th> <strong>Remarks</strong> </th>
                                        <td>
                                            {{ $service->remarks }}
                                        </td>
                                        <th> Accessories </th>
                                        <td>
                                            @php
                                                if ($service->accessories_id) {
                                                    $accessories_array = explode(',', $service->accessories_id);
                                                }
                                            @endphp
                                            @if ($service->accessories_id)
                                                @foreach ($access as $k => $v)
                                                    @if (in_array($k, $accessories_array))
                                                        {{ $v }},
                                                    @endif
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Brand </th>
                                        <td>{{ $service->brand }}</td>
                                        <th> Model </th>
                                        <td>{{ $service->model }}
                                            ({{ $service->color }}
                                            @if ($service->ram)
                                                ,{{ $service->ram }}
                                            @endif
                                            @if ($service->rom)
                                                ,{{ $service->rom }}
                                            @endif
                                            )

                                        </td>
                                        <th> Serial Num </th>
                                        <td>{{ $service->sn_no }}</td>

                                    </tr>
                                    <tr>
                                        <th> IMEI NO 1 </th>
                                        <td>{{ $service->IMEI_no1 }}</td>
                                        <th> IMEI NO 2 </th>
                                        <td>{{ $service->IMEI_no2 }}</td>
                                        <th> Branch </th>
                                        <td>{{ $service->branch_name }}</td>

                                    </tr>
                                    <tr>
                                      <th> Software Version </th>
                                      <th colspan="3">
                                        <select name="old_version" class="old_version form-control hide" onchange="return updateVersion(this.value,'stock_service',{{$service->id}});">
                                            <option value="">Select Software Version</option>
                                            @foreach($version as $key => $v)
                                                <option value="{{$key}}" @if($key==$service->new_version_id) selected @endif>{{$v}}</option>
                                            @endforeach
                                        </select>
                                        <div class="version_hide">
                                          {{($service->new_software_name?$service->new_software_name:$service->old_software_name)}}
                                        </div>
                                        <div class="{{stockServiceButtonHide($service->status)}}">
                                            (<a href="javascript:void(0)" id="versionlink">Change Version</a>)
                                        </div>
                                      </th>
                                      <th colspan="2"></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                        <div class="alert alert-danger alert-block d-none" id="finish-err">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <p id="finish-err-msg"></p>
                        </div>
                    <div class="clearfix"></div>
                    <div class="rp">
                        <div class="col-sm-12 rp">
                            <h3>
                                Complaint Details

                                    <small class="btn btn-primary accordion-title pull-right cp new_task_btn {{stockServiceButtonHide($service->status)}}" data-toggle="collapse"
                                        data-target="#collapseOne"><i class="fa fa-plus"></i>
                                        Add New Task
                                    </small>

                            </h3>
                            <hr>

                            <div aria-labelledby="headingOne">
                                <div id="collapseOne" class="collapse card card-task">
                                    <p class="card-title-text">New Task</p>
                                    <div class="card-body">
                                        {{ Form::open(['', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'service-complaint', 'auto-complete' => 'off', 'enctype' => 'multipart/form-data']) }}
                                        <div class="row">
                                            {{ Form::hidden('stock_service_id', $service->id) }}
                                            <div class="form-group col-md-12">
                                                {{ Form::label('name', __('Complaints')) }}
                                                <select class="js-example-basic-multiple select2-hidden-accessible"
                                                    name="complaint[]" multiple="">
                                                    @foreach ($levels as $key => $complaint)
                                                        <optgroup label="{{ $key }}">
                                                            @foreach ($complaint as $k => $v)
                                                                <option value="{{ $k }}">
                                                                    {{ $v }}</option>
                                                            @endforeach
                                                        </optgroup>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-sm btn-primary float-right"
                                            id="complaints_add">Add</button>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                                <div class="card-tasks">
                                    <table class="table table-bordered table-condensed ">
                                        <tbody>
                                            <tr style="background: #E0DCDC !important">
                                                <th width="20"></th>
                                                <th> Task </th>
                                                <th width="100"> Status </th>
                                                <th width="100" class="{{stockServiceButtonHide($service->status)}}"> Action </th>
                                            </tr>
                                            @php
                                                $i = 1;
                                            @endphp
                                            @if (!empty($get_complaints))
                                                @foreach ($get_complaints as $complaint)
                                                    @if ($complaint['status'] == 0)
                                                        @php
                                                            $status = 'Pending';
                                                            $style = 'background-color:#eea236; color:#fff;';
                                                        @endphp
                                                    @elseif ($complaint['status'] == 1)
                                                        @php
                                                            $status = 'Finished';
                                                            $style = 'background-color:#18a863; color:#fff;';
                                                        @endphp
                                                    @else
                                                        @php
                                                            $status = 'Non Repairable';
                                                            $style = 'background-color:#ee3636; color:#fff;';
                                                        @endphp
                                                    @endif
                                                    <tr>
                                                        <td>{{ $i }}.</td>
                                                        <td>{{ $complaint['description'] }} </td>
                                                        <td id="comp_status{{ $complaint['id'] }}" align="right">

                                                            <!-- <button class="btn status-btn"
                                                                style="{{ $style }}">{{ $status }}</button> -->
                                                                @if (stockServiceButtonHide($service->status) == "hide")
                                                                <a class="btn @if ($complaint['status'] == 0) btn-warning @elseif ($complaint['status'] == 2) btn-danger @else btn-success @endif">
                                                                    {{ $status }}
                                                                </a>
                                                                @endif
                                                                <div class="dropdown {{stockServiceButtonHide($service->status)}}">
                                                                    <a class="btn @if ($complaint['status'] == 0) btn-warning @elseif ($complaint['status'] == 2) btn-danger @else btn-success @endif dropdown-toggle " href="#" role="button" id="dropdownMenuLink{{ $complaint['id'] }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    {{ $status }}
                                                                    </a>
                                                                    <div class="dropdown-menu " aria-labelledby="dropdownMenuLink{{ $complaint['id'] }}">
                                                                    @if ($complaint['status'] == 0)
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-id="{{ $complaint['service_comp_id'] }}" data-status="1">Finished</a>
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-id="{{ $complaint['service_comp_id'] }}" data-status="2">Non Repairable</a>
                                                                    @elseif ($complaint['status'] == 1)
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-status="0" data-id="{{ $complaint['service_comp_id'] }}" >Pending</a>
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-status="2" data-id="{{ $complaint['service_comp_id'] }}" >Non Repairable</a>
                                                                    @elseif ($complaint['status'] == 2)
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-status="0" data-id="{{ $complaint['service_comp_id'] }}" >Pending</a>
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-status="1" data-id="{{ $complaint['service_comp_id'] }}" >Finished</a>
                                                                    @endif

                                                                    </div>
                                                                </div>
                                                        </td>
                                                        <!-- <td id="update-status{{ $complaint['id'] }}"
                                                            style="display: none;">
                                                            <select class="status_update"
                                                                data-id="{{ $complaint['service_comp_id'] }}">
                                                                <option @if ($complaint['status'] == 0) selected @endif
                                                                    value=0>Pending</option>
                                                                <option @if ($complaint['status'] == 1) selected @endif
                                                                    value=1>Finished</option>
                                                                <option @if ($complaint['status'] == 2) selected @endif
                                                                    value=2>Non Repairable</option>
                                                            </select>
                                                        </td>
                                                        <td id="comp_status{{ $complaint['id'] }}">
                                                            <button class="btn status-btn"
                                                                style="{{ $style }}">{{ $status }}</button>

                                                        </td> -->
                                                        <td class="{{stockServiceButtonHide($service->status)}}">

                                                        <button class="btn btn-danger complaint-remove" data-id="{{ $complaint['service_comp_id'] }}">Remove</button></td>
                                                    </tr>
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4" align="center">No data available</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="clearfix"></div>

                        <div class="col-sm-12 rp">
                            <h3>
                                Spare Details

                                <small class=" btn btn-success accordion-title pull-right cp new_task_btn {{stockServiceButtonHide($service->status)}}" data-toggle="collapse"
                                    data-target="#collapseTwo"><i class="fa fa-plus"></i>
                                    Add New Spare
                                </small>

                            </h3>
                            <hr>
                            <div id="collapseTwo" class="collapse card">
                                <h4 id="err-msg" class="alert alert-danger d-none text-center"></h4>
                                <p class="card-title-text">New Spare</p>
                                <div class="card-body">
                                    {{ Form::open(['', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'service-spare', 'auto-complete' => 'off', 'enctype' => 'multipart/form-data']) }}
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            {{ Form::label('name', __('Spares')) }}
                                            <select class="js-example-basic-single select2-hidden-accessible"
                                                name="spare" id="spare">
                                                @foreach ($models as $key => $spare)
                                                    <optgroup label="{{ $key }}">
                                                        @foreach ($spare as $k => $v)
                                                            <option value="{{ $k }}">
                                                                {{ $v }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            {{ Form::label('name', __('Price')) }}
                                            <input type="text" name="cost" id="spare_price" disabled
                                                class="form-control">
                                            <label id="spare-price-error" class="error" for="spare_price"></label>
                                        </div>
                                        <div class="form-group col-md-4">
                                                {{ Form::label('name', __('Quantity')) }}
                                                <input type="number" min="1" name="spare_qty" id="spare_qty"
                                                    class="form-control" value="1">
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-sm btn-primary float-right"
                                        id="spare_add">Add</button>
                                    {{ Form::close() }}
                                </div>
                            </div>
                            <form action="" method="post">
                                <table class="table table-bordered table-condensed ">
                                    <tbody>
                                        <tr style="background: #E0DCDC !important">
                                            <th width="20"></th>
                                            <th> Spare </th>
                                            <th> Part Code </th>
                                            <th> Focus Code </th>
                                            <th> Quantity </th>
                                            <th> Price </th>
                                            <th> Action </th>
                                        </tr>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @if (!empty($getSpareDetails))
                                            @foreach ($getSpareDetails as $sparedetails)
                                                <tr>
                                                    <td>{{ $i }}.</td>
                                                    <td> {{ $sparedetails['spare_name'] }} </td>
                                                    <td> {{ $sparedetails['part_code'] }} </td>
                                                    <td> {{ $sparedetails['focus_code'] }} </td>
                                                    <td>{{ $sparedetails['quantity'] }}</td>
                                                    <td> {{ $sparedetails['local_price'] }} </td>
                                                    <td>@if ($service->status != 4 || userHasPermission('Service','stock_service_edit'))<button type="button" class="btn btn-danger spare-remove"
                                                        data-id="{{ $sparedetails['id'] }}">Remove</button></td>@endif
                                                </tr>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8" align="center">No data available</td>
                                            </tr>
                                        @endif
                                    </tbody>

                                </table>
                            </form>
                            <div class="row {{stockServiceButtonHide($service->status)}}">
                                <div class="col-md-8">
                                    <form  action="" method="post" name="pcb_request" id="pcb_request">
                                        <div class="form-row">
                                            @if($service->pcb_serial_no!='')
                                            <div class="col">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="is_pcb" name="is_pcb" value="1" {{($service->new_pcb_serial_no?"checked":"")}}>
                                                    <label class="custom-control-label" for="is_pcb">If change PCB</label>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col pcbcls {{($service->new_pcb_serial_no?"":"hide")}}">
                                                <div class="custom-control">
                                                    <input type="text" class="form-control" id="pcb_serial_no" name="pcb_serial_no" placeholder="PCB Serial No" value={{($service->new_pcb_serial_no?$service->pcb_serial_no:"")}}>
                                                </div>
                                            </div>
                                            <div class="col pcbcls {{($service->new_pcb_serial_no?"":"hide")}}">
                                                <div class="custom-control">
                                                   <button type="submit" class="btn btn-sm btn-primary  pcb_replace">Save</button>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="row rp">
                            <div class="col-sm-12">
                                <div class="group-btn">
                                @if($service->status == 4 && ($service->IMEI_no1!='' || $service->IMEI_no2!=''))
                                <button class="btn btn-lg btn-primary pull-left ml-2"  data-toggle="modal" data-target="#myModalIMEI" >{{isset($service->new_imei_no2)?"Update":"Add"}} New IMEI</button>
                                @endif
                                @if($service->is_approved == 0 && $service->appeal_id == 0 && userHasPermission('Service','appeal_request_approval'))
                                <button class="btn btn-lg btn-primary pull-left ml-2"  data-toggle="modal" data-target="#myModalAppeal" >Appeal Request</button>
                                <button class="btn btn-lg btn-primary pull-left ml-2" type="button" data-toggle="modal" data-target="#myModalLevel" data-id="{{$service->id}}" data-type="1">Approve</button>
                                @elseif($service->appeal_id > 0 && $service->appeal_status == 0)
                                <a target="_blank" href="{{ url('service/stock_service/'.$service->id.'/appeal/get') }}" class="btn btn-lg btn-primary pull-left ml-2">Open Ticket </a>
                                @elseif($service->appeal_id > 0 && $service->appeal_status == 1 && $service->is_approved == 0)
                                <button class="btn btn-lg btn-primary pull-left ml-2" type="button" data-toggle="modal" data-target="#myModalLevel" data-id="{{$service->id}}" data-type="1">Approve</button>
                                <a target="_blank" data-type="0" class="btn btn-lg btn-primary pull-left ml-2 commentclose">Re-Open Ticket </a>
                                @elseif($service->is_approved == 1)
                                <button class="btn btn-lg btn-success pull-left ml-2  {{stockServiceButtonHide($service->status)}}" type="button" style="cursor: text;"> <i class="fa fa-check"></i> Verified</button>
                                @endif
                                <button type="button" class="btn btn-lg btn-primary float-right" id="finish">@if($service->status == 4) Update @else Finish @endif</button>
                            </div>
                            </div>
                            <input type="hidden" id="comment_id" name="comment_id" value="{{$service->appeal_id}}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class="modal fade" id="stocksaleModal" tabindex="-1" role="dialog" aria-labelledby="stocksaleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">STOCK SERVICE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="doa_area">
                    <div class="alert alert-success alert-dismissible doa_msg hide">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        Stock Service <strong>successfully created!</strong>
                    </div>
                    <form action="" method="post" id="doa_form" name="doa_form">
                        <table class="table table-bordered table-condensed doa_table">
                            <tbody>
                                {{-- <tr>
                                    <th class="success"> {{ Form::label('name', __('IMEI')) }}</th>
                                    <td colspan="3">
                                        <div class=" input-group">
                                            {{ Form::text('defective_imei', '', ['class' => 'form-control imei_add', 'id' => 'imei_selected']) }}
                                            {{ Form::hidden('imei_id', '', ['class' => 'form-control', 'id' => 'imei_id']) }}
                                            {{ Form::hidden('doa_id', '', ['class' => 'form-control', 'id' => 'doa_id']) }}
                                            <div class="input-group-append">
                                                {{ Form::button('Check', ['class' => 'btn btn-overlay-dark btns check_replace_imei cp', 'id' => 'search']) }}
                                            </div>
                                        </div>
                                    </td>
                                </tr> --}}
                                <tr>
                                    <th class="success" width="20%">IMEI-1</th>
                                    <td id="imei_no_1" width="30%">{{$service->IMEI_no1}}</td>
                                    <th class="success" width="20%">IMEI-2</th>
                                    <td id="imei_no_2" width="30%">{{$service->IMEI_no2}}</td>
                                </tr>
                                <tr>
                                    <th class="success" width="20%">Model No</th>
                                    <td id="model_name" width="30%">{{$service->model.'('.$service->ram.','.$service->rom.','.$service->color.')'}}</td>
                                    <th class="success" width="20%">Status</th>
                                    <td id="billed_status" width="30%">{{$service->editstatus}}</td>
                                </tr>
                                <tr>
                                    <th class="success">Serial No</th>
                                    <td colspan="3">
                                        {{$service->sn_no}}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="success">Technician</th>
                                    <td colspan="3">
                                        <select name="technician_id" id="technician_id" class="js-example-basic-single">
                                            @foreach($users as $key => $u)
                                                <option value="{{$key}}" @if($key==$service->technician_id) selected @endif>{{$u}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="success">Software Version</th>
                                    <td colspan="3">
                                        <select name="new_version" id="version_id" class="js-example-basic-single">
                                                <option value="">Select Software Version</option>
                                            @foreach($version as $key => $v)
                                                <option value="{{$key}}" @if($key==$service->version_id) selected @endif>{{$v}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                {{-- <tr>
                                    <th class="success">Complaint</th>
                                    <td colspan="3">
                                        <select class="js-example-basic-multiple select2-hidden-accessible" name="complaint[]" multiple="">
                                            @foreach ($levels as $key => $complaint)
                                                <optgroup label="{{ $key }}">
                                                    @foreach ($complaint as $k => $v)
                                                        <option value="{{ $k }}">{{ $v }}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr> --}}
                                <tr>
                                    <th class="success">Remarks</th>
                                    <td colspan="3">
                                        <textarea name="problem_remarks" class="form-control" id="problem_remarks">{{$service->remarks}}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="25%" class="success">Check List</th>
                                    <td colspan="3">
                                        @php
                                            if ($service->accessories_id) {
                                                $accessories_array = explode(',', $service->accessories_id);
                                            }
                                        @endphp
                                        @foreach ($access as $k => $v)
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" id="accessories_{{ $k }}"
                                                    value="{{ $k }}" name="device_check[]"
                                                    @if ($service->accessories_id) @if (in_array($k, $accessories_array)) checked @endif
                                                    @endif
                                                    class="custom-control-input form-control">
                                                <label class="custom-control-label"
                                                    for="accessories_{{ $k }}">{{ $v }}</label>
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <input id="stock_service_id" type="hidden" name="stock_service_id" value="{{ $service->id }}"/>
                        <input type="submit" class="btn btn-success imei_success pull-right" name="issue_doa"
                            value="Update">
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalLevel" tabindex="-1" role="dialog"
        aria-labelledby="myModalLevelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalAppealLabel">Change Level</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" name="level_update" id="level_update">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('Level')) }}
                                {{ Form::select('level_id', $approve_level,old('level_id', $service->level_id), ['class' => 'js-example-basic-single level', 'id' => 'level_id', 'placeholder' => 'Select Level']) }}
                            </div>
                            <div class="col-md-12 form-group">
                                {{ $currency }}
                                {{ Form::label('name', __('Rate')) }}
                                {{ Form::text('rate', '', ['class' => 'form-control', 'id' => 'rate','readonly']) }}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success level_update">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalAppeal" tabindex="-1" role="dialog"
        aria-labelledby="myModalAppealLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalAppealLabel">Appeal Request</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" name="appeal_request" id="appeal_request">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('Remarks')) }}
                                {{ Form::textarea('appeal_remarks', '', ['class' => 'form-control', 'id' => 'appeal_remarks']) }}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success appeal_send">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalIMEI" tabindex="-1" role="dialog"
        aria-labelledby="myModalIMEILabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalIMEILabel">NEW IMEI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" name="imei_request" id="imei_request">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('IMEI 1')) }}
                                {{ Form::text('imei_no_1', old('imei_no_1', isset($service->new_imei_no1) ? $service->IMEI_no1 : ''), ['class' => 'form-control', 'id' => 'imeino_1']) }}
                            </div>
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('IMEI 2')) }}
                                {{ Form::text('imei_no_2', old('imei_no_2', isset($service->new_imei_no2) ? $service->IMEI_no2 : ''), ['class' => 'form-control', 'id' => 'imeino_2']) }}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success imei_replace">Replace</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <input type="hidden" id="service_id" />
    <input type="hidden" id="status" />
    <input type="hidden" id="id" />
@endsection
@section('pagescript')
    <script src="{{ asset('js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('js/select2/bloodhound.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>
    <!-- Bootstrap tagsinput -->
    <script src="{{ asset('js/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>

    <!-- tagsinput custom -->
    <script src="{{ asset('js/bootstrap-tagsinput/tagsinput.js') }}"></script>
    <script>
        $("#is_pcb").click(function() {
            if($('#is_pcb').is(':checked')) {
                $(".pcbcls").removeClass("hide");
            } else {
                $(".pcbcls").addClass("hide");
            }
        });
        $(".pcb_replace").click(function() {
                $("form[name='pcb_request']").validate({
                rules: {
                    pcb_serial_no: {
                        required: true,
                        remote: {
                            url: "{{ route('varifyPCBNo') }}",
                            type: "GET",
                            data: {
                                pcb_serial_no: function () {
                                    return $('#pcb_serial_no').val();
                                }
                            }
                        },
                    }
                },
                messages: {
                    pcb_serial_no:  {
                        required: "Please enter PCB serial no",
                        remote:"Sorry... PCB NO already taken"
                    }
                },
                submitHandler: function(form) {
                    var service_id = $("input[name='stock_service_id']").val();
                    var formData = $('#pcb_request').serialize()+"&service_id="+service_id+"&table=stock_service";
                    $.ajax({
                        type: "POST",
                        url: "{{ route('service.replace.pcbno') }}",
                        data: formData,
                        success: function(data) {
                            window.location.reload();
                        }
                    });
                }
            });
        });
        $(".imei_replace").click(function() {
            $("form[name='imei_request']").validate({
            rules: {
                imei_no_1: {
                    required: true,
                    remote: {
                        url: "{{ route('varifyIMEI') }}",
                        type: "GET",
                        data: {
                            imei_no: function () {
                                return $('#imeino_1').val();
                            }
                        }
                    },
                },
                imei_no_2: {
                    required: true,
                    remote: {
                        url: "{{ route('varifyIMEI') }}",
                        type: "GET",
                        data: {
                            imei_no: function () {
                                return $('#imeino_2').val();
                            }
                        }
                    }
                }
            },
            messages: {
                imei_no_1:  {
                    required: "Please enter IMEI NO1",
                    remote:"Sorry... IMEI NO already taken"
                },
                imei_no_2:  {
                    required: "Please enter IMEI NO2",
                    remote:"Sorry... IMEI NO already taken"
                }
            },
            submitHandler: function(form) {
                var service_id = $("input[name='stock_service_id']").val();
                var formData = $('#imei_request').serialize()+"&service_id="+service_id;
                $.ajax({
                    type: "POST",
                    url: "{{ route('replaceIMEI.update') }}",
                    data: formData,
                    success: function(data) {
                        window.location.reload();
                    }
                });
            }
        });

        });
        $(".level").change(function() {
            $("#rate").val('');
            if($(this).val()>0){
                $.ajax({
                type: "GET",
                url: "/service/level/rate/" +$(this).val()+'/'+ $("input[name='stock_service_id']").val(),
                    success: function(data) {
                        $("#rate").val(data);
                    }
                });
            }
        });
        $(".level_update").click(function() {
            var service_id = $("input[name='stock_service_id']").val();
            var formData = $('#level_update').serialize() +"&service_id="+service_id+"&service_type=1&table=stock_service";
            $("form[name='level_update']").validate({
                rules: {
                    level_id: "required"
                },
                messages: {
                    level_id: "Please select level"
                },
                submitHandler: function(form) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('service.appeal') }}",
                        data: formData,
                        success: function(data) {
                            window.location.reload();
                        }
                    });
                }
            });

        });
        $(".appeal_send").click(function() {
            var service_id = $("input[name='stock_service_id']").val();
            $("form[name='appeal_request']").validate({
                rules: {
                    appeal_remarks: "required"
                },
                messages: {
                    appeal_remarks: "Please enter remarks"
                },
                submitHandler: function(form) {
                    var formData = $('#appeal_request').serialize() + "&service_id="+service_id+"&service_type=2&table=stock_service";
                    $.ajax({
                        type: "POST",
                        url: "{{ route('service.appeal') }}",
                        data: formData,
                        success: function(data) {
                            window.location.reload();
                        }
                    });
                }
            });
        });

        $(".complaint-remove").click(function() {
            var complaint_id = $(this).attr("data-id");
            $.ajax({
                type: "POST",
                url: '{{ route('remove.complaint') }}', // where you wanna post
                data: {
                    'complaint': complaint_id
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });
        $("#finish").click(function() {
            var stock_service_id = $("input[name='stock_service_id']").val();
            $.ajax({
                type: "POST",
                url: '{{ route('doa.status') }}', // where you wanna post
                data: {
                    'stock_service' : stock_service_id
                },
                success: function(data) {
                    if (data == 2) {
                        $("#finish-err-msg").html("Can't finish this work.");
                        $("#finish-err").removeClass('d-none');
                        return false;
                    } else {
                        window.location.reload();
                    }
                }
            });
        });

        $(".spare-remove").click(function() {
            var spare_id = $(this).attr("data-id");
            $.ajax({
                type: "POST",
                url: '{{ route('remove.service.spare') }}', // where you wanna post
                data: {
                    'spare': spare_id
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });

        $(document).ready(function() {
            $('#spare').val(null).trigger("change");
            var spare = $("#spare").val();

            if (spare>0)getSparePrice(spare);

        });

        $("#spare").change(function() {
            var spare = $("#spare").val();
            if (spare>0)getSparePrice(spare);
        });

        function getSparePrice(spare) {

            $.ajax({
                type: "POST",
                url: '{{ route('get.price.spare') }}', // where you wanna post
                data: {
                    'spare': spare,
                    'service_id': $("input[name='stock_service_id']").val(),
                    'table':'stock_service'
                },
                success: function(data) {
                    //alert(data);
                    if (data == 1) {
                        $("#spare_price").val(0.00);
                        $("#spare_add").hide();
                        $("#err-msg").html("Spare quantity not available");
                        $("#err-msg").removeClass('d-none');
                    } else {
                        $("#spare_price").val(data);
                        $("#spare_add").show();
                        $("#err-msg").html('');
                        $("#err-msg").addClass('d-none');
                    }
                }
            });
        }
        $(".edit-status").click(function() {
            var id = $(this).data('id');
            $("#update-status" + id).show();
            $("#comp_status" + id).hide();
        });


        $(".status_update").click(function() {
            var service_id = $("input[name='service_id']").val();
            var service_comp_id = $(this).data('id');
            var status = $(this).data('status');
            $.ajax({
                type: "POST",
                url: '{{ route('service.complaint.status') }}', // where you wanna post
                data: {
                    'ref': service_comp_id,
                    'sta': status,
                    'id': service_id
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });


        $("#spare_add").click(function() {
            var spare_price = $("#spare_price").val();
            var stock_service_id = $("input[name='stock_service_id']").val();
            var qty = $("#spare_qty").val();
            var spare = $("#spare option:selected").val();
            if (spare_price == '') {
                $("#spare-price-error").html("Please enter spare price");
                return false;
            }
            $.ajax({
                type: "POST",
                url: '{{ route('stock.service.spare.add') }}', // where you wanna post
                data: {
                    'stock_service': stock_service_id,
                    'spare': spare,
                    'price': spare_price,
                    'qty': qty
                },
                success: function(data) {
                    if (data == 'NA') {
                        $("#err-msg").html("Spare quantity not available");
                        $("#err-msg").removeClass('d-none');
                        return false;
                    } else {
                        window.location.reload();
                    }
                }
            });
        });

        $("#complaints_add").click(function() {
            var formData = $("#service-complaint").serialize() + '&_token=' + $('meta[name="csrf-token"]').attr(
                'content');
            $.ajax({
                type: "POST",
                url: '{{ route('stock.service.complaint.add') }}', // where you wanna post
                data: formData,
                success: function(data) {
                    window.location.reload();
                }
            });
        });
        $(document).ready(function() {
            $(".edit_cust").click(function(e) {
                $(".cust_val, .cust_edit").toggle();
            });
            $(".edit_dealer").click(function(e) {
                $(".dealer_val, .dealer_edit").toggle();
            });
            $(".edit_salesman").click(function(e) {
                $(".salesman_val, .salesman_edit").toggle();
            });

            $("form[name='update_cus']").validate({
                rules: {
                    customer_name: "required",
                    customer_phone: {
                        required: true,
                        number: true,
                        remote: {
                            url: "{{ route('customer.checkPhone') }}",
                            type: "post",
                            data: {
                                customer_phone: function() {
                                    return $("#customer_phone").val();
                                },
                                customer_id: function() {
                                    return $("input[name='customer_id']").val();
                                }
                            }
                        }
                    },
                    customer_email: {
                        required: true,
                        email: true
                    },
                    dealer_name: "required",
                    dealer_phone: "required",
                },
                messages: {
                    customer_name: "Please enter name",
                    customer_phone: {
                        required: "Please enter phone no",
                        number: "Please valid phone no",
                        remote: "Phone no already in use!"

                    },
                    customer_email: "Please enter a valid email address",
                    dealer_name: "Please enter dealer name",
                    dealer_phone: "Please enter dealer phone no",
                },
                submitHandler: function(form) {
                    var formData = $(form).serialize() + '&_token=' + $('meta[name="csrf-token"]').attr(
                        'content');
                    $.ajax({
                        type: "POST",
                        url: '{{ route('customer.update') }}', // where you wanna post
                        data: formData,
                        error: function(jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage); // Optional
                        },
                        success: function(data) {
                            window.location.reload();
                            console.log(data)
                        }
                    });
                }
            });

        });
        $('.img_click').on('click', function() {
            $('#delete_pic').modal('show');
        });
        $('#delete').on('click', function() {
            $('#imgs').hide();
            $('#imgs-msg').hide();
            $('#msg-delete').show();

        });
        $('.btn-edit').on('click', function() {
            $('.txt_brand_id').attr("disabled", false);
            $('.txt_model_id').attr("disabled", false);
            $('#is_warranty').attr("disabled", false);
        });
        $(".delete-image").on('click', function() {
            var id = $(this).attr("data-id");
            var type = $(this).attr("data-type");
            var formData = 'id=' + id + '&type=' + type + '&_token=' + $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST",
                url: "{{ route('service.image.delete') }}",
                data: formData,
                success: function(data) {
                    $(".delete_" + type + "_" + id).hide();
                }
            });
        });

        $(".transfer_type").on('click', function() {
            var service_id = $(this).attr("data-service-id");
            var id = $(this).attr("data-id");
            var type = $(this).attr("data-type");
            var formData = 'id=' + id + '&type=' + type + '&service_id=' + service_id + '&_token=' + $(
                'meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST",
                url: "{{ route('servicetransfer.type') }}",
                data: formData,
                success: function(data) {
                    window.location.reload();
                }
            });
        });
        var _validator = $("form[name='create_transfer']").validate({
            rules: {
                branch_id: "required",
                transfer_remarks: "required"
            },
            messages: {
                branch_id: "Please select branch",
                transfer_remarks: "Please enter remarks"
            },
            submitHandler: function(form) {
                var formData = $(form).serialize() + '&_token=' + $('meta[name="csrf-token"]').attr('content') +
                    '&service_id=' + $("input[name='service_id']").val();
                $.ajax({
                    type: "POST",
                    url: "{{ route('service.transfer') }}",
                    data: formData,
                    success: function(data) {
                        $(".transfer_msg").slideDown();
                        setTimeout(function() {
                            $('#myModalTransfer').modal('hide')
                        }, 4000);
                        window.location.reload();
                    }
                });
            }
        });

        function changeAction(status, service_id, transfer_id) {
            $("#status").val(status);
            $("#service_id").val(service_id);
            $("#id").val(transfer_id);
            if (status == 1) {
                $("#myModalTechnician").modal();
            } else if (status == 2) {
                $("#myModalReject").modal();
            } else {
                assign();
            }
        }
        $(".assign_btn").click(function() {
            var _validator = $("form[name='assign_tech']").validate({
                rules: {
                    technician_id: "required"
                },
                messages: {
                    technician_id: "Please select technician"
                },
                submitHandler: function(form) {
                    assign();
                }
            });
        });
        $(".reject_btn").click(function() {
            assign();
        });


        function assign() {
            var status = $("#status").val();
            var service_id = $("#service_id").val();
            var id = $("#id").val();
            var formData = $('#assign_tech').serialize() + '&_token=' + $('meta[name="csrf-token"]').attr('content') +
                '&_token=' + $('meta[name="csrf-token"]').attr('content') + '&service_id=' + service_id + '&id=' + id +
                '&status=' + status;
            $.ajax({
                type: "POST",
                url: "{{ route('servicetransfer.status') }}",
                data: formData,
                success: function(data) {
                    if (status == 1) {
                        $(".technician_msg").slideDown();
                        setTimeout(function() {
                            $('#myModalTechnician').modal('hide');
                            window.location.reload();
                        }, 4000);
                    } else {
                        window.location.reload();
                    }
                }
            });
        }
        $(".commentclose").click(function() {
            var type = $(this).attr("data-type");
            var formData =  'type=' + type +'&comment_id='+$("#comment_id").val();
            $.ajax({
                type: "POST",
                url: "/service/update/appel",
                data: formData,
                success: function(data) {
                    window.location.href= "/"+data;
                }
            });
        });
    </script>
@endsection

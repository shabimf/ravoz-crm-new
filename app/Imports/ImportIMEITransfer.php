<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;

class ImportIMEITransfer implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        // echo "<pre>"; print_r($rows); echo "</pre>"; exit;
        Validator::make($rows->toArray(), [
            '*.imeisn' => 'required',
        ])->validate();
    }
}

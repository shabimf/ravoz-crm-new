<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use DB;
use Auth;

class BranchController extends Controller
{
    public function __construct()
    {
        $this->data['company'] = $this->NameAndID('companies');
        $this->data['country'] = $this->NameAndID('country');
        View::share('js', ['master']);
    }

    public function index()
    {
        return view('admin.branch.index');
    }

    public function create()
    {
        return view('admin.branch.create', $this->data);
    }

    public function edit($id)
    {

        $this->data['branch'] = $this->GetByID('branch', $id);
        $this->data['edit_id'] = $id;
        return view('admin.branch.create', $this->data);

    }

    public function store(Request $request)
    {
        $post = array();

        $validator = $this->validateBranchForm($request);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetBranchForm($post, $request);
        $post['created_by'] = Auth::id();
        $this->insertdata('branch', $post);

        return redirect()->back()->with("success","The branch was successfully created.");

    }

    public function update(Request $request, $id)
    {
        $post = array();

        $validator = $this->validateBranchForm($request, $id);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetBranchForm($post, $request, $id);
        $post['updated_by'] = Auth::id();
        $this->updatedata('branch', $post, $id);

        return redirect()->back()->with("success","The branch was successfully updated.");
    }


    public function getBranchList()
    {
        $data=DB::table("branch")
              ->leftJoin('companies', 'branch.company_id', '=', 'companies.id')
              ->leftJoin('country', 'branch.country_id', '=', 'country.id')
              ->select('branch.*', 'companies.name as company', 'country.name as country')
              ->get();
        return DataTables::of($data)
            ->escapeColumns(['name','company'])
            ->addColumn('actions', function($data) {
                return '<a href="'.route('branch.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            })
            ->addIndexColumn()
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="branch" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->make(true);
    }
}

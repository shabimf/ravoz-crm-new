<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Http\Traits\PermissionTrait;
use App\Http\Traits\CommonTrait;
class WarrantyAccess
{
    use PermissionTrait,CommonTrait;

    public function handle($request, Closure $next, $role=null)
    {
        
        // if($role == "view")
        //     return $next($request);
        // if($role == "purchase_create")
        //      dd($role);

        $module_id = $this->GetId("module", "Warranty");
        $permission_result= $this->GetPermission(Auth::user()->role_id, $module_id);
        
        if($permission_result){

            $permission_array = explode(",",$permission_result->permission_ids);
          
            
            if ($permission_result->all == 1) 
                return $next($request);             
             else if(!$role && !empty($permission_array))
                return $next($request);             
             else if($role){

                $permission = explode("|", $role);
    
                
                foreach ($permission as $val) {
                    $permision_id = $this->GetId("permissions", $val);
               
                    if(in_array($permision_id,$permission_array))
                        return $next($request);
                }
                 
             }


        }

        

        echo "Unauthorized"; exit;
    }


}

<!DOCTYPE html>
<html>
  <title>SERVICE BILL</title>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
      td,
      th {
        padding: 5px;
      }

      th {
        color: #000;
      }

      body {
        font-family: 'sans-serif';
      }

      .text-left {
        text-align: left;
      }

      .text-right {
        text-align: right;
      }

      /* .text-center {
            text-align: center;
        } */
      .text-justify {
        text-align: justify;
      }

      .table-bordered {
        border: 1px solid #000;
      }

      table {
        border-spacing: 0;
        border-collapse: collapse;
      }

      table {
        background-color: transparent;
      }

      caption {
        padding-top: 8px;
        padding-bottom: 8px;
        color: #777;
        text-align: left;
      }

      th {
        text-align: left;
      }

      .table {
        width: 100%;
        max-width: 100%;
        /* margin-bottom: 20px; */
        margin-top: 20px;
      }

      .table>thead>tr>th,
      .table>tbody>tr>th,
      .table>tfoot>tr>th,
      .table>thead>tr>td,
      .table>tbody>tr>td,
      .table>tfoot>tr>td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #000;
      }

      .table>thead>tr>th {
        vertical-align: bottom;
        border-bottom: 2px solid #000;
      }

      .table>caption+thead>tr:first-child>th,
      .table>colgroup+thead>tr:first-child>th,
      .table>thead:first-child>tr:first-child>th,
      .table>caption+thead>tr:first-child>td,
      .table>colgroup+thead>tr:first-child>td,
      .table>thead:first-child>tr:first-child>td {
        border-top: 0;
      }

      .table>tbody+tbody {
        border-top: 2px solid #000;
      }

      .table .table {
        background-color: #fff;
      }

      .table-condensed>thead>tr>th,
      .table-condensed>tbody>tr>th,
      .table-condensed>tfoot>tr>th,
      .table-condensed>thead>tr>td,
      .table-condensed>tbody>tr>td,
      .table-condensed>tfoot>tr>td {
        padding: 5px;
      }

      .table-bordered {
        border: 1px solid #000;
      }

      .table-bordered>thead>tr>th,
      .table-bordered>tbody>tr>th,
      .table-bordered>tfoot>tr>th,
      .table-bordered>thead>tr>td,
      .table-bordered>tbody>tr>td,
      .table-bordered>tfoot>tr>td {
        border: 1px solid #000;
      }

      .table-bordered>thead>tr>th,
      .table-bordered>thead>tr>td {
        border-bottom-width: 2px;
      }

      .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #f9f9f9;
      }

      .table-responsive {
        min-height: .01%;
        overflow-x: auto;
      }

      .table-bordered th,
      .table-bordered td {
        border: 1px solid #000 !important;
      }

      .table-hover>tbody>tr:hover {
        background-color: #f5f5f5;
      }

      table col[class*="col-"] {
        position: static;
        display: table-column;
        float: none;
      }

      table td[class*="col-"],
      table th[class*="col-"] {
        position: static;
        display: table-cell;
        float: none;
      }

      .clearfix:before,
      .clearfix:after {
        display: table;
        content: " ";
      }

      .clearfix:after {
        clear: both;
      }
    </style>
  </head> 
@php
// echo "<pre>"; print_r($service_bill); echo "</pre>"; exit;
$country = 1;
$row_bill['branch_id'] = 11;
$country_id=DB::table('branch')->where(['id' => $data->branch_id])->first()->country_id;
$taxpercentage = DB::table('country')->where(['id' => $country_id])->first()->tax;
@endphp
<body>
    <div class="container">
      <div>
        <div style="width:40%; float:left;">
          <img src="{{asset('images/logo_bill.svg')}}" width="100" style="float:left; ">
          <div style="clear:both"></div>
        </div>
        <div style="width:40%; float:left;">
          <h4 class="text-center" style="font-weight:bold; font-size:16px">SERVICE BILL</h4>
        </div>
        <div style="width:20%; float:right; text-align:right;">
          <p style=" padding:0; margin:0;font-size:12px;">
            <span class="text-danger">{{$data->bill_number}}</span>
            <br />
            <small>  {{ date('d/m/Y', str_replace('-','/',strtotime($data->billdate))) }} </small>
          </p>
        </div>
      </div>
      <div class="clearfix"></div>
      <div style="width:100%;float:left">
        <table class="table table-bordered" style="font-size:9px">
          <tr>
            <td colspan="8" align="center"><b  style="font-size:12px">@if($data->type == 2) Dealer @else Customer @endif Information</b></td>
          </tr>
          <tr>
            <th colspan="2">@if($data->type == 2) Dealer @else Customer @endif Name</th>
            <td align="left" colspan="2">{{$data->customer_name}}</td>
            <th colspan="2">@if($data->type == 2) Dealer @else Customer @endif Phone No</th>
            <td align="left" colspan="2">{{$data->customer_phone}}</td>
          </tr>
          <tr>
            <td colspan="8" align="center"><b  style="font-size:12px">Product Information</b></td>
          </tr>
          <tr>
            <th colspan="2">Model</th>
            <td align="left" colspan="2">{{$data->model}}</td>
            <th colspan="2">Color</th>
            <td align="left" colspan="2">{{$data->color}}</td>
          </tr>
          <tr>
            <th colspan="2">Purchase Date</th>
            <td align="left" colspan="2">@if($data->sale_date) {{ date('d/m/Y', strtotime($data->sale_date)) }} @else {{ date('d/m/Y', strtotime($data->active_date)) }} @endif</td>
            <th colspan="2">Receive Date</th>
            <td align="left" colspan="2"> {{ date('d/m/Y', strtotime($data->created_at)) }}</td>
          </tr>
          <tr>
            <th colspan="2">RAM+ROM</th>
            <td align="left" colspan="2">{{$data->ram}}  @if($data->rom) + {{$data->rom}} @endif</td>
            <th colspan="2">IMEI/SN</th>
            <td align="left" colspan="2">{{$data->IMEI_no1}} @if(!$data->IMEI_no1) {{$data->imei_sn_no}} @endif</td>
          </tr>
          <tr>
            
            <th colspan="2">Standard accessories</th>
            <td align="left" colspan="2">@if ($data->accessories_id) {{get_accessories($data->accessories_id)}} @endif</td>
            <th colspan="2">Customer Description Failure</th>
            <td align="left" colspan="2">{{$data->remarks}}</td>
          </tr>
          <tr>
            
            <th colspan="2">Work Description</th>
            <td align="left" colspan="2">{{$data->work_description}}</td>
            <th colspan="2">Work Status</th>
            <td align="left" colspan="2" style="font-size:14px;color:green">Service Completed</td>
          </tr>
          @php
            $spares=json_decode(json_encode($data->spares),1);
            $total = 0;
            $i=1;
          @endphp
          @if(!empty($spares))
          <tr>
            <td colspan="8" align="center"><b  style="font-size:12px">Repair Information</b></td>
          </tr>
          <tr>
            <th colspan="4">Solution</th>
            <td align="left" colspan="4">Material Replacement</td>
          </tr>
          <tr>
            <th >Focus Code</th>
            <th colspan="2">Material Description</th>
            <th >Qty</th>
            <th colspan="2">Unit Price</th>
            <th colspan="2">Total</th>
          </tr>
         
            @foreach($spares as $spare)
                        <tr>
                            <td class="text-left">{{$spare['focus_code']}}</td>
                            <td colspan="2">{{$spare['sparename']}} </td>
                            <td class="text-left">{{$spare['quantity']}}</td>
                            <td class="text-left" colspan="2">{{number_format((float)$spare['cus_local_price'], 2, '.', '')}}</td>
                            <td class="text-left" colspan="2"> {{number_format((float)($spare['cus_local_price']*$spare['quantity']), 2, '.', '')}} </td>
                        </tr>
                    @php
                        $total+=$spare['cus_local_price']*$spare['quantity'];
                        $i++;
                    @endphp
            @endforeach
            @endif
            @php
                $total=($total+$data->service_charge-$data->discount_local_price);
                $tax=$total*($taxpercentage/100);
            @endphp
            <tr>
                <th colspan="6" class="text-right btop  text-primary"> Service Charge</th>
                <td class="text-left  text-primary btop" colspan="2">{{number_format((float)($data->service_charge), 2, '.', '')}}</td>
            </tr>
            <tr>
                <th colspan="6" class="text-right btop  text-primary"> Discount</th>
                <td class="text-left  text-primary btop" colspan="2">{{number_format((float)($data->discount_local_price), 2, '.', '')}}</td>
            </tr>
            <tr>
                <th colspan="6" class="text-right btop  text-primary"> TOTAL</th>
                <td class="text-left  text-primary btop" colspan="2">{{number_format((float)($total), 2, '.', '')}}</td>
            </tr>
            
            <tr>
                <th colspan="6" class="text-right btop  text-danger"> Tax </th>
                <td class="text-left text-danger btop" colspan="2">{{$tax . ' ('.$taxpercentage.' %)'}}</td>
            </tr>
            <tr>
                <th colspan="6" class="text-right btop  text-danger"><strong> Net Total </th>
                <td class="text-left text-danger btop" colspan="2">{{number_format((float)($total+$tax), 2, '.', '')}}</td>
            </tr>
            
        </table>
      </div>
      <table class="table " style="font-size:10px;margin-top:6%;">
        <tr>
          <td width="40%">
            <b>------------------------------------ <br /> Customer Signature </b>
            <br />
          </td>
          <td width="60%" align="right"> <b>------------------------------------ <br /> Service Staff Signature </b></td>
    </table>
      <hr />
       <div style="width:  48% ; float:left;  ">
        <h6 style="font-size:10px">Terms and Conditions</h6>
        <ul style="font-size:8px; padding:0 ">
            <li>The device has warranty against the defects subject to the conditions given below </li>
            <li>The warranty included only the spare parts which has been replaced </li>
            <li>Delivery note is mandatory to claim the warranty</li>
        </ul>
        <br>
        <h6 style="font-size:9px">Warranty is void under the following circumstances:</h6>

        <ul style="font-size:8px; padding:0 ">
            <li>Device was misused, mishandled improperly tested neglected, altered or defected in anyway </li>
            <li>The device became defective resulting from the usage of non-approved accessories that were connected
                to the device</li>
            <li>Device was serviced, repaired or altered by any person other than easycare service center and/or
                technician </li>
            <li>Drop damage or damage caused by water or other liquid </li>
            <li>Deterioration due to natural were & tear, humidity, rest etc.</li>
            <li>Alteration Or tampering of the imei number or serial code on this device </li>
            <li>Standard warranty period -3 months from the date of purchase </li>
        </ul>
      </div>
      <div style="width:48%; float:right;text-align:right;  direction:rtl;">
            <h6 style="font-size:10px">الشروط و الأحكام </h6>
            <ul style="font-size:8px; padding:0 ">
                <li>يحمل هذا الجهاز ضمانا من الاعطـال حسب الشروط المنصوص عليها ادناه </li>
                <li> ويشمل الضمان قطع الغيار التي تم استبدالها </li>
                <li>من الضروري ان يتم استعراض امر التسليم عند المطالبة بحق الضمان </li>
            </ul>
            <br>
            <h6 style="font-size:9px"> ويعتبر الضمان لاغيا في الظروف التالية :</h6>

            <ul style="font-size:8px; padding:0 ">
                <li>إذا تمت إساءة استخدام الجهاز او تم فحصه والكشف عليه او تعديله </li>
                <li>اذا تضرر الجهاز جراء استخدام مستلزمات او اكسسوارات غير معتمدة تم توصيلها بالجهاز</li>
                <li>اذا تمت صيانة الجهاز واصلاحه او تغييره في أي مكان غير مكان الصيانة المعتمد والفنيين المعتمدين</li>
                <li>إذا تضرر بسبب سقوطه او غمره بالمياه او أي سائل آخر </li>
                <li>إذا اهترئ بسبب التقادم الاعتيادي او الرطوبة او الصدأ، الــخ</li>
                <li>تغيير او إزالة رقم التسلسل للجهاز </li>
                <li>يستمر ضمان الجهاز الاعتيادي لمدة 3 اشهر من تاريخ الشراء </li>
            </ul>
        </div>
  </body>
</html>
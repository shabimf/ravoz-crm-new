@extends('layouts.app')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                  <h4 class="card-title-text">{{((isset($edit_complaint)) ? "Edit":'Create')}} Complaint </h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ url('admin/complaint') }}"> <i
                            class="feather icon-list"></i> Complaint List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($edit_complaint, ['route' => ['complaint.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-complaint']) }}
            @else
                {{ Form::open(['route' => 'complaint.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-complaint']) }}
            @endif
            @csrf
            
            <div class="form-row">
                <!-- <div class="form-group col-md-6">
                    {{ Form::label('name', __('Level')) }}
                    {{ Form::select('level_id', $level, old('level_id',(isset($edit_complaint)) ? $edit_complaint->level_id:''), ['class' => 'form-control', 'placeholder' => 'Select Level', 'required' => 'required']) }}
                </div> -->
                <div class="form-group col-md-12">
                    {{ Form::label('name', __('Complaints Details')) }}
                    {{ Form::text('description', old('description',(isset($edit_complaint)) ? $edit_complaint->description:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!-- <div class="form-group col-md-6">
                    {{ Form::label('name', __('Complaints Details Arabic')) }}
                    {{ Form::text('arabic_description', old('arabic_description',(isset($edit_complaint)) ? $edit_complaint->arabic_description:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div> -->
            </div>
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection

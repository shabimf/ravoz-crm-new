@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Purchase Order List</h4>
            </div>
			<div class="col-md-3">
				<div class="add-new">
				  <a class="btn btn-primary" href="{{ route('request.create') }}">
					<i class="feather icon-plus"></i> Purchase Order
				  </a>
				</div>
            </div>

        </div>
        <div class="row">

			<div class="col-lg-12">
			    {{ Form::open(['route' => 'request','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-sale']) }}
					<div class="form-row">
						<div class="form-group col-md-2">
							{{ Form::label('name', __('From Date')) }}
							{{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control', 'required' => 'required','id'=>'datepicker' ]) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('To Date')) }}
							{{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control', 'required' => 'required' , 'id' => 'dates']) }}
						</div>
						<!-- <div class="form-group col-md-2">
							{{ Form::label('name', __('Branch To')) }}
							{{ Form::select('branch_id', $branch, old('branch_id',(isset($branch_id)) ? $branch_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch']) }}
						</div> -->
						<div class="form-group col-md-2">
						  <button class="btn btn-success search-btn">Search</button>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="transfer-table" class="table table-striped table-bordered" data-ajax_url="{{ route("request.get") }}">
					        <thead>
							    <tr>
								    <th width="5">Sl</th>
									<th>Date</th>
									<th>Serial No</th>
									<th>From Branch</th>
									<th>To Branch</th>
									<th>user</th>
									<th>Staus</th>
									<th>Action</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
$(function() {
	$( 'input[name="from"]').daterangepicker( {
        autoUpdateInput: false,
		singleDatePicker: true,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	});
	$( 'input[name="to"]').daterangepicker( {
        autoUpdateInput: false,
		singleDatePicker: true,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	});
});
var branch_id = getUrlParameter('branch_id');
var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
var status =  getUrlParameter('status');
FTX.Utils.documentReady(function() {
    FTX.Transfer.list.init(branch_id,from_date,to_date,status);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

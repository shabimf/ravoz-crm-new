@extends('layouts.app')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                  <h4 class="card-title-text">{{((isset($edit_accessories)) ? "Edit":'Create')}} Accessories </h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ url('admin/accessories') }}"> <i
                            class="feather icon-list"></i> Accessories List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($edit_accessories, ['route' => ['accessories.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-accessories']) }}
            @else
                {{ Form::open(['route' => 'accessories.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-accessories']) }}
            @endif
            @csrf
            
            <div class="form-row">
                <div class="form-group col-md-6">
                    {{ Form::label('name', __('Product Type')) }}
                    {{ Form::select('type_id', $product_type, old('type_id',(isset($edit_accessories)) ? $edit_accessories->product_type:''), ['class' => 'form-control', 'placeholder' => 'Select Type', 'required' => 'required']) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('name', __('Name')) }}
                    {{ Form::text('name', old('name',(isset($edit_accessories)) ? $edit_accessories->name:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <!-- <div class="form-group col-md-6">
                    {{ Form::label('name', __('Complaints Details Arabic')) }}
                    {{ Form::text('arabic_description', old('arabic_description',(isset($edit_complaint)) ? $edit_complaint->arabic_description:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div> -->
            </div>
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection

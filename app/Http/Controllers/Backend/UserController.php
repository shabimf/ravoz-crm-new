<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use Hash;
use Auth;
use DB;
use App\Models\Auth\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->data['roles'] = $this->NameAndID('roles');
        $this->data['country'] = $this->NameAndID('country');
        $this->data['company'] = $this->NameAndID('companies');
        $this->data['branch'] = $this->NameAndID('branch');
        View::share('js', ['master']);
    }
    
    public function index()
    {
        return view('admin.users.index');
    }

    public function create()
    {
        $this->data['country'] = $this->NameAndID('country');
        $this->data['company'] = $this->NameAndID('companies');
        $this->data['branch'] = $this->NameAndID('branch');
        return view('admin.users.create', $this->data);
    }

    public function edit($id)
    {
        $this->data['user'] = $this->GetByID('users', $id);
        $this->data['edit_id'] = $id;
        return view('admin.users.create', $this->data);
    }


    public function store(Request $request)
    {
        // echo "<pre>"; print_r($request->input()); echo "</pre>"; exit;
        $multi_access = $request->input('multi_access');
        $post = array();

        $validator = $this->validateUserForm($request);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);
        $res=array();
        if($multi_access == 2)
        {
            $res['country_id']=$request->input('country_id');
        }
        else if($multi_access == 3)
        {
            $res['company_id']=$request->input('company_id');
        }
        else if($multi_access == 4)
        {
            $res['branch_id']=$request->input('multi_branch_id');
        }
       
        $response=json_encode($res);
        $post = $this->GetUserForm($post, $request);
        $post['username'] = $request->input('username');
        $post['password'] = Hash::make($request->input('password'));
        $post['updated_by'] = Auth::id();
        $post['multiple_access'] = $response;
        // echo "<pre>"; print_r($post); echo "</pre>"; exit;
        $this->insertdata('users', $post);
        return redirect()->back()->with("success","User was successfully created.");
    }

    public function update(Request $request, $id)
    {
        $multi_access = $request->input('multi_access');
        $post = array();

        $validator = $this->validateUserForm($request, $id);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);
           
            $res=array();
            if($multi_access == 2)
            {
                $res['country_id']=$request->input('country_id');
            }
            else if($multi_access == 3)
            {
                $res['company_id']=$request->input('company_id');
            }
            else if($multi_access == 4)
            {
                $res['branch_id']=$request->input('multi_branch_id');
            }
           
        $response=json_encode($res);
        $post = $this->GetUserForm($post, $request, $id);
        $post['updated_by'] = Auth::id();
        $post['multiple_access'] = $response;
        $this->updatedata('users', $post, $id);
        return redirect()->back()->with("success","User was successfully updated.");

    }


    public function getUser()
    {
        $data=DB::table("users")
              ->leftJoin('roles', 'roles.id', '=', 'users.role_id')
              ->leftJoin('branch', 'branch.id', '=', 'users.branch_id')
              ->select('users.*', 'roles.name as role', 'branch.name as branch')
              ->get();
        return DataTables::of($data)
            ->escapeColumns(['first_name','role','branch','email','phoneno'])
            ->addColumn('actions', function($data) {
                return '<div class="btn-group" role="group" aria-label="User Actions">
                <a href="'.route('user.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>
                <a class="btn btn-secondary btn-sm" href="'.route('user.password.change', $data->id).'">
                    <i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Change Password">
                    </i>
                </a>
                </div>';
            })
            ->addColumn('google_status', function($data) {
                if($data->google_auth_status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="g'.$data->id.'" data-id='.$data->id.' data-tb="users" type="checkbox" value="'.$data->google_auth_status.'" class="primary ga_status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="users" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->addColumn('google2fa_secret', function($data) {

                return '<button id="'.$data->id.'" data-id='.$data->id.' data-tb="users" type="checkbox" value="1" class="btn btn-primary ga_rest_status_change">Reset</button>';
            })
            ->addIndexColumn()
            ->make(true);
    }
    public function varifyemail(Request $request)
    {
        $email1 = DB::table("users")->where('email', $request->email);
        if ($request->user_id > 0) {
            $email1->where('id', '!=' ,$request->user_id);
        }
        $query = $email1->get();
        if(count($query) > 0)
        {
            echo json_encode(false);
        } else {
            echo json_encode(true);
        }
    }

    public function varifycontact(Request $request)
    {
        $contact1 = DB::table("users")->where('phoneno', $request->mobile);
        if ($request->user_id > 0) {
            $contact1->where('id', '!=' ,$request->user_id);
        }
        $query = $contact1->get();
        if(count($query)> 0)
        {
            echo json_encode(false);
        } else { 
            echo json_encode(true);
        }
    }

    public function varifyusername(Request $request)
    {
        $username1 = DB::table("users")->where('username', $request->username);
        if ($request->user_id > 0) {
            $username1->where('id', '!=' ,$request->user_id);
        }
        $query = $username1->get();
       // dd(DB::getQueryLog());
        if(count($query)> 0)
        {
            echo json_encode(false);
        } else { 
            echo json_encode(true);
        }
    }

    public function changePassword ($id) {

        $this->data['user_id'] = $id;
        return view('admin.users.changepassword', $this->data);

    }

    public function updatePassword(Request $request, $id) {

        $validator = $this->validatePasswordForm($request);
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);
        User::find($id)->update(['password'=> Hash::make($request->new_password)]);
        return redirect()->back()->with("success","Password change successfully.");
        
    }
}

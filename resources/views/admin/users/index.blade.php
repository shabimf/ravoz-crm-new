@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
                <div class="add-new">
                    <a class="btn btn-primary" href="{{ route('user.create') }}"> <i class="feather icon-plus"></i> Create User</a>
                    <!-- <a class="btn btn-primary" href="{{ route('generate.invoice.pdf') }}"> <i class="feather icon-plus"></i> Download Invoice PDF</a>
                    <a class="btn btn-primary" href="{{ route('generate.service.pdf') }}"> <i class="feather icon-plus"></i> Download Service PDF</a> -->
                </div>
            </div>
        </div>
        <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">User Management</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="users-table" class="table table-striped table-bordered" data-ajax_url="{{ route('user.get') }}">
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <th>Name</th>
									<th>Username</th>
					                <th>Role</th>
                                    <th>Branch</th>
					                <th>Email Id</th>
                                    <th>Mobile</th>
					                <th>Actions</th>
                                    <th>Status</th>
                                    <th>Google Auth Enable</th>
									<th>Google Auth Reset</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('pagescript')
@include('inc.datatables-js')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Users.list.init();
    });
</script>
@endsection

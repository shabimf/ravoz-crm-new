<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class IMEIDetails extends Model
{
    use HasFactory;
    protected $table = "imei_details";  
    public $timestamps = false;
    protected $guarded = array();
}

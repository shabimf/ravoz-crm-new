@extends('layouts.service')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Ticket #{{$comments[0]->serial_no}}</h4>
            </div>
			@if($comments[0]->status == 0)
			<div class="col-md-3">
				<div class="add-new">
				  <button class="btn btn-primary commentclose float-right" type="button" data-type="1" >Ticket Close</button>
				</div>
			</div>
			@endif
        </div>
		@include('inc.alert')
        <div class="row">
			<div class="col-12">
				<div class="card">
				  <div class="card-body">
					<div class="blog blog-single">
						<div class="blog-comments">
						 <form action="{{ route('appeal.add', ['id' => $comments[0]->id]) }}" method="POST" enctype="multipart/form-data">
						 @csrf
						  <div class="form-row">
						    <div class="col-md-12 mb-3">
						      <label>Comment * </label>
							  <input type="hidden" id="comment_id" name="comment_id" value="{{$comments[0]->id}}" />
						      <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="" rows="5" name="comment"></textarea>
						    </div>
                            <div class="col-md-12 mb-3">
						      <label>Attach File </label>
						      <input type="file" name="filenames[]" class="form-control" id="customFile" multiple>
						    </div>
						  </div>
						  @if($comments[0]->status == 0)
						  <button class="btn btn-primary addcomment" type="submit">comment</button>
                          <button class="btn btn-primary" type="reset">Reset</button>
						  @endif
						</form>
						<div class="row">
							<div class="col-xl-12">
								<div class="comments-list">
									<ul class="list-inline"> 
										@foreach ($comments as $k => $val)
											<li class="comments">
												<div class="comment-avtar">
													<div class="avatar avatar-xll">
														<img class="img-fluid" src="{{asset('images/10.png')}}" alt="">
													</div>
												</div>
												<div class="comment-info">
													<h5>{{$val->first_name}}</h5>
													<span class="date">{{date('d F Y, h:i:s A', strtotime($val->created_at))}}</span>
													<p class="mt-2 mb-1">{{$val->msg}}</p>
													@if($val->file)
														<p class="mt-2 mb-1">
															@foreach (json_decode($val->file) as $k => $file_name)
															<a href="{{ asset('uploads/appeal/' . $file_name) }}">
																<span class="badge badge-success"> 
																<i class="zmdi zmdi-attachment-alt"></i> Attachement {{$k+1}} 
														        </span>
														    </a>
															@endforeach
														</p>
													@endif
												</div>
											</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
						</div>
					 </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
<script>
$(".commentclose").click(function() {
	var type = $(this).attr("data-type");
	var formData =  'type=' + type +'&comment_id='+$("#comment_id").val();
	$.ajax({
		type: "POST",
		url: "/service/update/appel",
		data: formData,
		success: function(data) {
			window.location.href= "/"+data;
		}
    });
  });
</script>
@endsection
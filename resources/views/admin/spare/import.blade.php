@extends('layouts.app')

@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                    <h4 class="card-title-text">Spare</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('spare.index') }}"> <i
                            class="feather icon-list"></i> Spare List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($spare, ['route' => ['spare.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-company']) }}
            @else
                {{ Form::open(['route' => 'spare.import_store','class' => 'form-horizontal','role' => 'form','method' => 'post','id' => 'create-spare', 'auto-complete' => 'off', 'enctype'=>"multipart/form-data"]) }}
            @endif
            @csrf
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('Model', __('Model')) }}
                    {{ Form::select('model_id[]', $model, old('currency_id',(isset($spare)) ? $spare->model_id:''), ['class' => 'js-example-basic-multiple select2-hidden-accessible','multiple']) }}
                </div>
                <div class="col form-group">
                   {{ Form::label('name', __('Spare Name')) }}
                    <input type="file" name="file" class="form-control" id="customFile" required>
                    <a class="pull-right" href="{{url('/uploads/format/spare_template_format.xlsx')}}"><small>Download Sample Excel File <i class="fa fa-download"></i></small></a>
                </div>
               
            </div>
           
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/select2/bloodhound.min.js')}}"></script>
@endsection
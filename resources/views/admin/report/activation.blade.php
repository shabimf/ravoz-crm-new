@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Activation</h4>
            </div>
        </div>
        <div class="row  ">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'report.activation','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-doa']) }}
					<div class="form-row well">
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Date From')) }}
							{{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control','id'=>'datepicker' ]) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Date To')) }}
							{{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control', 'id' => 'dates']) }}
						</div>
                        <div class="form-group col-md-2">
                            {{ Form::label('name', __('Country')) }}
							{{ Form::select('country_id', $country, old('country_id',(isset($country_id)) ? $country_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Country','id' => 'country_id']) }}

						</div>
						<div class="form-group col-md-2">
                            {{ Form::label('name', __('Model')) }}
							{{ Form::select('model_id', $model, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model','id' => 'model_id']) }}
						</div>
						<div class="form-group col-md-2">
						  <button class="btn btn-success search-btn">Search</button>
                          <a class="btn btn-success reset-btn" href="{{route('report.activation')}}">Reset</a>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
				    <div class="dt-buttons float-right">
					    <a class="btn btn-primary buttons-excel buttons-html5"  href="{{ url('report/activation/file-model-month-export?from='.$from.'&to='.$to.'&country_id='.$country_id.'&model_id='.$model_id.'') }}"><span>Month Wise Excel</span></a>
					    <a class="btn btn-primary buttons-excel buttons-html5"  href="{{ url('report/activation/file-model-export?from='.$from.'&to='.$to.'&country_id='.$country_id.'&model_id='.$model_id.'') }}"><span>Model Wise Excel</span></a>
						<a class="btn btn-primary buttons-excel buttons-html5"  href="{{ url('report/activation/file-export?from='.$from.'&to='.$to.'&country_id='.$country_id.'&model_id='.$model_id.'') }}"><span>Summary Excel</span></a>
					</div>

					  <div class="table-responsive">
					  <div class="float-right clearfix"><b>Last Updated : </b>{{$last_updated_on}}</div>
						<table id="activation-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("report.activation-get") }}">
							<thead>
								<tr>
									<th width="5">#</th>
									<!-- <th width="80">YEAR</th>
									<th>MONTH</th> -->
									<th>MODEL</th>
									<th>COLOR</th>
									<th>RAM </th>
									<th>ROM</th>
									<th>COUNTRY</th>
									<th>TOTAL DISPATCH QUANTITY</th>
									<th>ACTIVATED QUANTITY</th>
									<th>BALANCE</th>
								</tr>
							</thead>
						</table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>

$(function() {
	$( 'input[name="from"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
	$( 'input[name="to"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
});

var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
var country_id =  getUrlParameter('country_id');
var model_id =  getUrlParameter('model_id');
FTX.Utils.documentReady(function() {
    FTX.ACTIVATION.list.init(from_date,to_date,country_id,model_id);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

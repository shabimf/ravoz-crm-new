<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        td,
        th {
            padding: 5px;
        }

        th {
            color: #6E0C0E;
        }

        body {
            font-family: 'sans-serif';
        }

        .text-left {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        /* .text-center {
            text-align: center;
        } */
        .text-justify {
            text-align: justify;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table {
            background-color: transparent;
        }

        caption {
            padding-top: 8px;
            padding-bottom: 8px;
            color: #777;
            text-align: left;
        }

        th {
            text-align: left;
        }

        .table {
            width: 100%;
            max-width: 100%;
            /* margin-bottom: 20px; */
            margin-top: 20px;
        }

        .table>thead>tr>th,
        .table>tbody>tr>th,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>tbody>tr>td,
        .table>tfoot>tr>td {
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .table>thead>tr>th {
            vertical-align: bottom;
            border-bottom: 2px solid #ddd;
        }

        .table>caption+thead>tr:first-child>th,
        .table>colgroup+thead>tr:first-child>th,
        .table>thead:first-child>tr:first-child>th,
        .table>caption+thead>tr:first-child>td,
        .table>colgroup+thead>tr:first-child>td,
        .table>thead:first-child>tr:first-child>td {
            border-top: 0;
        }

        .table>tbody+tbody {
            border-top: 2px solid #ddd;
        }

        .table .table {
            background-color: #fff;
        }

        .table-condensed>thead>tr>th,
        .table-condensed>tbody>tr>th,
        .table-condensed>tfoot>tr>th,
        .table-condensed>thead>tr>td,
        .table-condensed>tbody>tr>td,
        .table-condensed>tfoot>tr>td {
            padding: 5px;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        .table-bordered>thead>tr>th,
        .table-bordered>tbody>tr>th,
        .table-bordered>tfoot>tr>th,
        .table-bordered>thead>tr>td,
        .table-bordered>tbody>tr>td,
        .table-bordered>tfoot>tr>td {
            border: 1px solid #ddd;
        }

        .table-bordered>thead>tr>th,
        .table-bordered>thead>tr>td {
            border-bottom-width: 2px;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #f9f9f9;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: auto;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #ddd !important;
        }

        .table-hover>tbody>tr:hover {
            background-color: #f5f5f5;
        }

        table col[class*="col-"] {
            position: static;
            display: table-column;
            float: none;
        }

        table td[class*="col-"],
        table th[class*="col-"] {
            position: static;
            display: table-cell;
            float: none;
        }

        .clearfix:before,
        .clearfix:after {
            display: table;
            content: " ";
        }

        .clearfix:after {
            clear: both;
        }

    </style>
</head>


@php
$status = 0;
$country = 1;
$model_id = 0;
$ec = 1;
$rwarranty_provider = 1;
$accessories = '';
@endphp

<body>
    <div class="container">
        <div style="border-bottom:1px solid grey">
            <div style="width:40%; float:left;">
                <img src="{{ asset('images/logo.png') }}" width="100" style="float:left; ">
                <div style="clear:both"></div>
                <p style="font-size:7px;margin-bottom:15px;">ARTC - RASCOTEC - ABUDHABI</p>
            </div>

            <div style="width:40%; float:left;">
                @if ($status)
                    <h4 class="text-center" style="font-weight:bold; font-size:16px">SERVICE RECEIPT</h4>
                @else
                    <h4 class="text-center" style="font-weight:bold; font-size:16px">SERVICE
                        RECEIPT <br> يصال القيام بالخدمه </h4>
                @endif

            </div>

            <div style="width:20%; float:right; text-align:right;">
                <p style=" padding:0; margin:0;font-size:12px;"> <span class="text-danger"
                        style=" color:#a94442;">ARTC22051924</span> <br />
                    <small> {{ date('d/m/Y', strtotime('2022/05/13')) }} </small>
                </p>
            </div>
        </div>

        <div class="clearfix"></div>

        @if ($status)

            <table class="table table-condensed" style="font-size:10px">

                <tr>
                    <th class="text-center">Customer Name </th>
                    <td align="left">ragavan</td>

                    <th>Phone </th>
                    <td align="left">+971502039769</td>
                </tr>


            </table>
        @else
            <table class="table table-condensed" style="font-size:10px">

                <tr>
                    <th>Customer Name @if ($country != 10)
                            اسم العميل
                        @endif
                    </th>
                    <td align="left">ragavan</td>

                    <th>Phone @if ($country != 10)
                            الهاتف
                        @endif
                    </th>
                    <td align="left">+971502039769</td>
                </tr>


            </table>
        @endif

        @if ($model_id != 0)
            @php
                $model_name = '';
            @endphp
        @else
            @php
                $model_name = '';
            @endphp
        @endif
        @if ($ec == 1)
            @php
                $warranty_provider = 'easyCare';
            @endphp
        @elseif($rwarranty_provider == 0)
            @php
                $warranty_provider = 'Non Warranty';
            @endphp
        @else
            @php
                $warranty_provider = 'bbb';
            @endphp
        @endif


        <div style="width:100%;float:left">

            @if ($status)
                <table class="table table-bordered device_table" style="font-size:9px">
                    <tr>
                        <th>Device @if ($country != 10)
                            @endif
                        </th>
                        <td width="70">Mobile</td>
                        <th>Model @if ($country != 10)
                            @endif
                        </th>
                        <td>HUAWEI Y5</td>
                    </tr>
                    <tr>
                        <th>Serial No / IMEI @if ($country != 10)
                            @endif
                        </th>
                        <td>1111111</td>
                        <th>Warranty @if ($country != 10)
                            @endif
                        </th>
                        <td>Non Warranty</td>
                    </tr>
                    <tr>
                        <th>Complaint @if ($country != 10)
                            @endif
                        </th>
                        <td>LOW BATTERY </td>
                        <th>Proposed Charge @if ($country != 10)
                            @endif
                        </th>
                        <td>0.00</td>
                    </tr>
                    <tr>
                        <!-- Added For inventory ------------------------>
                        @php
                            $branchstatus = 0;
                        @endphp
                        @if ($branchstatus)
                            <th>Advance Received</th>
                            <td>Advance</td>
                        @endif
                        <!-- Added For invemtory-------------------------->
                        <th>Accessories @if ($country != 10)
                                الإكسسوارات
                            @endif
                        </th>
                        <td colspan="3">UNIT ONLY</td>
                    </tr>
                    <tr>
                        <th>Device Condition @if ($country != 10)
                                الجهاز الحالة
                            @endif
                        </th>
                        <td>Scraches</td>
                        <th>Remarks @if ($country != 10)
                                ملاحظات
                            @endif
                        </th>
                        <td></td>
                    </tr>
                </table>
            @else
                <table class="table table-bordered device_table" style="font-size:9px">
                    <tr>
                        <th>Device @if ($country != 10)
                                نوع الجهاز
                            @endif
                        </th>
                        <td width="70">Mobile</td>
                        <th>Model @if ($country != 10)
                                الموديل/الطراز
                            @endif
                        </th>
                        <td>HUAWEI Y5</td>
                    </tr>
                    <tr>
                        <th>Serial No / IMEI @if ($country != 10)
                                الرقم المتسلسل
                            @endif
                        </th>
                        <td>111111</td>
                        <th>Warranty @if ($country != 10)
                                الضمان
                            @endif
                        </th>
                        <td>{{ $warranty_provider }}</td>
                    </tr>
                    <tr>
                        <th>Complaint @if ($country != 10)
                                الشكوى/العطل
                            @endif
                        </th>
                        <td>LOW BATTERY -
                            Arabic Name
                        </td>
                        <th>Proposed Charge @if ($country != 10)
                                التكلفه المقترحه
                            @endif
                        </th>
                        <td>0.00</td>
                    </tr>
                    @if ($accessories != '')
                        <tr>
                            <th>Accessories @if ($country != 10)
                                    الإكسسوارات
                                @endif
                            </th>
                            <td colspan="3">UNIT ONLY</td>
                        </tr>
                    @endif
                    <tr>
                        <th>Device Condition @if ($country != 10)
                                الجهاز الحالة
                            @endif
                        </th>
                        <td>Scraches</td>
                        <th>Remarks @if ($country != 10)
                                ملاحظات
                            @endif
                        </th>
                        <td></td>
                    </tr>

                </table>
            @endif

        </div>


        <table class="table " style="font-size:10px;margin-top:6%;">
            <tr>
                <td width="40%">
                    @if ($status)
                        <p><b>------------------------------------<br />Customer Signature
                            </b></p><br />
                    @else
                        <p><b>------------------------------------<br /> Customer Signature
                                @if ($country != 10)
                                    توقيع العميل
                                @endif
                            </b></p><br />
                    @endif
                </td>
                <td width="60%">

                </td>
        </table>


        <p style="font-size:8px;margin-top:20%;">
            Electra Street, Behind Pink Building, Abudhabi, +971 2 6444021, +971 05 64820676
        </p>

        <hr />
        <div style="width: @if ($country == 10) 100% @else 48% @endif; float:left;  ">
            <h6 style="font-size:10px">Terms and Conditions</h6>
            <ul style="font-size:7px; padding:0 ">


                <li> The contents of Job Sheet should be verified and Original copy to be given to customer as an
                    acknowledgement of receiving the device.</li>
                <li> This receipt is to be submitted by the customer at the time of collecting the device. No delivery
                    will
                    be
                    made if this receipt is not produced or is lost. In the event of loss of this receipt, customer
                    shall
                    produce a copy of valid Identity card and a scanned copy of such identity card should be kept in our
                    file or
                    system.</li>
                <li> For any enquiry about status of the device, customer to quote Job Sheet No. and contact number
                    given at
                    the
                    time of call registration in all cases.</li>
                <li> Our estimated charges are only approximate based on initial inspection carried out while receiving
                    the
                    device. If during repairs, additional spares are required, will be charged extra. Fresh approval
                    will be
                    taken for such additional spare parts used.</li>
                <li> Acceptance of device and completion of the job are subject to availability of spare parts. If
                    device
                    cannot
                    be repaired due to non availability of spares, it will be returned to customer on “As is where is”
                    condition.</li>
                <li> Our responsibility is limited to service of this device only. We are not responsible for any
                    consequential
                    damages arising from delay in repairing or non-repairs of the device.</li>
                <li> The device accepted for service is subsequently subject to internal verification. Should we find
                    device
                    to
                    be tampered, water logged, misused, components missing, physically damaged, it will be returned to
                    customer
                    without repairs and the same will be intimated to the customer.</li>
                <li> Force Majeure Clause: While every effort shall be made to take proper care of the customer’s
                    device,
                    the
                    company assumes no responsibilities for loss due to theft, fire, flood, or any other unforeseen
                    circumstances beyond its control.</li>
                <li> Company assumes no responsibility whatsoever if this device is not collected by the customer within
                    30
                    days
                    from the date of such intimation.</li>
                <li> No guarantee is extended for the serviced device beyond 30 days and guarantee is only for service
                    of
                    the
                    device and parts used thereof in repairs.</li>
                <li> Defective parts replaced during service/repairs of the device during warranty will not be returned.
                    However
                    for “out of warranty service/repairs”, defective parts will be returned to the customer on demand.
                </li>
                <li> In case of SWAPPING of the handset or software flashing, phone book, SMS and other third party
                    applications’ memory cannot be warranted or saved.</li>
                <li> Please carefully read Terms and Conditions of Warranty on the warranty card supplied along with the
                    device
                    package.</li>
                <li> All payments for service/repairs should be made in cash only.</li>
                <li> Job Sheet is being an internal document, original issued copy will be retained by EASYCARE while
                    returning
                    handset/accessory and no copy will be given to customer after returning the handset/accessory.</li>
                <li> Water logged/Physically damaged/Tampered sets or accessories will not be covered under warranty at
                    all
                    and
                    NO WARRANTY will be offered on the repairs, if done as per customer’s consent.</li>
                <li> In case if the receiving person of the items as mentioned overleaf are different, a
                    declaration/authorization by the depositor along with valid identification/ID proof of self &
                    receiving
                    person is mandatory to be deposited with EASYCARE. Else the item will not be returned.</li>

                <p style="font-weight:bold">By signing this Job Sheet, it is deemed that the customer has read and
                    agreed to
                    the
                    contents of this Job Sheet (mentioned overleaf) & terms and conditions mentioned above.</p>

            </ul>
        </div>

        @if ($country != 10)
            <div style="width:48%; float:right;text-align:right;  direction:rtl;">
                <h6 style="font-size:10px">الشروط و الأحكام </h6>
                <ul style="font-size:7px;padding:0 ;">

                    <li> يجب التأكد من الاطلاع على جميع محتويات ورقة العمل هذه , كما يجب إعطاء العميل نسخة أصلية منها
                        كإقرار
                        بإستلام
                        الجهاز. </li>
                    <li> يجب تقديم هذا الإيصال من قبل العميل عند إستلام الجهاز . لن يتم تسليم الجهاز فى حالة عدم تقديم
                        هذا
                        الايصال
                        أو فقدانه. في حالة فقدان هذا الإيصال , يجب على العميل تقديم بطاقة الهوية السارية الخاصة به حيث
                        سيتم
                        الاحتفاظ
                        بنسخة ضوئية منها في قاعدة البيانات والملفات الخاصة بنا .</li>
                    <li> فى حالة وجود أي استفسار عن حالة الجهاز ، يجب على العميل تقديم رقم ورقة العمل بالاضافة الى رقم
                        الاتصال
                        المقدم في وقت التسجيل في جميع الحالات.</li>
                    <li> التكلفة المتوقعة يتم تقديريها فقط على أساس الفحص المبدئى للجهاز اثناء عملية الاستلام من قِبلنا
                        .سيتم
                        اضافة
                        رسوم إضافيه فى حالة الحاجة الى تركيب قطع غيار إضافية اثناء عملية الاصلاح بحيث سيتم الحصول على
                        موافقة
                        العميل
                        قبل البدء فى تركيب تلك القطع.</li>
                    <li> تخضع عملية قبول الجهاز والقيام بعملية الاصلاح الى مدى توافر قطع الغيار. فى حالة تعذر القيام
                        بإصلاح
                        الجهاز
                        بسبب عدم توافر قطع الغيار سيتم إعادته إلى العميل مرة اخرى على ذات الحالة التى تم إستلامه عليها.
                    </li>
                    <li> تقتصر المسئولية الخاصة بنا على القيام بإصلاح الجهاز فقط و نحن لا نتحمل أى مسؤولية عن أي أضرار
                        لاحقة
                        قد
                        تنشأ
                        عن التأخير في القيام بالإصلاح أو عدم قابلية الجهاز للإصلاح.</li>
                    <li> يتم اخضاع الجهاز المقبول من قبلنا للفحص الداخلي لاحقاًً. في حالة وجود عبث بالجهاز , كسر , ,
                        وجود
                        مياة ,
                        فقدان أى من مكونات الجهاز , سوء استخدامة أو تعرضة للتلف الكامل ، سيتم إعادتة إلى العميل مرة اخرى
                        بدون
                        الإصلاح وسيتم إبلاغ العميل بذلك.</li>
                    <li> الحوادث /الأحداث القهرية : في الوقت الذي يتم فيه بذل كل الجهد للاعتناء بجهاز العميل ، لا تتحمل
                        الشركة
                        أية
                        مسئولية عن حدوث خسائر بسبب التعرض للسرقة , نشوب حريق , حدوث فيضان أو أية ظروف أخرى غير متوقعة
                        خارجة
                        عن
                        الإرادة والسيطرة. .</li>
                    <li> لا تتحمل الشركة أي مسؤولية من أى نوع فى حالة عدم إستلام العميل للجهاز في غضون 30 يوماًً من
                        تاريخ
                        قيامنا
                        بالتنوية عن موعد الاستلام .</li>
                    <li> لا يتم تمديد فترة الضمان للجهاز بعد القيام بالاصلاح لأكثر من 30 يوم بحيث يقتصر الضمان على قطع
                        الغيار
                        المستخدمة اثناء عملية الإصلاح فقط.</li>
                    <li> الأجزاء المعيبة التى يتم إستبدالها أثناء خدمة / إصلاح الجهاز وذلك اثناء سريان فترة الضمان لن
                        يتم
                        إعادتها
                        للعميل . أما في حالة القيام بالخدمة / الاصلاح بعد إنتهاء فترة الضمان سيتم إعادتها الى العميل فى
                        حالة
                        الطلب.
                    </li>
                    <li> في حالة تبديل سماعة الهاتف أو اعادة تثبيت برنامج التشغيل ، لا يمكن ضمان أو حفظ ذاكرة الهاتف أو
                        الرسائل
                        النصية القصيرة SMS أو أى من التطبيقات الأخرى .</li>
                    <li> يرجى قراءة الشروط والأحكام الخاصة بالضمان بعناية والموضحة ببطاقة الضمان المرفقة بداخل علبة
                        الجهاز.
                    </li>
                    <li> يجب دفع المبالغ المالية المستحقة مقابل القيام بــ "الخدمة / الإصلاح" نقداًََ فقط . </li>
                    <li> تعتبر ورقة العمل هذه وثيقة داخلية خاصة بـ EASYCAR”" لذلك سيتم الاحتفاظ بالنسخة الأصلية منها فى
                        حالة
                        إرجاع
                        السماعة او أى من الملحقات الاخرى بحيث لن يتم منح العميل اى نسخة منها بعد إرجاع السماعة أو أى من
                        الملحقات
                        الاخرى . </li>
                    <li> لا يشمل الضمان الخاص بالجهاز : التعرض للغمر بالمياة / محاولة العبث به / الكسر أو الملحقات ولن
                        يتم
                        تقديم
                        أي
                        ضمان عند القيام بعملية الإصلاح فى حالة القيام بذلك بعد الحصول على موافقة العميل بمحاولة الاصلاح
                        .
                    </li>
                    <li> في حالة اختلاف الشخص المستلم للجهاز عن العميل الأصلى ,يجب تقديم إثبات /تصريح من العميل الأصلى
                        بتسليم
                        الجهاز
                        لذلك الشخص بالإضافة الى تقديم إثبات الهوية سارية العمل لكلاَََ من العميل مودع الجهاز و الشخص
                        الموكل
                        بالاستلام وذلك بشكل إلزامي ليتم الاحتفاظ بصورة منها لدى “EASYCARE” وإلا لن يتم تسليمها لذلك
                        الشخص.
                    </li>


                    <p style="font-weight:bold">بالتوقيع على ورقة العمل هذه تعتبر بمثابة إقرار من العميل بالموافقه على
                        جميع
                        محتوياتها وعلى جميع الشروط والاحكام المذكورة بها.</p>

                </ul>
            </div>
        @endif

</body>

</html>

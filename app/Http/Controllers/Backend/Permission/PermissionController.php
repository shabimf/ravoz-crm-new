<?php

namespace App\Http\Controllers\Backend\Permission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Responses\RedirectResponse;
use Yajra\DataTables\Facades\DataTables;
use Auth;
use DB;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->data['module'] = $this->NameAndID('module');
        View::share('js', ['permissions','master']);
    }

    public function index()
    {
        return view('admin.permission.index');
    }

    public function create()
    {
        return view('admin.permission.create', $this->data);
    }

    public function edit($id)
    {
        $this->data['permission'] = $this->GetByID('permissions', $id);
        $this->data['edit_id'] = $id;
        return view('admin.permission.create', $this->data);
    }


    public function store(Request $request)
    {

        $post = array();

        $validator = $this->validatePermissionForm($request);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetPermissionForm($post, $request);
        $post['updated_by'] = Auth::id();
        $this->insertdata('permissions', $post);
        return redirect()->back()->with("success","The Permission was successfully created.");

    }

    public function update(Request $request, $id)
    {
        $post = array();

        $validator = $this->validatePermissionForm($request);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetPermissionForm($post, $request);
        $post['updated_by'] = Auth::id();
        $this->updatedata('permissions', $post, $id);
        return redirect()->back()->with("success","The permission was successfully updated.");

    }


    public function getPermissionList()
    {
        $data=DB::table("permissions")
              ->leftJoin('module', 'permissions.module_id', '=', 'module.id')
              ->select('permissions.*', 'module.name as module')
              ->get();
        return DataTables::of($data)
            ->escapeColumns(['name','module'])
            ->addIndexColumn()
            ->addColumn('actions', function($data) {
                return '<a href="'.route('permission.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            })
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="permissions" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->make(true);
    }
}

$(".search").click(function() {
    var date_from = $("#date_from").val();
    var date_to = $("#date_to").val();
    workStatus(date_from, date_to);
});

workStatus();

function workStatus(from, to) {

    $.ajax({
        url: "/service/getworkstatus",
        type: "POST",
        data: {
            _token: $('meta[name="csrf-token"]').attr('content'),
            from: from,
            to: to
        },
        cache: false,
        dataType: 'json',
        success: function(dataResult) {
            var from1 = to1 = "";
            let data = JSON.stringify(dataResult);
            var date_from = $("#date_from").val();
            var date_to = $("#date_to").val();
            let serviceObj = JSON.parse(data, (key, value) => {
                if (key == "pending") {
                    $(".pedcls").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".pending a").attr("href", "service/list?status_id=1&from=" + date_from + "&to=" + date_to);
                    }
                }
                if (key == "ready") {
                    $(".redcls").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".ready a").attr("href", "service/list?status_id=4&from=" + date_from + "&to=" + date_to);
                    }
                }
                if (key == "deliver") {
                    $(".delcls").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".delivered a").attr("href", "service/list?status_id=5&from=" + date_from + "&to=" + date_to);
                    }
                }
                if (key == "warranty") {
                    $(".warrcls").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".warranty a").attr("href", "service/list?warranty=1&from=" + date_from + "&to=" + date_to);
                    }
                }
                if (key == "nonwarranty") {
                    $(".nonwarrcls").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".non-warranty a").attr("href", "service/list?warranty=2&from=" + date_from + "&to=" + date_to);
                    }
                }
                if (key == "salesrequest") {
                    $(".salesrequest").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".salesrequest_count a").attr("href", "service/sales/request?status=0&from=" + date_from + "&to=" + date_to);
                    }
                }
                if (key == "appealrequest") {
                    $(".appealrequest").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".appealrequest_count a").attr("href", "service/list?status=5&from=" + date_from + "&to=" + date_to);
                    }
                }
                if (key == "purchaserequest") {
                    $(".purchaserequest").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".purchaserequest_count a").attr("href", "service/request?status=0&from=" + date_from + "&to=" + date_to);
                    }
                }
                if (key == "doarequest") {
                    $(".doarequest").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".doarequest_count a").attr("href", "service/doa?status=1&from=" + date_from + "&to=" + date_to);
                    }
                }
                if (key == "appeal_request") {
                    $(".approverequest").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".approverequest_count a").attr("href", "service/list?status=6&from=" + date_from + "&to=" + date_to);
                    }
                }
                if (key == "stock_service") {
                    $(".stock_service_request").html(value);
                    if (date_from != '' && date_to != '') {
                        $(".stock_service_request_count a").attr("href", "service/stock_service?is_approved=1&from=" + date_from + "&to=" + date_to);
                    }
                }
            });
        }
    });
}
$(".edit-date").click(function() {
    $(".date-show").toggle();
});

$(document).on("keypress", ".scan_imei", function(e) {
    if (e.which == 13) {
        e.preventDefault();
        var query = $(this).val();
        if (query) {
            $(".scan_result_dash").slideDown();
            $.ajax({
                url: "/service/getIMEIData",
                type: "POST",
                data: {
                    query: query,
                    product_type: 0
                },
                cache: false,
                dataType: 'json',
                success: function(dataResult) {
                    console.log(dataResult);
                    var resultData = dataResult.data;
                    var historyData = dataResult.history;
                    var doaHistory = dataResult.doa_history;
                    var html = '';

                    if (resultData.length > 0) {
                        var country_id = $("#country_id").val();
                        $.each(resultData, function(index, row) {
                            var flag = 0;

                            if (row.expiry_date) {
                                if (country_id == row.country_id) {
                                    var expiry_date = row.expiry_date;
                                } else {
                                   

                                    if (row.model_country_id != null) {
                                        var dataArr = row.model_country_id;
                                        var arr = dataArr.split(',');
                                        var flagstatus = 0;
                                        $.each(arr, function(index, value) {
                                            if (country_id == value) {
                                                flagstatus = 1;
                                            }

                                        });
                                        if (flagstatus == 1) {
                                             var international_warranty =  row.international_warranty;
                                             var dateAr = international_warranty.split('-');
                                             var expiry_date = dateAr[2]+'-'+dateAr[1]+'-'+dateAr[0];
                                        } else {
                                            var expiry_date = "--";
                                            flag = 1;
                                        }
                                    } else {
                                        var expiry_date = "--";
                                        flag = 1;
                                    }
                                }
                            } else {
                                var expiry_date = null;
                                flag = 0;
                            }

                            if (expiry_date) {
                                 var dateAr = expiry_date.split('-');
                                 var GivenDate = dateAr[2]+'-'+dateAr[1]+'-'+dateAr[0];
                                 var CurrentDate = new Date();
                                 GivenDate = new Date(GivenDate);
                            }

                            var device = "(" + row.color;
                            if (row.ram_name) device += "," + row.ram_name;
                            if (row.rom_name) device += "," + row.rom_name;
                            device += ")";
                            var clas = 'text-succes';

                            if (Date.parse(GivenDate) < Date.parse(CurrentDate)) {  
                                var txt_warranty = 'WARRANTY EXPIRED';
                                clas = 'text-danger';
                            } else if (expiry_date == "--" && flag == 1) {
                                var clas = 'text-danger';
                                var txt_warranty = 'NON-WARRANTY SERVICE';
                            } else if (expiry_date == null) {
                                clas = 'text-info';
                                var txt_warranty = 'Not registered under warranty required sales request';
                            } else {
                                var clas = 'text-succes';
                                var txt_warranty = 'UNDER WARRANTY';
                            }
                            var imeis = row.IMEI_no1 + "<br/>" + row.IMEI_no2;
                            var htmlhistory = '';
                            var html_doahistory = '';
                            if (historyData.length > 0) {
                                htmlhistory = '<tr><th scope="col" align="center" valign="center" colspan="2"><h4 class="card-title-text text-info">Service History </h4></th></tr>';
                                $.each(historyData, function(index, row) {
                                    htmlhistory += '<tr><td>Serial.Num</td><td><a href="/service/' + row.id + '">' + row.serial_no + '</a></td></tr><tr><td>Service Date</td><td>' + row.created_at + '</td></tr><tr><td>Branch</td><td>' + row.name + '</td></tr>';
                                })
                            }
                            if (doaHistory.length > 0) {
                                html_doahistory = '<tr><th scope="col" align="center" valign="center" colspan="2"><h4 class="card-title-text text-info">DOA History </h4></th></tr>';
                                $.each(doaHistory, function(index, row) {
                                    html_doahistory += '<tr><td>Serial.Num</td><td><a href="/service/' + row.id + '">' + row.serial_no + '</a></td></tr><tr><td>DOA Date</td><td>' + row.created_at + '</td></tr><tr><td>Branch</td><td>' + row.name + '</td></tr>';
                                })
                            }
                            html = '<div class="card"><div class="card-title"><div class="card-title-left"><h4 class="card-title-text ' + clas + '">' + txt_warranty + ' <i class="fa fa-check-circle close_popup" aria-hidden="true"></i></h4></div></div><div class="card-body"><div class="table-responsive"><table class="table table-centered table-hover mb-1"><thead><tr><th scope="col">Expiry </th><th scope="col">' + expiry_date + '</th></tr></thead><tbody><tr><td>IMEI</td><td>' + imeis + '</td></tr><tr><td>Serial.Num</td><td>' + row.serial_no + '</td></tr><tr><td>Device</td><td>' + row.model_name + device + '</td><tr><td>Sold Date</td><td>' + row.sales_date + '</td></tr>' + htmlhistory + html_doahistory + ' </<tbody></table><button class="btn btn-danger close_scan_window btn-outline" type="button" onclick="return closepopup();"> CLOSE <i class="fa fa-times-circle-o" ></i></button>';

                        })
                    } else {
                        html = '<div class="card"><div class="card-body not-warranty"><div class="table-responsive" style="text-align: center;"><i class="fa fa-times-circle close_popup" style="font-size:35px;color:#7c0808" onclick="return closepopup();"></i><h5>IMEI number not registered under RAVOZ Warranty</h5></div></div></div>';
                    }
                    $(".scan_result_dash").html(html);
                }
            });
        }
    }
});

function closepopup() {
    $(".scan_result_dash").slideToggle();
}
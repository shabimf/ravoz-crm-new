<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use DB;
use Auth;

class SoftwareversionController extends Controller
{
    public function __construct()
    {
        $this->data['model'] = $this->NameAndID('models');
        View::share('js', ['master']);
    }

    public function index()
    {
        return view('admin.softwareversion.index');
    }

    public function create()
    {
        // echo "<pre>"; print_r($this->data); echo "</pre>"; exit;
        return view('admin.softwareversion.create', $this->data);
    }

    public function edit($id)
    {
        $this->data['details'] = $this->GetByID('software_version', $id);
        $this->data['edit_id'] = $id;
        return view('admin.softwareversion.create', $this->data);
    }

    public function store(Request $request)
    {
        $post = array();
        $validator = $this->validateSoftwareVersionForm($request);
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);
        $post = $this->GetSoftwareVersionForm($post, $request);
        if($this->GetRowCount('software_version',array("model_id" => $post['model_id'],"name" => $post['name']),'id')==0)
        {
            $post['created_by'] = Auth::id();
            $this->insertdata('software_version', $post);
            return redirect()->back()->with("success","The version was successfully created.");
        }
        else
        {
            return redirect()->back()->with("error","The name has already been taken.");
        }
    }

    public function update(Request $request, $id)
    {
        $post = array();
        $validator = $this->validateSoftwareVersionForm($request, $id);
        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);
        $post = $this->GetSoftwareVersionForm($post, $request, $id);
        $post['updated_by'] = Auth::id();
        $this->updatedata('software_version', $post, $id);
        return redirect()->back()->with("success","The version was successfully updated.");
    }


    public function getSoftwareVersion()
    {
        $data=DB::table("software_version")
              ->leftJoin('models', 'software_version.model_id', '=', 'models.id')
              ->select('software_version.*', 'models.name as model')
              ->get();
        return DataTables::of($data)
            ->escapeColumns(['name','model'])
            ->addColumn('actions', function($data) {
                return '<a href="'.route('softwareversion.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            })
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="software_version" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->addIndexColumn()
            ->make(true);
    }
}

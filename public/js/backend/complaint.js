(function () {

    FTX.Complaints = {

        list: {

            selectors: {
                complaint_table: $('#complaint-table'),
            },

            init: function () {

                this.selectors.complaint_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.complaint_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        // { data: 'level', name: 'level' },
                        { data: 'description', name: 'description' },
                        // { data: 'arabic_description', name: 'arabic_description' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();

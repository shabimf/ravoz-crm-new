<?php

namespace App\Http\Controllers\Backend;

use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use DB;
use Auth;
use Session;

class ServiceTransferController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['model'] = $this->NameAndID('models');
            $this->data['access'] = $this->NameAndID('accessories');
            $this->data['submission'] = $this->NameAndID('submission_category');
            $this->data['brands'] = $this->NameAndID('brands');
            $this->data['product_type'] = $this->NameAndID('product_type');
            $this->data['branch'] = $this->getBranches(Session::get('sessi_branch_id'));
            return $next($request);
        });
       
        View::share('js', ['master','service-transfer']);
    }

    public function index(Request $request) {
        $this->data['users'] = $this->GetUsers(Auth::user()->branch_id);
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['serial_num'] = $request->serial_num? $request->serial_num: "";
        $this->data['from_branch_id'] = $request->from_branch_id? $request->from_branch_id: 0;
        $this->data['to_branch_id'] = $request->to_branch_id? $request->to_branch_id: 0;
        return view('admin.service.transfer', $this->data);
    }

    public function getTransferData(Request $request) {

        $data = DB::table('service_transfer')
                ->leftJoin('service', 'service.id', '=', 'service_transfer.service_id')
                ->leftJoin('branch as from_branch', 'from_branch.id', '=', 'service_transfer.from_branch')
                ->leftJoin('branch as to_branch', 'to_branch.id', '=', 'service_transfer.to_branch')
                ->leftJoin(
                    'service_customers',
                    'service_customers.customer_id',
                    '=',
                    'service.customer_id'
                )
                ->leftJoin('brands', 'brands.id', '=', 'service.brand_id')
                ->leftJoin(
                    'variant_details',
                    'variant_details.id',
                    '=',
                    'service.variant_id'
                )
                ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
                ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
                ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
                ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
                ->select([
                    'service.id',
                    'service_transfer.id as transfer_id',
                    'service_transfer.transfer_date',
                    'from_branch.name as from_branch_name',
                    'to_branch.name as to_branch_name',
                    'service.serial_no',
                    'service_transfer.status',
                    'service_customers.customer_name',
                    'service_customers.customer_phone',
                    'service.is_warranty',
                    'brands.name as brand_name',
                    'models.name as model_name',
                    'color.name as color',
                    'ram.name as ram',
                    'rom.name as rom',
                    'service_transfer.remarks'
                ]);

            if (!empty($request->from_branch_id)) {
                $data->where("service_transfer.from_branch", $request->from_branch_id);
            }
            if (!empty($request->to_branch_id)) {
                $data->where("service_transfer.to_branch", $request->to_branch_id);
            } else {
                if (Session::get('sessi_branch_id')) {
                    $data->whereIn('service_transfer.to_branch', explode(',',Session::get('sessi_branch_id')));
                }
            }
            if (!empty($request->serial_num)) {
                $data->where('service.serial_no', 'LIKE', '%'.trim($request->serial_num).'%');
            }

            $from = $request->from? date('Y-m-d', strtotime($request->from)): "";
            $to = $request->to? date('Y-m-d', strtotime($request->to)): "";

            if (!empty($from) && !empty($to)) {
                $data->whereBetween('service_transfer.transfer_date', [$from, $to]);
            }

            $data->get();

            return DataTables::of($data)
                ->escapeColumns(['transfer_date', 'from_branch_name'])
                ->editColumn('transfer_date', function ($data) {
                    return date('d-m-Y', strtotime($data->transfer_date));
                })
                ->editColumn('is_warranty', function ($data) {
                    return $data->is_warranty == 1 ? "Warranty" : "Non Warranty";
                })
                ->editColumn('model_name', function ($data) {
                    $model = "";
                    if ($data->model_name) {
                        $model = $data->model_name . "(" . $data->color;
                        if ($data->ram) {
                            $model .= "," . $data->ram;
                        }
                        if ($data->rom) {
                            $model .= "," . $data->rom;
                        }
                        $model .= ")";
                    }
                    return $model;
                })
                ->editColumn('serial_no', function ($data) {
                    return '<a href="' .
                        url('service/' . $data->id) .
                        '">' .
                        $data->serial_no .
                        '</a>';
                })->editColumn('actions', function ($data) {
                    if ($data->status == 1 ) {
                        $actions = '<i class="fa fa-circle text-success mr-2"></i> Accepted';
                    } else if ($data->status == 2 ) {
                        $actions = '<i class="fa fa-circle text-danger mr-2"></i> Rejected';
                    } else {
                        $actions = '<i class="fa fa-circle text-warning mr-2"></i> Pending';
                    }
                    return $actions;
                })->filterColumn('serial_no', function ($query, $keyword) {
                    $query->whereRaw('service.serial_no like ?', ["%{$keyword}%"]);
                })->filterColumn('from_branch_name', function ($query, $keyword) {
                    $query->whereRaw('from_branch.name like ?', ["%{$keyword}%"]);
                })->filterColumn('to_branch_name', function ($query, $keyword) {
                    $query->whereRaw('to_branch.name like ?', ["%{$keyword}%"]);
                })->filterColumn('transfer_date', function ($query, $keyword) {
                    $query->whereRaw(DB::raw("(DATE_FORMAT(service_transfer.transfer_date,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
                })->filterColumn('customer_name', function ($query, $keyword) {
                    $query->whereRaw('service_customers.customer_name like ?', ["%{$keyword}%"]);
                })->filterColumn('customer_phone', function ($query, $keyword) {
                    $query->whereRaw('service_customers.customer_phone like ?', ["%{$keyword}%"]);
                })->filterColumn('brand_name', function ($query, $keyword) {
                    $query->whereRaw('brands.name like ?', ["%{$keyword}%"]);
                })->filterColumn('remarks', function ($query, $keyword) {
                    $query->whereRaw('service_transfer.remarks like ?', ["%{$keyword}%"]);
                })->filterColumn('model_name', function ($query, $keyword) {
                    $query->whereRaw('models.name like ?', ["%{$keyword}%"])
                    ->orWhere('color.name', 'LIKE', "%{$keyword}%")
                    ->orWhere('rom.name', 'LIKE', "%{$keyword}%")
                    ->orWhere('rom.name', 'LIKE', "%{$keyword}%");
                })->addIndexColumn()->make(true);
    }

    public function updateTransferStatus(Request $request) {

        $post = $postTransfer = [];
        if ($request->status == 1) {
            $post['technician_id'] = $request->technician_id;
            $this->updatedata('service', $post, $request->service_id);
        } if ($request->status == 3) {
            $this->deletedata("service_transfer","service_id",$request->service_id);
        } else {
            $postTransfer['accepted_user'] = Auth::id();
            $postTransfer['accepted_date'] = date('Y-m-d H:i:s');
            $postTransfer['status'] = $request->status;
            $this->updatedata('service_transfer', $postTransfer, $request->id);
        }
        return $request->id;

    }

    public function updateTransferType(Request $request) {
        $where_total = array("service_id" => $request->service_id);
        $where_pending = array("service_id" => $request->service_id, "status" => '0');
        $total_complaint = $this->GetRowCount('service_complaint', $where_total, 'id');
        $pending_complaint = $this->GetRowCount('service_complaint', $where_pending, 'id');
        if ($total_complaint == $pending_complaint) {
            return 2;
        }
        else
        {
            $post = [];
            if ($request->type == 1) {
                $post['return_user'] = Auth::id();
                $post['return_date'] = date('Y-m-d H:i:s');
                $this->updatedata('service_transfer', $post, $request->id);
            } if ($request->type == 2) {
                $post['received_back_user'] = Auth::id();
                $post['received_back_date'] = date('Y-m-d H:i:s');
                $this->updatedata('service_transfer', $post, $request->id);
            }
            return $request->id;
        }
    }
}

@extends('layouts.service')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                <h4 class="card-title-text">Import Purchase Spare</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('purchase') }}"> <i
                            class="feather icon-list"></i> Spare Purchase List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['route' => 'purchase.import.store','class' => 'form-horizontal','role' => 'form','method' => 'POST','id' => 'create-sales', 'enctype'=>"multipart/form-data"]) }}
            @csrf
            <div class="form-row">
               <div class="form-group col-md-6">
                {{ Form::label('date', __('Purchase Date')) }}
                {{ Form::text('date', old('date'), ['class' => 'form-control', 'required' => 'required']) }}
               </div>
               <div class="form-group col-md-6">
                {{ Form::label('name', __('Branch')) }}
                {{ Form::select('branch_id', $branch, old('branch_id',Auth::user()->branch_id), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch', 'required' => 'required']) }}
               </div>
               <div class="col form-group">
                    {{ Form::label('name', __('File')) }}
                    <input type="file" name="file" class="form-control" id="customFile" required>
                    <a class="pull-right" href="{{url('/uploads/format/purchase_spare_template_format.xlsx')}}"><small>Download Sample Excel File <i class="fa fa-download"></i></small></a>
                </div>
            </div>

            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script>
$(function() {
	$( 'input[name="date"]').daterangepicker( {
		singleDatePicker: true,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	});
});
</script>
@endsection

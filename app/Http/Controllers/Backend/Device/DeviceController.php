<?php
namespace App\Http\Controllers\Backend\Device;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportDevice;
use Yajra\DataTables\Facades\DataTables;
use App\Models\IMEI;
use App\Exports\IMEIExport;
use Illuminate\Support\Facades\View;
use DB;
use Auth;


class DeviceController extends Controller
{

    public function __construct()
    {
        $this->data['country'] = $this->NameAndID('country');
        $this->data['model'] = $this->getModelSpecification();
        View::share('js', ['device','IMEI','master']);
    }

    public function index(Request $request)
    {
        $this->data['country_id'] = $request->country_id ? $request->country_id : 0;
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        return view('admin.device.index', $this->data);
    }

    public function create()
    {
        return view('admin.device.create', $this->data);
    }

    public function import(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); echo "</pre>"; exit;
        $type=$request->input('type');
        $this->validate($request, ['file' => 'required|file|mimes:xls,xlsx,csv|max:10240']);
        if ($request->file('file')
            ->isValid())
        {
            $file = $request->file('file');
            Excel::import(new ImportDevice($request->input('type')), request()
                ->file('file'));
            $data = Excel::toCollection(new ImportDevice( $request->input('type')) , $request->file('file'));
            // echo "<pre>"; print_r($data); echo "</pre>"; exit;
            $imei1_arr = $imei2_arr = $sn_arr = $error = $pcb_arr = [];
            if (count($data) > 0)
            {
                $record_count = 0;
                DB::beginTransaction();
                $path = $file->store('device', ['disk' => 'my_files']);
                $model_id = $this->FieldByID("variant_details","model_id",$request->model_id);
                $group = IMEI::Create(['model_id' => $model_id, 'variant_id' => $request->model_id,'file' => $path, 'created_by' => Auth::id() ]);
                $im1_d = $im2_d = $sn_d = $im1_ex = $im2_ex = $sn_ex  = $pcb_ex = $date_valid = $pcb_d = [];
                if($type==1)
                {
                    foreach ($data[0] as $key => $value)
                    {
                        if (in_array($value['imei_1'], $imei1_arr))
                        {
                            $im1_d[] = $value['imei_1'];
                        }
                        if (in_array($value['imei_2'], $imei2_arr))
                        {
                            $im2_d[] = $value['imei_2'];
                        }
                        if (in_array($value['sn'], $sn_arr))
                        {
                            $sn_d[] = $value['sn'];
                        }
                        if (in_array($value['pcb_serial_no'], $pcb_arr))
                        {
                            $pcb_d[] = $value['pcb_serial_no'];
                        }

                        $is_exits_IMEINO1 = $this->isModelRecordExist('imei_details', 'IMEI_no1', trim($value['imei_1']));
                        if ($is_exits_IMEINO1 > 0 && $value['imei_1'] && !in_array($value['imei_1'], $imei1_arr))
                        {
                            $im1_ex[] = $value['imei_1'];
                        }
                        $is_exits_IMEINO2 = $this->isModelRecordExist('imei_details', 'IMEI_no2', trim($value['imei_2']));
                        if ($is_exits_IMEINO2 > 0 && $value['imei_2'] && !in_array($value['imei_2'], $imei2_arr))
                        {
                            $im2_ex[] = $value['imei_2'];
                        }
                        $is_exits_sno = $this->isModelRecordExist('imei_details', 'serial_no', trim($value['sn']));

                        if ($is_exits_sno > 0 && $value['sn'] && !in_array($value['sn'], $sn_arr))
                        {
                            $sn_ex[] = $value['sn'];
                        }

                        $is_exits_pcb_sno = $this->isModelRecordExist('imei_details', 'pcb_serial_no', trim($value['pcb_serial_no']));
                        if ($is_exits_pcb_sno > 0 && $value['pcb_serial_no'] && !in_array($value['pcb_serial_no'], $pcb_arr))
                        {
                            $pcb_ex[] = $value['pcb_serial_no'];
                        }

                        if ($this->transformDate($value['date_of_manufacture']) == 1 && $file->extension() != "csv")
                        {
                            $date_valid[] = $value['imei_1'];
                        }

                        if ($this->transformDate($value['date_of_manufacture']) != 1) {
                            $date_of_manufacture = $this->transformDate($value['date_of_manufacture']);
                        } else {
                            $date_of_manufacture = '1970-01-01';
                        }


                        $imei1_arr[] = trim($value['imei_1']);
                        $imei2_arr[] = trim($value['imei_2']);
                        $sn_arr[] = trim($value['sn']);
                        $pcb_arr[] = trim($value['pcb_serial_no']);
                        if ($value['imei_1'] && $value['imei_2'] )
                        {

                            DB::table('imei_details')->insertGetId([
                                'group_id' => $group->id,
                                'variant_id' => $request->model_id,
                                'IMEI_no1' => $value['imei_1'],
                                'IMEI_no2' => $value['imei_2'],
                                'serial_no' => $value['sn'],
                                'date_of_manufacture' => $date_of_manufacture,
                                'batch_no' => $value['batch_no'],
                                'pcb_serial_no' => $value['pcb_serial_no']
                            ]);
                            $record_count = $record_count+1;
                        }
                    }
                }
                else
                {
                    foreach ($data[0] as $key => $value)
                    {
                        if (in_array($value['sn'], $sn_arr))
                        {
                            $sn_d[] = $value['sn'];
                        }
                        if (in_array($value['pcb_serial_no'], $pcb_arr))
                        {
                            $pcb_d[] = $value['pcb_serial_no'];
                        }
                        $is_exits_sno = $this->isModelRecordExist('imei_details', 'serial_no', trim($value['sn']));
                        if ($is_exits_sno > 0 && $value['sn'] && !in_array($value['sn'], $sn_arr))
                        {
                            $sn_ex[] = $value['sn'];
                        }
                        $is_exits_pcb_sno = $this->isModelRecordExist('imei_details', 'pcb_serial_no', trim($value['pcb_serial_no']));
                        if ($is_exits_pcb_sno > 0 && $value['pcb_serial_no'] && !in_array($value['pcb_serial_no'], $pcb_arr))
                        {
                            $pcb_ex[] = $value['pcb_serial_no'];
                        }
                        if ($this->transformDate($value['date_of_manufacture']) == 1 && $file->extension() != "csv")
                        {
                            $date_valid[] = $value['imei_1'];
                        }
                        if ($this->transformDate($value['date_of_manufacture']) != 1)
                        {
                            $date_of_manufacture = $this->transformDate($value['date_of_manufacture']);
                        }
                        else
                        {
                            $date_of_manufacture = '1970-01-01';
                        }
                        $sn_arr[] = trim($value['sn']);
                        $pcb_arr[] = trim($value['pcb_serial_no']);
                        if ($value['sn'])
                        {

                            DB::table('imei_details')->insertGetId([
                                'group_id' => $group->id,
                                'variant_id' => $request->model_id,
                                'IMEI_no1' => $value['imei_1'],
                                'IMEI_no2' => $value['imei_2'],
                                'serial_no' => $value['sn'],
                                'date_of_manufacture' => $date_of_manufacture,
                                'batch_no' => $value['batch_no'],
                                'pcb_serial_no' => $value['pcb_serial_no']
                            ]);
                            $record_count = $record_count+1;
                        }
                    }
                }
            }
            if (count($im1_d) > 0)
            {
                $error[] = "Duplication entry of IMEI NO1 " . implode(', ', array_unique($im1_d));
            }
            if (count($im2_d) > 0)
            {
                $error[] = "Duplication entry of IMEI NO2 " . implode(', ', array_unique($im2_d));
            }
            if (count($sn_d) > 0)
            {
                $error[] = "Duplication entry of Serial NO " . implode(', ', array_unique($sn_d));
            }
            if (count($pcb_d) > 0)
            {
                $error[] = "Duplication entry of PCB Serial NO " . implode(', ', array_unique($pcb_d));
            }
            if (count($im1_ex) > 0)
            {
                $error[] = "Already exists IMEI NO1 " . implode(', ', array_unique($im1_ex));
            }
            if (count($im2_ex) > 0)
            {
                $error[] = "Already exists IMEI NO2 " . implode(', ', array_unique($im2_ex));
            }
            if (count($sn_ex) > 0)
            {
                $error[] = "Already exists serial no " . implode(', ', array_unique($sn_ex));
            }
            if (count($pcb_ex) > 0)
            {
                $error[] = "Already exists PCB serial no " . implode(', ', array_unique($pcb_ex));
            }
            if (count($date_valid) > 0)
            {
                $error[] = "Not valid date enter in IMEI NO1 " . implode(', ', array_unique($date_valid));
            }
            if (count($error) > 0)
            {
                return redirect()->back()->with("info", array_unique($error));
            }
            else
            {
                DB::commit();
                return redirect()->back()
                    ->with("success", $request->model_hidden." ".$record_count." Records Uploaded Successfully!");
            }
        }
    }

    public function getDeviceData(Request $request)
    {
        $data = $this->getIMEIDataList($request->all());
        return Datatables::of($data)->removeColumn('id')->editColumn('date_of_manufacture', function ($data)
        {
            return date('d-m-Y', strtotime($data->date_of_manufacture) );
        })
        ->editColumn('country_name', function ($data) {
            return ($data->country_name?$data->country_name:"Freezone");
        })
        ->filterColumn('country_name', function ($query, $keyword) {
            if (str_contains('freezone', strtolower($keyword))) {
              $query->whereNull('imei_details.country_id');
            } else {
              $query->whereRaw('country.name like ?', ["%{$keyword}%"]);
            }
        })
        ->filterColumn('model_name', function ($query, $keyword) {
            $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
        })->filterColumn('color_name', function ($query, $keyword) {
            $query->whereRaw('color.name like ?', ["%{$keyword}%"]);
        })->filterColumn('ram_name', function ($query, $keyword) {
            $query->whereRaw('ram.name like ?', ["%{$keyword}%"]);
        })->filterColumn('rom_name', function ($query, $keyword) {
            $query->whereRaw('rom.name like ?', ["%{$keyword}%"]);
        })->filterColumn('date_of_manufacture', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(imei_details.date_of_manufacture,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
        })
        ->addIndexColumn()->make(true);

    }

    public function getIMEIList(Request $request)
    {
        $this->data['mode_id'] = ($request->model_id?$request->model_id:0);
        $this->data['from'] = ($request->from?$request->from:date('01-m-Y'));
        $this->data['to'] = ($request->to?$request->to:date('t-m-Y'));
        return view('admin.device.imei-list', $this->data);
    }

    public function getIMEIData(Request $request) {

        $data = IMEI::leftJoin('imei_details', 'imei.id', '=', 'imei_details.group_id')
                ->leftJoin('models', 'imei.model_id', '=', 'models.id')
                ->leftJoin('variant_details', 'variant_details.id', '=', 'imei.variant_id')
                ->leftJoin('color', 'variant_details.color_id', '=', 'color.id')
                ->leftJoin('ram', 'variant_details.ram_id', '=', 'ram.id')
                ->leftJoin('rom', 'variant_details.rom_id', '=', 'rom.id')
                ->select(['imei.id','models.name as model_name','imei.created_at','imei.file', DB::raw('count(imei_details.group_id) as IMEICount'),'color.name as color','ram.name as ram','rom.name as rom'])
                ->groupBy('imei_details.group_id');

        if (!empty($request->model_id)) {
            $data->where("imei.variant_id", $request->model_id);
        }

        $from = ($request->from?date('Y-m-d',strtotime($request->from)):date('Y-m-01'));
        $to = ($request->to?date('Y-m-d',strtotime($request->to)):date('Y-m-t'));

        $data->whereBetween('imei.created_at', [$from, $to]);
        //$data->orderBy('imei.id', 'DESC');
        //$data->get();
        return Datatables::of($data)->escapeColumns(['model_name','country_name'])->editColumn('created_at', function ($data)
        {
            return date('d-m-Y H:i:s', strtotime($data->created_at) )."(".$data->IMEICount.")";
        })->addColumn('actions', function ($data) {
            $action = '<a href="' . url('uploads/'.$data->file) . '" data-toggle="tooltip" data-placement="top" title="Download" class="btn btn-primary btn-sm">Download Excel</a>  <a href="' . url('warranty/devices?id='.$data->id) . '"  class="view btn btn-primary btn-sm">View</a>';
            return $action;
        })->filterColumn('model_name', function ($query, $keyword) {
            $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
        })->filterColumn('created_at', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(imei.created_at,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
        })->editColumn('model_name', function ($data) {
            $model = "";
            if ($data->model_name) {
                $model = $data->model_name . "(" . $data->color;
                if ($data->ram) {
                    $model .= "," . $data->ram;
                }
                if ($data->rom) {
                    $model .= "," . $data->rom;
                }
                $model .= ")";
            }
            return $model;
        })->addIndexColumn()->make(true);
    }

    public function getIMEIExport(Request $request) {
      return Excel::download(new IMEIExport($request->country_id,$request->model_id), 'IMEI.xlsx');
    }

}


<div class="main-panel">
  <div class="panel-hedding">
    <div class="row">
      <div class="col-md-6 col-xl-4 col-xxl-4">
        <div class="card">
          <div class="card-body">
            <div class="d-flex align-items-end">
              <div class="mr-2 edit-date">
                <h3 class="mb-2">Today</h3>
                <span>{{ date("jS F Y") }}</span>
              </div>
              <button type="button" class="btn btn-lg btn-overlay-primary btn-icon ml-auto edit-date">
                <i class="feather icon-edit"></i>
              </button>
            </div>
          </div>
          <div class="card-body date-show hide">
            <div class="row">
              <div class="col-md-5">
                <h6 class="mb-2">From</h6>
                <div class="grid-item date">
                  <input type="date" class="form-control" id="date_from" name="date_from">
                </div>
              </div>
              <div class="col-md-5">
                <h6 class="mb-2">To</h6>
                <div class="grid-item date">
                  <input type="date" class="form-control" id="date_to" name="date_to">
                </div>
              </div>
              <div class="col-md-2">
                <button type="button" class="btn btn-lg search btn-overlay-success btn-icon ml-auto">
                  <i class="fa fa-search sicon"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xl-8 col-xxl-8">
        <div class="card warranty">
          <div class="card-body warranty">
            <span class="warranty">CHECK WARRANTY</span>
            <div class="todo-task">
              <div class="search-box mb-2">
                <input class="form-control border-0 search-emei scan_imei" type="text" placeholder="SCAN IMEI/SL NO HERE" aria-label="Search">
                {{ Form::hidden('country_id', old('country_id',(isset($country_id)) ? $country_id:''), ['class' => 'form-control', 'id' => 'country_id']) }}
              </div>
              <div class="list-group scan_result_dash hide" id="imei_details"></div>
            </div>
          </div>
        </div>
        <button type="button" class="btn btn-lg btn-overlay-primary btn-icon ml-auto edit-warranty">
          <i class="la la-get-pocket"></i>
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-xl-4 col-xxl-4">
        <div class="row notification">
          <div class="col padd {{(userHasPermission('Service','sales_request_list')?'':'hide')}}">
            <div class="bg-light p-4 border-radius mb-3 sales">
              <h6>NEW SALES REQUEST</h6>
            </div>
            <div class="project-badge salesrequest_count">
              <a href="{{url('service/sales/request?status=0')}}">
                <span class="badge badge-overlay-danger ml-2 float-right sales salesrequest">0</span>
              </a>
            </div>
          </div>
          <div class="col padd {{(userHasPermission('Service','purchase_order_list')?'':'hide')}}">
            <div class="bg-light p-4 border-radius mb-3 purchase">
              <h6>NEW PURCHASE REQUEST</h6>
            </div>
            <div class="project-badge purchaserequest_count">
              <a href="{{url('service/request?status=0')}}">
                <span class="badge badge-overlay-danger ml-2 float-right purchase purchaserequest">0</span>
              </a>
            </div>
          </div>
          <div class="col padd {{(userHasPermission('Service','appeal_request_approval')?'':'hide')}}">
            <div class="bg-light p-4 border-radius mb-3 appeal">
              <h6>SERVICE APPROVE REQUEST</h6>
            </div>
            <div class="project-badge appealrequest_count">
              <a href="{{url('service/list?status=5')}}">
                <span class="badge badge-overlay-danger ml-2 float-right appeal appealrequest">0</span>
              </a>
            </div>
          </div>
          <div class="col padd {{(userHasPermission('Service','appeal_request_approval')?'':'hide')}}">
            <div class="bg-light p-4 border-radius mb-3 approve">
              <h6>SERVICE APPEAL REQUEST</h6>
            </div>
            <div class="project-badge approverequest_count">
              <a href="{{url('service/list?status=6')}}">
                <span class="badge badge-overlay-danger ml-2 float-right approve approverequest">0</span>
              </a>
            </div>
          </div>
          <div class="col padd {{(userHasPermission('Service','doa_status_change')?'':'hide')}}">
            <div class="bg-light p-4 border-radius mb-3 doa">
              <h6>DOA REQUEST</h6>
            </div>
            <div class="project-badge doarequest_count">
              <a href="{{url('service/doa?status=1')}}">
                <span class="badge badge-overlay-danger ml-2 float-right doa doarequest">0</span>
              </a>
            </div>
          </div>
          <div class="col padd {{(userHasPermission('Service','appeal_request_approval')?'':'hide')}}">
            <div class="bg-light p-4 border-radius mb-3 stock_service">
              <h6>STOCK SERVICE APPROVE REQUEST</h6>
            </div>
            <div class="project-badge stock_service_request_count">
              <a href="{{url('service/stock_service?is_approved=1')}}">
                <span class="badge badge-overlay-danger ml-2 float-right stock_service stock_service_request">0</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xl-8 col-xxl-8">
        <div class="card" style="border: unset !important;background-color: unset !important;box-shadow: none;margin-bottom: unset !important;">
          <div class="card-body" style="padding: unset !important;">
            <div class="row">
              <div class="col">
                <div class="bg-light p-4 border-radius mb-4 pending">
                  <a href="{{url('service/list?status_id=1')}}">
                    <h3 class="count pedcls">0</h3>
                  </a>
                  <hr class="line">
                  <span class="status">PENDINGS</span>
                </div>
              </div>
              <div class="col">
                <div class="bg-light p-4 border-radius mb-4 delivered">
                  <a href="{{url('service/list?status_id=5')}}">
                    <h3 class="count delcls">0</h3>
                  </a>
                  <hr class="line">
                  <span class="status">DELIVERED</span>
                </div>
              </div>
              <div class="col">
                <div class="bg-light p-4 border-radius mb-4 ready">
                  <a href="{{url('service/list?status_id=4')}}">
                    <h3 class="count redcls">0</h3>
                  </a>
                  <hr class="line">
                  <span class="status">READY TO DELIVER</span>
                </div>
              </div>
              <div class="col">
                <div class="bg-light p-4 border-radius mb-4 warranty">
                  <a href="{{url('service/list?warranty=1')}}">
                    <h3 class="count warrcls">0</h3>
                  </a>
                  <hr class="line">
                  <span class="status">WARRANTY</span>
                </div>
              </div>
              <div class="col">
                <div class="bg-light p-4 border-radius mb-4 non-warranty">
                  <a href="{{url('service/list?warranty=2')}}">
                    <h3 class="count nonwarrcls">0</h3>
                  </a>
                  <hr class="line">
                  <span class="status">NON WARRANTY</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @if(!empty($data) && $data->count())
    <div class="row">
      <div class="col-lg-6 col-xxl-4">
        <div class="card">
          <div class="card-title">
            <div class="card-title-left">
              <h4 class="card-title-text">FAQ</h4>
            </div>
          </div>
          <div class="card-body">
            <div class="accordion accordion-box" id="accordionExample">

                        @foreach($data as $key => $value)
                        <div class="card">
                           <div class="card-header" id="headingOne-{{ $value->id }}">
                              <div class="accordion-title @if($key != 0) collapsed @endif" data-toggle="collapse" data-target="#collapseOne-{{ $value->id }}">
                                 <h5 class="mb-0"> <i class="zmdi zmdi-help-outline text-primary mr-3"></i> {{ $value->question }} </h5>
                              </div>
                           </div>
                           <div id="collapseOne-{{ $value->id }}" class="collapse @if($key == 0) show @endif" aria-labelledby="headingOne-{{ $value->id }}" data-parent="#accordionExample">
                              <div class="card-body">
                                 <p>{{ $value->answer }}</p>
                              </div>
                           </div>
                        </div>
                        @endforeach


                    {{ $data->appends(['search' => Request::get('search') ])->links() }}
              </div>
              <a href="{{url('service/faq')}}" class="btn btn-primary active float-right"> View more</a>

            </div>
          </div>
        </div>
      </div>
    </div>
    @endif
  </div>

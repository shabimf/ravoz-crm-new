<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;
use DB;
use Auth;

class CurrencyController extends Controller
{
    public function __construct()
    {
        View::share('js', ['master']);
    }

    public function index()
    {
        return view('admin.currency.index');
    }

    public function create()
    {
        return view('admin.currency.create');
    }

    public function edit($id)
    {
        $this->data['details'] = $this->GetByID('currency', $id);
        $this->data['edit_id'] = $id;
        return view('admin.currency.create', $this->data);

    }

    public function store(Request $request)
    {
        $post = array();

        $validator = $this->validateCurrencyForm($request);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetCurrencyForm($post, $request);
        $post['created_by'] = Auth::id();
        $post_data['currency_id'] = $this->insertGetId('currency', $post);
        $post_data['rate'] = $request->rate;
        $this->insertdata('currency_history', $post_data);

        return redirect()->back()->with("success","The currency was successfully created.");

    }

    public function update(Request $request, $id)
    {
        $post=array();

        $validator = $this->validateCurrencyForm($request, $id);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator);

        $post = $this->GetCurrencyForm($post, $request, $id);
        $post['updated_by'] = Auth::id();
        $this->updatedata('currency', $post, $id);
        $post_data['currency_id'] = $id;
        $post_data['rate'] = $request->rate;
        $this->insertdata('currency_history', $post_data);

        return redirect()->back()->with("success","The currency was successfully updated.");
    }


    public function getCurrency()
    {
        $data=DB::table("currency")
              ->select('*')
              ->get();
        return DataTables::of($data)
            ->escapeColumns(['name','rate'])
            ->addColumn('actions', function($data) {
                return '<a href="'.route('currency.edit', $data->id).'" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="feather icon-edit"></i></a>';
            })
            ->addColumn('status', function($data) {
                if($data->status==1)
                {
                    $checked="checked";
                }
                else
                {
                    $checked="";
                }
                return '<label class="switch "><input id="'.$data->id.'" data-id='.$data->id.' data-tb="currency" type="checkbox" value="'.$data->status.'" class="primary status_change" '.$checked.' ><span class="slider round"></span></label>';
            })
            ->addIndexColumn()
            ->make(true);
    }

}

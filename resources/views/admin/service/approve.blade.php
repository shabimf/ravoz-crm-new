@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Approve Request List</h4>
            </div>
			<div class="col-md-3">
				
            </div>

        </div>
        <div class="row  ">
			<div class="col-lg-12">
			    
				<div class="card">
				  <div class="card-body">
					  <div class="table-responsive">
				  	    <table id="approve-request-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("approve.request.get") }}">
					        <thead>
					            <tr>
								    <th width="5">#</th>
					                <th width="80">Reg.Date</th>
									<th>Service.Num</th>
									<th>Branch</th>
					                <th>Customer</th>
									<th>Phone.Num</th>
									<th>Warranty</th>
									<th>Brand</th>
									<th>Model</th>
									<th>Work Status</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
FTX.Utils.documentReady(function() {
    FTX.APPROVE.list.init();
});
</script>
@endsection

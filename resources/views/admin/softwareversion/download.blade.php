@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Software List</h4>
            </div>
			<div class="col-md-3">
				
            </div>

        </div>
        <div class="row">
			<div class="col-lg-12">
                {{ Form::open(['route' => 'software.index','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-imei']) }}
					<div class="form-row well">
						<div class="form-group col-md-6">
							{{ Form::label('name', __('Model')) }}
							{{ Form::select('model_id', $model, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model']) }}
						</div>
						<div class="form-group col-md-6">
						   <button class="btn btn-primary search-btn">Search</button>
                           <a class="btn btn-primary reset-btn"  href="{{url('service/software')}}">Reset</a>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				    <div class="card-body">
					    <div class="table-responsive">
                            <table id="software-download-table" class="table table-striped table-bordered" data-ajax_url="{{ route('software.get') }}">
                                <thead>
                                    <tr>
                                        <th width="5">Sl</th>
                                        <th>Model</th>
                                        <th>Version</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
					    </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
    var model_id = getUrlParameter('model_id');
    FTX.Utils.documentReady(function() {
        FTX.Software.list.init(model_id);
    });
</script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

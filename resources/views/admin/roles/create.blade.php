@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-6">
              <h4 class="card-title-text">{{((isset($role)) ? "Edit":'Create')}} Role </h4>
            </div>
            <div class="col-md-6">
                <div class="add-new">
                    <a class="btn btn-primary" href="{{ route('role') }}"> <i class="feather icon-list"></i> Role
                        List</a>
                </div>
            </div>
        </div>
        @include('inc.alert')

        @if (isset($edit_id))
            {{ Form::model($role, ['route' => ['role.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-role']) }}
        @else
            {{ Form::open(['route' => 'role.store','class' => 'form-horizontal','role' => 'form','method' => 'post','id' => 'create-role']) }}
        @endif
        @csrf


        <div class="row">
            <div class="col form-group">
                {{ Form::label('name', __('Name')) }}

                {{ Form::text('name', old('name', isset($role) ? $role->name : ''), ['class' => 'form-control','required' => 'required']) }}
            </div>
        </div>
        
        @foreach($module as $key => $m)
            @if(isset($user_role[$m['module_id']]))
                @php

                    $permission_ids = explode(',',$user_role[$m['module_id']]['permission_ids']);
                @endphp
            @else
                @php
                    $permission_ids = array();
                @endphp
            @endif
                        
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('module', __('Module')) }}
                    {{ Form::hidden('module_id['.$key.']', $m['module_id']) }}
                    @if(isset($user_role[$m['module_id']]))
                        {{ Form::hidden('user_role_id['.$key.']', $user_role[$m['module_id']]['id']) }}
                    @endif
                    {{ Form::text('module['.$key.']', $m['module_name'],  ['class' => 'form-control module','placeholder' => 'Select Module','required' => 'required', 'readonly']) }}
                </div>
                <div class="col form-group">
                    {{ Form::label('permission', __('Permissions')) }}
                    <select name="permission_id[{{$key}}][]" class="form-control js-example-basic-multiple" multiple>
                        @foreach($m['permissions'] as $p)
                        <option value="{{$p['id']}}" @if(!empty($permission_ids) && in_array($p['id'],$permission_ids)) selected="selected" @endif>{{$p['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col form-group">
                   
                    <div class="custom-control custom-checkbox mt-5">
                        <input type="checkbox" class="custom-control-input" id="all{{$key}}" name="all[{{$key}}]" value="1" @if(isset($user_role[$m['module_id']]) && $user_role[$m['module_id']]['all']==1) checked @endif>
                        <label class="custom-control-label" for="all{{$key}}">All</label>
                    </div>
                </div>
            </div>
        @endforeach
        <button type="submit" class="btn btn-primary float-right">Submit</button>
        {{ Form::close() }}
    </div>
</div>
@endsection
@section('pagescript')

<script>
    // $(document).on('change', '.module', function() {
    //     var module_id = $(this).val();
    //     var url = '{{ route('role.get.permission') }}';
    //     $.ajax({
    //         type: 'post',
    //         url: url,
    //         data: {
    //             'ref': module_id
    //         },
    //         success: function(result) {
    //             $('#permission').empty();
    //             $('#permission').append(result.permission);
    //         }
    //     });
    // });
</script>
<!-- select2 -->
<script src="{{asset('js/select2/select2.min.js')}}"></script>

<!-- bloodhound -->
<script src="{{asset('js/select2/bloodhound.min.js')}}"></script>

<!-- Bootstrap tagsinput -->
<script src="{{asset('js/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>

<!-- tagsinput custom -->
<script src="{{asset('js/bootstrap-tagsinput/tagsinput.js')}}"></script>

@endsection

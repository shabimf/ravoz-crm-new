(function () {
    var i = 1;
    FTX.SALE = {

        list: {

            selectors: {
                Sale_table: $('#sale-table'),
            },

            init: function (country_id,model_id,from,to) {
                this.selectors.Sale_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.Sale_table.data('ajax_url'),
                        type: 'post',
                        data: function (d) {
                            d.from = from,
                            d.to = to,
                            d.country_id = country_id,
                            d.model_id = model_id
                        },
                    },
                    columns: [
                        {
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'sales_date', name: 'sales_date' },
                        { data: 'country_name', name: 'country_name'},
                        { data: 'activation_date', name: 'activation_date' },
                        { data: 'IMEI_no1', name: 'IMEI_no1' },
                        { data: 'IMEI_no2', name: 'IMEI_no2' },
                        { data: 'invoice_no', name: 'invoice_no' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'user_name', name: 'user_name'},



                    ],
                    order: [[2, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        if (data['activation_date']) {
                            $('td', row).addClass('text-primary');
                        }
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();

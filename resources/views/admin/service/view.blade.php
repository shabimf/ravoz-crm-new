@extends('layouts.service')
@section('content')
    <div class="main-panel">
        <div class="panel-hedding padding-top">
             <div class="row">
                <div class="col-lg-12">
                    <div class="card  service_list">
                        <div class="card-body p-0">
                        <div class="col-sm-12 transfer-div" style="background:#fff; border-top-left-radius: 15px; border-top-right-radius: 15px;">
                    @include('inc.alert')
                    <div style="border-bottom:1px solid #000000">
                        <div class="col-sm-6 custxt">
                            <h3 class="text-left">
                                @if ($service->type == 1)
                                    Customer
                                @elseif($service->type == 2)
                                    Dealer
                                @else
                                    Salesman
                                @endif Details
                            </h3>
                        </div>
                        <div class="col-sm-6 custxt">
                            <h3 class="text-right"> {{ $service->serial_no }} </h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @if ($service->status != 5)
                        @if ($service->transfer_id > 0 &&
                            $service->transfer_status == 1 &&
                            $service->to_branch == Auth::user()->branch_id &&
                            $service->return_user == '' &&
                            userHasPermission('Service','edit_service_form'))
                            <a
                                class="pull-right text-right col-sm-3 @if(isDOA($service->doa_id,$service->doa_status)) hide @endif @if ($service->type == 2) edit_dealer @elseif($service->type == 3) edit_salesman @else edit_cust @endif cp">
                                <img src="{{asset('images/user-edit.png')}}" class="user-edit {{buttonHide($service->status)}}" alt="user-edit">
                                <!-- Edit
                                @if ($service->type == 1)
                                    Customer
                                @elseif($service->type == 2)
                                    Dealer
                                @else
                                    Salesman
                                @endif -->
                            </a>
                        @elseif($service->transfer_id == '' && $service->transfer_status == 0 && userHasPermission('Service','edit_service_form'))
                            <a
                                class="pull-right text-right col-sm-3 @if(isDOA($service->doa_id,$service->doa_status)) hide @endif @if ($service->type == 2) edit_dealer @elseif($service->type == 3) edit_salesman @else edit_cust @endif cp">
                                <img src="{{asset('images/user-edit.png')}}" class="user-edit {{buttonHide($service->status)}}"  alt="user-edit">
                                <!-- Edit
                                @if ($service->type == 1)
                                    Customer
                                @elseif($service->type == 2)
                                    Dealer
                                @else
                                    Salesman
                                @endif -->
                            </a>
                            <br>
                        @endif
                    @endif
                    <form action="" method="post" name="update_cus">
                        <input type="hidden" id="customer_id" name="customer_id" value="{{ $service->customer_id }}" />
                        <input type="hidden" id="parent_cus_id" name="parent_cus_id"
                            value="{{ $service->parent_cus_id }}" />
                        <input type="hidden" id="cus_type" name="cus_type" value="{{ $service->type }}" />
                        @if ($service->type == 2)
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefault01">Dealer Name<span style="color: red">*</span></label>
                                    <br />
                                    <span class="dealer_val">{{ $service->customer_name }}</span>
                                    <input type="text" name="dealer_name" id="dealer_name"
                                        class="form-control dealer_edit" value="{{ $service->customer_name }}">
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefault02">Dealer Phone No<span
                                            style="color: red">*</span></label><br />
                                    <span class="dealer_val">{{ $service->customer_phone }}</span>
                                    <input type="text" name="dealer_phone" id="dealer_phone"
                                        class="form-control dealer_edit" value="{{ $service->customer_phone }}">
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefaultUsername">Dealer Contact</label><br />
                                    <span class="dealer_val">{{ $service->dealer_contact }}</span>
                                    <input type="text" name="dealer_contact" id="dealer_contact"
                                        class="form-control dealer_edit" value="{{ $service->dealer_contact }}">
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefaultUsername">Address</label><br />
                                    <span class="dealer_val">{{ $service->address }}</span>
                                    <textarea name="dealer_address" id="dealer_address" class="form-control dealer_edit">{{ $service->address }}</textarea>
                                </div>
                                <div class="col-sm-12">
                                    <button class="btn btn-primary pull-right dealer_btn dealer_edit">Update</button>
                                </div>
                            </div>
                        @elseif($service->type == 3)
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefault01">Salesman Name<span style="color: red">*</span></label>
                                    <br />
                                    <span class="salesman_val">{{ $service->customer_name }}</span>
                                    <input type="text" name="salesman_name" id="salesman_name"
                                        class="form-control salesman_edit" value="{{ $service->customer_name }}">
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefault01">Salesman Phone No<span
                                            style="color: red">*</span></label>
                                    <br />
                                    <span class="salesman_val">{{ $service->customer_phone }}</span>
                                    <input type="text" name="salesman_contact" id="salesman_contact"
                                        class="form-control salesman_edit" value="{{ $service->customer_phone }}">
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefault01">Salesman District</label>
                                    <br />
                                    <span class="salesman_val">{{ $service->salesman_district }}</span>
                                    <input type="text" name="salesman_district" id="salesman_district"
                                        class="form-control salesman_edit" value="{{ $service->salesman_district }}">
                                </div>
                                <div class="col-sm-12">
                                    <button class="btn  btn-primary pull-right salesman_btn salesman_edit">Update</button>
                                </div>
                            </div>
                        @else
                            <div class="form-row customer-form">
                                <div class="col-md-4 mb-4">
                                    <label for="validationDefault01">Name<span style="color: red">*</span></label>
                                    <br />
                                    <span class="cust_val">{{ $service->customer_name }}</span>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <select class="form-control cust_edit" id="gender" name="gender"
                                                aria-invalid="false">
                                                <option value="Mr."
                                                    @if ($service->gender == 'Mr.') selected="selected" @endif>Mr.
                                                </option>
                                                <option value="Mrs."
                                                    @if ($service->gender == 'Mrs.') selected="selected" @endif>Mrs.
                                                </option>
                                                <option value="Miss"
                                                    @if ($service->gender == 'Miss') selected="selected" @endif>Miss
                                                </option>
                                                <option value="Ms."
                                                    @if ($service->gender == 'Ms.') selected="selected" @endif>Ms.
                                                </option>
                                                <option value="Dr."
                                                    @if ($service->gender == 'Dr.') selected="selected" @endif>Dr.
                                                </option>
                                            </select>
                                            <input class="form-control cust_edit" id="customer_name" name="customer_name"
                                                type="text" value="{{ $service->customer_name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefault01">Phone Number<span style="color: red">*</span></label>
                                    <br />
                                    <span class="cust_val">{{ $service->customer_phone }}</span>
                                    <input type="text" name="customer_phone" id="customer_phone"
                                        class="form-control cust_edit" value="{{ $service->customer_phone }}">
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefault01">Email Address<span
                                            style="color: red">*</span></label>
                                    <br />
                                    <span class="cust_val">{{ $service->customer_email }}</span>
                                    <input type="text" name="customer_email" id="customer_email"
                                        class="form-control cust_edit" value="{{ $service->customer_email }}">
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefault01">Address</label>
                                    <br />
                                    <span class="cust_val">{{ $service->address }}</span>
                                    <textarea class="form-control cust_edit" name="address" id="address">{{ $service->address }}</textarea>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefault01">Postal Code</label>
                                    <br />
                                    <span class="cust_val">{{ $service->postal_code }}</span>
                                    <input type="text" name="postal_code" id="postal_code"
                                        class="form-control cust_edit" value="{{ $service->postal_code }}">
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationDefault01">Backup Number</label>
                                    <br />
                                    <span class="cust_val">{{ $service->backup_no }}</span>
                                    <input type="text" name="backup_no" id="backup_no" class="form-control cust_edit"
                                        value="{{ $service->backup_no }}">
                                </div>
                                <div class="col-sm-12">
                                    <button class="btn  btn-primary pull-right customer_btn cust_edit">Update</button>
                                </div>
                            </div>
                        @endif
                    </form>
                    <div class="clearfix"> </div>
                </div>
                <div class="col-sm-12 transfer-div">
                    <br>
                    <!-- <div class="row left-btns">
                        <div class="col-md-12">

                        <a target="_blank" href="{{ route('service.receipt', ['id' => $service->id]) }}"
                        class="btn btn-sm btn-danger pull-left @if(isDOA($service->doa_id,$service->doa_status)) hide @endif"><i class="fa fa-print"></i>
                        View Receipt </a>
                        @if ($serviceBill > 0)
                            <a target="_blank" href="{{ route('service.view.bill', ['id' => $service->id]) }}"
                                class="btn btn-sm btn-primary pull-left ml-2"><i class="fa fa-print"></i>
                                View Bill </a>
                                @if (userHasPermission('Service','regenerate_bill'))
                                    <a target="_blank" data-id="{{ $service->id }}" data-toggle="modal"
                                        data-target="#myModalBill" class="btn btn-sm btn-info pull-left ml-2">
                                        <i class="fa fa-print"></i>
                                        Re-Generate Bill
                                    </a>
                                @endif
                        @endif
                        </div>
                    </div> -->



                    <input type="hidden" id="comment_id" name="comment_id" value="{{$service->appeal_id}}" />
                    <div class="clearfix"></div>
                    <div class="alert alert-danger alert-block d-none" id="return-err">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <p id="return-err-msg"></p>
                    </div>
                    @if ($service->transfer_id > 0)
                        <div class="panel panel-primary rp transfer-section" id="complaint6" style="border:none; !important">
                            <div class="panel-heading work_panel_head"> Transfer Details</div>
                            <br />
                            <div class="panel-body table-responsive">
                                <table class="table table-bordered table-condensed">
                                    <tr class="success">
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Remark</th>
                                        <th>Transfered date</th>
                                        <th>Accepted date</th>
                                        <th>Returned date</th>
                                        <th> Received back date</th>
                                        <th>Status</th>
                                    </tr>
                                    <tr class="danger">
                                        <td>{{ $service->from_branch_name }}</td>
                                        <td>{{ $service->to_branch_name }}</td>
                                        <td>{{ $service->transfer_remarks }}</td>
                                        <td>{{ date('d-m-Y', strtotime($service->transfer_date)) }}</td>
                                        <td>{{ $service->accepted_date ? date('d-m-Y', strtotime($service->accepted_date)) : '' }}
                                            @if ($service->transfer_status == 0 &&
                                                $service->accepted_date == '' &&
                                                $service->to_branch == Auth::user()->branch_id)
                                                <a class="btn btn-success btn-xs"
                                                    onclick="javascript:changeAction(1,{{ $service->id }},{{ $service->transfer_id }})">Accept</a>
                                                <a class="btn btn-danger btn-xs"
                                                    onclick="javascript:changeAction(2,{{ $service->id }},{{ $service->transfer_id }})">Reject</a>
                                            @endif
                                            @if ($service->transfer_status == 0 &&
                                                $service->accepted_date == '' &&
                                                $service->from_branch == Auth::user()->branch_id)
                                                <a class="btn btn-danger btn-xs"
                                                    onclick="javascript:changeAction(3,{{ $service->id }},{{ $service->transfer_id }})">Cancel</a>
                                            @endif
                                        </td>
                                        <td>{{ $service->return_date ? date('d-m-Y', strtotime($service->return_date)) : '' }}
                                            @if ($service->transfer_status == 1 &&
                                                $service->transfer_id > 0 &&
                                                $service->return_user == '' &&
                                                $service->to_branch == Auth::user()->branch_id)
                                                <a class="btn btn-success btn-xs transfer_type"
                                                    data-id="{{ $service->transfer_id }}"
                                                    data-service-id="{{ $service->id }}" data-type="1">
                                                    Return Back
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $service->received_back_date ? date('d-m-Y', strtotime($service->received_back_date)) : '' }}
                                            @if ($service->transfer_status == 1 &&
                                                $service->transfer_id > 0 &&
                                                $service->return_user != '' &&
                                                $service->received_back_user == '' &&
                                                $service->from_branch == Auth::user()->branch_id)
                                                <a class="btn btn-success btn-xs transfer_type"
                                                    data-id="{{ $service->transfer_id }}"
                                                    data-service-id="{{ $service->id }}" data-type="2">
                                                    Receive Back
                                                </a>
                                        </td>
                    @endif
                    <td>
                        @if ($service->transfer_status == 1)
                            <i class="fa fa-circle text-success mr-2"></i> Accepted
                        @elseif ($service->transfer_status == 2)
                            <i class="fa fa-circle text-danger mr-2"></i> Rejected
                            @if ($service->from_branch == Auth::user()->branch_id)
                                <button class="btn btn-danger btn-xs" data-id="{{ $service->id }}" data-toggle="modal"
                                    data-target="#myModalTransfer">Re-Transfer <i class="fa fa-exchange"></i></button>
                            @endif
                        @else
                            <i class="fa fa-circle text-warning mr-2"></i> Pending
                        @endif
                    </td>

                    </tr>
                    </table>







                    </div>

            <!-- <div class="container layout full_width"> -->

            <!-- </div> -->
            @endif
            <br>
            <div class="panel panel-primary rp" id="complaint{{ $service->id }}" style="border:none; !important">
                <div class="panel-heading work_panel_head"> Device Information </div>
                <div class="panel-body ">
                    <div class="col-sm-12 rp">
                        <style>
                            .device_table {}

                            .device_table th {
                                color: #337AB7
                            }

                            .device_table th,
                            .device_table td {
                                width: 100px;
                            }

                            .card-task {
                                width: 445px;
                            }

                            .card-title-text {
                                padding: 5px 0px 0px 15px;
                                font-weight: 600;
                            }

                            .status-btn {
                                padding: 4px 4px 4px 4px;
                                font-size: 12px;
                                border-radius: 10px;
                                margin: 0px 15px 3px 13px;
                            }

                            .status-btn:hover {
                                color: #fff;
                            }

                            .bill_table th {
                                color: #fff;
                                background: #000;
                            }

                            .bill_table th,
                            .bill_table td {
                                width: 100px;
                            }
                        </style>
                        <br />

                        @if (getComplaintsCount($service->id) == 1 &&
                            $service->branch_id == Auth::user()->branch_id &&
                            $service->transfer_id == '' &&
                            empty($getSpareDetails) && ($sale_request['is_saleReq'] == 0 || ($sale_request['is_saleReq'] == 1 && $sale_request['status'] == 1)))
                            <button class="btn btn-danger btn-xs @if(isDOA($service->doa_id,$service->doa_status)) hide @endif" data-id="{{ $service->id }}" data-toggle="modal"
                                data-target="#myModalTransfer">Transfer <i class="fa fa-exchange"></i></button>
                        @endif
                        @if ($service->transfer_id > 0 &&
                            $service->transfer_status == 1)
                            <a class="edit_work text-primary text-right pull-right cp {{buttonHide($service->status)}} @if(isDOA($service->doa_id,$service->doa_status)) hide @endif" data-id="{{ $service->id }}"
                                data-toggle="modal" data-target="#myModal">
                                <i class="la la-edit edit"></i>
                            </a>
                        @elseif($service->transfer_id == '' && $service->transfer_status == 0)
                            <a class="edit_work text-primary text-right pull-right cp  {{buttonHide($service->status)}}  @if(isDOA($service->doa_id,$service->doa_status)) hide @endif" data-id="{{ $service->id }}"
                                data-toggle="modal" data-target="#myModal">
                                <i class="la la-edit edit"></i>
                            </a>
                        @endif
                        @if($service->status == 1 && $service->doa_id == 0 && userHasPermission('Service','doa_create'))
                        <button class="btn btn-success btn-xs" data-id="{{ $service->id }}" data-toggle="modal" data-target="#doaModal">
                            Issue DOA
                        </button>
                        @endif
                        <div class="clearfix"></div>
                        <div class="table-responsive">
                            <table class="table table-condensed device_table  table-bordered table-hover no-margin sf">
                                <tbody>
                                    <tr>
                                        <th> Technician </th>
                                        <td><strong>{{ $service->technician }}</strong></td>
                                        <th> Warranty </th>
                                        <td><strong class="warranty_hide">
                                                @if ($service->is_warranty == 0)
                                                    Non Warranty
                                                @else
                                                    Yes
                                                @endif
                                            </strong>

                                        {!! Form::select('warranty', ['1' => 'Yes', '0' => 'No'], $service->is_warranty , [ 'class' =>  'form-control warrantycls hide',])!!}
                                        @if ($service->is_warranty == 1)
                                        <div class="{{buttonHide($service->status)}}">(<a href="javascript:void(0)" id="warrantylink"><b>Change Warranty</b></a>)</div>
                                        @endif
                                       </td>
                                        <td> <strong>Remarks</strong> </td>
                                        <td>
                                            {{ $service->remarks }}
                                        </td>
                                        <td> <strong>Billing Status</strong> </td>
                                        <td>
                                            @if ($service->status == 5)
                                                <label class="btn-xs btn btn-success" style="width: 100%;">
                                                    Delivered
                                                </label>
                                            @elseif ($service->doa_id > 0 && $service->doa_status == 0)
                                                <label class="btn-xs btn btn-warning" style="width: 100%;">
                                                  DOA Request Pending
                                                </label>


                                            @else
                                                <label class="btn-xs btn btn-warning" style="width: 100%;">
                                                    Pending
                                                </label>
                                            @endif

                                        </td>
                                    </tr>
                                    <tr>
                                        <th> IMEI NO 1 </th>
                                        <td>{{ $service->IMEI_no1 }}</td>
                                        <th> IMEI NO 2 </th>
                                        <td>{{ $service->IMEI_no2 }}
                                        </td>
                                        <th> Branch </th>
                                        <td>{{ $service->branch_name }}</td>
                                        <th> Brand </th>
                                        <td> {{ $service->brand }} </td>
                                    </tr>
                                    <tr>

                                        <th> Model </th>
                                        <td colspan="3">{{ $service->model }}
                                            ({{ $service->color }}
                                            @if ($service->ram)
                                                ,{{ $service->ram }}
                                            @endif
                                            @if ($service->rom)
                                                ,{{ $service->rom }}
                                            @endif
                                            )

                                        </td>
                                        <th> Serial Num </th>
                                        <td>{{ $service->sn_no }}</td>
                                        <th> Security Code </th>
                                        <td>{{ $service->security_code }}</td>
                                    </tr>
                                    <tr>
                                        @if($service->manufacture_date!='')
                                            @php
                                                $manufacture_date=date('d-m-Y', strtotime($service->manufacture_date));
                                            @endphp
                                        @else
                                            @php
                                                $manufacture_date='-';
                                            @endphp
                                        @endif
                                        <th> Sale Date </th>
                                        <td>{{ ($service->sale_date?date('d-m-Y', strtotime($service->sale_date)):'') }}</td>
                                        <th> Active Date </th>
                                        <td> {{ ($service->active_date?date('d-m-Y', strtotime($service->active_date)):'') }}</td>
                                        <th> Date of Manufacture </th>
                                        <td> {{ $manufacture_date }}</td>
                                        <th> Delivery Date </th>
                                        <td> {{($service->delivery_date?date('d-m-Y', strtotime($service->delivery_date)):'') }}</td>
                                    </tr>
                                    <tr>
                                        <th> Software Version </th>
                                        <td>
                                        <select name="old_version" class="old_version form-control hide" onchange="return updateVersion(this.value,'service',{{$service->id}});">
                                            <option value="">Select Software Version</option>
                                            @foreach($version as $key => $v)
                                                <option value="{{$key}}" @if($key==$service->new_version_id) selected @endif>{{$v}}</option>
                                            @endforeach
                                        </select>
                                        <div class="version_hide">
                                          {{($service->new_software_name?$service->new_software_name:$service->old_software_name)}}
                                        </div>
                                        <div class="{{buttonHide($service->status)}}">
                                            (<a href="javascript:void(0)" id="versionlink">Change Version</a>)
                                        </div>
                                        </td>
                                        <th> Images </th>
                                        <td>
                                            @if ($images)
                                                @php $c =0 @endphp
                                                @foreach ($images as $k => $file)
                                                    @php $c++; @endphp
                                                    <a href="{{ asset('uploads/service/' . $file) }}">Image
                                                        {{ $c }}</a><br />
                                                @endforeach
                                            @endif
                                        </td>
                                        <th> Invoice </th>
                                        <td>
                                            @if ($invoice)
                                                @php $c =0 @endphp
                                                @foreach ($invoice as $k => $file)
                                                    @php $c++; @endphp
                                                    <a href="{{ asset('uploads/invoice/' . $file) }}">Invoice
                                                        {{ $c }}</a><br />
                                                @endforeach
                                            @endif
                                        </td>
                                        @php
                                            if ($service->accessories_id) {
                                                $accessories_array = explode(',', $service->accessories_id);
                                            }
                                        @endphp
                                        <th> Accessories </th>
                                        <td>
                                            @if ($service->accessories_id)
                                                @foreach ($access as $k => $v)
                                                    @if (in_array($k, $accessories_array))
                                                        {{ $v }},
                                                    @endif
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @if(!empty($service_history))
                    <div class="clearfix"></div>
                    <div class="rp">
                        <div class="col-sm-12 rp">
                            <h3>Service History</h3>
                            <hr>
                            <div class="card-tasks">
                                <table class="table table-bordered table-condensed ">
                                    <tbody>
                                        <tr style="background: #E0DCDC !important">
                                            <th>Service No</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                @php
                                                    // echo "<pre>"; print_r($service_history); echo "</pre>";
                                                    $last_key=array_key_last($service_history);
                                                @endphp
                                                @foreach ($service_history as $key => $value)
                                                    @php
                                                        $history="";
                                                        $history.='<a href="'.url('service/'. $value["id"]).'">'.$value['serial_no'].'</a>';
                                                    @endphp
                                                        @if ($key != $last_key)
                                                            @php
                                                                $history.=', ';
                                                            @endphp
                                                        @endif
                                                    @php
                                                        echo $history;
                                                    @endphp
                                                @endforeach
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="rp" @if ($sale_request['is_saleReq'] == 1 && $sale_request['status'] == 0) style="filter: blur(2px);" @endif>
                        <div class="col-sm-12 rp">
                            <h3>
                                Complaint Details
                                @if (($sale_request['is_saleReq'] == 0 || ($sale_request['is_saleReq'] == 1 && $sale_request['status'] == 1)))
                                    @if ($service->transfer_id > 0 &&
                                        $service->transfer_status == 1 &&
                                        $service->return_user == '' && userHasPermission('Service','servicing'))
                                        <small class="btn btn-primary accordion-title pull-right cp new_task_btn {{buttonHide($service->status)}} @if(isDOA($service->doa_id,$service->doa_status)) hide @endif" data-toggle="collapse"
                                            data-target="#collapseOne" ><i class="fa fa-plus"></i>
                                            Add New Task
                                        </small>
                                    @elseif($service->transfer_id == '' && $service->transfer_status == 0 && userHasPermission('Service','servicing'))
                                        <small class="btn btn-primary accordion-title pull-right cp new_task_btn {{buttonHide($service->status)}} @if(isDOA($service->doa_id,$service->doa_status)) hide @endif" data-toggle="collapse"
                                            data-target="#collapseOne"><i class="fa fa-plus"></i>
                                            Add New Task
                                        </small>
                                    @endif
                                @endif
                            </h3>
                            <hr>

                            <div aria-labelledby="headingOne">
                                <div id="collapseOne" class="collapse card card-task">
                                    <p class="card-title-text">New Task</p>
                                    <div class="card-body">
                                        {{ Form::open(['', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'service-complaint', 'auto-complete' => 'off', 'enctype' => 'multipart/form-data']) }}
                                        <div class="row">
                                            {{ Form::hidden('service_id', $service->id) }}
                                            <div class="form-group col-md-12">
                                                {{ Form::label('name', __('Complaints')) }}
                                                <select class="js-example-basic-multiple select2-hidden-accessible"
                                                    name="complaint[]" multiple="">
                                                    @foreach ($levels as $key => $complaint)
                                                        <optgroup label="{{ $key }}">
                                                            @foreach ($complaint as $k => $v)
                                                                <option value="{{ $k }}">
                                                                    {{ $v }}</option>
                                                            @endforeach
                                                        </optgroup>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-sm btn-primary float-right"
                                            id="complaints_add">Add</button>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed ">
                                        <tbody>
                                            <tr style="background: #E0DCDC !important">
                                                <th width="20"></th>
                                                <th> Task </th>
                                                <th> Remarks </th>
                                                <th width="100"> Status </th>
                                                <th width="100"> Action </th>
                                            </tr>
                                            @php
                                                $i = 1;
                                            @endphp
                                            @if (!empty($get_complaints))
                                                @foreach ($get_complaints as $complaint)
                                                    @if ($complaint['status'] == 0)
                                                        @php
                                                            $status = 'Pending';
                                                            $style = 'background-color:#eea236; color:#fff;';
                                                        @endphp
                                                    @elseif ($complaint['status'] == 1)
                                                        @php
                                                            $status = 'Finished';
                                                            $style = 'background-color:#18a863; color:#fff;';
                                                        @endphp
                                                    @else
                                                        @php
                                                            $status = 'Non Repairable';
                                                            $style = 'background-color:#ee3636; color:#fff;';
                                                        @endphp
                                                    @endif
                                                    <tr>
                                                        <td>{{ $i }}.</td>
                                                        <td>{{ $complaint['description'] }} </td>
                                                        <td> @if (buttonHide($service->status) == "hide")
                                                            {{$complaint['remarks']}}
                                                            @else
                                                            {{ Form::textarea('complaint_description',$complaint['remarks'], array('class'=>'form-control complaint_description','rows' => 2, 'cols' => 2,'id'=>'complaint_description','data-id'=>$complaint['service_comp_id']))  }}

                                                            @endif
                                                        </td>
                                                        <!-- <td id="update-status{{ $complaint['id'] }}"
                                                            style="display: none;">
                                                            <select class="status_update"
                                                                data-id="{{ $complaint['service_comp_id'] }}">
                                                                <option @if ($complaint['status'] == 0) selected @endif
                                                                    value=0>Pending</option>
                                                                <option @if ($complaint['status'] == 1) selected @endif
                                                                    value=1>Finished</option>
                                                                <option @if ($complaint['status'] == 2) selected @endif
                                                                    value=2>Non Repairable</option>
                                                            </select>
                                                        </td> -->
                                                        <td id="comp_status{{ $complaint['id'] }}" align="right">

                                                            <!-- <button class="btn status-btn"
                                                                style="{{ $style }}">{{ $status }}</button> -->
                                                                @if (buttonHide($service->status) == "hide")
                                                                <a class="btn @if ($complaint['status'] == 0) btn-warning @elseif ($complaint['status'] == 2) btn-danger @else btn-success @endif">
                                                                    {{ $status }}
                                                                </a>
                                                                @endif
                                                                <div class="dropdown {{buttonHide($service->status)}}">
                                                                    <a class="btn @if ($complaint['status'] == 0) btn-warning @elseif ($complaint['status'] == 2) btn-danger @else btn-success @endif dropdown-toggle " href="#" role="button" id="dropdownMenuLink{{ $complaint['id'] }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    {{ $status }}
                                                                    </a>
                                                                    <div class="dropdown-menu " aria-labelledby="dropdownMenuLink{{ $complaint['id'] }}">
                                                                    @if ($complaint['status'] == 0)
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-id="{{ $complaint['service_comp_id'] }}" data-status="1">Finished</a>
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-id="{{ $complaint['service_comp_id'] }}" data-status="2">Non Repairable</a>
                                                                    @elseif ($complaint['status'] == 1)
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-status="0" data-id="{{ $complaint['service_comp_id'] }}" >Pending</a>
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-status="2" data-id="{{ $complaint['service_comp_id'] }}" >Non Repairable</a>
                                                                    @elseif ($complaint['status'] == 2)
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-status="0" data-id="{{ $complaint['service_comp_id'] }}" >Pending</a>
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item status_update" href="javascript:void(0);" data-status="1" data-id="{{ $complaint['service_comp_id'] }}" >Finished</a>
                                                                    @endif

                                                                    </div>
                                                                </div>
                                                        </td>
                                                        <td>


                                                            @if ($service->status != 5 && ($sale_request['is_saleReq'] == 0 || ($sale_request['is_saleReq'] == 1 && $sale_request['status'] == 1)))
                                                                @if ($service->transfer_id > 0 &&
                                                                    $service->transfer_status == 1 &&
                                                                    $service->to_branch == Auth::user()->branch_id &&
                                                                    $service->return_user == '')
                                                                    <!-- <a data-id="{{ $complaint['id'] }}"
                                                                        style="cursor: pointer;padding: 9px 35px 5px 9px;"
                                                                        class="edit-status @if(isDOA($service->doa_id,$service->doa_status)) hide @endif"><i class="la la-edit edit"></i></a> -->
                                                                @elseif($service->transfer_id == '' && $service->transfer_status == 0)
                                                                    <!-- <a data-id="{{ $complaint['id'] }}"
                                                                        style="cursor: pointer;padding: 9px 35px 5px 9px;"
                                                                        class="edit-status @if(isDOA($service->doa_id,$service->doa_status)) hide @endif"><i class="la la-edit edit"></i></a>-->
                                                                @endif
                                                            @endif

                                                            <button class="btn btn-danger {{buttonHide($service->status)}} @if(isDOA($service->doa_id,$service->doa_status)) hide @endif complaint-remove"
                                                                    data-id="{{ $complaint['service_comp_id'] }}"><i class="la la-remove remove"></i></button>

                                                        </td>
                                                    </tr>
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="5" align="center">No data available</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="clearfix"></div>

                        <div class="col-sm-12 rp">
                            <h3>Spare Details
                                @if (($sale_request['is_saleReq'] == 0 || ($sale_request['is_saleReq'] == 1 && $sale_request['status'] == 1)))
                                    @if ($service->transfer_id > 0 &&
                                        $service->transfer_status == 1)
                                        <small class="btn btn-success accordion-title pull-right cp new_task_btn {{buttonHide($service->status)}} @if(isDOA($service->doa_id,$service->doa_status)) hide @endif" data-toggle="collapse"
                                            data-target="#collapseTwo"><i class="fa fa-plus"></i>
                                            Add New Spare
                                        </small>
                                    @elseif($service->transfer_id == '' && $service->transfer_status == 0)
                                        <small class="btn btn-success accordion-title pull-right cp new_task_btn {{buttonHide($service->status)}} @if(isDOA($service->doa_id,$service->doa_status)) hide @endif" data-toggle="collapse"
                                            data-target="#collapseTwo"><i class="fa fa-plus"></i>
                                            Add New Spare
                                        </small>
                                    @endif
                                @endif
                            </h3>
                            <hr>
                            @php
                            $spare_col = "col-md-4";
                            if ($service->is_warranty == 0) {
                                $spare_col = "col-md-6";
                            }
                            @endphp
                            <div id="collapseTwo" class="collapse card">
                                <h4 id="err-msg" class="alert alert-danger d-none text-center"></h4>
                                <p class="card-title-text">New Spare</p>
                                <div class="card-body">
                                    {{ Form::open(['', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'service-spare', 'auto-complete' => 'off', 'enctype' => 'multipart/form-data']) }}
                                    <div class="row">
                                        {{ Form::hidden('service_id', $service->id) }}
                                        <div class="form-group {{$spare_col}}">
                                            {{ Form::label('name', __('Spares')) }}

                                            <select class="js-example-basic-single"
                                                name="spare" id="spare" placeholder="--Select--">
                                                <!-- <option>--Select--</option> -->
                                                @foreach ($models as $key => $spare)
                                                    <optgroup label="{{ $key }}">
                                                        @foreach ($spare as $k => $v)
                                                            <option value="{{ $k }}">
                                                                {{ $v }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group {{$spare_col}}">
                                            {{ Form::label('name', __('Price')) }}
                                            <input type="text" name="cost" id="spare_price" disabled
                                                class="form-control">
                                            <label id="spare-price-error" class="error" for="spare_price"></label>
                                        </div>
                                        @if ($service->is_warranty > 0)
                                        <div class="form-group col-md-4">
                                            <div class="custom-control custom-checkbox" style="margin-top:32px;">
                                                <input type="checkbox" class="custom-control-input" name="is_warranty"
                                                    id="is_warranty">
                                                <label class="custom-control-label" for="is_warranty">Is
                                                    Warranty</label>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="form-group {{$spare_col}}">
                                            {{ Form::label('name', __('Quantity')) }}
                                            <input type="number" min="1" name="spare_qty" id="spare_qty"
                                                class="form-control" value="1">
                                        </div>
                                        <div class="form-group {{$spare_col}}" id="customer-price">
                                            {{ Form::label('name', __('Customer Price')) }}
                                            <input type="text" name="price" id="spare_cus_price"
                                                class="form-control">
                                            <label id="spare-cusprice-error" class="error"
                                                for="spare_cus_price"></label>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-sm btn-primary float-right"
                                        id="spare_add">Add</button>
                                    {{ Form::close() }}
                                </div>
                            </div>

                            <div class="table-responsive">
                            <table class="table table-bordered table-condensed ">
                                <tbody>
                                    <tr style="background: #E0DCDC !important">
                                        <th width="20"></th>
                                        <th> Spare </th>
                                        <th> Part Code </th>
                                        <th> Focus Code </th>
                                        <th> Is Warranty </th>
                                        <th> Quantity </th>
                                        <th> Price </th>
                                        <th> Customer Price </th>
                                        <th> Action </th>
                                    </tr>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @if (!empty($getSpareDetails))
                                        @foreach ($getSpareDetails as $sparedetails)
                                            <tr>
                                                <td>{{ $i }}.</td>
                                                <td> {{ $sparedetails['spare_name'] }} </td>
                                                <td> {{ $sparedetails['part_code'] }} </td>
                                                <td> {{ $sparedetails['focus_code'] }} </td>
                                                <td>
                                                    @if ($sparedetails['is_warranty'] == 1)
                                                        <button type="button" class="btn btn-success btn-icon"> <i
                                                                class="fa fa-check"></i> </button>
                                                    @else
                                                        <button type="button" class="btn btn-success btn-icon"> <i
                                                                class="fa fa-times"></i> </button>
                                                    @endif
                                                </td>
                                                <td>{{ $sparedetails['quantity'] }}</td>
                                                <td> {{ $sparedetails['local_price'] }} </td>
                                                <td> {{ $sparedetails['cus_local_price'] }} </td>
                                                <td><button class="btn btn-danger spare-remove {{buttonHide($service->status)}}"
                                                        data-id="{{ $sparedetails['id'] }}">Remove</button></td>
                                            </tr>
                                            @php
                                                $i++;
                                            @endphp
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="9" align="center">No data available</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                                        </div>
                            <div class="row {{buttonHide($service->status)}}">
                                <div class="col-md-8">
                                    <form  action="" method="post" name="pcb_request" id="pcb_request">
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="is_pcb" name="is_pcb" value="1" {{($service->new_pcb_serial_no?"checked":"")}}>
                                                    <label class="custom-control-label" for="is_pcb">If change PCB</label>
                                                </div>
                                            </div>
                                            <div class="col pcbcls {{($service->new_pcb_serial_no?"":"hide")}}">
                                                <div class="custom-control">
                                                    <input type="text" class="form-control" id="pcb_serial_no" name="pcb_serial_no" placeholder="PCB Serial No" value={{($service->new_pcb_serial_no?$service->pcb_serial_no:"")}}>
                                                </div>
                                            </div>
                                            <div class="col pcbcls {{($service->new_pcb_serial_no?"":"hide")}}"">
                                                <div class="custom-control">
                                                   <button type="submit" class="btn btn-sm btn-primary  pcb_replace">Save</button>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="alert alert-danger alert-block d-none" id="finish-err">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <p id="finish-err-msg"></p>
                        </div>
                        <div class="col-sm-12 rp">
                            <h4>Add Service Charge</h4>
                            <hr>
                            <div class="card card-body">
                                <div class="row">
                                    <div class="form-group @if($service->is_warranty == 1) col-md-9 @else col-md-4 @endif">
                                        {{ Form::label('name', __('Service Charge')) }}
                                        <input type="text" name="cost" id="servicecharge" class="form-control"
                                            value="{{ $service->service_charge }}">
                                        <label id="service-charge-error" class="error" for="servicecharge"></label>
                                    </div>

                                    <div class="form-group col-md-4 @if($service->is_warranty == 1) hide @endif">
                                        {{ Form::label('name', __('Discount')) }}
                                        <input type="text" name="discount" id="discount" class="form-control" value="{{ $service->discount_local_price }}">
                                        <label id="service-discount-error" class="error" for="servicediscount"></label>
                                    </div>
                                    @if ($service->status != 5 && ($sale_request['is_saleReq'] == 0 || ($sale_request['is_saleReq'] == 1 && $sale_request['status'] == 1)))
                                        @if ($service->transfer_id > 0 &&
                                            $service->transfer_status == 1 &&
                                            $service->to_branch == Auth::user()->branch_id &&
                                            $service->return_user == '')
                                            <div class="form-group col-md-3">
                                                <button type="button" class="btn btn-sm btn-primary {{buttonHide($service->status)}} @if(isDOA($service->doa_id,$service->doa_status)) hide @endif"
                                                    style="margin-top: 28px;" id="service_charge">Add</button>
                                            </div>
                                        @elseif($service->transfer_id == '' && $service->transfer_status == 0)
                                            <div class="form-group col-md-3">
                                                <button type="button" class="btn btn-sm btn-primary {{buttonHide($service->status)}} @if(isDOA($service->doa_id,$service->doa_status)) hide @endif"
                                                    style="margin-top: 28px;" id="service_charge">Add</button>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                          <div class="row">
                          <div class="col-sm-12">
                            <div class="group-btn">
                            @if (userHasPermission('Service','service_cancel'))
                                @if (empty($getSpareDetails) && ($sale_request['is_saleReq'] == 0 || ($sale_request['is_saleReq'] == 1 && $sale_request['status'] == 1)))
                                    <button type="button" class="btn btn-lg btn-danger pull-left ml-2 @if(isDOA($service->doa_id,$service->doa_status)) hide @endif"
                                        id="service_cancel"> <i class="fa fa-times" aria-hidden="true"></i> Cancel</button>

                                @elseif(!empty($getSpareDetails) && ($sale_request['is_saleReq'] == 0 || ($sale_request['is_saleReq'] == 1 && $sale_request['status'] == 1)))
                                    <button type="button" class="btn btn-lg btn-danger pull-left @if(isDOA($service->doa_id,$service->doa_status)) hide @endif"
                                        data-toggle="modal" data-target="#myModalCancel"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                                @endif
                            @endif
                                <a target="_blank" href="{{ route('service.receipt', ['id' => $service->id]) }}" class="btn btn-lg btn-secondary pull-left @if(isDOA($service->doa_id,$service->doa_status)) hide @endif">
                                    <i class="fa fa-print"></i> View Receipt
                                </a>
                            @if ($service->status == 4)
                                <a target="_blank" class="btn btn-lg btn-dark pull-left" data-id="{{ $service->id }}" data-toggle="modal" data-target="#myModalBill">
                                    <i class="fa fa-print"></i> Bill Preview
                                </a>
                            @endif

                            @if ($serviceBill > 0)
                                <a target="_blank" href="{{ route('service.view.bill', ['id' => $service->id]) }}" class="btn btn-lg btn-light pull-left ml-2">
                                    <i class="fa fa-print"></i> View Bill
                                </a>
                                @if (userHasPermission('Service','regenerate_bill'))
                                    <a target="_blank" data-id="{{ $service->id }}" data-toggle="modal"
                                        data-target="#myModalBill" class="btn btn-lg btn-info pull-left ml-2">
                                        <i class="fa fa-print"></i>
                                        Re-Generate Bill
                                    </a>
                                @endif
                            @endif
                            @if (isset($sale_request['status']) && $sale_request['status'] == 2)
                               <button type="button" class="btn btn-lg btn-warning pull-right" id="salereq_resend">Resend</button>
                            @endif
                            @if (($sale_request['is_saleReq'] == 0 || ($sale_request['is_saleReq'] == 1 && $sale_request['status'] == 1)))
                               <button type="button" class="btn btn-lg btn-dark pull-right {{buttonHide($service->status)}} @if(isDOA($service->doa_id,$service->doa_status)) hide @endif" id="finish">
                               @if ($service->status == 1) Finish @else Update @endif
                               </button>
                            @endif
                            @if ($service->is_warranty == 1)
                                @if($service->is_approved == 0 && $serviceBill > 0 && $service->appeal_id == 0 && userHasPermission('Service','appeal_request_approval'))
                                <button class="btn btn-lg btn-primary pull-left ml-2"  data-toggle="modal" data-target="#myModalAppeal" >Appeal Request</button>
                                <button class="btn btn-lg btn-primary pull-left ml-2" type="button" data-toggle="modal" data-target="#myModalLevel" data-id="{{$service->id}}" data-type="1">Approve</button>
                                @elseif($service->appeal_id > 0 && $service->appeal_status == 0)
                                <a target="_blank" href="{{ url('service/'.$service->id.'/appeal/get') }}" class="btn btn-lg btn-primary pull-left ml-2">Open Ticket </a>
                                @elseif($service->appeal_id > 0 && $service->appeal_status == 1 && $service->is_approved == 0)
                                <button class="btn btn-lg btn-primary pull-left ml-2" type="button" data-toggle="modal" data-target="#myModalLevel" data-id="{{$service->id}}" data-type="1">Approve</button>
                                <a target="_blank" data-type="0" class="btn btn-lg btn-primary pull-left ml-2 commentclose">Re-Open Ticket </a>
                                @elseif($service->is_approved == 1)
                                <button class="btn btn-lg btn-success pull-left ml-2" type="button" style="cursor: text;"> <i class="fa fa-check"></i> Verified</button>
                                @endif
                            @endif
                            </div>
                          </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{ Form::open(['route' => ['service.update', $service->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-service', 'auto-complete' => 'off', 'enctype' => 'multipart/form-data']) }}
                @csrf
                <div class="modal-body">
                    <div class="col-sm-12  full_box rp   block_div" style=" padding-bottom:20px; font-size:11px">
                        @php
                            // echo "<pre>"; print_r($service); echo "</pre>"; exit;
                            $readonly = '';
                            $disabled = '';
                            if ($service->product_type == 'Phone') {
                                $readonly = 'readonly';
                            }
                            $disabled = 'disabled';
                        @endphp

                        <div class="row">
                            <div class="col-md-4 form-group">
                                {{ Form::label('name', __('Warranty')) }}<span style="color: red">*</span>
                                {{ Form::select('is_warranty', ['1' => 'Is Warranty', '0' => 'Non-Warranty'], old('is_warranty', $service->is_warranty), ['class' => 'form-control', 'id' => 'is_warranty', ]) }}
                            </div>
                            <div class="col-md-4 form-group">
                                {{ Form::label('brand_id', __('Brand')) }} <span style="color: red">*</span>
                                {{ Form::select('brand_id', $brands, old('brand_id', $service->brand_id), ['class' => 'js-example-basic-single txt_brand_id', 'id' => 'brand_id', 'placeholder' => 'Select Brand', 'onchange=getBrand(this.value,0)', $disabled]) }}

                            </div>
                            <div class="col-md-4 form-group">
                                {{ Form::label('model_id', __('Model')) }}
                                {{ Form::select('model_id', $model, old('model_id', $service->variant_id), ['class' => 'form-control js-example-basic-single txt_model_id', 'id' => 'model_id', 'onchange=getVersion(this.value)', $disabled]) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group"> {{ Form::label('name', __('Factory IMEI/SN Model')) }}
                                {{ Form::text('sn_no', old('sn_no', $service->sn_no), ['class' => 'form-control', 'id' => 'code', $readonly]) }}
                            </div>
                            <div class="col-md-4 form-group"> {{ Form::label('name', __('Sale Date')) }}
                                {{ Form::text('sale_date', old('sale_date', date('d-m-Y', strtotime($service->sale_date))), ['class' => 'form-control', 'id' => 'code', $readonly]) }}
                            </div>
                            @if($service->active_date!='')
                                @php
                                    $active_date=date('d-m-Y', strtotime($service->active_date));
                                @endphp
                            @else
                                @php
                                    $active_date='';
                                @endphp
                            @endif
                            <div class="col-md-4 form-group"> {{ Form::label('name', __('Active Date')) }}
                                {{ Form::text('active_date', old('active_date', $active_date), ['class' => 'form-control', 'id' => 'active_date', $readonly]) }}
                            </div>
                        </div>
                        <div class="row">
                            @if($service->manufacture_date!='')
                                @php
                                    $manufacture_date=date('d-m-Y', strtotime($service->manufacture_date));
                                @endphp
                            @else
                                @php
                                    $manufacture_date='';
                                @endphp
                            @endif
                            @if($service->delivery_date!='')
                                @php
                                    $delivery_date=date('d-m-Y', strtotime($service->delivery_date));
                                @endphp
                            @else
                                @php
                                    $delivery_date='';
                                @endphp
                            @endif
                            <div class="col-md-4 form-group"> {{ Form::label('name', __('Date of Manufacture')) }}
                                {{ Form::text('manufacture_date', old('manufacture_date', $manufacture_date), ['class' => 'form-control', 'id' => 'manufacture_date', $readonly]) }}
                            </div>
                            <div class="col-md-4 form-group"> {{ Form::label('name', __('Delivery Date')) }}
                                {{ Form::text('delivery_date', old('delivery_date', $delivery_date), ['class' => 'form-control', 'id' => 'delivery_date']) }}
                            </div>
                            <div class="col-md-4 form-group"> {{ Form::label('name', __('Software Version')) }}
                                {{ Form::select('version_id', $version, old('version_id', $service->version_id), ['class' => 'form-control js-example-basic-single', 'id' => 'version']) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group"> {{ Form::label('name', __('Technician')) }}
                                {{ Form::select('technician_id', $users, old('technician_id', $service->technician_id), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Technician', 'id' => 'technician_id','required'=>'required']) }}
                            </div>
                            <div class="col-md-4 form-group"> {{ Form::label('name', __('Security Code')) }}
                                {{ Form::text('security_code', old('security_code', $service->security_code), ['class' => 'form-control', 'id' => 'security_code']) }}
                            </div>
                            <div class="col-md-4 form-group"> {{ Form::label('name', __('Remarks')) }}
                                {{ Form::text('remarks', old('remarks', $service->remarks), ['class' => 'form-control', 'id' => 'remarks']) }}
                            </div>
                        </div>
                        @if ($sale_request['is_saleReq'])
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    {{ Form::label('name', __('Shop Name')) }}<span style="color: red">*</span>
                                    {{ Form::text('shop_name', old('shop_name', $sale_request['shop_name']), ['class' => 'form-control', 'id' => 'shop_name']) }}
                                </div>
                                <div class="col-md-4 form-group">
                                    {{ Form::label('name', __('Invoice No')) }}<span style="color: red">*</span>
                                    {{ Form::text('invoice_no', old('invoice_no', $sale_request['invoice_no']), ['class' => 'form-control', 'id' => 'invoice_no']) }}
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('Accessories')) }}
                                <div class="search-box">
                                    <div class="search">
                                        @foreach ($access as $k => $v)
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" id="accessories_{{ $k }}"
                                                    value="{{ $k }}" name="accessories_id[]"
                                                    class="custom-control-input form-control"
                                                    @if ($service->accessories_id) @if (in_array($k, $accessories_array)) checked @endif
                                                    @endif >
                                                <label class="custom-control-label"
                                                    for="accessories_{{ $k }}">{{ $v }}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('Images')) }}
                                <input type="file" name="filename[]" multiple="" class="form-control">
                            </div>
                            <div class="col-md-12 form-group">
                                @if ($images)
                                    @php($count = 0)
                                    @foreach ($images as $k => $file)
                                        @php($count++)
                                        <div class="delete_service_{{ $k }}">
                                            <a href="{{ asset('uploads/service/' . $file) }}" target="_blank">Image
                                                {{ $count }}</a>
                                            <button type="button" class="close delete-image" aria-label="Close"
                                                data-id="{{ $k }}" data-type="service">
                                                <span aria-hidden="true">&times;</span>
                                            </button><br />
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('Invoice')) }}
                                <input type="file" name="invoice[]" multiple="" class="form-control">
                                @if ($invoice)
                                    @php($count = 0)
                                    @foreach ($invoice as $k => $file)
                                        @php($count++)
                                        <div class="delete_invoice_{{ $k }}">
                                            <br /><a href="{{ asset('uploads/invoice/' . $file) }}"
                                                target="_blank">Invoice {{ $count }}</a>
                                            <button type="button" class="close delete-image" aria-label="Close"
                                                data-id="{{ $k }}" data-type="invoice">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary float-right btn-edit">Submit</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalTransfer" tabindex="-1" role="dialog" aria-labelledby="myModalTransferLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalTransferLabel">Transfer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" name="create_transfer" id="create_transfer">
                    <div class="modal-body">
                        <div class="alert alert-success alert-dismissible transfer_msg hide">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            Transfer to branch <strong>successfully!</strong>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('Branch')) }}
                                {{ Form::select('branch_id', $transfer_branches, old('branch_id'), ['class' => 'js-example-basic-single', 'id' => 'branch_id', 'placeholder' => 'Select Branch']) }}
                                <label id="branch_id-error" class="error" for="branch_id"></label>
                            </div>
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('Remarks')) }}
                                {!! Form::textarea('transfer_remarks', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success transfer_btn">Transfer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalAppeal" tabindex="-1" role="dialog"
        aria-labelledby="myModalAppealLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalAppealLabel">Appeal Request</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" name="appeal_request" id="appeal_request">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('Remarks')) }}
                                {{ Form::textarea('appeal_remarks', '', ['class' => 'form-control', 'id' => 'appeal_remarks']) }}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success appeal_send">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalLevel" tabindex="-1" role="dialog"
        aria-labelledby="myModalLevelLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalAppealLabel">Change Level</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" name="level_update" id="level_update">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('Level')) }}
                                {{ Form::select('level_id', $approve_level,old('level_id', $service->level_id), ['class' => 'js-example-basic-single level', 'id' => 'level_id', 'placeholder' => 'Select Level']) }}
                            </div>
                            <div class="col-md-12 form-group">
                                {{ $currency }}
                                {{ Form::label('name', __('Rate')) }}
                                {{ Form::text('rate', '', ['class' => 'form-control', 'id' => 'rate','readonly']) }}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success level_update">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalTechnician" tabindex="-1" role="dialog"
        aria-labelledby="myModalTechnicianLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalTechnicianLabel">Assign To Technician</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" name="assign_tech" id="assign_tech">
                    <div class="modal-body">
                        <div class="alert alert-success alert-dismissible technician_msg hide">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            Technician assigned <strong>successfully!</strong>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                {{ Form::label('name', __('Technician')) }}
                                {{ Form::select('technician_id', $users, old('technician_id'), ['class' => 'form-control js-example-basic-single', 'id' => 'technician_id', 'placeholder' => 'Select Technician', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success assign_btn">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="myModalReject" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalTechnicianLabel">Are you sure?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to reject these device?</p>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger reject_btn">Reject</button>
                </div>
            </div>
        </div>
    </div>
    <div id="myModalCancel" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Remove all spares before cancle this service</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalBill" tabindex="-1" role="dialog" aria-labelledby="myModalBillLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalBillLabel">BILLING</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @foreach ($get_complaints as $gc)
                    <?php
                        $complaint_description[] = $gc['description'];
                    ?>
                @endforeach
                <div class="modal-body">
                    <h4>Bill To : {{$service->customer_name}} </h4>
                    <table class="table table-condensed table-hover no-margin sf bill_table">
                        <tbody>
                            <tr>
                                <th>Particulars</th>
                                <th class="text-right">Quantity</th>
                                <th class="text-right" width="100">Amount</th>
                                <th class="text-right" width="100">Total</th>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    @if($get_complaints)
                                       {{ implode(', ', $complaint_description); }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>1. Service Charge</td>
                                <td class="text-right">1</td>
                                <td class="text-right">{{ number_format((float) $service->service_charge, 2, '.', '') }}
                                </td>
                                <td class="text-right">{{ number_format((float) $service->service_charge, 2, '.', '') }}
                                </td>
                            </tr>
                            <?php
                            $total = 0;
                            $i = 1;
                            ?>
                            @if (!empty($spares))
                                <tr>
                                    <th colspan="4" class=" bh">Spares </th>
                                </tr>
                                @foreach ($spares as $spare)
                                    <tr>
                                        <td>{{ $i.'. '.  $spare['sparename'] }} </td>
                                        <td class="text-right">{{ $spare['quantity'] }}</td>
                                        <td class="text-right">
                                            {{ number_format((float) $spare['cus_local_price'], 2, '.', '') }}</td>
                                        <td class="text-right">
                                            {{ number_format((float) ($spare['cus_local_price'] * $spare['quantity']), 2, '.', '') }}
                                        </td>
                                    </tr>
                                    <?php
                                    $total += $spare['cus_local_price'] * $spare['quantity'];
                                    $i++;
                                    ?>
                                @endforeach
                            @endif
                            <?php
                            // echo "<pre>"; print_r($service); echo "</pre>"; exit;
                            $country_id = DB::table('branch')
                                ->where(['id' => $service->branch_id])
                                ->first()->country_id;
                            $taxpercentage = DB::table('country')
                                ->where(['id' => $country_id])
                                ->first()->tax;
                            $total1 = ($total + $service->service_charge-$service->discount_local_price);
                            $tax = $total1 * ($taxpercentage / 100);
                            ?>
                             <tr>
                                <td colspan="3" class="text-right">DISCOUNT</td>
                                <td class="text-right">
                                    {{ number_format((float) ($service->discount_local_price), 2, '.', '') }}</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-right">TOTAL</td>
                                <td class="text-right">
                                    {{ number_format((float) ($total1), 2, '.', '') }}</td>
                            </tr>

                            <tr>
                                <td colspan="3" class="text-right text-danger"> Tax </td>
                                <td class="text-right text-danger">{{ $tax . ' (' . $taxpercentage . ' %)' }}</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-right">NET TOTAL</td>
                                <td class="text-right">
                                    {{ number_format((float) ($total1 + $tax), 2, '.', '') }}</td>
                            </tr>
                            <tr>
                                <td  class="text-right">Description</td>
                                <td class="text-right" colspan="3">
                                    <textarea class="form-control" name="work_description" id="work_description">{{trim($service->work_description)}}</textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    @if ($serviceBill > 0)
                        <button type="button" class="btn btn-md btn-primary float-right" id="regenerate_invoice">Generate
                            Invoice</button>
                    @else
                        <button type="button" class="btn btn-md btn-primary float-right" id="generate_invoice">Generate
                            Invoice</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="service_id" />
    <input type="hidden" id="status" />
    <input type="hidden" id="id" />
    <div class="modal fade" id="doaModal" tabindex="-1" role="dialog" aria-labelledby="doaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">ISSUE DOA</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="doa_area">
            <div class="alert alert-success alert-dismissible doa_msg hide">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> DOA details <strong>successfully created!</strong>
            </div>
        <form action="" method="post" id="doa_form" name="doa_form">
           {{ Form::hidden('imei_id', old('imei_id',(isset($service)) ? $service->imei_id:''), ['class' => 'form-control', 'id' => 'imei_id']) }}
           <div class="table-responsive">
            <table class="table table-bordered table-condensed doa_table">
            <tbody>
                <tr>
                <th class="success"> {{ Form::label('name', __('IMEI NO1')) }}</th>
                <td>{{$service->IMEI_no1}}</td>
                </tr>
                <tr>
                <th class="success"> {{ Form::label('name', __('IMEI NO2')) }}</th>
                <td>{{$service->IMEI_no2}}</td>
                </tr>
                <tr>
                <th class="success">Model No</th>
                <td>
                    {{ $service->model }}
                    ({{ $service->color }}
                    @if ($service->ram)
                        ,{{ $service->ram }}
                    @endif
                    @if ($service->rom)
                        ,{{ $service->rom }}
                    @endif
                    )
                </td>
                </tr>
                <tr>
                <th class="success">Status</th>
                <td>Sold</td>
                </tr>
                <tr>
                <th class="success">Remarks</th>
                <td>
                {{ Form::textarea('problem_remarks', '', ['class' => 'form-control', 'id' => 'problem_remarks']) }}
                </td>
                </tr>
                <tr>
                    <th width="25%" class="success">Check List</th>
                    <td>
                        @foreach ($access as $k => $v)
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="accessoriesdoa_{{ $k }}"
                                    value="{{ $k }}" name="device_check[]"
                                    class="custom-control-input form-control">
                                <label class="custom-control-label"
                                    for="accessoriesdoa_{{ $k }}">{{ $v }}</label>
                            </div>
                        @endforeach
                    </td>
                </tr>
                <tr>
                <th class="success">Special Request</th>
                <td>
                    <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="is_special_request" value="1" name="is_special_request">
                    <label class="custom-control-label" for="is_special_request"></label>
                    </div>
                </td>
                </tr>
                <tr>
                    <th class="success">Invoice Upload</th>
                    <td><input type="file" class="form-control" id="invoice_upload" name="invoice_upload"></td>
                </tr>
                <tr>
                    <th class="success">Images Upload</th>
                    <td><input type="file" class="form-control" id="image_upload" name="image_upload[]" multiple></td>
                </tr>
            </tbody>
            </table>
           </div>
            <hr>
            <input type="submit" class="btn btn-success pull-right imei_success" name="issue_doa" value="Register">
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    </div>
    </div>
    <div class="modal" tabindex="-1" id="confirmModal" >
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Confirm Message</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <p>No Spare Added</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" onClick="window.location.reload();">OK</button>
        </div>
        </div>
    </div>
    </div>
@endsection
@section('pagescript')
    <script src="{{ asset('js/select2/select2.min.js') }}"></script>
    <script src="{{ asset('js/select2/bloodhound.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/datepicker/daterangepicker.js') }}"></script>
    <!-- Bootstrap tagsinput -->
    <script src="{{ asset('js/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>

    <!-- tagsinput custom -->
    <script src="{{ asset('js/bootstrap-tagsinput/tagsinput.js') }}"></script>
    <script>
        $("#is_pcb").click(function() {
            if($('#is_pcb').is(':checked')) {
                $(".pcbcls").removeClass("hide");
            } else {
                $(".pcbcls").addClass("hide");
            }
        });
        $(".pcb_replace").click(function() {
                $("form[name='pcb_request']").validate({
                rules: {
                    pcb_serial_no: {
                        required: true,
                        remote: {
                            url: "{{ route('varifyPCBNo') }}",
                            type: "GET",
                            data: {
                                pcb_serial_no: function () {
                                    return $('#pcb_serial_no').val();
                                }
                            }
                        },
                    }
                },
                messages: {
                    pcb_serial_no:  {
                        required: "Please enter PCB serial no",
                        remote:"Sorry... PCB NO already taken"
                    }
                },
                submitHandler: function(form) {
                    var service_id = $("input[name='service_id']").val();
                    var formData = $('#pcb_request').serialize()+"&service_id="+service_id+"&table=service";
                    $.ajax({
                        type: "POST",
                        url: "{{ route('service.replace.pcbno') }}",
                        data: formData,
                        success: function(data) {
                            window.location.reload();
                        }
                    });
                }
            });
        });
        $("#generate_invoice").click(function() {
            var service_id = $("input[name='service_id']").val();
            var url = "{{ route('service.bill', ':id') }}";
            url = url.replace(':id', service_id);
            $.ajax({
                type: "GET",
                url: url, // where you wanna post
                success: function(data) {
                    window.location.reload();
                }
            });
        });
        $("#regenerate_invoice").click(function() {
            var service_id = $("input[name='service_id']").val();
            var url = "{{ route('service.bill.regenerate', ':id') }}";
            url = url.replace(':id', service_id);
            //alert($("#work_description").val());
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    'work_description': $("#work_description").val()
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });
        $("#service_cancel").click(function(event) {
            event.stopPropagation();
            if (confirm("Do you want to cancel the service?")) {
                var service_id = $("input[name='service_id']").val();
                var url = "{{ route('service.cancel', ':id') }}";
                url = url.replace(':id', service_id);
                $.ajax({
                    type: "GET",
                    url: url, // where you wanna post
                    success: function(data) {
                        window.location.replace(data);
                    }
                });
            }
            event.preventDefault();
        });
        $("#salereq_resend").click(function() {
            var service_id = $("input[name='service_id']").val();

            $.ajax({
                type: "POST",
                url: '{{ route('salerequest.resend') }}', // where you wanna post
                data: {
                    'service': service_id
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });
        $(".complaint-remove").click(function() {
            var complaint_id = $(this).attr("data-id");
            $.ajax({
                type: "POST",
                url: '{{ route('remove.complaint') }}', // where you wanna post
                data: {
                    'complaint': complaint_id
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });

        $(".spare-remove").click(function() {
            var spare_id = $(this).attr("data-id");

            $.ajax({
                type: "POST",
                url: '{{ route('remove.service.spare') }}', // where you wanna post
                data: {
                    'spare': spare_id
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });

        $("#finish").click(function() {
            var service_id = $("input[name='service_id']").val();

            $.ajax({
                type: "POST",
                url: '{{ route('service.status') }}', // where you wanna post
                data: {
                    'service': service_id
                },
                success: function(data) {
                    if (data == 2) {
                        $("#finish-err-msg").html("Can't finish this work.");
                        $("#finish-err").removeClass('d-none');
                        return false;
                    } else if(data == 0) {
                        var modal = $("#confirmModal");
                        modal.modal("show");
                    } else {
                        window.location.reload();
                    }
                }
            });
        });

        $("#service_charge").click(function() {
            var service_charge = $("#servicecharge").val();
            var discount  = $("#discount").val();
            var service_id = $("input[name='service_id']").val();
            if (service_charge == '') {
                $("#service-charge-error").html("Please enter service charge");
                return false;
            }

            $.ajax({
                type: "POST",
                url: '{{ route('service_charge.add') }}', // where you wanna post
                data: {
                    'service_charge': service_charge,
                    'discount': discount,
                    'service': service_id
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });

        $(document).ready(function() {
            $('#spare').val(null).trigger("change");
            var spare = $("#spare").val();

            if (spare>0)getSparePrice(spare);

        });

        $("#spare").change(function() {
            var spare = $("#spare").val();
            if (spare>0)getSparePrice(spare);
        });

        function getSparePrice(spare) {

            $.ajax({
                type: "POST",
                url: '{{ route('get.price.spare') }}', // where you wanna post
                data: {
                    'spare': spare,
                    'service_id': $("input[name='service_id']").val(),
                    'table':'service'
                },
                success: function(data) {
                    //alert(data);
                    if (data == 1) {
                        $("#spare_price").val(0.00);
                        $("#spare_add").hide();
                        $("#err-msg").html("Spare quantity not available");
                        $("#err-msg").removeClass('d-none');
                    } else {
                        $("#spare_price").val(data);
                        $("#spare_add").show();
                        $("#err-msg").html('');
                        $("#err-msg").addClass('d-none');
                    }
                }
            });
        }
        $(".edit-status").click(function() {
            var id = $(this).data('id');
            $("#update-status" + id).show();
            $("#comp_status" + id).hide();
        });

        $(".status_update").click(function() {
            var service_id = $("input[name='service_id']").val();
            var service_comp_id = $(this).data('id');
            var status = $(this).data('status');
            $.ajax({
                type: "POST",
                url: '{{ route('service.complaint.status') }}', // where you wanna post
                data: {
                    'ref': service_comp_id,
                    'sta': status,
                    'id': service_id
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        });

        $('#is_warranty').change(function() {
            if (this.checked) {
                $("#customer-price").hide();
            } else {
                $("#customer-price").show();
            }
        });
        $("#spare_add").click(function() {
            var spare_price = $("#spare_price").val();
            var service_id = $("input[name='service_id']").val();
            var qty = $("#spare_qty").val();

            if ($('#is_warranty').prop('checked')) {
                var is_warranty = 1;
                var spare_cus_price = 0.00;
            } else {
                var is_warranty = 0;
                var spare_cus_price = $("#spare_cus_price").val();
                if (spare_cus_price == '') {
                    $("#spare-cusprice-error").html("Please enter customer price");
                    return false;
                }
            }
            var spare = $("#spare option:selected").val();

            if (spare_price == '') {
                $("#spare-price-error").html("Please enter spare price");
                return false;
            }

            $.ajax({
                type: "POST",
                url: '{{ route('service.spare.add') }}', // where you wanna post
                data: {
                    'service': service_id,
                    'spare': spare,
                    'price': spare_price,
                    'cus_price': spare_cus_price,
                    'qty': qty,
                    'warranty': is_warranty
                },
                success: function(data) {
                    if (data == 'NA') {
                        $("#err-msg").html("Spare quantity not available");
                        $("#err-msg").removeClass('d-none');
                        return false;
                    } else {
                        window.location.reload();
                    }
                }
            });
        });

        $("#complaints_add").click(function() {
            var formData = $("#service-complaint").serialize() + '&_token=' + $('meta[name="csrf-token"]').attr(
                'content');
            $.ajax({
                type: "POST",
                url: '{{ route('service.complaint.add') }}', // where you wanna post
                data: formData,
                success: function(data) {
                    window.location.reload();
                }
            });

        });
        $(document).ready(function() {
            $(".edit_cust").click(function(e) {
                $(".cust_val, .cust_edit").toggle();
            });
            $(".edit_dealer").click(function(e) {
                $(".dealer_val, .dealer_edit").toggle();
            });
            $(".edit_salesman").click(function(e) {
                $(".salesman_val, .salesman_edit").toggle();
            });

            $("form[name='update_cus']").validate({
                rules: {
                    customer_name: "required",
                    customer_phone: {
                        required: true,
                        number: true,
                        remote: {
                            url: "{{ route('customer.checkPhone') }}",
                            type: "post",
                            data: {
                                customer_phone: function() {
                                    return $("#customer_phone").val();
                                },
                                customer_id: function() {
                                    return $("input[name='customer_id']").val();
                                }
                            }
                        }
                    },
                    customer_email: {
                        required: true,
                        email: true,
                        remote:{
                            url: "{{ route('customer.checkEmail') }}",
                            type: "post",
                            data: {
                                customer_phone: function() {
                                    return $("#customer_email").val();
                                },
                                customer_id: function() {
                                    return $("input[name='customer_id']").val();
                                }
                            }
                        }
                    },
                    dealer_name: "required",
                    dealer_phone: "required",
                },
                messages: {
                    customer_name: "Please enter name",
                    customer_phone: {
                        required: "Please enter phone no",
                        number: "Please valid phone no",
                        remote: "Phone no already in use!"

                    },
                    customer_email:
                    {
                        required: "Please enter a valid email address",
                        remote: "Email id already in use!"
                    },
                    dealer_name: "Please enter dealer name",
                    dealer_phone: "Please enter dealer phone no",
                },
                submitHandler: function(form) {
                    var formData = $(form).serialize() + '&_token=' + $('meta[name="csrf-token"]').attr(
                        'content');
                    $.ajax({
                        type: "POST",
                        url: '{{ route('customer.update') }}', // where you wanna post
                        data: formData,
                        error: function(jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage); // Optional
                        },
                        success: function(data) {

                            console.log(data)
                        }
                    });
                }
            });

        });
        $('.img_click').on('click', function() {
            $('#delete_pic').modal('show');
        });
        $('#delete').on('click', function() {
            $('#imgs').hide();
            $('#imgs-msg').hide();
            $('#msg-delete').show();

        });
        $('.btn-edit').on('click', function() {
            $('.txt_brand_id').attr("disabled", false);
            $('.txt_model_id').attr("disabled", false);
            $('#is_warranty').attr("disabled", false);
        });
        $(".delete-image").on('click', function() {
            var id = $(this).attr("data-id");
            var type = $(this).attr("data-type");
            var formData = 'id=' + id + '&type=' + type + '&_token=' + $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST",
                url: "{{ route('service.image.delete') }}",
                data: formData,
                success: function(data) {
                    $(".delete_" + type + "_" + id).hide();
                }
            });
        });

        $(".transfer_type").on('click', function() {
            var service_id = $(this).attr("data-service-id");
            var id = $(this).attr("data-id");
            var type = $(this).attr("data-type");
            var formData = 'id=' + id + '&type=' + type + '&service_id=' + service_id + '&_token=' + $(
                'meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST",
                url: "{{ route('servicetransfer.type') }}",
                data: formData,
                success: function(data) {
                    if (data == 2) {
                        $("#return-err-msg").html("Can't return this work.");
                        $("#return-err").removeClass('d-none');
                        return false;
                    } else {
                        window.location.reload();
                    }
                }
            });
        });
        var _validator = $("form[name='create_transfer']").validate({
            rules: {
                branch_id: "required",
                transfer_remarks: "required"
            },
            messages: {
                branch_id: "Please select branch",
                transfer_remarks: "Please enter remarks"
            },
            submitHandler: function(form) {
                var formData = $(form).serialize() + '&_token=' + $('meta[name="csrf-token"]').attr('content') +
                    '&service_id=' + $("input[name='service_id']").val();
                $.ajax({
                    type: "PUT",
                    url: "{{ route('service.transfer') }}",
                    data: formData,
                    success: function(data) {
                        $(".transfer_msg").slideDown();
                        setTimeout(function() {
                            $('#myModalTransfer').modal('hide')
                        }, 4000);
                        window.location.reload();
                    }
                });
            }
        });

        function changeAction(status, service_id, transfer_id) {
            $("#status").val(status);
            $("#service_id").val(service_id);
            $("#id").val(transfer_id);
            if (status == 1) {
                $("#myModalTechnician").modal();
            } else if (status == 2) {
                $("#myModalReject").modal();
            } else {
                assign();
            }
        }
        $(".assign_btn").click(function() {
            var _validator = $("form[name='assign_tech']").validate({
                rules: {
                    technician_id: "required"
                },
                messages: {
                    technician_id: "Please select technician"
                },
                submitHandler: function(form) {
                    assign();
                }
            });
        });
        $(".reject_btn").click(function() {
            assign();
        });

        function assign() {
            var status = $("#status").val();
            var service_id = $("#service_id").val();
            var id = $("#id").val();
            var formData = $('#assign_tech').serialize() +
                '&_token=' + $('meta[name="csrf-token"]').attr('content') + '&service_id=' + service_id + '&id=' + id +
                '&status=' + status;
            $.ajax({
                type: "POST",
                url: "{{ route('servicetransfer.status') }}",
                data: formData,
                success: function(data) {
                    if (status == 1) {
                        $(".technician_msg").slideDown();
                        setTimeout(function() {
                            $('#myModalTechnician').modal('hide');
                            window.location.reload();
                        }, 4000);
                    } else {
                        window.location.reload();
                    }
                }
            });
        }
        $(".level_update").click(function() {
            var service_id = $("input[name='service_id']").val();
            var formData = $('#level_update').serialize() +"&service_id="+service_id+"&service_type=1&table=service";
            $("form[name='level_update']").validate({
                rules: {
                    level_id: "required"
                },
                messages: {
                    level_id: "Please select level"
                },
                submitHandler: function(form) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('service.appeal') }}",
                        data: formData,
                        success: function(data) {
                            window.location.href = '/service/list?status=5';
                        }
                    });
                }
            });

        });
        $(".appeal_send").click(function() {
            var service_id = $("input[name='service_id']").val();
            $("form[name='appeal_request']").validate({
                rules: {
                    appeal_remarks: "required"
                },
                messages: {
                    appeal_remarks: "Please enter remarks"
                },
                submitHandler: function(form) {
                    var formData = $('#appeal_request').serialize() + "&service_id="+service_id+"&service_type=2&table=service";
                    $.ajax({
                        type: "POST",
                        url: "{{ route('service.appeal') }}",
                        data: formData,
                        success: function(data) {
                            //window.location.reload();
                            window.location.href= '/service/list?status=5';
                        }
                    });
                }
            });
        });
        $(".level").change(function() {
            $("#rate").val('');
            if($(this).val()>0){
                $.ajax({
                type: "GET",
                url: "/service/level/rate/" +$(this).val()+'/'+ $("input[name='service_id']").val(),
                    success: function(data) {
                        $("#rate").val(data);
                    }
                });
            }
        });
        $('.complaint_description').on('change', function() {
            var formData = 'service_id=' + $("input[name='service_id']").val() + '&complaint_id=' + $(this).attr('data-id') +'&remarks=' + this.value;
            //alert(formData);
            $.ajax({
                type: "POST",
                url: "{{ route('service.compliant.description') }}",
                data: formData,
                success: function(data) {
                    window.location.reload();
                }
            });
        });
        $(".commentclose").click(function() {
            var type = $(this).attr("data-type");
            var formData =  'type=' + type +'&comment_id='+$("#comment_id").val();
            $.ajax({
                type: "POST",
                url: "/service/update/appel",
                data: formData,
                success: function(data) {
                    window.location.href= "/"+data;
                }
            });
        });
    </script>
@endsection

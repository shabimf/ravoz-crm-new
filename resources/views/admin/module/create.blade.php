@extends('layouts.app')

@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                    <h4 class="card-title-text">Module</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('module') }}"> <i
                            class="feather icon-list"></i> Module List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($details, ['route' => ['module.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-company']) }}
            @else
                {{ Form::open(['route' => 'module.store','class' => 'form-horizontal','role' => 'form','method' => 'post','id' => 'create-company', 'auto-complete' => 'off']) }}
            @endif
            @csrf
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('name', __('Name')) }}
                    {{ Form::text('name', old('name',(isset($details)) ? $details->name:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
            </div>
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection


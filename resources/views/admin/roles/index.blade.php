@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
                <div class="add-new">
                    <a class="btn btn-primary" href="{{ route('role.create') }}"> <i class="feather icon-plus"></i> Create Role</a>
                </div>
            </div>
        </div>
        <div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-title">
						<div class="card-title-left">
							<h4 class="card-title-text">Role Management</h4>
						</div>
					</div>
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="roles-table" class="table table-striped table-bordered" data-ajax_url="{{ route('role.get') }}">
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <th>Role</th>
					                <th>Module</th>
					                <th>Actions</th>
                                    <th>Status</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('pagescript')
@include('inc.datatables-js')
<script>
    FTX.Utils.documentReady(function() {
        FTX.Roles.list.init();
    });
</script>
@endsection

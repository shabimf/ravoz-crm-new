@extends('layouts.warranty')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Sales List</h4>
            </div>
			<div class="col-md-3 {{(userHasPermission('Warranty','sales_create')?'':'hide')}}">
				<div class="add-new">
				  <a class="btn btn-primary" href="{{ route('sale.create') }}">
					<i class="feather icon-plus"></i> Sale
				  </a>
				</div>
            </div>

        </div>
        <div class="row">

			<div class="col-lg-12">
			    {{ Form::open(['route' => 'sale','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-sale']) }}
					<div class="form-row">
						<div class="form-group col-md-2">
							{{ Form::label('name', __('From Date')) }}
							{{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control', 'required' => 'required','id'=>'datepicker' ]) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('To Date')) }}
							{{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control', 'required' => 'required' , 'id' => 'dates']) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Country')) }}
							{{ Form::select('country_id', $country, old('country_id',(isset($country_id)) ? $country_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Country']) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Model')) }}
							{{ Form::select('model_id', $model, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model']) }}
						</div>
						<div class="form-group col-md-2">
						  <button class="btn btn-primary search-btn">Search</button>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="sale-table" class="table  table-bordered" data-ajax_url="{{ route("sale.get") }}">
					        <thead>
					            <tr>
								    <th width="5">Sl</th>
					                <th width="80">Sale Date</th>

					                <th>Country</th>
									<th>Act.Date</th>
									<th>IMEI-1</th>
									<th>IMEI-2</th>
									<th>Invoice</th>
									<th>Model</th>
									<th>User</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
$(function() {
	$( 'input[name="from"]').daterangepicker( {
		singleDatePicker: true,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	});
	$( 'input[name="to"]').daterangepicker( {
		singleDatePicker: true,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	});
});
var country_id = getUrlParameter('country_id');
var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
var model_id =  getUrlParameter('model_id');
FTX.Utils.documentReady(function() {
    FTX.SALE.list.init(country_id,model_id,from_date,to_date);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

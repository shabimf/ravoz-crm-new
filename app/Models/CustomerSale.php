<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class CustomerSale extends Model
{
    use HasFactory;
    protected $table = "customer_sales";  
    public $timestamps = false;
    protected $guarded = array();
}

<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use App\Http\Traits\CommonTrait;

class ConsumptionExport implements FromArray, WithHeadings, WithMapping
{
    protected $from,$to,$country_id,$branch_id,$model_id,$branch_ids;

    use CommonTrait;

    function __construct($from, $to, $country_id, $branch_id ,$model_id,$branch_ids) {

        $this->data['from'] = $from;
        $this->data['to'] = $to;
        $this->data['country_id'] = $country_id;
        $this->data['branch_id'] = $branch_id;
        $this->data['model_id'] = $model_id;
        $this->data['branch_ids'] = $branch_ids;
    }

    public function array(): array
    {
        return $this->getConsumptionTableData($this->data);
    }

    public function headings(): array
    {
        return [
            'SL NO',
            'MODEL',
            'SPARE NAME',
            'TOTAL DISPATCH',
            'WARRANTY QUANTITY',
            'NONWARRANTY QUANTITY',
            'SALE QUANTITY',
            'CONSUMTION QUANTITY',
            'BALANCE'
        ];
    }
    
    public function map($consumption): array
    {
        $con_qty = ($consumption->warranty_count+$consumption->nonwarranty_count+$consumption->sale_count);
        $balance = ($consumption->purchase_count-$con_qty);
        return [
            $consumption->number,
            ($consumption->model_name?$consumption->model_name:"Others"),
            $consumption->spare_name,
            ($consumption->purchase_count??"0"),
            ($consumption->warranty_count??"0"),
            ($consumption->nonwarranty_count??"0"),
            ($consumption->sale_count??"0"),
            ($con_qty??"0"),
            ($balance??"0"),
        ];
    }
}

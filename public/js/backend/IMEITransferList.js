(function () {
    var i = 1;
    FTX.IMEITransferList = {

        list: {

            selectors: {
                IMEITransfer_table: $('#IMEITransfer-table'),
            },

            init: function (from,to) {
                this.selectors.IMEITransfer_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.IMEITransfer_table.data('ajax_url'),
                        type: 'post',
                        data: function (d) {
                            d.from = from,
                            d.to = to
                        },
                    },
                    columns: [
                        {
                            "data": 'DT_RowIndex',
                            orderable: false, 
                            searchable: false
                        },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'from_country_name', name: 'from_country_name' },
                        { data: 'to_country_name', name: 'to_country_name' },
                        { data: 'brand_name', name: 'brand_name' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },

                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,
                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();

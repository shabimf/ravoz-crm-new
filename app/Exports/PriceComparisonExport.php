<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use App\Http\Traits\CommonTrait;

use Session;

class PriceComparisonExport implements FromArray,WithHeadings,WithCustomStartCell,WithEvents
{
    use CommonTrait;
    protected $branch_ids;

    function __construct($branch_ids) {
        $this->data['branch_ids'] = $branch_ids;
    }

    public function array(): array
    {
        return $this->getPriceComparisonTableData($this->data);
    }
    public function headings(): array
    {
        $branch_Arr = $this->getBranches(Session::get('sessi_branch_id'));
        $i=0;
        foreach($branch_Arr as $val) {
            $fields[] = "QUANTITY"; 
            $fields[] = "PRICE";
            if($i == 0) {
                $header0[] = " ";
                $header0[] = " ";
            }
            $header0[] = $val;
            $header0[] = " ";
            $i++;
        }
        $header1 = [
            'FOCUS CODE',
            'PRODUCT DESCRIPTION'
        ]; 
        //dd($header0);
        $header = array_merge($header1,$fields);
        return [$header0,$header];  
    }
    public function startCell(): string
    {
        return 'A1';
    }

    public function registerEvents(): array {
        
        return [
            AfterSheet::class => function(AfterSheet $event) {
                /** @var Sheet $sheet */
                $sheet = $event->sheet;
                
                $sheet->mergeCells('C1:D1');


                $sheet->mergeCells('E1:F1');

                
                $styleArray = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ];
                
                $cellRange = 'C1:F1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);
            },
        ];
    }
    


}

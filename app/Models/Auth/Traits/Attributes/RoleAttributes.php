<?php

namespace App\Models\Auth\Traits\Attributes;

trait RoleAttributes
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        //if (access()->allow('edit-role')) {
            return '<a class="btn btn-flat btn-primary btn-sm" href="'.route('role.edit', $this).'">
                    <i data-toggle="tooltip" data-placement="top" title="Edit" class="feather icon-edit"></i>
                </a>';
        //}
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        //Can't delete master admin role
        //if ($this->id != 1 && access()->allow('delete-role')) {
            return '<a class="btn btn-flat btn-danger btn-sm" href="'.route('role.destroy', $this).'" data-method="delete"
                        data-trans-button-cancel="Cancel"
                        data-trans-button-confirm="Delete"
                        data-trans-title="Are you sure you want to do this?">
                            <i data-toggle="tooltip" data-placement="top" title="Delete" class="feather icon-trash"></i>
                    </a>';
       // }

        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                    '.$this->getEditButtonAttribute('edit-role', 'role.edit').'
                    '.$this->getDeleteButtonAttribute().'
                </div>';
    }
}

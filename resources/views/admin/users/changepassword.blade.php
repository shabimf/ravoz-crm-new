@extends('layouts.app')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-6">
                <h4 class="card-title-text">Change Password </h4>
            </div>
            <div class="col-md-6">
                <div class="add-new">
                    <a class="btn btn-primary" href="{{ route('user') }}"> <i class="feather icon-list"></i> User List</a>
                </div>
            </div>
        </div>

        @include('inc.alert')
        {{ Form::open(['route' => ['user.password.update', $user_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'update-password']) }}
        @csrf
        <div class="row">
            <div class="col form-group">
                {{ Form::label('name', __('New Password')) }}
                {{ Form::text('new_password', old('new_password'), ['class' => 'form-control','required' => 'required']) }}
            </div>
            <div class="col form-group">
                {{ Form::label('name', __('New Confirm Password')) }}
                {{ Form::text('new_confirm_password', old('new_confirm_password'), ['class' => 'form-control','required' => 'required']) }}
            </div>
        </div>
        <button type="submit" class="btn btn-success float-right">Update</button>
        {{ Form::close() }}
    </div>
</div>
@endsection

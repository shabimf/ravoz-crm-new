<?php
namespace App\Http\Controllers\Backend\Sales;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\Imports\ImportSalesReturn;
use Illuminate\Support\Facades\View;
use App\Models\CustomerSale;
use DB;
use Auth;

class SalesReturnController extends Controller
{
    public function __construct()
    {
        $this->data['country'] = $this->NameAndID('country');
        $this->data['model'] = $this->NameAndID('models');
        View::share('js', ['sales-return','master']);
    }

    public function index(Request $request)
    {
        $this->data['country_id'] = ($request->country_id?$request->country_id:0);
        $this->data['model_id'] = ($request->model_id?$request->model_id:0);
        $this->data['from'] = ($request->from?$request->from:date('01-m-Y'));
        $this->data['to'] = ($request->to?$request->to:date('t-m-Y'));
        return view('admin.sales-return.index', $this->data);
    }

    public function getSaleReturnData(Request $request) {
        $data = CustomerSale::leftJoin('imei_details', 'customer_sales.imei_id', '=', 'imei_details.id')
                    ->leftJoin('users', 'customer_sales.return_by', '=', 'users.id')
                    ->leftJoin('imei','imei.id','=','imei_details.group_id')
                    ->leftJoin('country', 'imei_details.country_id', '=', 'country.id')
                    ->leftJoin('variant_details', 'imei_details.variant_id', '=', 'variant_details.id')
                    ->leftJoin('models', 'variant_details.model_id', '=', 'models.id')
                    ->select(['imei_details.id','imei_details.IMEI_no1','country.name as country_name','imei_details.IMEI_no2','imei_details.serial_no','customer_sales.return_date','customer_sales.invoice_no','models.name as model_name','imei_details.activation_date','users.first_name as user_name'])
                    ->groupBy('customer_sales.imei_id')->whereNotNull('customer_sales.return_date');
                    if (!empty($request->country_id)) {
                        $data->where("imei_details.country_id", $request->country_id);
                    }
                    if (!empty($request->model_id)) {
                    $data->where("models.id", $request->model_id);
                    }

                    $from = ($request->from?date('Y-m-d',strtotime($request->from)):date('Y-m-01'));
                    $to = ($request->to?date('Y-m-d',strtotime($request->to)):date('Y-m-t'));
                    $data->where('customer_sales.status', '=', 1);
                    $data->whereBetween('customer_sales.return_date', [$from, $to]);
                    $data->get();
                    return Datatables::of($data)->escapeColumns(['IMEI_no1','country_name'])
                    ->filterColumn('IMEI_no1', function ($query, $keyword) {
                        $query->whereRaw('imei_details.IMEI_no1 like ?', ["%{$keyword}%"]);
                    })
                    ->filterColumn('IMEI_no2', function ($query, $keyword) {
                        $query->whereRaw('imei_details.IMEI_no2 like ?', ["%{$keyword}%"]);
                    })
                    ->filterColumn('serial_no', function ($query, $keyword) {
                        $query->whereRaw('imei_details.serial_no like ?', ["%{$keyword}%"]);
                    })
                    ->filterColumn('country_name', function ($query, $keyword) {
                        $query->whereRaw('country.name like ?', ["%{$keyword}%"]);
                    })
                    ->filterColumn('model_name', function ($query, $keyword) {
                        $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
                    })
                    ->filterColumn('user_name', function ($query, $keyword) {
                        $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
                    })
                    ->filterColumn('return_date', function ($query, $keyword) {
                        $query->whereRaw(DB::raw("(DATE_FORMAT(customer_sales.return_date,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
                    })->filterColumn('activation_date', function ($query, $keyword) {
                        $query->whereRaw(DB::raw("(DATE_FORMAT(imei_details.activation_date,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
                    })
                    ->editColumn('return_date', function ($data)
                    {
                    return date('d-m-Y', strtotime($data->return_date) );
                    })->editColumn('activation_date', function ($data)
                    {
                    if($data->activation_date) {
                        return date('d-m-Y', strtotime($data->activation_date) );
                    }
                    })->addIndexColumn()->make(true);
    }

    public function create()
    {
        return view('admin.sales-return.create', $this->data);
    }

    public function store(Request $request)
    {
        $this->validate($request, ['file' => 'required|file|mimes:xls,xlsx,csv|max:10240']);
        if ($request->file('file')->isValid())
        {
            DB::beginTransaction();
            Excel::import(new ImportSalesReturn, request()->file('file'));
            $data = Excel::toCollection(new ImportSalesReturn() , $request->file('file'));
            // echo "<pre>"; print_r($data); echo "</pre>"; exit;
            $imei_arr = $im_d = $error = $date_valid = $im_ex = $im_sale =  $im_doa_ex = [];
            if (count($data) > 0)
            {   $record_count = 0;
                foreach ($data[0] as $key => $value)
                {

                    if (in_array($value['imeisn'], $imei_arr))
                    {
                        $im_d[] = $value['imeisn'];
                    }

                    $imei_check = DB::table('imei_details')->where('IMEI_no1', '=', $value['imeisn'])->orWhere('IMEI_no2', '=', $value['imeisn'])->orWhere('serial_no', '=', trim($value['imeisn']))->first();

                    $imei_id = ($imei_check ? $imei_check->id : 0);
                    $is_doa  = ($imei_check ? $imei_check->is_doa : 1);

                    if ($imei_id == 0) {
                        $im_ex[] = $value['imeisn'];

                    } elseif($is_doa == 1) {
                        $im_doa_ex[] = $value['imeisn'];
                    } else {
                        $expire_exist = DB::table('imei_details')->where('id', $imei_id)->whereNull('expiry_date')->first();
                        if($expire_exist) {
                          $im_sale[] = $value['imeisn'];
                        }
                    }

                    $return_date = date('Y-m-d');
                    if (count($im_d) == 0  && count($im_ex) == 0 && count($im_sale) == 0) {
                        $last = DB::table('customer_sales')->where('imei_id', $imei_id)->orderBy('id','desc')->first();
                        if ($last) {
                            DB::table('customer_sales')->where('id', $last->id)->update(['status' => 1,'return_date' => $return_date,'return_by' => Auth::id()]);
                        }
                        DB::table('imei_details')->where('id', $imei_id)->update(['sales_date' => NULL,'expiry_date' => NULL,'activation_date' => NULL,'activated_by' => NULL,'sale_id' => NULL]);
                        $record_count = $record_count+1;
                    }


                }
            }
            if (count($im_d) > 0)
            {
                $error[] = "Duplication entry of IMEI NO / Serial NO " . implode(',', array_unique($im_d));
            }
            if (count($im_ex) > 0)
            {
                $error[] = "Not exists IMEI NO / Serial NO " . implode(',', array_unique($im_ex));
            }
            if (count($im_doa_ex) > 0)
            {
                $error[] = "This IMEI / Serial nos already exists in DOA list:  " . implode(',', array_unique($im_doa_ex));
            }
            if (count($im_sale) > 0)
            {
                $error[] = "Not activated this IMEI / Serial Nos " . implode(',', array_unique($im_sale));
            }
            if (count($error) > 0)
            {
                return redirect()->back()->with("info", array_unique($error));
            }
            else
            {
                DB::commit();
                return redirect()->back()->with("success", $record_count." Records Uploaded Successfully!");
            }
        }
    }

    public function activation(Request $request) {
        return view('admin.sales-return.activation', $this->data);
    }

    public function activationStore(Request $request) {

        $error = $im_ex = $im_sale =  $im_doa_ex = [];
        $imei_check = DB::table('imei_details')->where('IMEI_no1', '=', $request->imei)->orWhere('IMEI_no2', '=', $request->imei)->orWhere('serial_no', '=', $request->imei)->first();
        $imei_id = ($imei_check ? $imei_check->id : 0);
        $is_doa  = ($imei_check ? $imei_check->is_doa : 1);
        if ($imei_id == 0) {
            $im_ex[] = $request->imei;

        } elseif($is_doa == 1) {
            $im_doa_ex[] = $request->imei;
        } else {
            $expire_exist = DB::table('imei_details')->where('id', $imei_id)->whereNull('expiry_date')->first();
            if($expire_exist) {
              $im_sale[] = $request->imei;
            }
        }

        $return_date = date('Y-m-d');
        if (count($im_ex) == 0 && count($im_sale) == 0) {
            $last = DB::table('customer_sales')->where('imei_id', $imei_id)->orderBy('id','desc')->first();
            if ($last) {
                DB::table('customer_sales')->where('id', $last->id)->update(['status' => 1,'return_date' => $return_date,'return_by' => Auth::id()]);
            }
            DB::table('imei_details')->where('id', $imei_id)->update(['sales_date' => NULL,'expiry_date' => NULL,'activation_date' => NULL,'activated_by' => NULL,'sale_id' => NULL]);
        }

        if (count($im_ex) > 0)
        {
            $error[] = "Not exists IMEI NO / Serial NO " . implode(',', array_unique($im_ex));
        }
        if (count($im_doa_ex) > 0)
        {
            $error[] = "This IMEI / Serial nos already exists in DOA list:  " . implode(',', array_unique($im_doa_ex));
        }
        if (count($im_sale) > 0)
        {
            $error[] = "Not activated this IMEI / Serial Nos " . implode(',', array_unique($im_sale));
        }
        if (count($error) > 0)
        {
            return redirect()->back()->with("info", array_unique($error));
        }
        else
        {
            DB::commit();
            return redirect()->back()->with("success","Sales return successfully!");
        }
    }
}

<?php
namespace App\Http\Controllers\Backend\Stock;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use DB;
use Auth;
use Session;

class TransferController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['models'] = $this->NameAndID('models');
            $this->data['branch'] = $this->NameAndID('branch');
            $this->data['spare'] = $this->GetSpareData();
            $this->data['from_branch'] = $this->getBranches(Session::get('sessi_branch_id'));
            return $next($request);
        });

        View::share('js', ['transfer','master']);
    }

    public function index(Request $request)
    {
        $this->data['branch_id'] = ($request->branch_id?$request->branch_id:0);
        $this->data['from'] = ($request->from?$request->from:date('01-m-Y'));
        $this->data['to'] = ($request->to?$request->to:date('t-m-Y'));
        return view('admin.transfer.index', $this->data);
    }

    public function getStockTransferData(Request $request) {

        $data = DB::table('transfer')
                ->leftJoin('users', 'users.id', '=', 'transfer.created_by')
                ->leftJoin('branch as from', 'from.id', '=', 'transfer.from_branch')
                ->leftJoin('branch as to', 'to.id', '=', 'transfer.to_branch')
                ->select(['transfer.id', 'transfer.date','transfer.serial_no','transfer.to_branch as to_branch_id','from.name as from_branch','to.name as to_branch','users.first_name','transfer.status'
                ]);

        if (!empty($request->branch_id)) {
            $data->where("transfer.to_branch", $request->branch_id);
        } else {
            if (Session::get('sessi_branch_id')) {
                $data->orWhereIn('transfer.from_branch', explode(',',Session::get('sessi_branch_id')));
                $data->orWhereIn('transfer.to_branch', explode(',',Session::get('sessi_branch_id')));
            }
        }
        $from = ($request->from?date('Y-m-d',strtotime($request->from)):date('Y-m-01'));
        $to = ($request->to?date('Y-m-d',strtotime($request->to)):date('Y-m-t'));

        $data->whereBetween('transfer.date', [$from, $to]);
        
        //$data->orderBy('transfer.id', 'DESC')->get();
        return DataTables::of($data)->escapeColumns(['from_branch'])
        ->filterColumn('date', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(transfer.date,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
        })->filterColumn('from_branch', function ($query, $keyword) {
            $query->whereRaw('from.name like ?', ["%{$keyword}%"]);
        })->filterColumn('to_branch', function ($query, $keyword) {
            $query->whereRaw('to.name like ?', ["%{$keyword}%"]);
        })->filterColumn('first_name', function ($query, $keyword) {
            $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
        })->filterColumn('serial_no', function ($query, $keyword) {
            $query->whereRaw('transfer.serial_no like ?', ["%{$keyword}%"]);
        })->addColumn('actions', function ($data) {
            if($data->status == 1) {
                $viewtxt = '<i class="feather icon-eye"></i>';
            } elseif ($data->to_branch_id != Auth::user()->branch_id) { 
                $viewtxt = '<i class="feather icon-eye"></i>';
            }
            else {
                $viewtxt = "Convert To Purchase";
            }
            $view = '<a  href="' .url('service/sales/transfer/'.$data->id.'/edit'). '" class="btn btn-info btn-sm">'.$viewtxt.'</a>';
            return $view;
        })->addColumn('status', function ($data) {
            if($data->status == 1) {
                $status = '<button type="button" class="btn btn-sm btn-success">Completed</button>';
            } else {
                $status = '<button type="button" class="btn btn-sm btn-warning">Pending</button>';
            }
            return $status;
        })->editColumn('date', function ($data)
        {
        return date('d-m-Y', strtotime($data->date) );
        })->addIndexColumn()->make(true);
    }

    public function create()
    {
        return view('admin.transfer.create', $this->data);
    }

    public function store(Request $request)
    {

        $currency = $this->GetCurrencyRate($request->branch_id);
        $currency_rate = ($currency[0] ? $currency[0]->rate : 0);

        $validator = $this->validateStockForm($request, 0);
        if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);
        $spare_arr = $error = [];
        DB::beginTransaction();
        $serial_no = $this->serialNumber('transfer',$request->from_branch);
        $id = DB::table('transfer')->insertGetId(['from_branch' => $request->from_branch,'to_branch' => $request->branch_id, 'date' => date('Y-m-d', strtotime($request->date)), 'created_by' => Auth::id(), 'status' => 0, 'serial_no'=> $serial_no]);
        foreach ($request->addmore as $key => $value) {
            $usd_price = $this->GetAverageCost($request->from_branch, $value['spare_id']);
            $usd_price = ($usd_price?$usd_price->price:0);
            
            $local_price = round($usd_price*$currency_rate);

            DB::table('transfer_items')->insertGetId(['parent_id' => $id, 'spare_id' => $value['spare_id'], 'qty' => $value['qty'],'price' => $usd_price, 'local_price' => $local_price]);

            if(isset($spare_arr[$value['spare_id']])) {
                $spare_arr[$value['spare_id']] = (int)$spare_arr[$value['spare_id']] + $value['qty'];
            } else {
                $spare_arr[$value['spare_id']] = (int)$value['qty'];
            }
            $current_stock = $this->getAvailableStock($request->from_branch,$value['spare_id']);

            if ($spare_arr[$value['spare_id']] > $current_stock) {
                $error [] = "This Spare (".$this->FieldByID('spare', 'spare_name', $value['spare_id']).") available stock is ".$current_stock;
            }
            DB::table('stock')->where('branch_id', $request->from_branch)->where('spare_id', $value['spare_id'])->update(['qty' => DB::raw('qty - '.$value['qty']), 'updated_by' => Auth::id()]);
        }

        if (count($error) > 0)
        {
            return redirect()->back()
                ->with("info", array_unique($error));
        }
        else
        {
            DB::commit();
            return redirect()->back()->with("success", "Stock transfer added successfully.");
        }

    }

    public function edit($id)
    {
        $this->data['edit_id'] = $id;
        $this->data['list'] = $this->viewItems("transfer",$id);
        return view('admin.transfer.view', $this->data);
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $items = $this->viewItems("transfer",$id);
        $serial_no = $this->serialNumber('purchase_master',$items[0]->to_branch);
        $currency = $this->GetCurrencyRate($items[0]->to_branch);
        $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        $pid = DB::table('purchase_master')->insertGetId(['branch_id' => $items[0]->to_branch, 'date' => date('Y-m-d'), 'created_by' => Auth::id(), 'tansfer_id' => $id,'serial_no'=>$serial_no]);
        $post['parent_id'] = $pid;
        $post['branch_id'] = $items[0]->to_branch;
        foreach ($items as $value) {
            $post['price'] =  $value->price;
            $post['local_price'] =  round($value->price*$currency_rate);
            $post['spare_id'] =  $value->spare_id;
            $post['qty'] = $value->qty;
            $this->insertPurchaseSpare($post);
        }
        DB::table('transfer')->where('id', $id)->update(['status' => 1]);
        DB::commit();
        return redirect()->back()->with("success", "Stock accepted successfully.");
    }



}

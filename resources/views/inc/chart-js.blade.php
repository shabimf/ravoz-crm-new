<!-- apexcharts -->
<script src="{{ asset('js/apexcharts/apexcharts.min.js')}}"></script>
<script src="{{ asset('js/apexcharts/apexcharts-custom.js')}}"></script>

<!-- jquery mask -->
<script src="{{ asset('js/jquery-jvectormap/jquery-jvectormap-2.0.3.min.js')}}"></script>
<script src="{{ asset('js/jquery-jvectormap/jquery-jvectormap-world-mill.js')}}"></script>

<!-- sparkline -->
<script src="{{ asset('js/sparkline/jquery.sparkline.min.js')}}"></script>
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Doa extends Model
{
    use HasFactory;
    protected $table = "doa";  
    public $timestamps = false;
    protected $guarded = array();
}

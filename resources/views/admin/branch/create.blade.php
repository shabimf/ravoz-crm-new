@extends('layouts.app')

@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                    <h4 class="card-title-text">Branch</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('branch.index') }}"> <i
                            class="feather icon-list"></i> Branch List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            @if (isset($edit_id))
                {{ Form::model($branch, ['route' => ['branch.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'create-company']) }}
            @else
                {{ Form::open(['route' => 'branch.store','class' => 'form-horizontal','role' => 'form','method' => 'post','id' => 'create-company', 'auto-complete' => 'off']) }}
            @endif
            @csrf
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('company', __('Company')) }}
                    {{ Form::select('company_id', $company, old('company_id',(isset($branch)) ? $branch->company_id:''), ['class' => 'form-control', 'placeholder' => 'Select Company', 'required' => 'required']) }}
                </div>
                <div class="col form-group">
                    {{ Form::label('country', __('Country')) }}
                    {{ Form::select('country_id', $country, old('company_id',(isset($branch)) ? $branch->country_id:''), ['class' => 'form-control', 'placeholder' => 'Select Country', 'required' => 'required']) }}
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col form-group">
                    {{ Form::label('name', __('Name')) }}
                    {{ Form::text('name', old('name',(isset($branch)) ? $branch->name:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
                <div class="col form-group">
                    {{ Form::label('code', __('Code')) }}
                    {{ Form::text('code', old('code',(isset($branch)) ? $branch->code:''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>

            </div>

            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection


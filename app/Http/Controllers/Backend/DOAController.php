<?php

namespace App\Http\Controllers\Backend;

use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use DB;
use Auth;
use PDF;
use Session;
use Validator;

class DOAController extends Controller
{
    public function __construct()
    {
        View::share('js', ['master', 'doa']);

        $this->middleware(function ($request, $next) {
            $this->data['access'] = $this->NameAndID('accessories');
            $this->data['branch'] = $this->getBranches(Session::get('sessi_branch_id'));
            $this->data['model'] = $this->getModelSpecification();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $this->data['branch_id'] = $request->branch_id ? $request->branch_id : 0;
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['doa_num'] = $request->doa_num ? $request->doa_num : "";
        $this->data['dead_imei'] = $request->dead_imei ? $request->dead_imei : "";
        $this->data['country_id'] = $this->FieldByID("branch", "country_id", Auth::user()->branch_id);
        return view('admin.doa.index', $this->data);
    }


    public function getDOAData(Request $request)
    {

        $data = DB::table('doa')
            ->leftJoin('branch', 'branch.id', '=', 'doa.branch_id')
            ->leftJoin('imei_details', 'imei_details.id', '=', 'doa.imei_id')
            ->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('imei_details as replace_imei', 'replace_imei.id', '=', 'doa.replace_imei_id')
            ->select([
                'doa.*',
                'branch.name as branch_name',
                'models.name as model_name',
                'color.name as color',
                'ram.name as ram',
                'rom.name as rom',
                'imei_details.IMEI_no1',
                'imei_details.IMEI_no2',
                'imei_details.serial_no as imei_sn',
                'replace_imei.IMEI_no1 as replace_imei1',
                'replace_imei.IMEI_no2 as replace_imei2'
            ]);

        if (!empty($request->branch_id)) {
            $data->where("doa.branch_id", $request->branch_id);
        } else {
            if (Session::get('sessi_branch_id')) {
                $data->whereIn('doa.branch_id', explode(',', Session::get('sessi_branch_id')));
            }
        }
        if (!empty($request->serial_num)) {
            $data->where(
                "doa.serial_no",
                'like',
                '%' . $request->serial_num . '%'
            );
        }

        if (!empty($request->imei)) {
            $data->where('imei_details.IMEI_no1', '=', $request->imei)
                ->orWhere('imei_details.IMEI_no2', '=', $request->imei);
        }
        if (!empty($request->model_id)) {
            $data->where("models.id", $request->model_id);
        }

        if (!empty($request->status) && $request->status == 1) {
            $data->where("doa.status", 0);
        }


        $from = $request->from
            ? date('Y-m-d', strtotime($request->from))
            : "";
        $to = $request->to
            ? date('Y-m-d', strtotime($request->to))
            : "";
        if (!empty($from) && !empty($to)) {
            $data->whereBetween('doa.created_at', [$from, $to]);
        }

        $data->get();
        // $res=$data->get();
        // echo "<pre>"; print_r($res); echo "</pre>"; exit;
        return DataTables::of($data)
            ->escapeColumns(['created_at', 'branch_name'])
            ->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw(DB::raw("(DATE_FORMAT(doa.created_at,'%d-%m-%Y'))") . ' like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('branch_name', function ($query, $keyword) {
                $query->whereRaw('branch.name like ?', ["%{$keyword}%"]);
            })
            ->filterColumn('IMEI_no1', function ($query, $keyword) {
                $query->whereRaw('imei_details.IMEI_no1 like ?', ["%{$keyword}%"]);
            })->filterColumn('replace_imei1', function ($query, $keyword) {
                $query->whereRaw('replace_imei.IMEI_no1 like ?', ["%{$keyword}%"]);
            })->filterColumn('model_name', function ($query, $keyword) {
                $query->whereRaw('models.name like ?', ["%{$keyword}%"])
                    ->orWhere('color.name', 'LIKE', "%{$keyword}%")
                    ->orWhere('rom.name', 'LIKE', "%{$keyword}%")
                    ->orWhere('rom.name', 'LIKE', "%{$keyword}%");
            })
            ->editColumn('created_at', function ($data) {
                return date('d-m-Y', strtotime($data->created_at));
            })
            ->editColumn('model_name', function ($data) {
                if($data->model_name!='')
                {
                    $model = "";
                    if ($data->model_name) {
                        $model = $data->model_name . "(" . $data->color;
                        if ($data->ram) {
                            $model .= "," . $data->ram;
                        }
                        if ($data->rom) {
                            $model .= "," . $data->rom;
                        }
                        $model .= ")";
                    }
                    return $model;
                }
                elseif($data->variant_id!=NULL)
                {
                    $res=DB::table('variant_details')
                        ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
                        ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
                        ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
                        ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
                        ->select([
                            'models.name as model_name',
                            'color.name as color',
                            'ram.name as ram',
                            'rom.name as rom',
                        ])->where("variant_details.id", '=', $data->variant_id)->first();
                        $model = "";
                        if ($res->model_name) {
                            $model = $res->model_name . "(" . $res->color;
                            if ($res->ram) {
                                $model .= "," . $res->ram;
                            }
                            if ($res->rom) {
                                $model .= "," . $res->rom;
                            }
                            $model .= ")";
                        }
                        return $model;
                }
            })
            ->editColumn('status', function ($data1) {
                if ($data1->status == 0) {
                    $status =
                        '<i class="fa fa-circle text-warning mr-2"></i> Pending';
                } elseif ($data1->status == 1) {
                    $status =
                        '<i class="fa fa-circle text-success mr-2"></i> Accepted';
                } elseif ($data1->status == 2) {
                    $status =
                        '<i class="fa fa-circle text-danger mr-2"></i> Rejected';
                }
                return $status;
            })
            ->editColumn('serial_no', function ($data1) {
                return '<a href="' .
                    url('service/doa/' . $data1->id) .
                    '">' .
                    $data1->serial_no .
                    '</a>';
            })

            ->addColumn('actions', function ($data1) {
                return '<a href="' . url('service/doa/' . $data1->id) . '"  class="btn btn-primary btn-xs">View</a>';
            })

            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        return view('admin.doa.create', $this->data);
    }

    public function doaUpdate(Request $request, $id = NULL)
    {
        // echo "<pre>"; print_r($request->all()); echo "</pre>"; exit;
        if ($request->has('doa_id')) {
            $doa_det = DB::table("doa")->find($request->doa_id);
            if ($request->doa_imei_id != '') {
                $post_update['is_doa'] = 1;
                $post['imei_id'] = $request->input('doa_imei_id');
                $post['remarks'] = $request->input('problem_remarks');
                $post['device_check'] = ($request->input('device_check') ? implode(',', $request->input('device_check')) : null);
                $post['is_special_request'] = ($request->input('is_special_request') ?? 0);
                $post['updated_by'] = Auth::id();
                $this->updatedata('doa', $post, $request->doa_id);
                $this->updatedata('imei_details', $post_update, $post['imei_id']);
                $post_update1['is_doa'] = 0;
                $this->updatedata('imei_details', $post_update1, $doa_det->imei_id);
            } elseif ($request->model_id != '') {
                $post_update['is_doa'] = 0;
                $post['imei_id'] = NULL;
                $post['variant_id'] = $request->input('model_id');
                $model_id = $this->FieldByID("variant_details", "model_id", $request->input('model_id'));
                $post['brand_id'] = $this->FieldByID("models", "brand_id", $model_id);
                $post['remarks'] = $request->input('problem_remarks');
                $post['device_check'] = ($request->input('device_check') ? implode(',', $request->input('device_check')) : null);
                $post['is_special_request'] = ($request->input('is_special_request') ?? 0);
                $post['updated_by'] = Auth::id();
                $this->updatedata('doa', $post, $request->doa_id);
                $this->updatedata('imei_details', $post_update, $doa_det->imei_id);
            }
            if ($request->hasFile('invoice_upload')) {
                $destinationPath = 'uploads/doa/';
                $inv_data['name'] = $this->UploadFile($destinationPath, $request->file('invoice_upload'));
                $inv_data['doa_id'] = $request->doa_id;
                $inv_data['status'] = 1;
                $this->insertdata('doa_invoice', $inv_data);
            }
            if ($request->hasFile('image_upload')) {
                $images = $request->file('image_upload');
                foreach ($images as $img) {
                    $destinationPath = 'uploads/doa/';
                    $img_data['name'] = $this->UploadFile($destinationPath, $img);
                    $img_data['doa_id'] = $request->doa_id;
                    $img_data['status'] = 1;
                    $this->insertdata('doa_attachments', $img_data);
                }
            }
            return true;
        }
        if ($request->imei_id != '') {
            $post_update['is_doa'] = 1;
            if ($request->type) {
                $post['status'] = $request->type;
                $post['updated_by'] = Auth::id();
                $this->updatedata('doa', $post, $request->id);
                if ($request->type == 2) {
                    $post_update['is_doa'] = 0;
                }
                $this->updatedata('imei_details', $post_update, $request->imei_id);
                return true;
            } else {
                $post = array();
                $post = $this->GetDOAForm($post, $request);
                $post['created_by'] = Auth::id();
                $post['branch_id'] = Auth::user()->branch_id;
                $post['serial_no'] = "DOA" . $this->serialNumber("doa", $post['branch_id']);
                $row_count=$this->GetRowCount('doa', array("imei_id" => $request->imei_id, "status" => 0, "issue_doa" => 0), 'id');
                if($row_count==0)
                {
                    $ins = $this->insertGetId('doa', $post);
                    if ($request->hasFile('invoice_upload')) {
                        $destinationPath = 'uploads/doa/';
                        $inv_data['name'] = $this->UploadFile($destinationPath, $request->file('invoice_upload'));
                        $inv_data['doa_id'] = $ins;
                        $inv_data['status'] = 1;
                        $this->insertdata('doa_invoice', $inv_data);
                    }
                    if ($request->hasFile('image_upload')) {
                        $images = $request->file('image_upload');
                        foreach ($images as $img) {
                            $destinationPath = 'uploads/doa/';
                            $img_data['name'] = $this->UploadFile($destinationPath, $img);
                            $img_data['doa_id'] = $ins;
                            $img_data['status'] = 1;
                            $this->insertdata('doa_attachments', $img_data);
                        }
                    }
                    $this->updatedata('imei_details', $post_update, $request->imei_id);
                    return true;
                }
            }
        } else {
            DB::beginTransaction();
            $post = array();
            $post = $this->GetDOAForm($post, $request);
            $post['created_by'] = Auth::id();
            $post['branch_id'] = Auth::user()->branch_id;
            $post['serial_no'] = "DOA" . $this->serialNumber("doa", $post['branch_id']);
            $ins = $this->insertGetId('doa', $post);
            if ($request->hasFile('invoice_upload')) {
                $destinationPath = 'uploads/doa/';
                $inv_data['name'] = $this->UploadFile($destinationPath, $request->file('invoice_upload'));
                $inv_data['doa_id'] = $ins;
                $inv_data['status'] = 1;
                $this->insertdata('doa_invoice', $inv_data);
            }
            if ($request->hasFile('image_upload')) {
                $images = $request->file('image_upload');
                foreach ($images as $img) {
                    $destinationPath = 'uploads/doa/';
                    $img_data['name'] = $this->UploadFile($destinationPath, $img);
                    $img_data['doa_id'] = $ins;
                    $img_data['status'] = 1;
                    $this->insertdata('doa_attachments', $img_data);
                }
            }
            DB::commit();
            return true;
        }
    }

    public function update(Request $request, $id = NULL)
    {
        // echo "<pre>"; print_r($request->all()); echo "</pre>"; exit;
        $post_update['is_doa'] = 1;
        if ($request->type) {
            $post['status'] = $request->type;
            $post['updated_by'] = Auth::id();
            $this->updatedata('doa', $post, $request->id);
            if ($request->type == 2) {
                $post_update['is_doa'] = 0;
            }
            if ($request->imei_id != '') {
                $this->updatedata('imei_details', $post_update, $request->imei_id);
                return true;
            }
        } else {
            $post = array();
            $post = $this->GetDOAForm($post, $request);
            $post['created_by'] = Auth::id();
            $post['branch_id'] = Auth::user()->branch_id;
            $post['serial_no'] = "DOA" . $this->serialNumber("doa", $post['branch_id']);
            if($post->imei_id) {
                $is_doa_exists = DB::table('doa') ->where([
                                    ['imei_id', '=', $post->imei_id],
                                    ['status', '=', 0],
                                    ['issue_doa', '=', 0]
                                ])->first();
                if ($is_doa_exists === null) {
                    $this->insertdata('doa', $post);
                } 
            } else {
                $this->insertdata('doa', $post);
            }
            $this->updatedata('imei_details', $post_update, $request->imei_id);
            return true;
        }
    }

    public function show($id)
    {

        $this->data['doa'] =  $this->getDOADetails($id);
        $this->data['country_id'] = $this->FieldByID("branch", "country_id", Auth::user()->branch_id);
        // echo "<pre>"; print_r($this->data); echo "</pre>"; exit;
        if ($this->data['doa']->issue_doa == 1 || $this->data['doa']->replace_imei_id > 0) {
            $pdf = PDF::loadView('admin.doa.pdf', $this->data);
            return $pdf->stream('doa.pdf');
        } else {
            return view('admin.doa.view', $this->data);
        }
    }

    public function getPDF($id)
    {
        $post['issue_doa'] = 1;
        $post['updated_by'] = Auth::id();
        $this->updatedata('doa', $post, $id);
        $this->data['doa'] =  $this->getDOADetails($id);
        $pdf = PDF::loadView('admin.doa.pdf', $this->data);
        return $pdf->stream('doa.pdf');
    }

    public function updateReplaceIMEI(Request $request)
    {
        $post['replace_imei_id'] = $request->imei_id;
        $post['updated_by'] = Auth::id();
        $this->updatedata('doa', $post, $request->id);
        return true;
    }

    public function doaInvoiceDelete(Request $request)
    {
        $id = $request->id;
        $invoice_det = DB::table("doa_invoice")->find($id);
        $path = 'uploads/doa/' . $invoice_det->name;
        unlink($path);
        return DB::table('doa_invoice')->where('id', $invoice_det->id)->delete();
    }

    public function doaImageDelete(Request $request)
    {
        $id = $request->id;
        $image_det = DB::table("doa_attachments")->find($id);
        $path = 'uploads/doa/' . $image_det->name;
        unlink($path);
        return DB::table('doa_attachments')->where('id', $image_det->id)->delete();
    }
}

<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;
use App\Http\Traits\CommonTrait;

class ModelComplaintExport implements FromArray,WithHeadings
{
    protected $model_id;

    use CommonTrait;

    function __construct($model_id) {
        $this->data['model_id'] = $model_id;

    }
    public function headings(): array
    {
        return [
            'Sl No',
            'Complaint',
            'Warranty',
            'Without Warranty',
            'Total'
        ];
    }

    public function array(): array
    {
        
        return $this->getModelComplaintTableData($this->data);
    }


}

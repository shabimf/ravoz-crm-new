@extends('layouts.service')
@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">DOA</h4>
            </div>
        </div>
        <div class="row  ">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'report.doa','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-doa']) }}
					<div class="form-row well">
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Date From')) }}
							{{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control','id'=>'datepicker' ]) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Date To')) }}
							{{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control', 'id' => 'dates']) }}
						</div>
						<div class="form-group col-md-3">
                            {{ Form::label('name', __('Model')) }}
							{{ Form::select('model_id', $model, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model','id' => 'model_id']) }}
						</div>
						<div class="form-group col-md-2">
						  <button class="btn btn-primary search-btn">Search</button>
                          <a class="btn btn-primary reset-btn" href="{{route('report.doa')}}">Reset</a>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
				    <div class="dt-buttons float-right">
						<a class="btn btn-primary buttons-excel buttons-html5"  href="{{ url('report/doa/file-export?from='.$from.'&to='.$to.'&model_id='.$model_id.'') }}"><span>Excel</span></a>
					</div>
					  <div class="table-responsive">
						<table id="doa-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("report.doa-get") }}">
							<thead>
								<tr>
									<th width="5">#</th>
									<th width="80">YEAR</th>
									<th>MONTH</th>
									<th>DATE</th>
									<th>DOA NO</th>
									<th>DOA TYPE</th>
									<th>IMEI 1 OLD PIECE</th>
									<th>IMEI 2 OLD PIECE</th>
									<th>IMEI 1 REPLACED</th>
									<th>IMEI 2 REPLACED</th>
									<th>COMPLAINT</th>
									<th>SPECIAL REQUEST</th>
								</tr>
							</thead>
						</table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
$(function() {
	$( 'input[name="from"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
	$( 'input[name="to"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
});

var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
var model_id = getUrlParameter('model_id');
FTX.Utils.documentReady(function() {
    FTX.DOA.list.init(from_date,to_date,model_id);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

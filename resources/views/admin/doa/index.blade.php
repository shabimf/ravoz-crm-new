@extends('layouts.service')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">DOA List</h4>
            </div>
			<div class="col-md-3 {{(userHasPermission('Service','doa_create')?'':'hide')}}">
				<div class="add-new">
				  <a class="btn btn-primary" data-toggle="modal" data-target="#doaModal">
					<i class="feather icon-plus"></i> DOA Create
				  </a>
				</div>
            </div>
        </div>
        <div class="row  ">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'doa.index','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-doa']) }}
					<div class="form-row well">
					    <div class="form-group col-md-2">
							{{ Form::label('name', __('DOA Number')) }}
							{{ Form::text('doa_num', old('doa_num',(isset($doa_num)) ? $doa_num:''), ['class' => 'form-control']) }}
                        </div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Date From')) }}
							{{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control','id'=>'datepicker' ]) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Date To')) }}
							{{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control', 'id' => 'dates']) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Dead IMEI')) }}
							{{ Form::text('dead_imei', old('dead_imei',(isset($dead_imei)) ? $dead_imei:''), ['class' => 'form-control']) }}
						</div>
						<div class="form-group col-md-2">
							{{ Form::label('name', __('Branch')) }}
							{{ Form::select('branch_id', $branch, old('branch_id',(isset($branch_id)) ? $branch_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch','id' => 'branch_id']) }}
						</div>
						<div class="form-group col-md-2">
						  <button class="btn btn-success search-btn">Search</button>
                          <button class="btn btn-success reset-btn" type="reset">Reset</button>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="doa-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("doa.get") }}">
					        <thead>
					            <tr>
								    <th width="5">#</th>
					                <th width="80">Date</th>
									<th>DOA</th>
									<th>Branch</th>
					                <th>Device</th>
									<th>IMEI</th>
                                    <th>Serial No</th>
									<th>Replace IMEI</th>
									<th>Status</th>
									<th>Reciept</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
<div class="modal fade" id="doaModal" tabindex="-1" role="dialog" aria-labelledby="doaModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
	<div class="modal-header">
        <h5 class="modal-title">ADD DOA</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body" id="doa_area">
	    <div class="alert alert-success alert-dismissible doa_msg hide">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			DOA details <strong>successfully created!</strong>
        </div>
      <form action="" method="post" id="doa_form" name="doa_form" enctype="multipart/form-data">
        @csrf
        <table class="table table-bordered table-condensed doa_table">
          <tbody>
            <tr>
              <th class="success imei-det"> {{ Form::label('name', __('IMEI/SL')) }}</th>
              <th class="success model-det d-none">{{ Form::label('name', __('Model')) }}</th>
              <td>
                <div class="imei-det">
                    <div class="input-group">
                        {{ Form::text('defective_imei', '', ['class' => 'form-control imei_add', 'id' => 'imei_selected']) }}
                        {{ Form::hidden('imei_id', '', ['class' => 'form-control', 'id' => 'imei_id']) }}
                        {{ Form::hidden('country_id', old('country_id',(isset($country_id)) ? $country_id:''), ['class' => 'form-control', 'id' => 'country_id']) }}
                        <div class="input-group-append">
                            {{ Form::button('Check', ['class' => 'btn btn-overlay-dark btns check_replace_imei cp', 'id' => 'search']) }}
                        </div>
                    </div>
                    <a href="javascript:void(0);" id="choose_model">Choose Model</a>
                </div>
                <div class="model-det d-none">
                    <div class="input-group">
                        {{ Form::select('model_id', $model, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model']) }}
                    </div>
                    <a href="javascript:void(0);" id="enter_manually">Enter Manually</a>
                </div>
              </td>
            </tr>
            <tr class="imei-det">
              <th class="success">Model No</th>
              <td id="model_name"></td>
            </tr>
            <tr class="imei-det">
              <th class="success">Status</th>
              <td id="billed_status">Not Sold </td>
			  {{ Form::hidden('billed', '0', ['class' => 'form-control', 'id' => 'billed_']) }}
            </tr>
			<tr>
              <th class="success">Remarks</th>
              <td>
			   {{ Form::textarea('problem_remarks', '', ['class' => 'form-control', 'id' => 'problem_remarks']) }}
              </td>
            </tr>
			<tr>
                <th width="25%" class="success">Check List</th>
                <td>
					@foreach ($access as $k => $v)
						<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" id="accessories_{{ $k }}"
								value="{{ $k }}" name="device_check[]"
								class="custom-control-input form-control">
							<label class="custom-control-label"
								for="accessories_{{ $k }}">{{ $v }}</label>
						</div>
					@endforeach
                </td>
            </tr>
            <tr>
                <th class="success">Special Request</th>
                <td>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="is_special_request" value="1" name="is_special_request">
                    <label class="custom-control-label" for="is_special_request"></label>
                  </div>
                </td>
            </tr>
            <tr>
                <th class="success">Invoice Upload</th>
                <td><input type="file" class="form-control" id="invoice_upload" name="invoice_upload"></td>
            </tr>
            <tr>
                <th class="success">Images Upload</th>
                <td><input type="file" class="form-control" id="image_upload" name="image_upload[]" multiple></td>
            </tr>

          </tbody>
        </table>
        <hr>
        <input type="submit" class="btn btn-success imei_success pull-right hide" name="issue_doa" value="Register">
        <div class="clearfix"></div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
var branch_id = getUrlParameter('branch_id');
var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
var dead_imei =  getUrlParameter('dead_imei');
var doa_num =  getUrlParameter('doa_num');
var status =  getUrlParameter('status');
FTX.Utils.documentReady(function() {
    FTX.DOA.list.init(branch_id,from_date,to_date,dead_imei,doa_num,status);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

@extends('layouts.service')

@section('content')

    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                <h4 class="card-title-text">STOCK EDIT</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('stock') }}"> <i
                            class="feather icon-list"></i> Stock List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['route' => ['stock.update', $edit_id],'class' => 'form-horizontal','role' => 'form','method' => 'PATCH','id' => 'edit-stock']) }}
            @csrf
            <div class="form-row">
               <div class="form-group col-md-6">
                {{ Form::label('name', __('Branch')) }}
                {{ Form::text('branch', old('branch',(isset($stock)) ? $stock->branch_name:''),  ['class' => 'form-control', 'readonly' => 'readonly']) }}
                {{ Form::hidden('branch_id', old('branch_id',(isset($stock)) ? $stock->branch_id:''),  ['class' => 'form-control']) }}
                
              </div>
               <div class="form-group col-md-6">
                {{ Form::label('name', __('Spare')) }}
                {{ Form::text('spare', old('spare',(isset($stock)) ? $stock->spare_name:''), ['class' => 'form-control','required' => 'required', 'readonly' => 'readonly']) }}
               </div>
               <div class="form-group col-md-6">
                {{ Form::label('name', __('Quantity')) }}
                {{ Form::text('qty', old('qty',(isset($stock)) ? $stock->qty:''), ['class' => 'form-control', 'required' => 'required']) }}
               </div>
               <div class="form-group col-md-6">
                {{ Form::label('name', __('Average Price')) }} @if(isset($stock)) ({{$stock->currency}}) @endif
                {{ Form::text('price', old('price',(isset($stock)) ? $stock->local_price:''), ['class' => 'form-control','required' => 'required']) }}
               </div>
               <button type="submit" class="btn btn-success float-right">Submit</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection

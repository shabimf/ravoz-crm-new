<?php

namespace App\Http\Controllers\Backend;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\ServiceFile;
use App\Models\InvoiceFile;
use App\Models\AppealComment;
use PDF;
use File;
use Session;

class ServiceController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware(function ($request, $next) {
            $this->data['model'] = $this->NameAndID('models');
            $this->data['access'] = $this->NameAndID('accessories');
            $this->data['submission'] = $this->NameAndID('submission_category');
            $this->data['brands'] = $this->NameAndID('brands');
            $this->data['product_type'] = $this->NameAndID('product_type');
            $this->data['country'] = $this->NameAndID('country');
            $this->data['order_status'] = $this->NameAndID('order_status');
            $this->data['branch'] = $this->getBranches(Session::get('sessi_branch_id'));
            $this->data['access_all'] = $this->checkAllPermission(Auth::user()->role_id);
            $this->data['country_id'] = $this->FieldByID("branch","country_id",Auth::user()->branch_id);
            return $next($request);
        });

        View::share('js', ['master', 'service', 'service-create','doa']);
    }

    public function index()
    {
        View::share('js', ['dashboard']);
        $this->data['data'] = $this->getFAQDataList('');
        return view('service.dashboard', $this->data);
    }

    public function create(Request $request)
    {

        $level = $this->GetComplaints();
        $this->data['users'] = $this->GetUsers(Auth::user()->branch_id);
        //$this->data['country_id'] = $this->FieldByID("branch","country_id",Auth::user()->branch_id);
        $levels = array();
        foreach ($level as $v) {
            if (!isset($levels[$v->name])) {
                $levels[$v->name] = array();
            }
            $levels[$v->name][$v->id] = $v->description;
        }
        $this->data['levels'] = $levels;
        return view('admin.service.create', $this->data);
    }
    public function getDeviceType(Request $request)
    {
        return $this->checkDeviceType($request);
    }
    public function getIMEIData(Request $request)
    {
        return $this->checkIMEIData($request);
    }

    public function getSoftwareVersion($id)
    {
        $model_id = $this->FieldByID('variant_details', 'model_id', $id);
        $software = DB::table("software_version")
            ->where("model_id", $model_id)
            ->pluck("name", "id")
            ->toArray();
        return response()->json($software);
    }

    public function getCustomerData(Request $request)
    {
        $input = $request->all();
        $name = $input['query'];
        $data = DB::table("customers")
            ->where('customer_phone', '=', $name)
            ->where('type', '=', $input['type'])
            ->select("customers.*")
            ->get();
        return response()->json($data);
    }

    public function getModels($id)
    {
        return $this->GetModel($id);
    }

    public function store(Request $request)
    {
        $accessories_id = (isset($request->accessories_id)?implode(",", $request->accessories_id):'');
        $complaint = (isset($request->complaint)?implode(",", $request->complaint):'');
        $delivery_date=$sale_date=$active_date=$manufacture_date=NULL;
        $service_history=$this->getServiceHistory(array("imei_id" => $request->imeiId, "status" => 1));
        if(count($service_history)==0)
        {
            DB::beginTransaction();
            if ($request->shop_name) {
                $request_id = DB::table('sales_request')->insertGetId([
                    'imei_id' =>  $request->imeiId,
                    'branch_id' => Auth::user()->branch_id,
                    'shop_name' =>  $request->shop_name,
                    'sale_date' => date('Y-m-d', strtotime($request->sale_date)),
                    'created_by'  => Auth::user()->id,
                    'invoice_no' =>  $request->invoice_no
                ]);
            }
            if($request->delivery_date!='')
            {
                $delivery_date=date('Y-m-d', strtotime($request->delivery_date));
            }
            if($request->sale_date!='')
            {
                $sale_date=date('Y-m-d', strtotime($request->sale_date));
            }
            if($request->active_date!='')
            {
                $active_date=date('Y-m-d', strtotime($request->active_date));
            }
            if($request->manufacture_date!='')
            {
                $manufacture_date=date('Y-m-d', strtotime($request->manufacture_date));
            }

            $service_id = DB::table('service')->insertGetId([
                'sales_request_id' => ($request_id ?? 0),
                'product_type' => $request->product_type,
                'imei_id' => $request->imeiId,
                'branch_id' => Auth::user()->branch_id,
                'technician_id' => $request->technician_id,
                'version_id' => $request->version_id,
                'security_code' => $request->security_code,
                'delivery_date' => $delivery_date,
                'submission_cat_id' => $request->submission_cat_id,
                'remarks' => $request->remarks,
                'created_by' => Auth::id(),
                'serial_no' => $this->serialNumber(
                    "service",
                    Auth::user()->branch_id
                ),
                'accessories_id' => $accessories_id,
                'complaint' => $complaint,
                'brand_id' => $request->brand_id,
                'variant_id' => $request->model_id,
                'is_warranty' => $request->is_warranty,
                'sn_no' => $request->sn_no,
                'sale_date' => $sale_date,
                'active_date' => $active_date,
                'manufacture_date' => $manufacture_date,
                'status' => 1
            ]);
            if ($complaint) {
                foreach ($request->complaint as $complaint_id) {
                    DB::table('service_complaint')->insertGetId([
                        'service_id' => $service_id,
                        'complaint_id' => $complaint_id
                    ]);
                }
            }
            $email = $gender = $address = $postal_code = $backup_no = $dealer_contact = $salesman_district =
                "";
            if ($request->submission_cat_id == 1) {
                $phone = $request->customer_phone;
                $name = $request->customer_name;
                $email = $request->customer_email;
                $gender = $request->gender;
                $address = $request->address;
                $postal_code = $request->postal_code;
                $backup_no = $request->backup_no;
            } elseif ($request->submission_cat_id == 2) {
                $phone = $request->dealer_phone;
                $name = $request->dealer_name;
                $dealer_contact = $request->dealer_contact;
                $address = $request->dealer_address;
            } else {
                $phone = $request->salesman_contact;
                $name = $request->salesman_name;
                $salesman_district = $request->salesman_district;
            }

            $user = DB::table('customers')
                ->where('customer_phone', '=', $phone)
                ->first();
            if ($user === null) {
                $custmer_parent_id = DB::table('customers')->insertGetId([
                    'customer_name' => $name,
                    'type' => $request->submission_cat_id,
                    'customer_email' => $email,
                    'customer_phone' => $phone,
                    'gender' => $gender,
                    'address' => $address,
                    'postal_code' => $postal_code,
                    'backup_no' => $backup_no,
                    'dealer_contact' => $dealer_contact,
                    'salesman_district' => $salesman_district,
                    'created_by' => Auth::id()
                ]);
            } else {
                $custmer_parent_id = $user->customer_id;
                DB::table('customers')
                    ->where('customer_id', $custmer_parent_id)
                    ->update([
                        'customer_name' => $name,
                        'type' => $request->submission_cat_id,
                        'customer_email' => $email,
                        'customer_phone' => $phone,
                        'gender' => $gender,
                        'address' => $address,
                        'postal_code' => $postal_code,
                        'backup_no' => $backup_no,
                        'updated_by' => Auth::id(),
                        'dealer_contact' => $dealer_contact,
                        'salesman_district' => $salesman_district
                    ]);
            }

            $custmer_id = DB::table('service_customers')->insertGetId([
                'customer_name' => $name,
                'parent_id' => $custmer_parent_id,
                'customer_email' => $email,
                'customer_phone' => $phone,
                'gender' => $gender,
                'address' => $address,
                'postal_code' => $postal_code,
                'backup_no' => $backup_no,
                'dealer_contact' => $dealer_contact,
                'salesman_district' => $salesman_district
            ]);

            DB::table('service_status')->insertGetId([
                'service_id' => $service_id,
                'status' => 1,
                'created_by' => Auth::id()
            ]);
            DB::table('service')
                ->where('id', $service_id)
                ->update(['customer_id' => $custmer_id]);

            if ($request->hasfile('filename')) {
                foreach ($request->file('filename') as $file) {
                    $name =time().rand(1, 100) . '.' .$file->getClientOriginalName();
                    $file->move(public_path() . '/uploads/service/', $name);
                    $file = new ServiceFile();
                    $file->filename = $name;
                    $file->service_id = $service_id;
                    $file->save();
                }
            }

            if ($request->hasfile('invoice')) {
                foreach ($request->file('invoice') as $file) {
                    $name =
                        time() .
                        rand(1, 100) .
                        '.' .
                        $file->getClientOriginalName();
                    $file->move(public_path() . '/uploads/invoice/', $name);
                    $file = new InvoiceFile();
                    $file->filename = $name;
                    $file->service_id = $service_id;
                    $file->save();
                }
            }
            if($request->is_warranty == 0) {
                $udata['warranty_end_date'] = date('Y-m-d');
                $this->updatedata("imei_details", $udata, $request->imeiId);
            }
            DB::commit();
            Session::flash('success', 'Service data successfully added.');
            return $service_id;
        }
        else
        {
            Session::flash('success', 'This IMEI/SL No has already given for repair.');
            return 0;
        }
        // return redirect()
        //     ->back()
        //     ->with("success", "Service data successfully added.");
    }

    public function getlist(Request $request)
    {

        $this->data['branch_id'] = $request->branch_id
            ? $request->branch_id
            : 0;
        $this->data['from'] = $request->from ? $request->from : "";
        $this->data['to'] = $request->to ? $request->to : "";
        $this->data['nums'] = $request->nums ? $request->nums : "";
        $this->data['serial_num'] = $request->serial_num
            ? $request->serial_num
            : "";
        $this->data['model_id'] = $request->model_id ? $request->model_id : 0;
        $this->data['status_id'] = $request->status_id ? $request->status_id : 0;
        return view('admin.service.index', $this->data);
    }

    public function getServiceRequest(Request $request)
    {
        $data = DB::table('service')

            ->leftJoin('branch', 'branch.id', '=', 'service.branch_id')
            ->leftJoin(
                'service_customers',
                'service_customers.customer_id',
                '=',
                'service.customer_id'
            )

            ->leftJoin('brands', 'brands.id', '=', 'service.brand_id')
            ->leftJoin(
                'variant_details',
                'variant_details.id',
                '=',
                'service.variant_id'
            )
            ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
            ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
            ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
            ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
            ->leftJoin('doa', 'service.id', '=', 'doa.service_id')
            ->select([
                'service.id',
                'service.created_at',
                'branch.name as branch_name',
                'service.serial_no',
                'service.status',
                'service_customers.customer_name',
                'service_customers.customer_phone',
                'service.is_warranty',
                'brands.name as brand_name',
                'models.name as model_name',
                'color.name as color',
                'ram.name as ram',
                'rom.name as rom',
                'doa.id as doa_id',
                'doa.status as doa_status',
                'service.is_approved'
            ]);

        if (!empty($request->branch_id)) {
            $data->where("service.branch_id", $request->branch_id);
        } else {
            if (Session::get('sessi_branch_id')) {
                $data->whereIn('service.branch_id', explode(',',Session::get('sessi_branch_id')));
            }
        }

        if (!empty($request->nums)) {
            $data->where(
                "service_customers.customer_phone",
                "=",
                $request->nums
            );
        }
        if (!empty($request->serial_num)) {
            $data->where(
                "service.serial_no",
                'like',
                '%' . $request->serial_num . '%'
            );
        }
        if (!empty($request->model_id)) {
            $data->where("models.id", $request->model_id);
        }
        if (!empty($request->warranty)) {
            if ($request->warranty == 2) {
                $data->where("service.is_warranty", 0);
            } else {
                $data->where("service.is_warranty", 1);
            }
        }
        if (!empty($request->status_id)) {
            $data->where("service.status", $request->status_id);
        }
        if (!empty($request->status)) {
            if ($request->status == 6) {
                $data->leftJoin('appeal_master', 'service.id', '=', 'appeal_master.service_id');
                $data->where("appeal_master.status", 0);
            } else {
                $data->leftJoin('appeal_master', 'service.id', '=', 'appeal_master.service_id');
                $data->whereNull("appeal_master.service_id");
                $data->where("service.status", $request->status)->where("service.is_approved",0)->where("service.is_warranty",1);
            }
        } else {
            $data->where('service.status', '!=', 6);
        }

        $from = $request->from
            ? date('Y-m-d', strtotime($request->from))
            : "";
        $to = $request->to
            ? date('Y-m-d', strtotime($request->to))
            : "";
        if (!empty($from) && !empty($to)) {
            $data->whereBetween('service.created_at', [$from, $to]);
        }

        // echo "<pre>"; print_r($data->get()); echo "</pre>"; exit;

        return DataTables::of($data)
            ->escapeColumns(['created_at', 'branch_name'])
            ->editColumn('created_at', function ($data) {
                return date('d-m-Y', strtotime($data->created_at));
            })
            ->editColumn('is_warranty', function ($data) {
                return $data->is_warranty == 1 ? "Warranty" : "Non Warranty";
            })
            ->editColumn('model_name', function ($data) {
                $model = "";
                if ($data->model_name) {
                    $model = $data->model_name . "(" . $data->color;
                    if ($data->ram) {
                        $model .= "," . $data->ram;
                    }
                    if ($data->rom) {
                        $model .= "," . $data->rom;
                    }
                    $model .= ")";
                }
                return $model;
            })
            ->editColumn('status', function ($data) {
                $status = "";
                if ($data->status == 1) {
                    if ($data->doa_id > 0 && $data->doa_status == 0) {
                        $status ='<i class="fa fa-circle text-warning mr-2"></i> DOA Request Pending';
                    }
                    // else if ($data->doa_id > 0 && $data->doa_status == 1) {
                    //     $status ='<i class="fa fa-circle text-success mr-2"></i> DOA Request Accepted';
                    // } else if ($data->doa_id > 0 && $data->doa_status == 2) {
                    //     $status ='<i class="fa fa-circle text-danger mr-2"></i> DOA Request Rejected';
                    // }
                    else {
                        $status ='<i class="fa fa-circle text-warning mr-2"></i> Pending';
                    }
                } elseif ($data->status == 5) {
                    $status =
                        '<i class="fa fa-circle text-success mr-2"></i> Delivered';
                } elseif ($data->status == 4) {
                    $status =
                        '<i class="fa fa-circle text-success mr-2"></i> Completed';
                }
                return $status;
            })
            ->editColumn('serial_no', function ($data) {
                return '<a href="' .
                    url('service/' . $data->id) .
                    '">' .
                    $data->serial_no .
                    '</a>';
            })
            ->editColumn('is_approved', function ($data) {
                $status = '<i class="fa fa-times-circle-o text-danger mr-2"></i> Not Verified';
                if ($data->is_approved == 1) {
                    $status =
                    '<i class="fa fa-check-circle text-success mr-2"></i> Verified';
                }
                return $status;
            })->filterColumn('serial_no', function ($query, $keyword)
            {
                $query->whereRaw('service.serial_no like ?', ["%{$keyword}%"]);
            })->filterColumn('branch_name', function ($query, $keyword)
            {
                $query->whereRaw('branch.name like ?', ["%{$keyword}%"]);
            })->filterColumn('customer_name', function ($query, $keyword)
            {
                $query->whereRaw('service_customers.customer_name like ?', ["%{$keyword}%"]);
            })
            // ->filterColumn('customer_phone', function ($query, $keyword)
            // {
            //     $query->whereRaw('service_customers.customer_phone like ?', ["%{$keyword}%"]);
            // })
            ->filterColumn('brand_name', function ($query, $keyword)
            {
                $query->whereRaw('brands.name like ?', ["%{$keyword}%"]);
            })->filterColumn('model_name', function ($query, $keyword)
            {
                $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
            })->filterColumn('created_at', function ($query, $keyword) {
                $query->whereRaw(DB::raw("(DATE_FORMAT(service.created_at,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function show($id)
    {
        $this->data['service'] = $this->getService($id);
        // echo "<pre>"; print_r($this->data); echo "</pre>"; exit;
        if($this->data['service']->model=="" && $this->data['service']->imei_id!='')
        {
            $service_det=DB::table('imei_details')
                        ->leftJoin('variant_details','variant_details.id','=','imei_details.variant_id')
                        ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
                        ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
                        ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
                        ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
                        ->leftJoin('brands', 'brands.id', '=', 'models.brand_id')
                        ->select('brands.name as brand',
                        'brands.id as brand_id',
                        'models.name as model',
                        'models.id as model_id',
                        'color.name as color',
                        'ram.name as ram',
                        'rom.name as rom')
                        ->where('imei_details.id', $this->data['service']->imei_id)
                        ->first();
            $this->data['service']->model=$service_det->model;
            $this->data['service']->brand=$service_det->brand;
            $this->data['service']->brand_id=$service_det->brand_id;
            $this->data['service']->model_id=$service_det->model_id;
            $this->data['service']->color=$service_det->color;
            $this->data['service']->ram=$service_det->ram;
            $this->data['service']->rom=$service_det->rom;
        }
        $this->data['approve_level'] = $this->getLevels($id);
        $branch_id = Auth::user()->branch_id;
        $this->data['technician'] = DB::table('users')
            ->leftJoin('roles', 'roles.id', '=', 'users.role_id')
            ->select('users.first_name', 'users.id')
            ->where(['roles.name' => 'Technician', 'users.branch_id' => $branch_id])
            ->pluck("first_name", "id")
            ->all();
        $level = $this->GetComplaints();
        $levels = array();
        foreach ($level as $v) {
            if (!isset($levels[$v->name])) {
                $levels[$v->name] = array();
            }
            $levels[$v->name][$v->id] = $v->description;
        }
        $this->data['levels'] = $levels;
        $this->data['get_complaints'] = json_decode(json_encode(DB::table('complaints')
            ->leftJoin('service_complaint', 'service_complaint.complaint_id', '=', 'complaints.id')
            ->select('complaints.*', 'service_complaint.status', 'service_complaint.id as service_comp_id','service_complaint.remarks')
            ->where('service_complaint.service_id', $id)
            ->get()), 1);
        $models = $this->GetSpares($this->data['service']->model_id);
        $model = array();
        foreach ($models as $v) {

            if (!isset($model[$this->data['service']->model])) {
                $model[$this->data['service']->model] = array();
            }
            if(!isset($v->name)) $v->name  = "Others";
            else $v->name = $this->data['service']->model;
            $model[$v->name][$v->id] = $v->spare_name;
        }
        $this->data['models'] = $model;
        // $this->data['users'] = $this->GetUsers(Auth::user()->branch_id);
        $this->data['users'] = $this->GetUsers($this->data['service']->branch_id);
        $this->data['getSpareDetails'] = json_decode(json_encode(DB::table('service_spare')
            ->leftJoin('spare', 'service_spare.spare_id', '=', 'spare.id')
            ->select('service_spare.*', 'spare.spare_name', 'spare.part_code', 'spare.focus_code')
            ->where('service_spare.service_id', $id)
            ->get()), 1);

        $model = json_decode($this->GetModel($this->data['service']->brand_id)->getContent());
        $this->data['model'] = array();
        foreach ($model as $k => $v) {
            $this->data['model'][$v->id] =  $v->name . "(" . $v->color_name . "," . $v->ram_name . "," . $v->rom_name . ")";
        }
        $this->data['version'] = $this->getNameByID('software_version', 'model_id', $this->data['service']->model_id);
        $this->data['images'] = DB::table('service_image')
            ->leftJoin('service', 'service.id', '=', 'service_image.service_id')
            ->where('service_image.service_id', $id)
            ->pluck('service_image.filename', 'service_image.id');
        $this->data['invoice'] = DB::table('invoice_file')
            ->leftJoin('service', 'service.id', '=', 'invoice_file.service_id')
            ->where('invoice_file.service_id', $id)
            ->pluck('invoice_file.filename', 'invoice_file.id');
        $country_id = $this->FieldByID("branch", "country_id", Auth::user()->branch_id);
        $this->data['transfer_branches'] = $this->getBranch($country_id, Auth::user()->branch_id);
        $this->data['serviceBill'] = $serviceBill = $this->GetRowCount('service_bill', array("service_id" => $id), 'id');
        $getSaleReq = $this->GetFirst('sales_request', array("id" => $this->data['service']->sales_request_id));
        if(!empty($getSaleReq))
        {
            $sale_request['is_saleReq']=1;
            $sale_request['status']=$getSaleReq->status;
            $sale_request['shop_name']=$getSaleReq->shop_name;
            $sale_request['invoice_no']=$getSaleReq->invoice_no;
            $sale_request['updated_by']=$getSaleReq->updated_by;
        }
        else
        {
            $sale_request['is_saleReq']=0;
        }
        $this->data['sale_request']=$sale_request;
        $this->data['spares']=json_decode(json_encode(DB::table('service_spare')
                            ->leftJoin('service', 'service.id', '=', 'service_spare.service_id')
                            ->leftJoin('spare', 'spare.id', '=', 'service_spare.spare_id')
                            ->where('service_spare.service_id', $id)
                            ->where('service_spare.is_warranty', 0)
                            ->select('service_spare.*',
                                    'spare.spare_name as sparename')
                            ->get()),1);
        if($this->data['service']->imei_id!="")
        {
            $service_history=$this->getServiceHistory(array("imei_id" => $this->data['service']->imei_id,array("created_at", '<', $this->data['service']->created_at)));
        }
        else
        {
            $service_history="";
        }
        // echo "<pre>"; print_r($this->data); echo "</pre>"; exit;
        $this->data['currency'] = $this->getCompanyCurrency(NULL);;
        $this->data['service_history'] = $service_history;
        // echo "<pre>"; print_r($this->data); echo "</pre>"; exit;
        return view('admin.service.view', $this->data);
    }

    public function updateCustomerData(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); echo "</pre>"; exit;
        $email = $gender = $address = $postal_code = $backup_no = $dealer_contact = $salesman_district = NULL;
        if ($request->cus_type == 1) {
            $cus_type = "Customer";
            $phone = $request->customer_phone;
            $name = $request->customer_name;
            $email = $request->customer_email;
            $gender = $request->gender;
            $address = $request->address;
            $postal_code = $request->postal_code;
            $backup_no = $request->backup_no;
        } elseif ($request->cus_type == 2) {
            $cus_type = "Dealer";
            $phone = $request->dealer_phone;
            $name = $request->dealer_name;
            $dealer_contact = $request->dealer_contact;
            $address = $request->dealer_address;
        } else {
            $cus_type = "Salesman";
            $phone = $request->salesman_contact;
            $name = $request->salesman_name;
            $salesman_district = $request->salesman_district;
        }
        $custmer_parent_id = $request->parent_cus_id;

        DB::beginTransaction();

        DB::table('customers')
            ->where('customer_id', $custmer_parent_id)
            ->update([
                'customer_name' => $name,
                'customer_email' => $email,
                'customer_phone' => $phone,
                'gender' => $gender,
                'address' => $address,
                'postal_code' => $postal_code,
                'backup_no' => $backup_no,
                'updated_by' => Auth::id(),
                'dealer_contact' => $dealer_contact,
                'salesman_district' => $salesman_district
            ]);

        DB::table('service_customers')
            ->where('customer_id',  $request->customer_id)
            ->update([
                'customer_name' => $name,
                'customer_email' => $email,
                'customer_phone' => $phone,
                'gender' => $gender,
                'address' => $address,
                'postal_code' => $postal_code,
                'backup_no' => $backup_no,
                'dealer_contact' => $dealer_contact,
                'salesman_district' => $salesman_district
            ]);
        // $servicebill_count=$this->GetRowCount('service_bill',array("service_id" => $service_id), 'id');
        // if($servicebill_count>0)
        // {
        //     $this->updatedata('service', array("status" => 1), $service_id);
        // }
        DB::commit();
        session()->flash('success', $cus_type . ' Details updated successfully');
    }

    public function varifyphone(Request $request)
    {
        $customer = DB::table('customers')->where('customer_phone', '=', $request->customer_phone)->first();
        if ($customer === null) {
            return response()->json("false");
        } else {
            return response()->json("true");
        }
    }
    public function addServiceComplaint(Request $request)
    {
        $service_id = $request->input('service_id');
        $complaints = $request->input('complaint');
        $service_transfer=$this->GetFirst('service_transfer', array("service_id" => $service_id, "status" => 1));
        if (!empty($service_transfer)) {
            $branch_id = $service_transfer->to_branch;
            $technician_id = $service_transfer->accepted_user;
        } else {
            $branch_id = Auth::user()->branch_id;
            $technician_id = Auth::user()->id;
        }
        // $currency = $this->GetCurrencyRate($branch_id);
        // $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        foreach ($complaints as $comp) {
            // $level_id = DB::table('complaints')->where('id', $comp)->first()->level_id;
            // $complaint_rate = DB::table('levels')->where('id', $level_id)->first()->rate;
            // $local_price = ($complaint_rate * $currency_rate);
            if ($this->GetRowCount('service_complaint', array("service_id" => $service_id, "complaint_id" => $comp), 'id') == 0) {
                DB::table('service_complaint')->insertGetId(["service_id" => $service_id, "complaint_id" => $comp, "branch_id" => $branch_id, "technician_id" => $technician_id, "status" => 0]);
            }
        }
        return 1;
    }
    public function UpdateServiceComplaintStatus(Request $request)
    {
        $id = $request->input('ref');
        $status = $request->input('sta');
        $service_id = $request->input('service_id');
        $data['status'] = $status;
        if ($status == 1) {
            $service_transfer=$this->GetFirst('service_transfer', array("service_id" => $service_id, "status" => 1));
            if (!empty($service_transfer)) {
                $branch_id = $service_transfer->to_branch;
                $technician_id = $service_transfer->accepted_user;
            } else {
                $branch_id = Auth::user()->branch_id;
                $technician_id = Auth::user()->id;
            }
            $currency = $this->GetCurrencyRate($branch_id);
            $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
            $complaint_id = $this->GetFirst('service_complaint', array("id" => $id))->complaint_id;
            // $level_id = $this->GetFirst('complaints', array("id" => $complaint_id))->level_id;
            // $complaint_rate = $this->GetFirst('levels', array("id" => $level_id))->rate;
            // $local_price = ($complaint_rate * $currency_rate);
            // $data['price']=$complaint_rate;
            // $data['local_price']=$local_price;
        }
        $servicebill_count=$this->GetRowCount('service_bill',array("service_id" => $service_id), 'id');
        if($servicebill_count>0)
        {
            $this->updatedata('service', array("status" => 1), $service_id);
        }
        return $update = $this->updatedata('service_complaint', $data, $id);
    }
    public function addServiceSpare(Request $request)
    {
        $spare_id = $request->input('spare');
        $service_id = $request->input('service');
        $service_transfer=$this->GetFirst('service_transfer', array("service_id" => $service_id, "status" => 1));
        if (!empty($service_transfer)) {
            $branch_id = $service_transfer->to_branch;
        } else {
            $branch_id = $this->FieldByID('service','branch_id',$service_id);
        }
        $currency = $this->GetCurrencyRate($branch_id);
        $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        $stock = DB::table('stock')->where(['spare_id' => $spare_id, "branch_id" => $branch_id])->first();
        $stock = json_decode(json_encode($stock), 1);
        $quantity = $request->input('qty');
        if (empty($stock)) {
            return 'NA';
        }
        if ($quantity > $stock['qty']) {
            return 'NA';
        } else {
            $stock_qty = $stock['qty'] - $quantity;
        }
        $price = $request->input('price');
        $cus_price = $request->input('cus_price');
        $warranty = $request->input('warranty');
        $insdata = array();
        $insdata['spare_id'] = $spare_id;
        $insdata['service_id'] = $service_id;
        $insdata['is_warranty'] = $warranty;
        $insdata['price'] = DB::table('stock')->where('spare_id', $spare_id)->first()->price;
        $insdata['local_price'] = $price;
        $insdata['cus_local_price'] = $cus_price;
        $insdata['quantity'] = $quantity;
        if ($cus_price == 0) {
            $insdata['cus_price'] = 0;
        } else {
            $insdata['cus_price'] = ($cus_price/$currency_rate);
        }
        $insdata['branch_id'] = $branch_id;
        $insdata['created_by'] = Auth::user()->id;
        if(isset($stock_qty))
        {
            $updateData['qty'] = $stock_qty;
            $this->updatedata('stock', $updateData, $stock['id']);
        }
        $servicespare = DB::table('service_spare')->where(['spare_id' => $spare_id, "service_id" => $service_id, "is_warranty" => $warranty])->first();
        $servicespare = json_decode(json_encode($servicespare), 1);
        if (!empty($servicespare) && $warranty==1) {
            $updatespare['quantity'] = $servicespare['quantity'] + $quantity;
            return $this->updatedata('service_spare', $updatespare, $servicespare['id']);
        }
        else {
            return DB::table('service_spare')->insertGetId($insdata);
        }
    }
    public function getSparePrice(Request $request)
    {
        if ($request->table == "stock_service") {
            $branch_id = $this->FieldByID('stock_service','branch_id',$request->service_id);
        } else {
            $service_transfer=$this->GetFirst('service_transfer', array("service_id" => $request->service_id, "status" => 1));
            if (!empty($service_transfer)) {
                $branch_id = $service_transfer->to_branch;
            } else {
                $branch_id = $this->FieldByID('service','branch_id',$request->service_id);
            }
        }
        $spare = $request->spare;
        $spareprice = DB::table('stock')->where(['spare_id'=>$spare,"branch_id"=>$branch_id])->first();
        if (!empty($spareprice)) {
            return number_format((float)($spareprice->local_price), 2, '.', '');
        } else {
            return 1;
        }
    }
    public function addServiceCharge(Request $request)
    {
        $service_charge = $request->input('service_charge');
        $discount = $request->input('discount');
        $service_id = $request->input('service');
        $service_transfer=$this->GetFirst('service_transfer', array("service_id" => $service_id, "status" => 1));
        if (!empty($service_transfer)) {
            $branch_id = $service_transfer->to_branch;
        } else {
            $branch_id = Auth::user()->branch_id;
        }
        $currency = $this->GetCurrencyRate($branch_id);
        $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
        $data['service_id'] = $service_id;
        $data['branch_id'] = $branch_id;
        $data['created_by'] = Auth::user()->id;
        $data['local_price'] = $service_charge;
        $data['price']=$service_charge/$currency_rate;
        $update_data['discount_local_price'] =  $discount;
        $update_data['discount'] = $discount/$currency_rate;
        $service_charge=$this->GetFirst('service_charge', array("service_id" => $service_id, "branch_id" => $branch_id));
        if(!empty($service_charge))
        {
            $this->updatedata('service_charge', $data, $service_charge->id);
            $this->updatedata('service', $update_data, $service_id);
            $servicebill_count=$this->GetRowCount('service_bill',array("service_id" => $service_id), 'id');
            if($servicebill_count>0)
            {
                $this->updatedata('service', array("status" => 1), $service_id);
            }
        }
        else
        {
            return DB::table('service_charge')->insertGetId($data);
        }
    }
    public function serviceStatusUpdate(Request $request)
    {
        $service_id = $request->input('service');
        $where_total = array("service_id" => $service_id);
        $where_pending = array("service_id" => $service_id, "status" => '0');
        $total_complaint = $this->GetRowCount('service_complaint', $where_total, 'id');
        $pending_complaint = $this->GetRowCount('service_complaint', $where_pending, 'id');
        $spare_count = $this->GetRowCount('service_spare', $where_total, 'id');
        $service=$this->GetFirst('service', ["id" => $service_id]);


        if ($pending_complaint!=0) {
            Session::put('spare_set', 0);
            return 2;
        }   else {
            Session::put('spare_set', 0);
            $service_data['status'] = 4;
            $insData['service_id'] = $service_id;
            $insData['status'] = 4;
            $insData['created_by'] = Auth::id();
            $update_service = $this->updatedata('service', $service_data, $service_id);
            $insServiceStatus = $this->insertdata('service_status', $insData);
            if ($spare_count == 0 && Session::get('spare_set') == 0 && $service->status == 1) {
                Session::put('spare_set', 1);
                return 0;
            } else {
                return 1;
            }
        }
    }
    public function serviceBill($id)
    {
        $branch_id = Auth::user()->branch_id;
        $user_id = Auth::user()->id;
        $bill_number = 'CB' . $this->serialNumber('service_bill', $branch_id);
        $insBilldata['branch_id'] = $branch_id;
        $insBilldata['user_id'] = $user_id;
        $insBilldata['service_id'] = $id;
        $insBilldata['bill_number'] = $bill_number;
        $insServiceStatus = $this->insertdata('service_bill', $insBilldata);
        $service_data['status'] = 5;
        $update_service = $this->updatedata('service', $service_data, $id);
        $insData['service_id'] = $id;
        $insData['status'] = 5;
        $insData['created_by'] = Auth::id();
        $insServiceStatus = $this->insertdata('service_status', $insData);
        return 1;
    }
    public function serviceBillRegenerate(Request $request, $id)
    {
        $branch_id = Auth::user()->branch_id;
        $user_id = Auth::user()->id;
        $servicebill=$this->GetFirst('service_bill', array("service_id" => $id));
        $sb_id=$servicebill->id;
        DB::table('service_bill')->where('id', $sb_id)->delete();
        // echo "<pre>"; print_r($servicebill); echo "</pre>"; exit;
        $bill_number = 'CB' . $this->serialNumber('service_bill', $branch_id);
        $insBilldata['branch_id'] = $branch_id;
        $insBilldata['user_id'] = $user_id;
        $insBilldata['service_id'] = $id;
        $insBilldata['bill_number'] = $bill_number;
        $insBilldata['description'] = $request->work_description;
        $udata['status'] = 5;
        $this->updatedata("service", $udata, $id);
        return $insServiceStatus = $this->insertdata('service_bill', $insBilldata);
    }

    public function getServiceReceipt($id)
    {
        $this->data['service'] = $this->getServiceById($id);
        //return view('admin.service.receipt', $this->data);
        $pdf = PDF::loadView('admin.service.receipt', $this->data);
        return $pdf->stream('receipt.pdf');
    }

    public function update(Request $request, $id)
    {
        $post = array();
        $post = $this->GetServiceForm($post, $request, $id);
        $post['updated_by'] = Auth::id();
        $post['status']=1;
        DB::beginTransaction();
        if($request->is_warranty==1)
        {
            $serviceDet=$this->GetFirst('service', array("id" => $id));
            $imeiDet=$this->GetFirst('imei_details', array("id" => $serviceDet->imei_id));
            $country_id=$this->FieldByID("branch", "country_id", $serviceDet->branch_id);
            $warranty=$this->FieldByID("country", "warranty", $country_id);
            $international_warranty = date('Y-m-d', strtotime(" + 1 years", strtotime($imeiDet->activation_date)));
            $expiry_date = date('Y-m-d', strtotime("+" . $warranty . " months", strtotime($imeiDet->activation_date)));
            if($imeiDet->expiry_date=='')
            {
                $update_data['expiry_date']=$expiry_date;
                $update_data['international_warranty']=$international_warranty;
                $this->updatedata('imei_details', $update_data, $imeiDet->id);
            }
        }
        $this->updatedata('service', $post, $id);
        if($request->has('shop_name') || $request->has('invoice_no'))
        {
            $service_det=$this->GetFirst('service', array("id" => $id));
            $salereq = array();
            $salereq['shop_name'] = $request->input('shop_name');
            $salereq['invoice_no'] = $request->input('invoice_no');
            $this->updatedata('sales_request', $salereq, $service_det->sales_request_id);
        }
        if ($request->hasfile('filename')) {
            foreach ($request->file('filename') as $file) {
                $name =
                    time() .
                    rand(1, 100) .
                    '.' .
                    $file->getClientOriginalName();
                $file->move(public_path() . '/uploads/service/', $name);
                $file = new ServiceFile();
                $file->filename = $name;
                $file->service_id = $id;
                $file->save();
            }
        }

        if ($request->hasfile('invoice')) {
            foreach ($request->file('invoice') as $file) {
                $name =
                    time() .
                    rand(1, 100) .
                    '.' .
                    $file->getClientOriginalName();
                $file->move(public_path() . '/uploads/invoice/', $name);
                $file = new InvoiceFile();
                $file->filename = $name;
                $file->service_id = $id;
                $file->save();
            }
        }
        DB::commit();
        return back()->with("success", "Updated successfully.");
    }

    public function removeImage(Request $request)
    {
        if ($request->type == "invoice") {
            $file_name = $this->FieldByID("invoice_file", "filename", $request->id);
            $file_path = public_path() . '/uploads/invoice/' . $file_name;
            $file = InvoiceFile::findOrFail($request->id);
        } else {
            $file_name = $this->FieldByID("service_image", "filename", $request->id);
            $file_path = public_path() . '/uploads/service/' . $file_name;
            $file = ServiceFile::findOrFail($request->id);
        }
        if (File::exists($file_path)) {
            File::delete($file_path);
        }
        $file->delete();
        return $request->id;
    }

    public function transferCreate(Request $request)
    {
        $post = array();
        $post = $this->GetTransferForm($post, $request);
        $post['from_branch'] = Auth::user()->branch_id;
        $post['from_user'] = Auth::id();
        if ($this->isModelRecordExist("service_transfer", "service_id", $request->service_id)) {
            $this->deletedata("service_transfer", "service_id", $request->service_id);
        }
        $this->insertdata("service_transfer", $post);
    }

    public function checkPhoneExists(Request $request)
    {
        $customer = DB::table("service_customers")
            ->select("parent_id")
            ->where("customer_id", "=", $request->customer_id)
            ->first();
        $customer_id = ($customer ? $customer->parent_id : 0);
        $isExists  = DB::table('customers')
            ->where('customer_phone', '=', $request->customer_phone)
            ->where('customer_id', '=', $customer_id)
            ->first();
        if ($isExists) {
            return response()->json(false);
        } else {
            return response()->json(true);
        }
    }

    public function checkEmailExists(Request $request)
    {
        $customer = DB::table("service_customers")
            ->select("parent_id")
            ->where("customer_id", "=", $request->customer_id)
            ->first();
        $customer_id = ($customer ? $customer->parent_id : 0);
        $isExists  = DB::table('customers')
            ->where('customer_email', '=', $request->customer_email)
            ->where('customer_id', '=', $customer_id)
            ->first();
        if ($isExists) {
            return response()->json(false);
        } else {
            return response()->json(true);
        }
    }

    public function serviceViewBill($id)
    {
        $this->data['service_bill'] = $this->getServiceBill($id);
        // echo "<pre>"; print_r($this->data['service_bill']); echo "</pre>"; exit;
        $pdf = PDF::loadView('admin.service.bill', $this->data);
        return $pdf->stream('bill.pdf');
    }
    public function removeComplaint(Request $request)
    {
        $complaint_id=$request->input('complaint');
        return DB::table('service_complaint')->where('id', $complaint_id)->delete();
    }
    public function removeServiceSpare(Request $request)
    {
        $service_spareid=$request->input('spare');
        if($this->spareRestore($service_spareid))
        {
            return DB::table('service_spare')->where('id', $service_spareid)->delete();
        }
    }
    public function serviceCancel($service_id)
    {
        $data['status']=6;
        $this->updatedata("service", $data, $service_id);
        return route('service.list');
    }

    public function sentAppealRequest(Request $request) {
        if($request->service_type == 1) {
            $post['level_id'] = $request->level_id;
            $post['level_price'] = ($request->rate??0);
            $currency = $this->GetCurrencyRate(Auth::user()->branch_id);
            $currency_rate = ($currency[0] ? $currency[0]->rate : 0);
            $post['level_local_price'] = ($post['level_price']*$currency_rate);
            $post['is_approved'] = 1;
            $post['is_approved_by'] = Auth::user()->id;
            $post['is_approved_at'] = date("Y-m-d H:i:s");
            $this->updatedata($request->table,$post,$request->service_id);
            return true;
        } else {

            $post['serial_no'] = "APPE".$this->serialNumber("appeal_master",Auth::user()->branch_id);
            if ($request->table == "stock_service") {
                $post['stock_service_id'] = $request->service_id;
                $branch = $this->GetFirst("stock_service",["id" => $post['stock_service_id']]);
            } else {
                $post['service_id'] = $request->service_id;
                $branch = $this->GetFirst("service_bill",array("service_id" => $post['service_id']));
            }
            $post['from_branch'] = Auth::user()->branch_id;
            $post['to_branch'] = $branch->branch_id;
            $post_data['parent_id'] = $this->insertGetId("appeal_master",$post);
            $post_data['msg'] = $request->appeal_remarks;
            $post_data['created_by'] = Auth::user()->id;
            $this->insertdata("appeal_rel_comments",$post_data);
            return true;
        }
    }

    public function getAppeal(Request $request, $id) {
        $this->data['comments'] = DB::table('appeal_master')
                                    ->select('appeal_master.id','appeal_rel_comments.msg','appeal_rel_comments.file','appeal_rel_comments.created_at','appeal_master.serial_no','users.first_name','appeal_master.status')
                                    ->leftJoin('appeal_rel_comments', 'appeal_master.id', '=', 'appeal_rel_comments.parent_id')
                                    ->leftJoin('users', 'users.id', '=', 'appeal_rel_comments.created_by')
                                    ->where('appeal_master.service_id', '=', $id)
                                    ->orderBy('appeal_rel_comments.id','DESC')
                                    ->get();
        return view('admin.service.appeal', $this->data);
    }

    public function addAppeal(Request $request, $id) {
        $this->validate($request, [
            'comment' => 'required',
            // 'filenames' => 'required',
            // 'filenames.*' => 'mimes:doc,pdf,docx,zip,jpg,png,jpeg'
        ]);
        $data = [];
        if($request->hasfile('filenames'))
        {
           foreach($request->file('filenames') as $file)
           {
               $name = time().'.'.$file->extension();
               $file->move(public_path().'/uploads/appeal/', $name);
               $data[] = $name;
           }
        }
        $file = new AppealComment();
        $file->parent_id = $id;
        $file->msg = $request->comment;
        $file->created_by  = Auth::user()->id;
        $file->file = ($data?json_encode($data):NULL);
        $file->save();
        return back()->with('success', 'Comment has been successfully added');
    }

    public function updateAppeal(Request $request) {
        $data['status'] = $request->type;
        $this->updatedata("appeal_master", $data, $request->comment_id);
        $service_id = $this->FieldByID('appeal_master','service_id',$request->comment_id);
        $table = "";
        if (empty($service_id)) {
            $service_id = $this->FieldByID('appeal_master','stock_service_id',$request->comment_id);
            $table = "stock_service/";
        }
        if ($data['status'] == 1)  return 'service/'.$table.$service_id;
        else return 'service/'.$table.$service_id.'/appeal/get';
    }

    public function updateVersion (Request $request) {
        $data['new_version_id'] = $request->version_id;
        $this->updatedata($request->table, $data, $request->service_id);
        return true;
    }

    public function updateWarranty (Request $request) {
        $data['is_warranty'] = $request->is_warranty;
        $udata['warranty_end_date'] = date('Y-m-d');
        $imei_id = $this->FieldByID('service','imei_id',$request->service_id);
        $this->updatedata("service", $data, $request->service_id);
        if ($data['is_warranty'] == 0) {
            $this->updatedata("imei_details", $udata, $imei_id);
            $spare = $this->WhereByData('service_spare','service_id',$request->service_id);
            if ($spare) {
                foreach ($spare as $val) {
                    $this->updatedata("service_spare", $data, $val->id);
                }
            }
        }
        return true;
    }

    public function updateDescription (Request $request) {
        $data['remarks'] = $request->remarks;
        $this->updatedata("service_complaint", $data, $request->complaint_id);
        return true;
    }

    public function approveList() {
        return view('admin.service.approve', $this->data);
    }
    public function approveListGet () {
        $data = DB::table('service')
        ->leftJoin('branch', 'branch.id', '=', 'service.branch_id')
        ->leftJoin(
            'service_customers',
            'service_customers.customer_id',
            '=',
            'service.customer_id'
        )
        ->leftJoin('brands', 'brands.id', '=', 'service.brand_id')
        ->leftJoin(
            'variant_details',
            'variant_details.id',
            '=',
            'service.variant_id'
        )
        ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')
        ->leftJoin('color', 'color.id', '=', 'variant_details.color_id')
        ->leftJoin('ram', 'ram.id', '=', 'variant_details.ram_id')
        ->leftJoin('rom', 'rom.id', '=', 'variant_details.rom_id')
        ->leftJoin('doa', 'service.id', '=', 'doa.service_id')
        ->select([
            'service.id',
            'service.created_at',
            'branch.name as branch_name',
            'service.serial_no',
            'service.status',
            'service_customers.customer_name',
            'service_customers.customer_phone',
            'service.is_warranty',
            'brands.name as brand_name',
            'models.name as model_name',
            'color.name as color',
            'ram.name as ram',
            'rom.name as rom',
            'doa.id as doa_id',
            'doa.status as doa_status',
            'service.is_approved'
        ])->where('service.is_approved', 1);
    if (Session::get('sessi_branch_id')) {
        $data->whereIn('service.branch_id', explode(',',Session::get('sessi_branch_id')));
    }

    return DataTables::of($data)
        ->escapeColumns(['created_at', 'branch_name'])
        ->editColumn('created_at', function ($data) {
            return date('d-m-Y', strtotime($data->created_at));
        })
        ->editColumn('is_warranty', function ($data) {
            return $data->is_warranty == 1 ? "Warranty" : "Non Warranty";
        })
        ->editColumn('model_name', function ($data) {
            $model = "";
            if ($data->model_name) {
                $model = $data->model_name . "(" . $data->color;
                if ($data->ram) {
                    $model .= "," . $data->ram;
                }
                if ($data->rom) {
                    $model .= "," . $data->rom;
                }
                $model .= ")";
            }
            return $model;
        })
        ->editColumn('status', function ($data) {
            $status = "";
            if ($data->status == 1) {
                if ($data->doa_id > 0 && $data->doa_status == 0) {
                    $status ='<i class="fa fa-circle text-warning mr-2"></i> DOA Request Pending';
                }
                else {
                    $status ='<i class="fa fa-circle text-warning mr-2"></i> Pending';
                }
            } elseif ($data->status == 5) {
                $status =
                    '<i class="fa fa-circle text-success mr-2"></i> Delivered';
            } elseif ($data->status == 4) {
                $status =
                    '<i class="fa fa-circle text-success mr-2"></i> Completed';
            }
            return $status;
        })
        ->editColumn('serial_no', function ($data) {
            return '<a href="' .
                url('service/' . $data->id) .
                '">' .
                $data->serial_no .
                '</a>';
        })
        ->editColumn('is_approved', function ($data) {
            $status = '<i class="fa fa-times-circle-o text-danger mr-2"></i> Not Verified';
            if ($data->is_approved == 1) {
                $status =
                '<i class="fa fa-check-circle text-success mr-2"></i> Verified';
            }
            return $status;
        })->filterColumn('serial_no', function ($query, $keyword)
        {
            $query->whereRaw('service.serial_no like ?', ["%{$keyword}%"]);
        })->filterColumn('branch_name', function ($query, $keyword)
        {
            $query->whereRaw('branch.name like ?', ["%{$keyword}%"]);
        })->filterColumn('customer_name', function ($query, $keyword)
        {
            $query->whereRaw('service_customers.customer_name like ?', ["%{$keyword}%"]);
        })->filterColumn('customer_phone', function ($query, $keyword)
        {
            $query->whereRaw('service_customers.customer_phone like ?', ["%{$keyword}%"]);
        })->filterColumn('brand_name', function ($query, $keyword)
        {
            $query->whereRaw('brands.name like ?', ["%{$keyword}%"]);
        })->filterColumn('model_name', function ($query, $keyword)
        {
            $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
        })->filterColumn('created_at', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(service.created_at,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function replacePCBNo(Request $request) {
        $imei_id = $this->FieldByID($request->table,'imei_id',$request->service_id);
        if($imei_id) {
            $imei = $this->GetFirst('imei_details',['id'=>$imei_id]);
            $data['imei_id'] =  $imei_id;
            if($request->table == "stock_service") {
                $data['stock_service_id'] =  $request->service_id;
                $checkExist = $this->GetFirst('pcb_history',['stock_service_id'=>$data['stock_service_id'],'imei_id'=>$data['imei_id']]);
            } else {
                $data['service_id'] =  $request->service_id;
                $checkExist = $this->GetFirst('pcb_history',['service_id'=>$data['service_id'],'imei_id'=>$data['imei_id']]);
            }
            $data['pcb_serial_no'] =   $udatah['pcb_serial_no'] =  $imei->pcb_serial_no;
            $udata['pcb_serial_no'] = $request->pcb_serial_no;
            if ($checkExist) {
                $id = $checkExist->id;
                $this->updatedata('pcb_history',$udatah,$id);
            } else {
                $id = $this->insertGetId('pcb_history', $data);
            }
            $checkPCBExist = $this->GetFirst('imei_details',['pcb_serial_no'=>$request->pcb_serial_no]);
            if ($checkPCBExist) {
                $this->updatedata('imei_details',["pcb_serial_no"=>NULL],$checkPCBExist->id);
            }
            $this->updatedata('imei_details',$udata,$imei_id);
        }
       return $id;
    }
    public function updateBrandVariant()
    {
        $services=DB::table("service")->whereNotNull('imei_id')->whereNull('brand_id')->whereNull('variant_id')->get();
        foreach($services as $ser)
        {
            $imei_det=DB::table("imei_details")->leftJoin('variant_details', 'variant_details.id', '=', 'imei_details.variant_id')
                    ->leftJoin('models', 'models.id', '=', 'variant_details.model_id')->select("models.brand_id as brand_id","variant_details.id as variant_id")->where('imei_details.id', $ser->imei_id)->first();
                    echo "<pre>"; print_r($imei_det); echo "</pre>";
            $update_data['brand_id']=$imei_det->brand_id;
            $update_data['variant_id']=$imei_det->variant_id;
            $this->updatedata('service',$update_data,$ser->id);
        }
    }
}

<?php
namespace App\Http\Controllers\Backend\Sales;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportSale;
use Yajra\DataTables\Facades\DataTables;
use App\Models\IMEIDetails;
use Illuminate\Support\Facades\View;
use App\Models\CustomerSale;
use DB;
use Auth;


class SalesController extends Controller
{
    public function __construct()
    {
        $this->data['country'] = $this->NameAndID('country');
        $this->data['model'] = $this->NameAndID('models');
        View::share('js', ['sales','master']);
    }

    public function index(Request $request)
    {
        $this->data['country_id'] = ($request->country_id?$request->country_id:0);
        $this->data['model_id'] = ($request->model_id?$request->model_id:0);
        $this->data['from'] = ($request->from?$request->from:date('01-m-Y'));
        $this->data['to'] = ($request->to?$request->to:date('t-m-Y'));
        return view('admin.sales.index', $this->data);
    }

    public function getSaleData(Request $request) {
        $data = IMEIDetails::leftJoin('customer_sales', 'customer_sales.id', '=', 'imei_details.sale_id')
                    ->leftJoin('users', 'customer_sales.created_by', '=', 'users.id')
                    ->leftJoin('imei','imei.id','=','imei_details.group_id')
                    ->leftJoin('country', 'imei_details.country_id', '=', 'country.id')
                    ->leftJoin('variant_details', 'imei_details.variant_id', '=', 'variant_details.id')
                    ->leftJoin('models', 'variant_details.model_id', '=', 'models.id')
                    ->select(['imei_details.id','imei_details.IMEI_no1','country.name as country_name','imei_details.IMEI_no2','imei_details.sales_date','customer_sales.invoice_no','models.name as model_name','imei_details.activation_date','users.first_name as user_name'])
                    ->groupBy('imei_details.sale_id')->whereNotNull('imei_details.expiry_date');
        if (!empty($request->country_id)) {
            $data->where("imei_details.country_id", $request->country_id);
        }
        if (!empty($request->model_id)) {
            $data->where("models.id", $request->model_id);
        }

        $from = ($request->from?date('Y-m-d',strtotime($request->from)):date('Y-m-01'));
        $to = ($request->to?date('Y-m-d',strtotime($request->to)):date('Y-m-t'));

        $data->whereBetween('imei_details.sales_date', [$from, $to]);
        $data->get();
        return Datatables::of($data)->escapeColumns(['IMEI_no1','country_name'])->editColumn('sales_date', function ($data)
        {
            return date('d-m-Y', strtotime($data->sales_date) );
        })->editColumn('activation_date', function ($data)
        {
            if($data->activation_date) {
                return date('d-m-Y', strtotime($data->activation_date) );
            }

        })->filterColumn('country_name', function ($query, $keyword) {
            $query->whereRaw('country.name like ?', ["%{$keyword}%"]);
        })->filterColumn('model_name', function ($query, $keyword) {
            $query->whereRaw('models.name like ?', ["%{$keyword}%"]);
        })->filterColumn('user_name', function ($query, $keyword) {
            $query->whereRaw('users.first_name like ?', ["%{$keyword}%"]);
        })->filterColumn('sales_date', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(imei_details.sales_date,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
        })->filterColumn('activation_date', function ($query, $keyword) {
            $query->whereRaw(DB::raw("(DATE_FORMAT(imei_details.activation_date,'%d-%m-%Y'))").' like ?', ["%{$keyword}%"]);
        })->filterColumn('invoice_no', function ($query, $keyword) {
            $query->whereRaw('customer_sales.invoice_no like ?', ["%{$keyword}%"]);
        })->addIndexColumn()->make(true);
    }

    public function create()
    {
        return view('admin.sales.create', $this->data);
    }

    public function store(Request $request)
    {
        $this->validate($request, ['file' => 'required|file|mimes:xls,xlsx,csv|max:10240']);
        if ($request->file('file')->isValid())
        {
            Excel::import(new ImportSale, request()
                ->file('file'));
            $data = Excel::toCollection(new ImportSale() , $request->file('file'));
            $record_count = 0;
            $imei_arr = $im_d = $error = $date_valid = $im_ex = $im_sale = $im_country = $im_doa_ex = [];
            if (count($data) > 0)
            {
                DB::beginTransaction();
                foreach ($data[0] as $key => $value)
                {

                    if ($this->transformDate($value['date']) == 1)
                    {
                        $date_valid[] = $value['imeisn'];
                    }
                    if (in_array($value['imeisn'], $imei_arr))
                    {
                        $im_d[] = $value['imeisn'];
                    }
                    $imei_check = DB::table('imei_details')->where('IMEI_no1', '=', $value['imeisn'])->orWhere('IMEI_no2', '=', $value['imeisn'])->orWhere('serial_no', '=', trim($value['imeisn']))->first();
                    $imei_id = ($imei_check ? $imei_check->id : 0);
                    $is_doa  = ($imei_check ? $imei_check->is_doa : 1);
                    if ($imei_id == 0) {
                        $im_ex[] = $value['imeisn'];

                    } elseif($is_doa == 1) {
                        $im_doa_ex[] = $value['imeisn'];
                    } else {
                        $expire_exist = DB::table('imei_details')->where('id', $imei_id)->whereNotNull('expiry_date')->first();
                        $sale_exist = DB::table('imei_details')->where('id', $imei_id)->whereNotNull('sales_date')->first();
                        if($sale_exist) {
                          $im_sale[] = $value['imeisn'];
                        }
                    }

                    if ($this->transformDate($value['date']) != 1) {
                        $sales_date = $this->transformDate($value['date']);
                        $country_id = ($imei_check?$imei_check->country_id:0);
                        if ($country_id == 0) {
                            $im_country[] = $value['imeisn'];
                        } else {
                            $country = $this->GetByID('country',$country_id);
                            $warranty = ($country?$country->warranty:0);
                            $expiry_date = date('Y-m-d', strtotime("+".$warranty." months", strtotime($sales_date)));
                        }
                        if (count($im_d) == 0 && count($date_valid) == 0 && count($im_ex) == 0 && count($im_doa_ex) == 0 && count($im_sale) == 0 && count($im_country) == 0) {
                            try {
                                $sale = CustomerSale::Create(['imei_id' => $imei_id, 'invoice_no' => $value['invoice'], 'sales_date' => $sales_date,'created_by' => Auth::id()]);
                                DB::table('imei_details')->where('id', $imei_id)->update(['sales_date' => $sales_date,'sale_id' => $sale->id]);
                                $record_count = $record_count+1;
                                if(!$expire_exist) {
                                    DB::table('imei_details')->where('id', $imei_id)->update(['expiry_date' => $expiry_date]);
                                }
                            } catch (\Illuminate\Database\QueryException $e) {
                                  echo $e;
                            }
                        }
                    }
                }
            }

            if (count($im_d) > 0)
            {
                $error[] = "Duplication on uploaded excel file  :  " . implode(',', array_unique($im_d));
            }
            if (count($date_valid) > 0)
            {
                $error[] = "Invalid date : " . implode(',', array_unique($date_valid));
            }
            if (count($im_ex) > 0)
            {
                $error[] = "IMEI / Serial Number Not found in databse :  " . implode(',', array_unique($im_ex));
            }
            if (count($im_doa_ex) > 0)
            {
                $error[] = "This IMEI / Serial nos already exists in DOA list:  " . implode(',', array_unique($im_doa_ex));
            }
            if (count($im_sale) > 0)
            {
                $error[] = "Already Registered Sales : " . implode(',', array_unique($im_sale));
            }
            if (count($im_country) > 0)
            {
                $error[] = "Kindly transfer IMEI / Serial Number " . implode(',', array_unique($im_country)) ."to any country";
            }
            if (count($error) > 0)
            {
                return redirect()->back()
                    ->with("info", array_unique($error));
            }
            else
            {
                DB::commit();
                return redirect()->back()->with("success", $record_count." Records Uploaded Successfully!");
            }

        }
    }
}

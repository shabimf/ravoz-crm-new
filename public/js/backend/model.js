(function () {

    FTX.Models = {

        list: {

            selectors: {
                model_table: $('#model-table'),
            },

            init: function () {

                this.selectors.model_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.model_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        {
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'brand', name: 'brand' },
                        { data: 'name', name: 'name' },
                        { data: 'factory_name', name: 'factory_name' },
                        { data: 'device', name: 'device' },
                        { data: 'view', name: 'view', searchable: false, sortable: false },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [[0, "asc"]],
                    searchDelay: 500,

                    "createdRow": function (row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
})();

<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;

class ImportDevice implements ToCollection, WithHeadingRow
{
    protected $type;

    function __construct($type) {
        $this->data['type'] = $type;
    }
    public function collection(Collection $rows)
    {
        if($this->data['type']==1)
        {
            Validator::make($rows->toArray(), [
                '*.imei_1' => 'required',
                '*.imei_2' => 'required',
                '*.sn' => 'required',
            ])->validate();
        }
        else
        {
            Validator::make($rows->toArray(), [
                '*.sn' => 'required',
            ])->validate();
        }

    }



}

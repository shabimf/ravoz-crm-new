@extends('layouts.service')
@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-9">
                    <h4 class="card-title-text">Level Claim Report</h4>
                </div>
            </div>
            <div class="row ">
                <div class="col-lg-12">
                    {{ Form::open(['route' => 'report.level.claim', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'get', 'id' => 'search-complaint']) }}
                    <div class="form-row well">
                        <div class="form-group col-md-3">
                            {{ Form::label('name', __('Company')) }}
                            {{ Form::select('company_id', $company, old('company_id', isset($company_id) ? $company_id : ''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Company', 'id' => 'company_id', 'required' => 'required']) }}
                        </div>
                        <div class="form-group col-md-3">
                            {{ Form::label('name', __('Year')) }}
                            <select id="year" name="year" class="form-control js-example-basic-single"
                                placeholder="Select">
                                {{ $last = date('Y') - 5 }}
                                {{ $now = date('Y') }}
                                @for ($i = $now; $i >= $last; $i--)
                                    <option value="{{ $i }}" {{ $i == $year ? 'selected' : '' }}>{{ $i }}
                                    </option>
                                @endfor
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            {{ Form::label('name', __('Type')) }}
                            {{ Form::select('type_id', ['1' => 'Service', '2' => 'Stock Service'], old('type_id', isset($type_id) ? $type_id : '1'), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Type', 'id' => 'type_id', 'required' => 'required']) }}
                        </div>
                        <div class="form-group col-md-3">
                            <button class="btn btn-primary search-btn">Search</button>
                            <a class="btn btn-primary reset-btn" href="{{ route('report.level.claim') }}">Reset</a>
                        </div>
                    </div>
                    {{ Form::close() }}
					
                    @if (isset($cliam) && count($cliam) > 0 )
                        <div class="card">
                            <div class="card-body">

                                <div class="table-responsive">
                                    <table id="level-claim-table" class="table table-centered table-hover mb-0" style="padding-top:5px ;">
                                        <thead>
                                            <tr role="row">
                                                <th rowspan="1" colspan="1"></th>
                                                @foreach ($levels as $level)
                                                    <th colspan="3" rowspan="1">{{ $level }}</th>
                                                @endforeach
                                                <th rowspan="1" colspan="1"></th>
                                            </tr>
                                            <tr role="row">
                                                <th rowspan="1" colspan="1">Month</th>
                                                @foreach ($levels as $level)
                                                    <th rowspan="1" colspan="1">Unit Price</th>
                                                    <th rowspan="1" colspan="1">No Of Unit</th>
                                                    <th rowspan="1" colspan="1">Total</th>
                                                @endforeach
                                                <th rowspan="1" colspan="1">Month Total</th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            @foreach ($months as $key => $m)
                                                <tr>
                                                    <td>{{ $m }}</td>
                                                    @php $month_total = 0; @endphp
                                                    @foreach ($levels as $leve_name)
                                                        @php
                                                            $total = 0;
                                                            if (isset($cliam[$key][$leve_name])) {
                                                                $total = $cliam[$key][$leve_name]->level_price * $cliam[$key][$leve_name]->qty;
                                                            }
                                                            $month_total += $total;
                                                        @endphp
                                                        <td>{{ isset($cliam[$key][$leve_name]) ? $cliam[$key][$leve_name]->level_price : '--' }}
                                                        </td>
                                                        <td>{{ isset($cliam[$key][$leve_name]) ? $cliam[$key][$leve_name]->qty : '--' }}
                                                        </td>
                                                        <td>{{ isset($cliam[$key][$leve_name]) ? $total : '--' }}</td>
                                                    @endforeach
                                                    <td>{{ $month_total }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @elseif(count($cliam) == 0 && ($type_id)>0 && ($company_id)>0)
                        <div class="alert alert-dark" role="alert">
                            No data available in table 
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pagescript')
    @include('inc.datatables-js')
    <script src="{{ asset('js/select2/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
			
            $('#level-claim-table').DataTable({
                dom: 'Bfrtip',
                "pageLength": 15,
                "bFilter": false,
                buttons: [
					{ extend: 'excelHtml5', className: 'btn btn-primary buttons-excel ',title: 'Level Claim Report' },
                ]
            });
        });
    </script>
@endsection

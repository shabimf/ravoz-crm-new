<?php

namespace App\Http\Controllers\Backend;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use DB;
use Auth;
use Session;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->data['country'] = $this->NameAndID('country');

        View::share('js', ['master']);
    }


    public function getWorkStatusData(Request $request)
    {
        if(isset($request->from) && $request->from!='')
        {
            $from=$request->from;
        }
        else
        {
            // $from=date('Y-m-d', strtotime('-30 days')).' 00:00:00';
            $from = "";
        }
        if(isset($request->to) && $request->to!='')
        {
            $to=$request->to;
        }
        else
        {
            // $to=date('Y-m-d').' 23:59:59';
            $to = "";
        }

        $this->data['service']['pending'] = $this->getDashboardCount('service', [["status",'=', 1],["created_at",">=",$from],["created_at","<=",$to]], 'id',Session::get('sessi_branch_id'));
        $this->data['service']['ready'] = $this->getDashboardCount('service', [["status",'=', 4],["created_at",">=",$from],["created_at","<=",$to]], 'id',Session::get('sessi_branch_id'));
        $this->data['service']['deliver'] = $this->getDashboardCount('service', [["status",'=', 5],["created_at",">=",$from],["created_at","<=",$to]], 'id',Session::get('sessi_branch_id'));
        $this->data['service']['warranty'] = $this->getDashboardCount('service', [["status",'!=', 6],["created_at",">=",$from],["created_at","<=",$to],["is_warranty",'=', 1]], 'id',Session::get('sessi_branch_id'));
        $this->data['service']['nonwarranty'] = $this->getDashboardCount('service', [["status",'!=', 6],["created_at",">=",$from],["created_at","<=",$to],["is_warranty",'=', 0]], 'id',Session::get('sessi_branch_id'));
        $this->data['service']['salesrequest'] = $this->getDashboardCount('sales_request', [["status",'=', 0],["created_at",">=",$from],["created_at","<=",$to]], 'id',Session::get('sessi_branch_id'));
        $this->data['service']['appealrequest'] = $this->getDashboardCount('service', [["service.is_warranty","=", 1],["service.created_at",">=",$from],["service.created_at","<=",$to],["service.status",'=', 5],["service.is_approved",'=', 0]], 'service.id',Session::get('sessi_branch_id'),1);
        $this->data['service']['purchaserequest'] = $this->getDashboardCount('order', [["status",'=', 0],["created_at",">=",$from],["created_at","<=",$to]], 'id',Session::get('sessi_branch_id'));
        $this->data['service']['doarequest'] = $this->getDashboardCount('doa', [["status",'=', 0],["created_at",">=",$from],["created_at","<=",$to]], 'id',Session::get('sessi_branch_id'));
        $this->data['service']['appeal_request'] = $this->getDashboardCount('appeal_master', [["status",'=', 0],["created_at",">=",$from],["created_at","<=",$to]], 'id',Session::get('sessi_branch_id'));
        $this->data['service']['stock_service'] = $this->getDashboardCount('stock_service', [["is_approved",'=', 0],["created_at",">=",$from],["created_at","<=",$to],["status",'=', 4]], 'id',Session::get('sessi_branch_id'));
        $this->data['service']['from']=$from;
        $this->data['service']['to']=$to;
        return response()->json($this->data['service']);
        // return response()->json(array("service" => $this->data['service'], "from" => $from, "to" => $to));

    }

}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\IMEIDetails;

class SearchController extends Controller
{
    public function autocomplete(Request $request)
    {
        $input = $request->all();

        $data = IMEIDetails::select(["id", "IMEI_no1"])
                ->where("IMEI_no1","LIKE","%{$input['query']}%")
                ->get();
   
      $imei_details = [];
      
      if(count($data) > 0){

            foreach($data as $imei){
                $imei_details[] = array(
					"value" => $imei->id,
					"label" => $imei->IMEI_no1,
				);
            }
        }        
        return response()->json($imei_details);
    }
}

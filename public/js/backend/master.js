(function() {

    FTX.Master = {

        list: {

            selectors: {
                master_table: $('#master-table'),
            },

            init: function() {

                this.selectors.master_table.dataTable({

                    processing: false,
                    serverSide: true,

                    ajax: {
                        url: this.selectors.master_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'name', name: 'name' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }


    FTX.Currency = {

        list: {

            selectors: {
                currency_table: $('#currency-table'),
            },

            init: function() {

                this.selectors.currency_table.dataTable({

                    processing: false,
                    serverSide: true,

                    ajax: {
                        url: this.selectors.currency_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'name', name: 'name' },
                        { data: 'rate', name: 'rate' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }

    FTX.Country = {

        list: {

            selectors: {
                country_table: $('#country-table'),
            },

            init: function() {

                this.selectors.country_table.dataTable({

                    processing: false,
                    serverSide: true,

                    ajax: {
                        url: this.selectors.country_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'name', name: 'name' },
                        { data: 'currency', name: 'currency' },
                        { data: 'time_zone', name: 'time_zone' },
                        { data: 'warranty', name: 'warranty' },
                        { data: 'tax', name: 'tax' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }

    FTX.Users = {

        list: {

            selectors: {
                user_table: $('#users-table'),
            },

            init: function() {

                this.selectors.user_table.dataTable({

                    processing: false,
                    serverSide: true,
                    cache: false,
                    ajax: {
                        url: this.selectors.user_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'first_name', name: 'first_name' },
                        { data: 'username', name: 'username' },
                        { data: 'role', name: 'role' },
                        { data: 'branch', name: 'branch' },
                        { data: 'email', name: 'email' },
                        { data: 'phoneno', name: 'phoneno' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false },
                        { data: 'google_status', name: 'google_status', searchable: false, sortable: false },
                        { data: 'google2fa_secret', name: 'google2fa_secret', searchable: false, sortable: false }

                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }

    FTX.Spare = {

        list: {

            selectors: {
                spare_table: $('#spare-table'),
            },

            init: function() {

                this.selectors.spare_table.dataTable({

                    processing: false,
                    serverSide: true,

                    ajax: {
                        url: this.selectors.spare_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'spare_name', name: 'spare_name' },
                        { data: 'part_code', name: 'part_code' },
                        { data: 'focus_code', name: 'focus_code' },
                        { data: 'unique_code', name: 'unique_code' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }

    FTX.Branch = {

        list: {

            selectors: {
                branch_table: $('#branch-table'),
            },

            init: function() {

                this.selectors.branch_table.dataTable({

                    processing: false,
                    serverSide: true,

                    ajax: {
                        url: this.selectors.branch_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'name', name: 'name' },
                        { data: 'code', name: 'code' },
                        { data: 'country', name: 'country' },
                        { data: 'company', name: 'company' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }

    FTX.Softwareversion = {

        list: {

            selectors: {
                software_table: $('#software-table'),
            },

            init: function() {

                this.selectors.software_table.dataTable({

                    processing: false,
                    serverSide: true,
                    cache: false,
                    ajax: {
                        url: this.selectors.software_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'name', name: 'name' },
                        { data: 'model', name: 'model' },
                        { data: 'url', name: 'url' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
    FTX.Faqs = {

        list: {

            selectors: {
                faq_table: $('#faq-table'),
            },

            init: function() {

                this.selectors.faq_table.dataTable({

                    processing: false,
                    serverSide: true,

                    ajax: {
                        url: this.selectors.faq_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'question', name: 'question' },
                        { data: 'answer', name: 'answer' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }

    FTX.DOA = {

        list: {

            selectors: {
                doa_table: $('#doa-table'),
            },

            init: function(branch_id, from, to, imei, serial_num, status) {

                this.selectors.doa_table.dataTable({

                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.doa_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.branch_id = branch_id,
                                d.imei = imei,
                                d.serial_num = serial_num,
                                d.status = status
                        },

                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'branch_name', name: 'branch_name' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'IMEI_no1', name: 'IMEI_no1' },
                        { data: 'imei_sn', name: 'imei_sn' },
                        { data: 'replace_imei1', name: 'replace_imei1' },
                        { data: 'status', name: 'status', searchable: false, sortable: false },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
    FTX.STOCKSERVICE = {

        list: {

            selectors: {
                stock_service_table: $('#stock-service-table'),
            },

            init: function(branch_id, from, to, imei, is_approved) {

                this.selectors.stock_service_table.dataTable({

                    processing: false,
                    serverSide: true,

                    ajax: {
                        url: this.selectors.stock_service_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to,
                                d.branch_id = branch_id,
                                d.imei = imei,
                                d.is_approved = is_approved
                        },

                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'branch_name', name: 'branch_name' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'IMEI_no1', name: 'IMEI_no1' },
                        { data: 'imei_sn', name: 'imei_sn' },
                        { data: 'status', name: 'status', searchable: false, sortable: false },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                    ],
                    order: [
                        [2, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
    FTX.SPARESALE = {

        list: {

            selectors: {
                Spare_Sale_table: $('#spare-sale-table'),
            },

            init: function(from, to) {
                this.selectors.Spare_Sale_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.Spare_Sale_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'branch_name', name: 'branch_name' },
                        { data: 'first_name', name: 'first_name' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }

    FTX.LevelRate = {

        list: {

            selectors: {
                Level_Rate_table: $('#level-rate-table'),
            },

            init: function(from, to) {
                this.selectors.Level_Rate_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.Level_Rate_table.data('ajax_url'),
                        type: 'post'
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'date', name: 'date' },

                        { data: 'company_name', name: 'company_name' },
                        { data: 'level_name', name: 'level_name' },
                        { data: 'rate', name: 'rate' },

                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }


    FTX.Activation = {
        list: {

            selectors: {
                activation_table: $('#activation-table'),
            },

            init: function(from, to) {
                this.selectors.activation_table.dataTable({
                    processing: false,
                    serverSide: true,
                    pageLength: 10,
                    "bFilter": false,
                    cache: false,
                    ajax: {
                        url: this.selectors.activation_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.from = from,
                                d.to = to
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'country_name', name: 'country_name' },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'IMEI_no1', name: 'IMEI_no1' },
                        { data: 'IMEI_no2', name: 'IMEI_no2' },
                        { data: 'serial_no', name: 'serial_no' },
                        { data: 'color_name', name: 'color_name' },
                        { data: 'ram_name', name: 'ram_name' },
                        { data: 'rom_name', name: 'rom_name' },
                        { data: 'date_of_manufacture', name: 'date_of_manufacture' },
                        { data: 'pcb_serial_no', name: 'pcb_serial_no' },
                        { data: 'activation_date', name: 'activation_date' },
                        { data: 'expiry_date', name: 'expiry_date' },

                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }

    FTX.Bom = {

        list: {

            selectors: {
                bom_table: $('#bom-table'),
            },

            init: function(model_id) {
                this.selectors.bom_table.dataTable({
                    processing: false,
                    serverSide: true,
                    cache: false,
                    ajax: {
                        url: this.selectors.bom_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.model_id = model_id
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'model_name', name: 'model_name' },
                        { data: 'spare_name', name: 'spare_name' },
                        { data: 'part_code', name: 'part_code' },
                        { data: 'focus_code', name: 'focus_code' },
                        { data: 'unique_code', name: 'unique_code' }
                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
    FTX.Company = {
        list: {
            selectors: {
                company_table: $('#company-table'),
            },

            init: function() {
                this.selectors.company_table.dataTable({
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: this.selectors.company_table.data('ajax_url'),
                        type: 'post',
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'name', name: 'name' },
                        { data: 'currency_name', name: 'currency_name' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                        { data: 'status', name: 'status', searchable: false, sortable: false }
                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }
    FTX.Software = {

        list: {

            selectors: {
                software_table: $('#software-download-table'),
            },

            init: function(model_id) {
                this.selectors.software_table.dataTable({
                    processing: false,
                    serverSide: true,
                    cache: false,
                    ajax: {
                        url: this.selectors.software_table.data('ajax_url'),
                        type: 'post',
                        data: function(d) {
                            d.model_id = model_id
                        },
                    },
                    columns: [{
                            "data": 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        { data: 'model', name: 'model' },
                        { data: 'name', name: 'name' },
                        { data: 'actions', name: 'actions', searchable: false, sortable: false },
                    ],
                    order: [
                        [0, "asc"]
                    ],
                    searchDelay: 500,
                    "createdRow": function(row, data, dataIndex) {
                        FTX.Utils.dtAnchorToForm(row);
                    }
                });
            }
        },
    }

})();



$(document).on('click', '.status_change', function() {

    var $this = $(this);
    id = $this.data("id");
    tb = $this.data("tb");
    if ($(this).prop('checked') == true) {
        value = 1;
    } else {
        value = 0;
    }

    var url = window.location.protocol + "//" + window.location.host + "/admin/activation";

    $.ajax({
        type: 'post',
        url: url,
        data: {
            'name': tb,
            'ref': id,
            'val': value
        },
        success: function() {

        },
        error: function() {
            $this.prop('checked', false);
        }
    });

});

$(document).on('click', '.ga_status_change', function() {

    var $this = $(this);
    id = $this.data("id");
    tb = $this.data("tb");
    if ($(this).prop('checked') == true) {
        value = 1;
    } else {
        value = 0;
    }

    var url = window.location.protocol + "//" + window.location.host + "/admin/ga_activation";

    $.ajax({
        type: 'post',
        url: url,
        data: {
            'name': tb,
            'ref': id,
            'val': value
        },
        success: function() {

        },
        error: function() {
            $this.prop('checked', false);
        }
    });

});

$(document).on('click', '.ga_rest_status_change', function() {

    var $this = $(this);
    id = $this.data("id");
    tb = $this.data("tb");
    if ($(this).prop('checked') == true) {
        value = 1;
    } else {
        value = 0;
    }

    var url = window.location.protocol + "//" + window.location.host + "/admin/ga_rest_activation";

    $.ajax({
        type: 'post',
        url: url,
        data: {
            'name': tb,
            'ref': id,
            'val': value
        },
        success: function() {
            alert("Google authenticator has been reset successfully");
        },
        error: function() {

        }
    });

});

function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


function getVersion(model_id) {
    if (model_id) {
        $.ajax({
            type: "get",
            url: "/service/getversion/" + model_id,
            success: function(res) {
                $("#version").empty();
                if (res) {
                    $('#version').append("<option value=''>Select Software Version</option>");
                    $.each(res, function(key, value) {
                        $('#version').append($("<option/>", {
                            value: key,
                            text: value
                        }));
                    });
                }
            }
        });
    }
}

function getSpare(id, num) {
    var modelID = id;
    $("#spare_id_" + num).prop("required", true);
    if (modelID) {
        $.ajax({
            type: "GET",
            url: "/getSpare?model_id=" + modelID,
            success: function(res) {
                if (res) {
                    $("#spare_id_" + num).empty();
                    $("#spare_id_" + num).append('<option value="">Select Spare</option>');
                    $.each(res, function(key, value) {
                        $("#spare_id_" + num).append('<option value="' + key + '">' + value + '</option>');
                    });

                } else {
                    $("#spare_id_" + num).empty();
                    $("#spare_id_" + num).append('<option value="">Select Spare</option>');
                }
            }
        });
    } else {
        $("#spare_id_" + num).empty();
        $("#spare_id_" + num).append('<option value="">Select Spare</option>');
    }
}

function updateVersion(version_id, table, service_id) {
    var formData = 'service_id=' + service_id + '&version_id=' + version_id + '&table=' + table;
    $.ajax({
        type: "POST",
        url: "/service/update/version",
        data: formData,
        success: function(data) {
            window.location.reload();
        }
    });
}

$("#versionlink").click(function() {
    $(".old_version").toggle();
    $(".version_hide").toggle();
});
@extends('layouts.service')

@section('content')
<div class="main-panel">
    <div class="panel-hedding">
        <div class="row mb-4">
            <div class="col-md-9">
			  <h4 class="card-title-text">Sales Request List</h4>
            </div>
			<div class="col-md-3">
				<div class="add-new">
				  <a class="btn btn-primary" href="{{ route('service.create') }}">
					<i class="feather icon-plus"></i> Sales Request Create
				  </a>
				</div>
            </div>

        </div>
		@include('inc.alert')
        <div class="row  ">
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'salesrequest.index','class' => 'form-horizontal','role' => 'form','method' => 'get','id' => 'search-service']) }}
					<div class="form-row well">
					    <div class="form-group col-md-3">
							{{ Form::label('name', __('Phone Number')) }}
							{{ Form::text('nums', old('nums',(isset($nums)) ? $nums:''), ['class' => 'form-control']) }}
						</div>

						<div class="form-group col-md-3">
							{{ Form::label('name', __('Date From')) }}
							{{ Form::text('from', old('from',(isset($from)) ? $from:''), ['class' => 'form-control','id'=>'datepicker' ]) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Date To')) }}
							{{ Form::text('to', old('to',(isset($to)) ? $to:''), ['class' => 'form-control', 'id' => 'dates']) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('SERIAL NUM.')) }}
							{{ Form::text('serial_num', old('serial_num',(isset($serial_num)) ? $serial_num:''), ['class' => 'form-control']) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Branch')) }}
							{{ Form::select('branch_id', $branch, old('branch_id',(isset($branch_id)) ? $branch_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Branch','id' => 'branch_id']) }}
						</div>
						<div class="form-group col-md-3">
							{{ Form::label('name', __('Model')) }}
							{{ Form::select('model_id', $model, old('model_id',(isset($model_id)) ? $model_id:''), ['class' => 'form-control js-example-basic-single', 'placeholder' => 'Select Model', 'id' => 'model_id']) }}
						</div>
						<div class="form-group col-md-6 two-btn">
						  <button class="btn btn-success search-btn">Search</button>
                          <button class="btn btn-success reset-btn" type="reset">Reset</button>
                        </div>
					</div>
				{{ Form::close() }}
				<div class="card">
				  <div class="card-body">
					  <div class="table-responsive">
				  	<table id="service-table" class="table table-centered table-hover mb-0" data-ajax_url="{{ route("salesrequest.get") }}">
					        <thead>
					            <tr>
								    <th width="5">#</th>
					                <th width="80">Request.Date</th>
									<th>Shop Name</th>
									<th>Service.Num</th>
									<th>Branch</th>
					                <th>Customer</th>
									<th>Phone.Num</th>
									<th>Warranty</th>
									<th>Brand</th>
									<th>Model</th>
									<th>Sales Date</th>
									<th>Status</th>
					            </tr>
					        </thead>
					    </table>
					  </div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection
@section('pagescript')
@include('inc.datatables-js')
<script>
$(".reset-btn").click(function() {
    $("#branch_id").val(null).trigger("change");
    $("#model_id").val(null).trigger("change")
});
$(function() {
	$( 'input[name="from"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
	$( 'input[name="to"]').daterangepicker( {
		singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
		locale: {
		  format: 'DD-MM-YYYY'
		}
	}).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
});

var branch_id = getUrlParameter('branch_id');
var from_date =  getUrlParameter('from');
var to_date =  getUrlParameter('to');
var model_id =  getUrlParameter('model_id');
var nums =  getUrlParameter('nums');
var serial_num =  getUrlParameter('serial_num');
var status =  getUrlParameter('status');
FTX.Utils.documentReady(function() {
    FTX.SERVICE.list.init(branch_id,model_id,from_date,to_date,nums,serial_num,status);
});
</script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/datepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
@endsection

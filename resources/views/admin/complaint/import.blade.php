@extends('layouts.app')

@section('content')
    <div class="main-panel">
        <div class="panel-hedding">
            <div class="row mb-4">
                <div class="col-md-6">
                    <h4 class="card-title-text">Complaint</h4>
                </div>
                <div class="col-md-6">
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{ route('complaint.index') }}"> <i
                            class="feather icon-list"></i> Complaint List</a>
                    </div>
                </div>
            </div>
            @include('inc.alert')
            {{ Form::open(['route' => 'complaint.import_store','class' => 'form-horizontal','role' => 'form','method' => 'post','id' => 'create-complaint', 'auto-complete' => 'off', 'enctype'=>"multipart/form-data"]) }}
            @csrf
            <div class="row">
                <div class="col form-group">
                  
                    <input type="file" name="file" class="form-control" id="customFile" required>
                    <a class="pull-right" href="{{url('/uploads/format/complaint_template.xlsx')}}"><small>Download Sample Excel File <i class="fa fa-download"></i></small></a>
                </div>
               
            </div>
           
            <button type="submit" class="btn btn-success float-right">Submit</button>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagescript')
<script src="{{ asset('js/select2/select2.min.js')}}"></script>
<script src="{{ asset('js/select2/bloodhound.min.js')}}"></script>
@endsection
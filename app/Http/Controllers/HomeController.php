<?php

namespace App\Http\Controllers;
use App\Http\Traits\CommonTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View;
class HomeController extends Controller
{
    use CommonTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
       
        // $this->middleware('auth');
        $this->middleware(['auth', '2fa']);
        
        $this->middleware(function ($request, $next) {
            $this->data['access_all'] = $this->checkAllPermission(Auth::user()->role_id);
            $this->data['country_id'] = $this->FieldByID("branch","country_id",Auth::user()->branch_id);
            return $next($request);
        });
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::id() > 0){
            View::share('js', ['dashboard']);
            $this->data['data'] = $this->getFAQDataList(''); 
            if ($this->data['access_all'] == 1) {
                return view('admin.dashboard',$this->data);
            } else {
                return view('service.dashboard',$this->data);
            }
           
        }
        return view('auth.login');
    }

    public function googleAuthRegistration(Request $request)
    {
        $registration_data = $request->session()->get('registration_data');
        return view('google2fa.register', ['QR_Image' => $registration_data['QR_Image'], 'secret' => $registration_data['google2fa_secret']]);
    }

    public function completeRegistration(Request $request)
    {
        echo "<pre>"; print_r($request->session()); echo "</pre>"; exit;
    }

}
